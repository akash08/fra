<?php
namespace FRA\App\Core;
use FRA\App\Core\App;
use FRA\App\Core\Database;
/**
* BaseModel class
* Standard File
*/
abstract class BaseModel extends App
{
	protected $tableName;
	protected $primaryId;
	protected $orderBy;
	protected $dbHandler;

	function __construct()
	{
		parent::__construct();
	}

	public function runSql($sql)
	{
		$result = null;
		$this->dbHandler = Database::connection($this->database);
		$statement = $this->dbHandler->prepare($sql);
		try{
			$statement->execute();
			$statement->setFetchMode(\PDO::FETCH_ASSOC);
			$result = $statement->fetchAll();
		}catch(\PDOException $e){
			$error = $e->getMessage();
		}
		
		return count($result) >= 1 ? $result : null;
	}

	abstract protected function join($joinModel, $joinCondition, $columns);
}