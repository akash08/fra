<?php
namespace FRA\App\Core;
use FRA\App\Core\App;
/**
* Database wrapper
* Standard File
*/
class Database
{
	public static function connection($database)
	{
		if($database['driver'] == 'pgsql')
		{
			try{
				$dbh = null;
				$dbh = new \PDO($database['driver'].":host=".$database['host'].";dbname=".$database['database'], $database['username'], $database['password']);
				$dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
				if($dbh){
					return $dbh;
				}
				return false;
			}catch(\PDOException $e){
				$title = "Error";
				echo $error = $e->getMessage();exit;
			}	
		}else{
			// Other connections
		}
	}
}