<?php
namespace FRA\App\Core;
use FRA\App\Model\UserModel;
/**
* Auth component for user/api authentication
* Standard File
*/
class Auth 
{
	private $token;
	private $userModel;

	public function __construct()
	{
		$this->session = $_SESSION;
	}

	public function userExists(){
		if(isset($this->session['user']))
		{
			return true;
		}else{
			return false;
		}
	}

	public function verifyDBUser($data){
		$this->userModel = new UserModel();
		return $this->userModel->verifyCredentials($data);
	}

	static public function destroyFlash()
	{
		unset($this->session['alerts']);
	}

}