<?php
namespace FRA\App\Core;
use FRA\App\Core\App;

class Query extends App
{
    private $allowedTypes;
    protected $dbHandler;
    public function __construct(){
        parent::__construct();
        $this->dbHandler =  Database::connection($this->database);
        $this->allowedTypes = array(
          'int' => \PDO::PARAM_INT,
          'str' => \PDO::PARAM_STR,
          'bool' => \PDO::PARAM_BOOL
        );
    }

    private function setQuery($sql){
        $this->query = $sql;
    }

    private function setStatement()
    {
        $this->statement = $this->dbHandler->prepare($this->query);
    }
    
    private function bindParams($arr)
    {
        foreach($arr as $key=>$val)
        {
            $this->statement->bindParam($key, $val['value'], $this->allowedTypes[$val["type"]]);
        }
    }

    private function fetchAssoc()
    {
        try
        {
            $this->statement->execute();
            $this->statement->setFetchMode(\PDO::FETCH_ASSOC);
            $result = $this->statement->fetchAll();
            $resp = array('success'=>true, 'res'=>$result);
        }catch(\PDOException $e){
            $result = $e->getMessage();
            $resp = array('success'=>false, 'res'=>$result);
        }
        return $resp;
    }

    public function __destruct()
    {
        $this->dbhandler = null;
    }


    public function prepareQuery($sql, $params=array())
    {
        $this->setQuery($sql);
        $this->setStatement();
        if(!empty($params))
        {
            $this->bindParams($params);
        }
        return $this->fetchAssoc();
    }

    static function joinSql($mainModel, $joinModel, $joinCondition, $columns)
    {
        $classToCall = App::getModelNameSpace().$joinModel;
        $obj = new $classToCall;
        $joinTable = $obj->getTable();
        $condition = "";
        foreach($joinCondition as $k=>$v)
        {
            $condition .= implode(" = ",$v);
            if(($k+1) != count($joinCondition))
            {
                $condition .= " and ";
            }
        }
        foreach(array_keys($columns) as $k)
        {
            $arr[] = preg_filter('/^/', strtolower($k).'.', $columns[$k]);
        }
        $columnsList = "";
        foreach($arr as $v)
        {
            $columnsList .= implode($v, ", ");
            $columnsList .= ", ";
        }
        $columnsList = rtrim($columnsList, ", ");
        return "select $columnsList from $mainModel a, $joinTable b where $condition";
    }

    public function beginTransaction(){
        $this->dbHandler->beginTransaction();
    }

    public function rollBack(){
        $this->dbHandler->rollBack();
    }

    public function commit(){
        $this->dbHandler->commit();
    }
}