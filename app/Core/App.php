<?php
namespace FRA\App\Core;
use FRA\App\Core\Config;
use FRA\App\Core\Auth;
/**
* Standard File
*/
class App
{

	public $theme;
	
	protected $viewDir;

	public $session;

	protected $params;

	protected $controller;

	protected $method;

	protected $publicDir;

	public $debug;

	protected $uri;

	protected $protocol;

	protected $database;

	protected $dbHandler;

	protected $twig;

	protected $auth;

	public $request;

	public $modelNamespace;

	protected $namespace = "FRA\App\Main\\";
	
	public function __construct()
	{
		$this->params = (object) ['attrs'=>null,'id'=>null];
		$this->publicDir = dirname(dirname(__DIR__))."/public";
		$this->viewDir = dirname(__DIR__)."/Views";
		$this->initConfig();
		ini_set('error_reporting', E_ALL);
        error_reporting(E_ALL);
        ini_set('log_errors', TRUE);
        ini_set('html_errors', FALSE);
        ini_set('error_log', $this->publicDir.'/errors/error.log');
        ini_set('display_errors', TRUE);
	}

	static function getModelNameSpace()
	{
		$config = Config::init();
		return $config['modelNamespace'];
	}

	protected function initConfig()
	{
		$dotenv = new \Dotenv\Dotenv(dirname(dirname(__DIR__))."/public");
		$dotenv->load();
		$config = Config::init();
		$this->debug = $config['debug'];
		$this->theme = $config['theme'];
		$this->request = $_REQUEST;
		unset($this->request['path']);
		$this->database = $config['database'][$config['database']['in_use']];
		$this->database['in_use'] = $config['database']['in_use'];
		$loader = new \Twig_Loader_Filesystem($this->viewDir."/".$this->theme);
		$this->twig = new \Twig_Environment($loader);
		$filter = new \Twig_SimpleFunction('filemtime', 'filemtime');
		$filter1 = new \Twig_SimpleFunction('date', 'date');
		$this->twig->addFunction($filter);
		$this->twig->addFunction($filter1);
		$this->setConfig();
		$this->auth = new Auth();
		$this->registerTwigFunctions();
	}

	public function setConfig()
	{
		$this->setDebug();
		$uri = explode('/', $_SERVER['REQUEST_URI']);
		array_shift($uri);
		$this->uri = $uri;
		$this->setController();
		$this->setMethod();
		$this->session = &$_SESSION;
	}

	public function registerTwigFunctions(){
		$this->twig->addFunction(new \Twig_SimpleFunction('json_encode_twig', function ($data) {
		    return json_encode($data);
		}));
	}

	public function setDebug()
	{
		if($this->debug)
		{
			error_reporting(E_ALL);
			ini_set('display_errors',1);
		}
	}

	public function setController()
	{
		if(empty(array_filter($this->uri)))
		{
			$this->controller = 'Home';
		}else{
			$this->controller = ucfirst($this->uri[0]);
		}
	}
		
	public function setMethod()
	{
		if(empty(array_filter($this->uri)) || (isset($this->uri[1]) && $this->uri[1] == ""))
		{
			$this->method = 'index';
		} else if (isset($this->uri[1])) {
			if(strpos($this->uri[1], "?") === false )
			{
				$this->method = $this->uri[1];
			}else{
				$this->method = substr($this->uri[1], 0, strpos($this->uri[1], "?"));
				$this->params->attrs = substr($this->uri[1], strpos($this->uri[1], "?"));
			}
			if(isset($this->uri[2]))
			{
				$this->params->id = $this->uri[2];
			}
		} else {
			$this->method = 'index';
		}
	}

	public function call()
	{
		if($this->controller == 'User')
		{
			$this->controller =  $this->namespace . $this->controller;
			$obj = new $this->controller($this->auth);
			$method = (string) $this->method;
			if(method_exists($obj,$method))
			{
				echo $obj->$method();
			}else{
				$this->view('404.html');
			}
		}else{
			$this->controller =  $this->namespace . $this->controller;
			$method = (string) $this->method;
			$obj = new $this->controller();
			if(method_exists($obj,$method))
			{
				echo $obj->$method();
			}else{
				$this->view('404.html');
			}
		}
		
	}

	public function view($viewFile, $param=array())
	{
		$viewFile = $this->controller."/".$viewFile;
		if(isset($this->session['alerts']))
		{
			$param['alerts'] = $this->session['alerts'];
			unset($this->session['alerts']);
		}
		if(isset($this->session['user']))
		{
			$param['user'] = $this->session['user'];
		}

		if(file_exists($this->viewDir."/vali/".$viewFile))
		{
			echo $this->twig->render($viewFile,$param);
			unset($this->session['alerts']);
		}else{
			echo $this->twig->render("404.html");
		}
	}
	
	public function url($path)
	{
		return $this->publicDir . "/" . $path;
	}
	
	public function redirect($params=array())
	{
		$host = $_SERVER['HTTP_HOST'];
		if(!empty($params))
		{
			$controller = $params[0];
			$view = $params[1];
		}else{
			$controller = 'Home';
			$view = 'index';
		}
		
		header("Location: http://$host/$controller/$view");
		exit;
	}

	public function setFlash($params, $json=false)
	{
		if($json)
		{
			header('Content-Type: application/json; charset=utf-8');
			$response = array(
				'msg' => $params[0],
				'status' => $params[1]
			);

			if(isset($params[2])) {
				$response['data'] = $params[2];
			}

			die(json_encode($response));
		} else{
			$this->session['alerts'][$params[1]]=$params[0];
		}
	}
}