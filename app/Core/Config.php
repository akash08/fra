<?php
namespace FRA\App\Core;
/**
* Config class
* Standard File
*/
class Config
{
    public static function init()
    {
        return [
            'theme' => 'vali',
            'debug' => true,
            'modelNamespace' => '\FRA\App\Model\\',
            'database' => [
                'in_use' => 'pgsql_local',
                'pgsql_cloud' => [
                    'driver' => 'pgsql',
                    'host' => $_ENV["PRODUCTION_HOST"],
                    'port' => $_ENV["PRODUCTION_PORT"],
                    'database' => $_ENV["PRODUCTION_DATABASE"],
                    'username' => $_ENV["PRODUCTION_USERNAME"],
                    'password' => $_ENV["PRODUCTION_PASSWORD"],
                    'charset' => 'utf8',
                    'prefix' => '',
                    'schema' => 'public',
                    'sslmode' => 'prefer',
                ],
                'pgsql_local' => [
                   'driver' => 'pgsql',
                    'host' => $_ENV["DEV_HOST"],
                    'port' => $_ENV["DEV_PORT"],
                    'database' => $_ENV["DEV_DATABASE"],
                    'username' => $_ENV["DEV_USERNAME"],
                    'password' => $_ENV["DEV_PASSWORD"],
                    'charset' => 'utf8',
                    'prefix' => '',
                    'schema' => 'public',
                    'sslmode' => 'prefer',
                ]
            ]
        ];
    }
}