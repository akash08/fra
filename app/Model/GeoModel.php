<?php
namespace FRA\App\Model;
use FRA\App\Core\Query;
use FRA\App\Core\Database;
use FRA\App\Core\BaseModel;
use FRA\App\Model\ClaimModel;

/**
* GeoModel Model 
* Standard File
*/
class GeoModel extends BaseModel
{
    private $user;
	private $query;
    public function __construct()
	{
		parent::__construct();
		$this->query = new Query();
        $this->table = "geolocations";
    }

    public function join($joinModel, $joinCondition, $columns)
    {
        $joinSql = Query::joinSql($this->table, $joinModel, $joinCondition, $columns);
        $res = $this->query->prepareQuery($joinSql);
        return $res;
    }

    public function getTable()
    {
        return $this->table;
    }

    public function getPlots()
    {
        $claimModel = new ClaimModel();
        $claimModel = $claimModel->getTable();
        $sql = "select distinct(a.c_id), b.create_by,b.timestamp, b.status
                from $this->table a, $claimModel b where a.c_id = b.cl_id";
        $res = $this->query->prepareQuery($sql);
        $this->query = null;
		return $res;
    }

    public function getByIds($ids, $columns = null){
        $columns = $columns == null ? "*" : implode(",",$columns);
        $sql = "select $columns from $this->table where c_id in (".implode(",",$ids).")";
        $res = $this->query->prepareQuery($sql);
        $this->query = null;
		return $res;
    }

    public function updatePolygons($points, $id){
        $this->query->beginTransaction();
        $sql = "delete from $this->table where c_id = '$id'";
        $res = $this->query->prepareQuery($sql);
        if($res['res'])
        {
            $psql = "insert into $this->table (c_id, declatitude, declongitude) values ";
            foreach($points as $p)
            {
                $psql .= "('".$id."','";
                $psql .= implode("','",array_reverse($p))."'),";
            }
            $psql = rtrim($psql, ",");
            $pres = $this->query->prepareQuery($psql);
            if($pres['success'])
            {
                $this->query->commit();
            }else{
                $this->query->rollBack();    
            }
        }else{
            $this->query->rollBack();
        }
        $this->query = null;
        echo $pres['success'] ? 1 : 0;
    }
}