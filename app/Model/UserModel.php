<?php
namespace FRA\App\Model;
use FRA\App\Core\BaseModel;
use FRA\App\Core\Database;
use FRA\App\Core\Query;
/**
* User Model 
* Standard File
*/
class UserModel extends BaseModel
{
	private $user;
	private $query;
	public function __construct()
	{
		parent::__construct();
		$this->query = new Query();
		$this->table = "user_master";
	}

	public function verifyCredentials($data){
		$sql = "select * from $this->table where username = ? and password = ?";
		$params = array(
			"1" => array('value' => $data['email'],'type' => 'str'),
			"2" => array('value' => $data['password'],'type' => 'str')
		);
		$res = $this->query->prepareQuery($sql, $params);
		$this->query = null;
		if($res['success'] && !empty($res['res']))
		{
			$this->session['user'] = $res['res'][0];
			return true;
		}else{
			return false;
		}
	}

	public function register($data){
		$sql = "insert into $this->table (name, username, password, designation, mob_number, email, address) values  (?,?,?,?,?,?,?)";
		$params = array(
			"1" => array('value' => $data['name'],'type' => 'str'),
			"2" => array('value' => $data['username'],'type' => 'str'),
			"3" => array('value' => $data['password'],'type' => 'str'),
			"4" => array('value' => $data['designation'],'type' => 'str'),
			"5" => array('value' => $data['mob_number'],'type' => 'str'),
			"6" => array('value' => $data['email'],'type' => 'str'),
			"7" => array('value' => $data['address'],'type' => 'str')
		);
		$res = $this->query->prepareQuery($sql, $params);
		$this->query = null;
		if($res['success'])
		{
			return true;
		}else{
			if($this->debug)
			{
				var_dump($res);
			}
			return false;
		}
	}

	public function getUsers()
	{
		$sql = "select * from $this->table";
		$res = $this->query->prepareQuery($sql);
		$this->query = null;
		return $res;
	}

	public function join($joinModel, $joinCondition, $columns){
        $joinSql = Query::joinSql($this->table, $joinModel, $joinCondition, $columns);
        $res = $this->query->prepareQuery($joinSql);
        return $res;
    }
}