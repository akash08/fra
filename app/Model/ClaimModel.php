<?php
namespace FRA\App\Model;
use FRA\App\Core\BaseModel;
use FRA\App\Core\Database;
use FRA\App\Core\Query;

/**
* Claim Model 
* Standard File
*/
class ClaimModel extends BaseModel
{
	private $user;
	private $query;
	public function __construct()
	{
		parent::__construct();
		$this->query = new Query();
        $this->table = "claim";
    }

    public function getClaims(){
        $sql = "select * from $this->table where create_by = ?";
        $params = array(
            "1" => array('value'=>$this->session['user']['username'],'type'=>'str')
        );
        $res = $this->query->prepareQuery($sql, $params);
        $this->query = null;
		return $res;
    }
    
    public function join($joinModel, $joinCondition, $columns){
        $joinSql = Query::joinSql($this->table, $joinModel, $joinCondition, $columns);
        $res = $this->query->prepareQuery($joinSql);
        return $res;
    }

    public function getByIds($ids, $columns = null){
        $columns = $columns == null ? "*" : implode(",",$columns);
        $sql = "select $columns from $this->table where id in (".implode(",",$ids).")";
        $res = $this->query->prepareQuery($sql);
        $this->query = null;
		return $res;
    }

    public function getTable(){
        return $this->table;
    }
}