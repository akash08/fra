<?php
namespace FRA\App\Model;
use FRA\App\Core\BaseModel;
use FRA\App\Core\Database;
use FRA\App\Core\Query;

/**
* Claimant Model 
* Standard File
*/
class ClaimantModel extends BaseModel
{
	private $user;
	private $query;
	public function __construct()
	{
		parent::__construct();
		$this->query = new Query();
        $this->table = "claimant";
    }

    public function getClaimants(){
        $sql = "select * from $this->table where create_by = ?";
        $params = array(
            "1" => array('value'=>$this->session['user']['username'],'type'=>'str')
        );
        $res = $this->query->prepareQuery($sql, $params);
        $this->query = null;
		return $res;
    }
    
    public function join($joinModel, $joinCondition, $columns){
        $joinSql = Query::joinSql($this->table, $joinModel, $joinCondition, $columns);
        $res = $this->query->prepareQuery($joinSql);
        return $res;
    }

    public function getTable(){
        return $this->table;
    }
}