<?php
namespace FRA\App\Main;
use FRA\App\Core\App;
/**
* Home Controller
*/
class Home extends App
{
        protected $auth;
        public function __construct()
        {
                parent::__construct();
        }

        public function index()
        {
                $title = 'Homepage';
                return $this->view('index.html',compact('title'));
        }

        public function dashboard(){
                $title = 'dashboard';
                return $this->view('dashboard.html',compact('title'));
        }
       
}