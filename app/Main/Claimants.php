<?php
namespace  FRA\App\Main;

use FRA\App\Core\App;
use FRA\App\Model\ClaimantModel;

class Claimants extends App 
{
    private $model;
    public function __construct()
    {
        parent::__construct();
        $this->model = new ClaimantModel();
    }

    public function index()
    {
        if($this->auth->userExists())
        {
            $title = 'Claimants';
            $res = $this->model->getClaimants();
            if($res['success'])
            {
                $claimants = $res['res'];
            }
            return $this->view('index.html',compact('title','claimants'));
        }
        return $this->view('login.html');
    }
}

