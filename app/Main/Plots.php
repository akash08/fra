<?php
namespace  FRA\App\Main;

use FRA\App\Core\App;
use FRA\App\Model\GeoModel;
use FRA\App\Model\ClaimModel;

class Plots extends App 
{
    private $model;
    private $claimModel;
    public function __construct()
    {
        parent::__construct();
        $this->model = new GeoModel();
    }

    public function index(){
        if($this->auth->userExists())
        {
            $plots = "";
            $title = 'Plots List';
            $res = $this->model->getPlots();
            if($res['success'])
            {
                $plots = $res['res'];
            }
            return $this->view('index.html',compact('title','plots'));
        }
        return $this->view('login.html');
    }

    public function viewPlot(){
        if($this->auth->userExists())
        {
            $title = "Plot View";
            if(isset($this->params->id))
            {
                $id = $this->params->id;
                $res = $this->model->getByIds([$this->params->id]);
                if($res['success'])
                {
                    $geoPoints = array();
                    foreach($res['res'] as $v)
                    {
                        $geoPoints[] = $v['declatitude'].",".$v['declongitude'];
                    }
                    $geoPoints = array_unique($geoPoints);
                    foreach($geoPoints as $val)
                    {
                        $val = explode(",",$val);
                        $geoPts[] = array(
                            "lat"=>$val[0],
                            "long"=>$val[1]
                        );
                    }
                    $geoPoints = json_encode($geoPoints);
                    $bbox = $this->bbox($geoPts);
                    return $this->view('view.html',compact('title','geoPoints','bbox','id'));
                }else{
                    echo "Error";
                }
            }
        }
    }

    private function bbox($arr){
        $minLat = min(array_column($arr, 'lat'));
        $maxLat = max(array_column($arr, 'lat'));
        $minLong = min(array_column($arr, 'long'));
        $maxLong = max(array_column($arr, 'long'));
        return $minLong.",".$minLat.",".$maxLong.",".$maxLat;
    }

    public function updatePolygon(){
        $arr = $_POST['polyArr'];
        $id = $_POST['id'];
        $resp = $this->model->updatePolygons(json_decode($arr),$id);
        return $resp;
    }
}