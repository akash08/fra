<?php
namespace  FRA\App\Main;

use FRA\App\Core\App;
use FRA\App\Model\ClaimModel;

class Claims extends App 
{
    private $model;
    public function __construct()
    {
        parent::__construct();
        $this->model = new ClaimModel();
    }

    public function index()
    {
        if($this->auth->userExists())
        {
            $title = 'Claims';
            $columns = array(
                'A' => array('id','timestamp', 'create_by'),
                'B' => array('name')
            );
            $joinCondition = array(['a.cl_id','b.c_id']);
            $res = $this->model->join("ClaimantModel",$joinCondition, $columns);
            if($res['success'])
            {
                $claims = $res['res'];
            }
            return $this->view('index.html',compact('title','claims'));
        }
        return $this->view('login.html');
    }

}

