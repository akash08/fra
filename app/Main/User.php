<?php
namespace FRA\App\Main;

use FRA\App\Core\App;
use FRA\App\Core\Auth;
use FRA\App\Model\UserModel;
/**
* Home Controller
*/
class User extends App
{
	protected $auth;
	private $model;

	public function __construct(Auth $auth)
	{
		parent::__construct(); 
		$this->auth = $auth;
		$this->model = new Usermodel();
	}

	public function login(){
		if($this->auth->userExists())
		{
			return $this->redirect(array('Home','dashboard'));
		}
		if(!empty($this->request))
		{
			if($this->auth->verifyDBUser($this->request))
			{
				return $this->redirect(array('Home','dashboard'));
			}else{
				$this->setFlash(array('No user found.','success'));
				return $this->view('login.html');
			}
		}
		return $this->view('login.html');
	}

	public function register(){
		if($this->auth->userExists())
		{
			return $this->redirect(array('Home','dashboard'));
		}
		if(!empty($this->request))
		{
			if($this->model->register($this->request))
			{
				$this->setFlash(array('User created successfully.','success'));
			}else{
				$this->setFlash(array('Error in creating user please contant administrator.','danger'));
			}
			return $this->view('login.html');
		}
		return $this->view('register.html');
	}

	public function logout(){
		unset($this->session['user']);
		return $this->redirect(array('User','login'));
	}

	public function getUsers(){
		$title = "Users List";
		if($this->auth->userExists())
		{
			$users = array();
			$res = $this->model->getUsers();
			if($res['success'])
			{
				$users = $res['res'];
			}
			return $this->view('userList.html',compact('users','title'));
		}
	}

}
