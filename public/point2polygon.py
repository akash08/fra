from osgeo import ogr
import os
import csv
import sys

ring = ogr.Geometry(ogr.wkbLinearRing)
firstLine = True
taxafile = sys.argv[1]
dirpath = sys.argv[2]
with open(taxafile, 'rb') as csvfile:
	spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
	for row in spamreader:
		if firstLine:
			firstLine = False
			continue
		print row[1]+" "+row[2]
		ring.AddPoint(float(row[1]),float(row[2]))
poly = ogr.Geometry(ogr.wkbPolygon)
poly.AddGeometry(ring)
wktFormat=poly.ExportToWkt()

if(os.path.exists(dirpath+'conputed_shape.csv')):
	os.remove(dirpath+'computed_shape.csv')
with open(dirpath+'computed_shape.csv', 'wb') as csvfile:
	spamwriter = csv.writer(csvfile, delimiter=',', quotechar='"')
	spamwriter.writerow(['id','geom'])
	spamwriter.writerow(['0',wktFormat])

if not os.path.exists(dirpath+'computed_shape'):
	os.makedirs(dirpath+'computed_shape')

vrt_format='<OGRVRTDataSource>\
    <OGRVRTLayer name="computed_shape">\
       <SrcDataSource>'+dirpath+'computed_shape.csv</SrcDataSource>\
      <GeometryType>wkbLineString25D</GeometryType>\
 <LayerSRS>WGS84</LayerSRS>\
 <GeometryField encoding="WKT" field="geom" > </GeometryField >\
     </OGRVRTLayer>\
</OGRVRTDataSource>'

if(os.path.exists(dirpath+'computed_shape.vrt')):
        os.remove(dirpath+'computed_shape.vrt')

target=open(dirpath+'computed_shape.vrt','w')
target.write(vrt_format)
target.close()

print dirpath
