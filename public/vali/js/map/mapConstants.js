var proj = new ol.proj.Projection({
    code: 'EPSG:3857',
    units: "m"
});
var view = new ol.View({
    center:  ol.proj.fromLonLat([78.96, 21.59], "EPSG:4326", "EPSG:3857"),
    zoom: 19,
    maxZoom: 19,
    minZoom: 15,
    projection:proj
});
var elem;
var osmLayer = new ol.layer.Tile({
    source: new ol.source.OSM()
});	
var mousePositionControl = new ol.control.MousePosition({
        coordinateFormat: ol.coordinate.createStringXY(4),
        projection: 'EPSG:3857',
        className: 'custom-mouse-position',
        target: document.getElementById('latlonInfo'),
        undefinedHTML: '&nbsp;'
});
var image = new ol.style.Circle({
    radius: 3,
    fill: new ol.style.Fill({
        color: '#FF6347	'
    }),
    stroke: new ol.style.Stroke({color: 'green', width: 1})
});
var scaleLine = new ol.control.ScaleLine({
    minWidth: 100
});
var styles = {
    'Point': new ol.style.Style({
        image: image
    }),
    'Polygon': new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: '#FF4500',
            width: 3
        }),
        fill: new ol.style.Fill({
            color: 'rgba(255, 255, 255, 0.4)'
        })
    })
};
var layerAdded;
var styleFunction = function(feature) {
    if(feature != undefined)
    {
        return styles[feature.getGeometry().getType()];
    }
};