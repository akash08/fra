prop = "Plot ID";
points = JSON.parse(points);
var ring = [];
$.each(points, function(i,v)
{
    dimensions = v.split(",");
    lat = dimensions[0];
    long = dimensions[1];
    if(i==0)
    {
        start = [parseFloat(long),parseFloat(lat)];
    }
    ring.push([parseFloat(long),parseFloat(lat)]);
});
ring.push(start);
// Point tp polygon
var polygon = new ol.geom.Polygon([ring]);
polygon.transform('EPSG:4326', 'EPSG:3857');
var pfeature = new ol.Feature(polygon);
var newVectorSource = new ol.source.Vector();
newVectorSource.addFeature(pfeature);
var vectorLayer = new ol.layer.Vector({
    source: newVectorSource,
    style: styleFunction
});
map.addLayer(vectorLayer);
extent = ol.extent.applyTransform(extent, ol.proj.getTransform("EPSG:4326", "EPSG:3857"));
map.getView().fit(extent, map.getSize());

// Edit Map
var selectedCollection = new ol.Collection();
var snappableCollection = new ol.Collection();
selectInteraction = new ol.interaction.Select({
    features: newVectorSource,
    multi: true,
});
var modifyInteraction = new ol.interaction.Modify({
    features: selectInteraction.getFeatures(),
});
var snapInteraction = new ol.interaction.Snap({
    features: snappableCollection,
});
map.addInteraction(selectInteraction);
map.addInteraction(modifyInteraction);
map.addInteraction(snapInteraction);

function savePolygon()
{
    arrToSave = [];
    var features = newVectorSource.getFeatures();
    features.forEach(function(feature) {
        f = feature.getGeometry().transform('EPSG:3857', 'EPSG:4326');
        f = f.getCoordinates()[0];
        f.forEach(element => {
            arrToSave.push(element);
        });
        $.ajax({
            url : "/Plots/updatePolygon",
            data : "polyArr="+JSON.stringify(arrToSave)+"&id="+Id,
            type : "POST",
            success : function(response)
            {
                if(response)
                {
                    swal("Done!", "Plot updated successfully!", "success");
                    window.location.reload();
                }else{
                    swal("Ahh!", "Plot updation failed! Please try to reload page or contact system administrator.", "error");
                    window.location.reload();
                }
            }
        });
    });
}