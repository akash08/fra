map = new ol.Map({
    target: 'map'
});
map.setView(view);
map.addControl(mousePositionControl);
map.addControl(scaleLine);
var googleLayer = new olgm.layer.Google({mapTypeId: google.maps.MapTypeId.SATELLITE});
// map.addLayer(osmLayer);
map.addLayer(googleLayer);
var olGM = new olgm.OLGoogleMaps({
    map: map,
    watch: {
        'vector': false
    }
});
olGM.activate();