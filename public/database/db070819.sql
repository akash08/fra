--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.2
-- Dumped by pg_dump version 11.4 (Ubuntu 11.4-1.pgdg19.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: topology; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA topology;


ALTER SCHEMA topology OWNER TO postgres;

--
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


--
-- Name: btree_gin; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS btree_gin WITH SCHEMA public;


--
-- Name: EXTENSION btree_gin; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION btree_gin IS 'support for indexing common datatypes in GIN';


--
-- Name: btree_gist; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS btree_gist WITH SCHEMA public;


--
-- Name: EXTENSION btree_gist; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION btree_gist IS 'support for indexing common datatypes in GiST';


--
-- Name: dblink; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS dblink WITH SCHEMA public;


--
-- Name: EXTENSION dblink; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION dblink IS 'connect to other PostgreSQL databases from within a database';


--
-- Name: file_fdw; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS file_fdw WITH SCHEMA public;


--
-- Name: EXTENSION file_fdw; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION file_fdw IS 'foreign-data wrapper for flat file access';


--
-- Name: fuzzystrmatch; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS fuzzystrmatch WITH SCHEMA public;


--
-- Name: EXTENSION fuzzystrmatch; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION fuzzystrmatch IS 'determine similarities and distance between strings';


--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


--
-- Name: pg_stat_statements; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pg_stat_statements WITH SCHEMA public;


--
-- Name: EXTENSION pg_stat_statements; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pg_stat_statements IS 'track execution statistics of all SQL statements executed';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


--
-- Name: pgrouting; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pgrouting WITH SCHEMA public;


--
-- Name: EXTENSION pgrouting; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgrouting IS 'pgRouting Extension';


--
-- Name: postgis_sfcgal; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis_sfcgal WITH SCHEMA public;


--
-- Name: EXTENSION postgis_sfcgal; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis_sfcgal IS 'PostGIS SFCGAL functions';


--
-- Name: postgis_topology; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis_topology WITH SCHEMA topology;


--
-- Name: EXTENSION postgis_topology; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis_topology IS 'PostGIS topology spatial types and functions';


--
-- Name: postgres_fdw; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgres_fdw WITH SCHEMA public;


--
-- Name: EXTENSION postgres_fdw; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgres_fdw IS 'foreign-data wrapper for remote PostgreSQL servers';


--
-- Name: tsearch2; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS tsearch2 WITH SCHEMA public;


--
-- Name: EXTENSION tsearch2; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION tsearch2 IS 'compatibility package for pre-8.3 text search functions';


--
-- Name: availed_ec_id(text, numeric, numeric); Type: FUNCTION; Schema: public; Owner: arpit
--

CREATE FUNCTION public.availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric) RETURNS TABLE(return_ec_id numeric)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN query
select ec_id
from ind_master,region_allocation
where
ec_id IN (select ec_id from ind_Tracking where scheme_id=param_scheme_id)
and
ind_master.district_code=param_district_code
and
region_allocation.region_type=param_region_type;
END;
$$;


ALTER FUNCTION public.availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric) OWNER TO arpit;

--
-- Name: availed_ed_id(text, numeric, numeric); Type: FUNCTION; Schema: public; Owner: arpit
--

CREATE FUNCTION public.availed_ed_id(param_region_type text, param_scheme_id numeric, param_district_code numeric) RETURNS TABLE(return_ec_id numeric)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN query
select ec_id
from ind_master,region_allocation
where
ec_id IN (select ec_id from ind_Tracking where scheme_id=param_scheme_id)
and
ind_master.district_code=param_district_code
and
region_allocation.region_type=param_region_type;
END;
$$;


ALTER FUNCTION public.availed_ed_id(param_region_type text, param_scheme_id numeric, param_district_code numeric) OWNER TO arpit;

--
-- Name: bytea_import(text); Type: FUNCTION; Schema: public; Owner: arpit
--

CREATE FUNCTION public.bytea_import(p_path text, OUT p_result bytea) RETURNS bytea
    LANGUAGE plpgsql
    AS $$
declare
  l_oid oid;
  r record;
begin
  p_result := '';
  select lo_import(p_path) into l_oid;
  for r in ( select data
             from pg_largeobject
             where loid = l_oid 
             order by pageno ) loop
    p_result = p_result || r.data;
  end loop;
  perform lo_unlink(l_oid);
end;$$;


ALTER FUNCTION public.bytea_import(p_path text, OUT p_result bytea) OWNER TO arpit;

--
-- Name: clone_schema(text, text); Type: FUNCTION; Schema: public; Owner: pgadmin
--

CREATE FUNCTION public.clone_schema(source_schema text, dest_schema text) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE 
  objeto text;
  buffer text;
BEGIN
    EXECUTE 'CREATE SCHEMA ' || dest_schema ;
 
    FOR objeto IN
        SELECT TABLE_NAME::text FROM information_schema.TABLES WHERE table_schema = source_schema
    LOOP        
        buffer := dest_schema || '.' || objeto;
        EXECUTE 'CREATE TABLE ' || buffer || ' (LIKE ' || source_schema || '.' || objeto || ' INCLUDING CONSTRAINTS INCLUDING INDEXES INCLUDING DEFAULTS)';
        EXECUTE 'INSERT INTO ' || buffer || '(SELECT * FROM ' || source_schema || '.' || objeto || ')';
    END LOOP;
 
END;
$$;


ALTER FUNCTION public.clone_schema(source_schema text, dest_schema text) OWNER TO pgadmin;

--
-- Name: create_matview(name, name); Type: FUNCTION; Schema: public; Owner: arpit
--

CREATE FUNCTION public.create_matview(name, name) RETURNS void
    LANGUAGE plpgsql SECURITY DEFINER
    AS $_$
DECLARE
    matview ALIAS FOR $1;
    view_name ALIAS FOR $2;
    entry matviews%ROWTYPE;
BEGIN
    SELECT * INTO entry FROM matviews WHERE mv_name = matview;

    IF FOUND THEN
        RAISE EXCEPTION 'Materialized view ''%'' already exists.',
          matview;
    END IF;

    EXECUTE 'REVOKE ALL ON ' || view_name || ' FROM PUBLIC'; 

    EXECUTE 'GRANT SELECT ON ' || view_name || ' TO PUBLIC';

    EXECUTE 'CREATE TABLE ' || matview || ' AS SELECT * FROM ' || view_name;

    EXECUTE 'REVOKE ALL ON ' || matview || ' FROM PUBLIC';

    EXECUTE 'GRANT SELECT ON ' || matview || ' TO PUBLIC';

    INSERT INTO matviews (mv_name, mv_view, last_refresh)
      VALUES (matview, view_name, CURRENT_TIMESTAMP); 
    
    RETURN;
END
$_$;


ALTER FUNCTION public.create_matview(name, name) OWNER TO arpit;

--
-- Name: drop_matview(name); Type: FUNCTION; Schema: public; Owner: arpit
--

CREATE FUNCTION public.drop_matview(name) RETURNS void
    LANGUAGE plpgsql SECURITY DEFINER
    AS $_$
DECLARE
    matview ALIAS FOR $1;
    entry matviews%ROWTYPE;
BEGIN

    SELECT * INTO entry FROM matviews WHERE mv_name = matview;

    IF NOT FOUND THEN
        RAISE EXCEPTION 'Materialized view % does not exist.', matview;
    END IF;

    EXECUTE 'DROP TABLE ' || matview;
    DELETE FROM matviews WHERE mv_name=matview;

    RETURN;
END
$_$;


ALTER FUNCTION public.drop_matview(name) OWNER TO arpit;

--
-- Name: dynamic_view(); Type: FUNCTION; Schema: public; Owner: arpit
--

CREATE FUNCTION public.dynamic_view() RETURNS void
    LANGUAGE plpgsql STRICT
    AS $_$
BEGIN
  EXECUTE 'CREATE OR REPLACE VIEW newView AS ' ||
            'SELECT * FROM func2(' || $1 || ', ' || $2 || ', ' || array_to_string($3, ',') || ')';
  RETURN;
END;
$_$;


ALTER FUNCTION public.dynamic_view() OWNER TO arpit;

--
-- Name: ec_id_ai(); Type: FUNCTION; Schema: public; Owner: pgadmin
--

CREATE FUNCTION public.ec_id_ai() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
new.ec_id=substring(new.state_code::TEXT,3,2) || lpad(new.auto_i::TEXT,10,'0');
return new;
END;
 $$;


ALTER FUNCTION public.ec_id_ai() OWNER TO pgadmin;

--
-- Name: func1(integer, timestamp without time zone, integer[]); Type: FUNCTION; Schema: public; Owner: arpit
--

CREATE FUNCTION public.func1(a integer, b timestamp without time zone, c integer[]) RETURNS void
    LANGUAGE plpgsql STRICT
    AS $_$
BEGIN
  EXECUTE 'CREATE OR REPLACE VIEW newView AS ' ||
            'SELECT * FROM func2(' || $1 || ', ' || $2 || ', ' || array_to_string($3, ',') || ')';
  RETURN;
END;
$_$;


ALTER FUNCTION public.func1(a integer, b timestamp without time zone, c integer[]) OWNER TO arpit;

--
-- Name: geom_scheme_availed_ec_id(text, numeric, numeric); Type: FUNCTION; Schema: public; Owner: arpit
--

CREATE FUNCTION public.geom_scheme_availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric) RETURNS TABLE(return_ec_id numeric, return_geom public.geometry)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN query
select ec_id,geom
from ind_master,region_allocation,district_master
where
ec_id IN(select ec_id from ind_Tracking where scheme_id=param_scheme_id)
and
geom in (select geom from district_master where ind_master.district_code=district_master.mc)
and
ind_master.district_code=param_district_code
and
region_allocation.region_type=param_region_type;
END;
$$;


ALTER FUNCTION public.geom_scheme_availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric) OWNER TO arpit;

--
-- Name: geteciddistrict(numeric); Type: FUNCTION; Schema: public; Owner: arpit
--

CREATE FUNCTION public.geteciddistrict(ec_id numeric) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
BEGIN
EXECUTE format
('select ec_id
from ind_master
where
ec_id IN (select ec_id from ind_Tracking where scheme_id=13)
and
district_code=912035800000000000;')
into ec_id;
END;
$$;


ALTER FUNCTION public.geteciddistrict(ec_id numeric) OWNER TO arpit;

--
-- Name: geteciddistrict(numeric, numeric); Type: FUNCTION; Schema: public; Owner: arpit
--

CREATE FUNCTION public.geteciddistrict(param_scheme_id numeric, param_district_code numeric) RETURNS TABLE(return_ec_id numeric)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN query
select ec_id
from ind_master
where
ec_id IN (select ec_id from ind_Tracking where scheme_id=param_scheme_id)
and
district_code=param_district_code;
END;
$$;


ALTER FUNCTION public.geteciddistrict(param_scheme_id numeric, param_district_code numeric) OWNER TO arpit;

--
-- Name: geteciddistrict(text, numeric, numeric); Type: FUNCTION; Schema: public; Owner: arpit
--

CREATE FUNCTION public.geteciddistrict(param_region_type text, param_scheme_id numeric, param_district_code numeric) RETURNS TABLE(return_ec_id numeric)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN query
select ec_id
from ind_master,region_allocation
where
ec_id IN (select ec_id from ind_Tracking where scheme_id=param_scheme_id)
and
ind_master.district_code=param_district_code
and
region_allocation.region_type=param_region_type;
END;
$$;


ALTER FUNCTION public.geteciddistrict(param_region_type text, param_scheme_id numeric, param_district_code numeric) OWNER TO arpit;

--
-- Name: hh_copy(); Type: FUNCTION; Schema: public; Owner: pgadmin
--

CREATE FUNCTION public.hh_copy() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
new.hh_id=substring(new.state_code::TEXT,3,2) || lpad(new.auto_i::TEXT,10,'0');
return new;
END;
$$;


ALTER FUNCTION public.hh_copy() OWNER TO pgadmin;

--
-- Name: hh_id_ai(); Type: FUNCTION; Schema: public; Owner: pgadmin
--

CREATE FUNCTION public.hh_id_ai() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
new.hh_id=substring(new.state_code::TEXT,3,2) || lpad(new.auto_i::TEXT,10,'0');
return new;
END;
 $$;


ALTER FUNCTION public.hh_id_ai() OWNER TO pgadmin;

--
-- Name: insert_into_hh_if_not_exists(numeric, numeric, numeric, numeric, numeric, text); Type: FUNCTION; Schema: public; Owner: arpit
--

CREATE FUNCTION public.insert_into_hh_if_not_exists(hh_head_tin numeric, state_code numeric, district_code numeric, tehsil_code numeric, village_code numeric, create_by text) RETURNS TABLE(return_id character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN
   RETURN QUERY
	with s as (
	    select tin, hh_id
	    from hh_master
	    where tin=hh_head_tin
	), i as (
	    insert into hh_master ("tin", "state_code", "district_code", "tehsil_code", "village_code", "create_by", "create_date")
	    select hh_head_tin, state_code, district_code, tehsil_code, village_code, create_by, LOCALTIMESTAMP
	    where not exists (select 1 from s where tin=hh_head_tin)
	    returning hh_id
	)
	select hh_id
	from s
	union all
	select hh_id
	from i;
END
$$;


ALTER FUNCTION public.insert_into_hh_if_not_exists(hh_head_tin numeric, state_code numeric, district_code numeric, tehsil_code numeric, village_code numeric, create_by text) OWNER TO arpit;

--
-- Name: left(character varying); Type: FUNCTION; Schema: public; Owner: arpit
--

CREATE FUNCTION public."left"(s character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
BEGIN
  return substr(s, 1);
 END
$$;


ALTER FUNCTION public."left"(s character varying) OWNER TO arpit;

--
-- Name: load_csv_file(text, text, integer); Type: FUNCTION; Schema: public; Owner: arpit
--

CREATE FUNCTION public.load_csv_file(target_table text, csv_path text, col_count integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

declare

iter integer; -- dummy integer to iterate columns with
col text; -- variable to keep the column name at each iteration
col_first text; -- first column name, e.g., top left corner on a csv file or spreadsheet

begin
    set schema 'public';

    create table temp_table ();

    -- add just enough number of columns
    for iter in 1..col_count
    loop
        execute format('alter table temp_table add column col_%s text;', iter);
    end loop;

    -- copy the data from csv file
    execute format('copy temp_table from %L with delimiter '','' quote ''"'' csv ', csv_path);

    iter := 1;
    col_first := (select col_1 from temp_table limit 1);

    -- update the column names based on the first row which has the column names
    for col in execute format('select unnest(string_to_array(trim(temp_table::text, ''()''), '','')) from temp_table where col_1 = %L', col_first)
    loop
        execute format('alter table temp_table rename column col_%s to %s', iter, col);
        iter := iter + 1;
    end loop;

    -- delete the columns row
    execute format('delete from temp_table where %s = %L', col_first, col_first);

    -- change the temp table name to the name given as parameter, if not blank
    if length(target_table) > 0 then
        execute format('alter table temp_table rename to %I', target_table);
    end if;

end;

$$;


ALTER FUNCTION public.load_csv_file(target_table text, csv_path text, col_count integer) OWNER TO arpit;

--
-- Name: refresh_matview(name); Type: FUNCTION; Schema: public; Owner: arpit
--

CREATE FUNCTION public.refresh_matview(name) RETURNS void
    LANGUAGE plpgsql SECURITY DEFINER
    AS $_$
DECLARE 
    matview ALIAS FOR $1;
    entry matviews%ROWTYPE;
BEGIN

    SELECT * INTO entry FROM matviews WHERE mv_name = matview;

    IF NOT FOUND THEN
        RAISE EXCEPTION 'Materialized view % does not exist.', matview;
    END IF;

    EXECUTE 'DELETE FROM ' || matview;
    EXECUTE 'INSERT INTO ' || matview
        || ' SELECT * FROM ' || entry.mv_view;

    UPDATE matviews
        SET last_refresh=CURRENT_TIMESTAMP
        WHERE mv_name=matview;

    RETURN;
END
$_$;


ALTER FUNCTION public.refresh_matview(name) OWNER TO arpit;

--
-- Name: regions_selection(numeric, numeric, numeric, numeric); Type: FUNCTION; Schema: public; Owner: arpit
--

CREATE FUNCTION public.regions_selection(param_state_code numeric, param_district_code numeric, param_tehsil_code numeric, param_village_code numeric) RETURNS TABLE(state_name text, district_name text, tehsil_name text, village_name text)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN query
select states_master.name as state_name,district_master.name as district_name,tehsil_master.name as tehsil_name,village_master.name as village_name
from
states_master,district_master,tehsil_master,village_master
where
states_master.mc=param_state_code
and
district_master.mc=param_district_code
and
tehsil_master.mc=param_tehsil_code
and
village_master.mc=param_village_code;
END;
$$;


ALTER FUNCTION public.regions_selection(param_state_code numeric, param_district_code numeric, param_tehsil_code numeric, param_village_code numeric) OWNER TO arpit;

--
-- Name: regions_selection_dynamic(text, numeric, numeric, numeric, numeric); Type: FUNCTION; Schema: public; Owner: arpit
--

CREATE FUNCTION public.regions_selection_dynamic(param_region_type text, param_state_code numeric, param_district_code numeric, param_tehsil_code numeric, param_village_code numeric) RETURNS TABLE(return_region_type text, return_state_name text, return_district_name text, return_tehsil_name text, return_village_name text)
    LANGUAGE plpgsql
    AS $$
BEGIN
IF region_allocation.region_type=param_region_type
AND
states_master.mc=param_state_code
and
district_master.mc=param_district_code
and
tehsil_master.mc=param_tehsil_code
and
village_master.mc=param_village_code
THEN
RETURN query
select region_type,states_master.name as state_name,district_master.name as district_name,tehsil_master.name as tehsil_name,village_master.name as village_name
from
region_allocation,states_master,district_master,tehsil_master,village_master;
END IF;
END;
$$;


ALTER FUNCTION public.regions_selection_dynamic(param_region_type text, param_state_code numeric, param_district_code numeric, param_tehsil_code numeric, param_village_code numeric) OWNER TO arpit;

--
-- Name: rel_description(character varying, character varying); Type: FUNCTION; Schema: public; Owner: arpit
--

CREATE FUNCTION public.rel_description(p_relname character varying, p_schemaname character varying DEFAULT NULL::character varying) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT obj_description((CASE 
       WHEN strpos($1, '.')>0 THEN $1
       WHEN $2 IS NULL THEN 'public.'||$1
       ELSE $2||'.'||$1
            END)::regclass, 'pg_class');
 $_$;


ALTER FUNCTION public.rel_description(p_relname character varying, p_schemaname character varying) OWNER TO arpit;

--
-- Name: scheme_availed_ec_id(text, numeric, numeric); Type: FUNCTION; Schema: public; Owner: arpit
--

CREATE FUNCTION public.scheme_availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric) RETURNS TABLE(return_ec_id numeric)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN query
select ec_id
from ind_master,region_allocation
where
ec_id IN(select ec_id from ind_Tracking where scheme_id=param_scheme_id)
and
ind_master.district_code=param_district_code
and
region_allocation.region_type=param_region_type;
END;
$$;


ALTER FUNCTION public.scheme_availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric) OWNER TO arpit;

--
-- Name: scheme_availed_ec_id(text, numeric, numeric, public.geometry); Type: FUNCTION; Schema: public; Owner: arpit
--

CREATE FUNCTION public.scheme_availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric, param_geom public.geometry) RETURNS TABLE(return_ec_id numeric, geom public.geometry)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN query
select ec_id,geom
from ind_master,region_allocation,district_master
where
ec_id IN (select ec_id from ind_Tracking where scheme_id=param_scheme_id)
and
ind_master.district_code=param_district_code
and
region_allocation.region_type=param_region_type
and
ind_master.geom=param_geom;
END;
$$;


ALTER FUNCTION public.scheme_availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric, param_geom public.geometry) OWNER TO arpit;

--
-- Name: scheme_availed_hh_id(text, numeric, numeric); Type: FUNCTION; Schema: public; Owner: arpit
--

CREATE FUNCTION public.scheme_availed_hh_id(param_region_type text, param_scheme_id numeric, param_district_code numeric) RETURNS TABLE(return_ec_id numeric)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN query
select hh_id
from hh_master,region_allocation
where
hh_id IN(select hh_id from hh_Tracking where scheme_id=param_scheme_id)
and
hh_master.district_code=param_district_code
and
region_allocation.region_type=param_region_type;
END;
$$;


ALTER FUNCTION public.scheme_availed_hh_id(param_region_type text, param_scheme_id numeric, param_district_code numeric) OWNER TO arpit;

--
-- Name: schme_availed_ec_id(text, numeric, numeric); Type: FUNCTION; Schema: public; Owner: arpit
--

CREATE FUNCTION public.schme_availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric) RETURNS TABLE(return_ec_id numeric)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN query
select ec_id
from ind_master,region_allocation
where
ec_id IN (select ec_id from ind_Tracking where scheme_id=param_scheme_id)
and
ind_master.district_code=param_district_code
and
region_allocation.region_type=param_region_type;
END;
$$;


ALTER FUNCTION public.schme_availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric) OWNER TO arpit;

--
-- Name: sortdata(); Type: FUNCTION; Schema: public; Owner: pgadmin
--

CREATE FUNCTION public.sortdata() RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    r record;
    s record;
    abf integer;
    tmp_id bigint; 
    sql varchar;
    nsql varchar;
    tmpText varchar;
    tmmpText varchar;
    tmpIdTxt varchar;
    tmpIdInt bigint;
begin
    sql := 'select distinct abf_hh_id as hhId from abf_ind';
    for r in execute sql
    loop
	tmp_id := 0;
	tmpText := trim( both '(' from r::text);
	tmpText := trim( both ')' from tmpText::text);
	nsql := 'select gid from abf_ind where abf_hh_id = ''' || tmpText || ''';';
	for s in execute nsql
	loop
	    tmp_id := tmp_id+1;
	    tmpIdTxt := tmp_id::text;
	    tmpIdTxt := REPEAT('0', 2-length(tmpIdTxt))||tmpIdTxt;
	    tmmpText := trim( both '(' from s::text);
	    tmmpText := trim( both ')' from tmmpText::text);
	    abf := tmmpText::integer;
	    tmpIdTxt := 91||tmpText||tmpIdTxt;
	    update abf_ind set tin = tmpIdTxt where gid = abf;
	end loop;
    end loop;
    return 'ok';
end;
$$;


ALTER FUNCTION public.sortdata() OWNER TO pgadmin;

--
-- Name: dbrnd; Type: FOREIGN DATA WRAPPER; Schema: -; Owner: postgres
--

CREATE FOREIGN DATA WRAPPER dbrnd VALIDATOR postgresql_fdw_validator;


ALTER FOREIGN DATA WRAPPER dbrnd OWNER TO postgres;

--
-- Name: bigdata; Type: SERVER; Schema: -; Owner: pgadmin
--

CREATE SERVER bigdata FOREIGN DATA WRAPPER dbrnd OPTIONS (
    dbname 'bigdata',
    hostaddr 'db2.biota.in'
);


ALTER SERVER bigdata OWNER TO pgadmin;

--
-- Name: USER MAPPING himanshu SERVER bigdata; Type: USER MAPPING; Schema: -; Owner: pgadmin
--

CREATE USER MAPPING FOR himanshu SERVER bigdata OPTIONS (
    password 'madhav29',
    "user" 'himanshu'
);


--
-- Name: USER MAPPING pgadmin SERVER bigdata; Type: USER MAPPING; Schema: -; Owner: pgadmin
--

CREATE USER MAPPING FOR pgadmin SERVER bigdata OPTIONS (
    password 'madhav29',
    "user" 'himanshu'
);


--
-- Name: USER MAPPING postgres SERVER bigdata; Type: USER MAPPING; Schema: -; Owner: pgadmin
--

CREATE USER MAPPING FOR postgres SERVER bigdata OPTIONS (
    password 'madhav29',
    "user" 'himanshu'
);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: access_policy; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.access_policy (
    role_id numeric,
    accesspolicy text,
    srno integer NOT NULL
);


ALTER TABLE public.access_policy OWNER TO pgadmin;

--
-- Name: COLUMN access_policy.role_id; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.access_policy.role_id IS 'role id';


--
-- Name: COLUMN access_policy.accesspolicy; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.access_policy.accesspolicy IS 'access policy';


--
-- Name: access_policy_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.access_policy_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.access_policy_id_seq OWNER TO pgadmin;

--
-- Name: access_policy_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.access_policy_id_seq OWNED BY public.access_policy.srno;


--
-- Name: app_version_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.app_version_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_version_id_seq OWNER TO pgadmin;

--
-- Name: app_version; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.app_version (
    id integer DEFAULT nextval('public.app_version_id_seq'::regclass) NOT NULL,
    version text,
    release_date timestamp without time zone,
    path text,
    create_by text,
    stable_version boolean,
    dev_version boolean,
    in_use boolean,
    is_public boolean,
    is_enumerator boolean,
    is_monitor boolean
);


ALTER TABLE public.app_version OWNER TO pgadmin;

--
-- Name: attributes_master; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.attributes_master (
    id numeric NOT NULL,
    name text,
    data_type text,
    ui_element text,
    for_hh boolean,
    for_ind boolean,
    start_date date,
    end_date date,
    create_by text,
    update_by text,
    create_date timestamp without time zone,
    update_date timestamp without time zone,
    possible_values text,
    system_attr boolean,
    weight numeric,
    CONSTRAINT check_ui_element CHECK (((ui_element = 'textfield'::text) OR (ui_element = 'select'::text) OR (ui_element = 'checkbox'::text) OR (ui_element = 'radio'::text) OR (ui_element = 'textarea'::text)))
);


ALTER TABLE public.attributes_master OWNER TO pgadmin;

--
-- Name: COLUMN attributes_master.id; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.attributes_master.id IS 'attribute id';


--
-- Name: COLUMN attributes_master.name; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.attributes_master.name IS 'name';


--
-- Name: COLUMN attributes_master.data_type; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.attributes_master.data_type IS 'data type';


--
-- Name: COLUMN attributes_master.ui_element; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.attributes_master.ui_element IS 'ui element';


--
-- Name: COLUMN attributes_master.for_hh; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.attributes_master.for_hh IS 'for household';


--
-- Name: COLUMN attributes_master.for_ind; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.attributes_master.for_ind IS 'for individual';


--
-- Name: COLUMN attributes_master.start_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.attributes_master.start_date IS 'start date';


--
-- Name: COLUMN attributes_master.end_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.attributes_master.end_date IS 'end date';


--
-- Name: COLUMN attributes_master.create_by; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.attributes_master.create_by IS 'create by';


--
-- Name: COLUMN attributes_master.update_by; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.attributes_master.update_by IS 'update by';


--
-- Name: COLUMN attributes_master.create_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.attributes_master.create_date IS 'create date';


--
-- Name: COLUMN attributes_master.update_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.attributes_master.update_date IS 'update date';


--
-- Name: COLUMN attributes_master.possible_values; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.attributes_master.possible_values IS 'possible values';


--
-- Name: att_master; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.att_master
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.att_master OWNER TO pgadmin;

--
-- Name: att_master; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.att_master OWNED BY public.attributes_master.id;


--
-- Name: attributes_master_lang_relation; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.attributes_master_lang_relation (
    f_id numeric,
    srno integer NOT NULL,
    l_id numeric,
    name text,
    possible_values text,
    create_by text,
    update_by text,
    create_date timestamp without time zone,
    update_date timestamp without time zone
);


ALTER TABLE public.attributes_master_lang_relation OWNER TO pgadmin;

--
-- Name: attributes_master_lang_relation_srno_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.attributes_master_lang_relation_srno_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.attributes_master_lang_relation_srno_seq OWNER TO pgadmin;

--
-- Name: attributes_master_lang_relation_srno_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.attributes_master_lang_relation_srno_seq OWNED BY public.attributes_master_lang_relation.srno;


--
-- Name: beat_master; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beat_master (
    name text,
    create_by text,
    geom public.geometry(MultiPolygon,4326),
    id numeric(10,0) NOT NULL,
    parent_id numeric,
    "timestamp" timestamp without time zone
);


ALTER TABLE public.beat_master OWNER TO postgres;

--
-- Name: bh_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.bh_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bh_id_seq OWNER TO pgadmin;

--
-- Name: block_master; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.block_master (
    create_by text,
    parent_id numeric,
    name text,
    id integer NOT NULL,
    geom public.geometry(Geometry,4326),
    "timestamp" timestamp without time zone
);


ALTER TABLE public.block_master OWNER TO postgres;

--
-- Name: c_id; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.c_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.c_id OWNER TO pgadmin;

--
-- Name: circle_master; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.circle_master (
    id numeric NOT NULL,
    parent_id numeric,
    name text,
    create_by text,
    geom public.geometry,
    "timestamp" timestamp without time zone
);


ALTER TABLE public.circle_master OWNER TO postgres;

--
-- Name: claim; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.claim (
    cl_id numeric(10,0) NOT NULL,
    g_id bigint,
    status text,
    create_by text,
    itern numeric,
    "timestamp" timestamp without time zone,
    id integer
);


ALTER TABLE public.claim OWNER TO postgres;

--
-- Name: claimant; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.claimant (
    c_id numeric(10,0) NOT NULL,
    name text,
    create_by text,
    "timestamp" timestamp without time zone
);


ALTER TABLE public.claimant OWNER TO postgres;

--
-- Name: compartment_master; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.compartment_master (
    name text,
    create_by text,
    geom public.geometry(Geometry,4326),
    id numeric(10,0) NOT NULL,
    parent_id numeric,
    "timestamp" timestamp without time zone
);


ALTER TABLE public.compartment_master OWNER TO postgres;

--
-- Name: district_master; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.district_master (
    mc numeric NOT NULL,
    state_code numeric NOT NULL,
    name text NOT NULL,
    create_by text,
    update_by text,
    create_date timestamp without time zone,
    update_date timestamp without time zone,
    start_date date,
    end_date date,
    geom public.geometry,
    hindi_name character varying
);


ALTER TABLE public.district_master OWNER TO pgadmin;

--
-- Name: COLUMN district_master.mc; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.district_master.mc IS 'master code';


--
-- Name: COLUMN district_master.state_code; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.district_master.state_code IS 'state code';


--
-- Name: COLUMN district_master.name; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.district_master.name IS 'name';


--
-- Name: COLUMN district_master.create_by; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.district_master.create_by IS 'created by';


--
-- Name: COLUMN district_master.update_by; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.district_master.update_by IS 'updated by';


--
-- Name: COLUMN district_master.create_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.district_master.create_date IS 'create date';


--
-- Name: COLUMN district_master.update_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.district_master.update_date IS 'update date';


--
-- Name: COLUMN district_master.start_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.district_master.start_date IS 'start date';


--
-- Name: COLUMN district_master.end_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.district_master.end_date IS 'end date';


--
-- Name: COLUMN district_master.geom; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.district_master.geom IS 'geometry column';


--
-- Name: division_master; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.division_master (
    id numeric NOT NULL,
    name text,
    create_by text,
    parent_id numeric,
    geom public.geometry(Geometry,4326),
    "timestamp" timestamp without time zone
);


ALTER TABLE public.division_master OWNER TO postgres;

--
-- Name: eligibility_check_counter_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.eligibility_check_counter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.eligibility_check_counter_id_seq OWNER TO pgadmin;

--
-- Name: error_info; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.error_info (
    er_id numeric NOT NULL,
    er_discription text,
    start_date date,
    end_date date
);


ALTER TABLE public.error_info OWNER TO pgadmin;

--
-- Name: COLUMN error_info.er_id; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.error_info.er_id IS 'error id';


--
-- Name: COLUMN error_info.er_discription; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.error_info.er_discription IS 'description';


--
-- Name: COLUMN error_info.start_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.error_info.start_date IS 'start date';


--
-- Name: COLUMN error_info.end_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.error_info.end_date IS 'end date';


--
-- Name: err_master; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.err_master
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.err_master OWNER TO pgadmin;

--
-- Name: err_master; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.err_master OWNED BY public.error_info.er_id;


--
-- Name: form_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.form_data (
    id numeric(10,0) NOT NULL,
    f_id numeric,
    att_id numeric,
    answer text,
    create_by text,
    "timestamp" timestamp without time zone,
    grp_id integer,
    claim_id integer,
    claimant_id integer
);


ALTER TABLE public.form_data OWNER TO postgres;

--
-- Name: form_master; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.form_master (
    f_id numeric(10,0) NOT NULL,
    q_id numeric,
    create_by text,
    "timestamp" timestamp without time zone
);


ALTER TABLE public.form_master OWNER TO postgres;

--
-- Name: geolocations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.geolocations (
    id integer NOT NULL,
    c_id integer,
    declatitude numeric(10,7),
    declongitude numeric(10,7),
    decaltitude numeric(10,7),
    decaccuracy numeric(10,7),
    dtideleted date
);


ALTER TABLE public.geolocations OWNER TO postgres;

--
-- Name: gp_master; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.gp_master (
    state character varying,
    district character varying,
    tehsil character varying,
    name character varying,
    state_code numeric,
    district_code numeric,
    tehsil_code numeric,
    mc numeric NOT NULL,
    gp_code numeric
);


ALTER TABLE public.gp_master OWNER TO pgadmin;

--
-- Name: group_master; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.group_master (
    group_id numeric NOT NULL,
    group_name text,
    group_desc text,
    start_date date,
    end_date date,
    parent_group_id numeric,
    create_by text,
    update_by text,
    create_date timestamp without time zone,
    update_date timestamp without time zone
);


ALTER TABLE public.group_master OWNER TO pgadmin;

--
-- Name: COLUMN group_master.group_id; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.group_master.group_id IS 'group id';


--
-- Name: COLUMN group_master.group_name; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.group_master.group_name IS 'group name';


--
-- Name: COLUMN group_master.group_desc; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.group_master.group_desc IS 'group descirption';


--
-- Name: COLUMN group_master.start_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.group_master.start_date IS 'start date';


--
-- Name: COLUMN group_master.end_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.group_master.end_date IS 'end date';


--
-- Name: COLUMN group_master.parent_group_id; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.group_master.parent_group_id IS 'parent group id';


--
-- Name: COLUMN group_master.create_by; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.group_master.create_by IS 'create by';


--
-- Name: COLUMN group_master.update_by; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.group_master.update_by IS 'update by';


--
-- Name: COLUMN group_master.create_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.group_master.create_date IS 'create date';


--
-- Name: COLUMN group_master.update_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.group_master.update_date IS 'update date';


--
-- Name: groups_master; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.groups_master
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.groups_master OWNER TO pgadmin;

--
-- Name: groups_master; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.groups_master OWNED BY public.group_master.group_id;


--
-- Name: l_id; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.l_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.l_id OWNER TO pgadmin;

--
-- Name: language_master; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.language_master (
    id numeric NOT NULL,
    name text,
    "desc" text,
    created_by text,
    updated_by text,
    date timestamp without time zone,
    status boolean,
    language_code text,
    create_date timestamp without time zone,
    update_date timestamp without time zone,
    column1 character varying
);


ALTER TABLE public.language_master OWNER TO pgadmin;

--
-- Name: COLUMN language_master.id; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.language_master.id IS 'language id';


--
-- Name: COLUMN language_master.name; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.language_master.name IS 'language ';


--
-- Name: COLUMN language_master."desc"; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.language_master."desc" IS 'description';


--
-- Name: COLUMN language_master.created_by; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.language_master.created_by IS 'created by';


--
-- Name: COLUMN language_master.updated_by; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.language_master.updated_by IS 'modified by';


--
-- Name: COLUMN language_master.date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.language_master.date IS 'date';


--
-- Name: COLUMN language_master.status; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.language_master.status IS 'status';


--
-- Name: layer_info; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.layer_info (
    l_id integer DEFAULT nextval('public.l_id'::regclass) NOT NULL,
    c_id integer,
    workspace text,
    store text,
    server_url text,
    layer_name character varying(45),
    layer_date date,
    layer_displayname character varying(200),
    layer_metadata character varying(45),
    is_temporal bigint,
    "TIMESTAMP" timestamp without time zone,
    field_name character varying,
    field_filter boolean,
    for_hh boolean,
    for_ind boolean
);


ALTER TABLE public.layer_info OWNER TO pgadmin;

--
-- Name: layer_primary_category; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.layer_primary_category (
    c_id integer DEFAULT nextval('public.c_id'::regclass) NOT NULL,
    c_name character varying(45),
    c_discription character varying(45),
    "TIMESTAMP" timestamp without time zone NOT NULL,
    create_by character varying,
    update_by character varying,
    create_date date,
    update_date date
);


ALTER TABLE public.layer_primary_category OWNER TO pgadmin;

--
-- Name: org_master; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.org_master (
    id integer,
    name text,
    description text,
    start_date date,
    end_date date
);


ALTER TABLE public.org_master OWNER TO postgres;

--
-- Name: page_category_master_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.page_category_master_seq
    START WITH 10
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.page_category_master_seq OWNER TO pgadmin;

--
-- Name: page_category_master; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.page_category_master (
    page_category_id numeric DEFAULT nextval('public.page_category_master_seq'::regclass) NOT NULL,
    page_category_name text,
    weight numeric
);


ALTER TABLE public.page_category_master OWNER TO pgadmin;

--
-- Name: page_master_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.page_master_seq
    START WITH 31
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.page_master_seq OWNER TO pgadmin;

--
-- Name: page_master; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.page_master (
    page_id numeric DEFAULT nextval('public.page_master_seq'::regclass) NOT NULL,
    page_category_id numeric,
    page_link text,
    page_name text,
    for_mobile boolean,
    mobile_action text
);


ALTER TABLE public.page_master OWNER TO pgadmin;

--
-- Name: range_master; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.range_master (
    parent_id numeric,
    name text,
    create_by text,
    geom public.geometry(Geometry,4326),
    id numeric(10,0) NOT NULL,
    "timestamp" timestamp without time zone
);


ALTER TABLE public.range_master OWNER TO postgres;

--
-- Name: region_allocation; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.region_allocation (
    user_id text NOT NULL,
    region_type text,
    state_code text,
    district_code text,
    tehsil_code text,
    village_code text,
    start_date date,
    end_date date,
    create_by text,
    update_by text,
    create_date timestamp without time zone,
    update_date timestamp without time zone,
    srno numeric(10,0) NOT NULL,
    CONSTRAINT region_allocation_region_type_check CHECK (((region_type = 'state'::text) OR (region_type = 'district'::text) OR (region_type = 'tehsil'::text) OR (region_type = 'village'::text)))
);


ALTER TABLE public.region_allocation OWNER TO pgadmin;

--
-- Name: COLUMN region_allocation.user_id; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.region_allocation.user_id IS 'user id';


--
-- Name: COLUMN region_allocation.region_type; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.region_allocation.region_type IS 'region type';


--
-- Name: COLUMN region_allocation.state_code; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.region_allocation.state_code IS 'state code';


--
-- Name: COLUMN region_allocation.district_code; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.region_allocation.district_code IS 'district code';


--
-- Name: COLUMN region_allocation.tehsil_code; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.region_allocation.tehsil_code IS 'tehsil code';


--
-- Name: COLUMN region_allocation.village_code; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.region_allocation.village_code IS 'village code';


--
-- Name: COLUMN region_allocation.start_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.region_allocation.start_date IS 'start date';


--
-- Name: COLUMN region_allocation.end_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.region_allocation.end_date IS 'end date';


--
-- Name: COLUMN region_allocation.create_by; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.region_allocation.create_by IS 'create by';


--
-- Name: COLUMN region_allocation.update_by; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.region_allocation.update_by IS 'update by';


--
-- Name: COLUMN region_allocation.create_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.region_allocation.create_date IS 'create date';


--
-- Name: COLUMN region_allocation.update_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.region_allocation.update_date IS 'update date';


--
-- Name: region_allocation_srno_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.region_allocation_srno_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.region_allocation_srno_seq OWNER TO pgadmin;

--
-- Name: region_allocation_srno_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.region_allocation_srno_seq OWNED BY public.region_allocation.srno;


--
-- Name: user_roles_relation; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.user_roles_relation (
    user_id text NOT NULL,
    role_id numeric,
    start_date date,
    end_date date,
    sr_no integer NOT NULL
);


ALTER TABLE public.user_roles_relation OWNER TO pgadmin;

--
-- Name: COLUMN user_roles_relation.user_id; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.user_roles_relation.user_id IS 'user id';


--
-- Name: COLUMN user_roles_relation.role_id; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.user_roles_relation.role_id IS 'role id';


--
-- Name: COLUMN user_roles_relation.start_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.user_roles_relation.start_date IS 'start date';


--
-- Name: COLUMN user_roles_relation.end_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.user_roles_relation.end_date IS 'end date';


--
-- Name: roles_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.roles_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_seq OWNER TO pgadmin;

--
-- Name: roles_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.roles_seq OWNED BY public.user_roles_relation.sr_no;


--
-- Name: roles_master; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.roles_master (
    role_id numeric DEFAULT nextval('public.roles_seq'::regclass) NOT NULL,
    role_name text,
    role_desc text,
    start_date date,
    end_date date,
    create_by text,
    update_by text,
    create_date timestamp without time zone,
    update_date timestamp without time zone
);


ALTER TABLE public.roles_master OWNER TO pgadmin;

--
-- Name: COLUMN roles_master.role_id; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.roles_master.role_id IS 'role id';


--
-- Name: COLUMN roles_master.role_name; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.roles_master.role_name IS 'role name';


--
-- Name: COLUMN roles_master.role_desc; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.roles_master.role_desc IS 'role description';


--
-- Name: COLUMN roles_master.start_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.roles_master.start_date IS 'start date';


--
-- Name: COLUMN roles_master.end_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.roles_master.end_date IS 'end date';


--
-- Name: COLUMN roles_master.create_by; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.roles_master.create_by IS 'create by';


--
-- Name: COLUMN roles_master.update_by; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.roles_master.update_by IS 'update by';


--
-- Name: COLUMN roles_master.create_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.roles_master.create_date IS 'create date';


--
-- Name: COLUMN roles_master.update_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.roles_master.update_date IS 'update date';


--
-- Name: s_id; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.s_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_id OWNER TO pgadmin;

--
-- Name: sc_id; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.sc_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sc_id OWNER TO pgadmin;

--
-- Name: schemes_master_sc_id_no_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.schemes_master_sc_id_no_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.schemes_master_sc_id_no_seq OWNER TO pgadmin;

--
-- Name: shg_master_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.shg_master_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.shg_master_id_seq OWNER TO pgadmin;

--
-- Name: shg_master_ind_relation_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.shg_master_ind_relation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.shg_master_ind_relation_id_seq OWNER TO pgadmin;

--
-- Name: sms_logs; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.sms_logs (
    id numeric NOT NULL,
    db_query text,
    sms_body text,
    recipient_user_id text,
    recipient_phone_number text,
    status boolean,
    sent_date timestamp without time zone,
    sr_no integer NOT NULL
);


ALTER TABLE public.sms_logs OWNER TO pgadmin;

--
-- Name: COLUMN sms_logs.id; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.sms_logs.id IS 'id';


--
-- Name: COLUMN sms_logs.db_query; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.sms_logs.db_query IS 'database query';


--
-- Name: COLUMN sms_logs.sms_body; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.sms_logs.sms_body IS 'sms body';


--
-- Name: COLUMN sms_logs.recipient_user_id; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.sms_logs.recipient_user_id IS 'recipient user id';


--
-- Name: COLUMN sms_logs.recipient_phone_number; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.sms_logs.recipient_phone_number IS 'phone number';


--
-- Name: COLUMN sms_logs.status; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.sms_logs.status IS 'status';


--
-- Name: COLUMN sms_logs.sent_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.sms_logs.sent_date IS 'sent date';


--
-- Name: sms_logs_sr_no_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.sms_logs_sr_no_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sms_logs_sr_no_seq OWNER TO pgadmin;

--
-- Name: sms_logs_sr_no_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.sms_logs_sr_no_seq OWNED BY public.sms_logs.sr_no;


--
-- Name: sms_short_codes_master; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.sms_short_codes_master (
    code_id numeric NOT NULL,
    code_number text,
    response text,
    start_date date,
    end_date date,
    create_by text,
    update_by text,
    create_date timestamp without time zone,
    update_date timestamp without time zone,
    sr_no integer NOT NULL
);


ALTER TABLE public.sms_short_codes_master OWNER TO pgadmin;

--
-- Name: COLUMN sms_short_codes_master.code_id; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.sms_short_codes_master.code_id IS 'code id';


--
-- Name: COLUMN sms_short_codes_master.code_number; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.sms_short_codes_master.code_number IS 'code number';


--
-- Name: COLUMN sms_short_codes_master.response; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.sms_short_codes_master.response IS 'response';


--
-- Name: COLUMN sms_short_codes_master.start_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.sms_short_codes_master.start_date IS 'start date';


--
-- Name: COLUMN sms_short_codes_master.end_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.sms_short_codes_master.end_date IS 'end date';


--
-- Name: COLUMN sms_short_codes_master.create_by; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.sms_short_codes_master.create_by IS 'create by';


--
-- Name: COLUMN sms_short_codes_master.update_by; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.sms_short_codes_master.update_by IS 'update by';


--
-- Name: COLUMN sms_short_codes_master.create_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.sms_short_codes_master.create_date IS 'create date';


--
-- Name: COLUMN sms_short_codes_master.update_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.sms_short_codes_master.update_date IS 'update date';


--
-- Name: sms_short_codes_master_sr_no_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.sms_short_codes_master_sr_no_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sms_short_codes_master_sr_no_seq OWNER TO pgadmin;

--
-- Name: sms_short_codes_master_sr_no_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.sms_short_codes_master_sr_no_seq OWNED BY public.sms_short_codes_master.sr_no;


--
-- Name: sr_no; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.sr_no
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sr_no OWNER TO pgadmin;

--
-- Name: states_master; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.states_master (
    mc numeric NOT NULL,
    create_by text,
    update_by text,
    create_date timestamp without time zone,
    update_date timestamp without time zone,
    start_date date,
    end_date date,
    state_code numeric NOT NULL,
    name text,
    geom public.geometry,
    sr_no integer NOT NULL
);


ALTER TABLE public.states_master OWNER TO pgadmin;

--
-- Name: COLUMN states_master.mc; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.states_master.mc IS 'master code';


--
-- Name: COLUMN states_master.create_by; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.states_master.create_by IS 'create by';


--
-- Name: COLUMN states_master.update_by; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.states_master.update_by IS 'update by';


--
-- Name: COLUMN states_master.create_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.states_master.create_date IS 'create date';


--
-- Name: COLUMN states_master.update_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.states_master.update_date IS 'update date';


--
-- Name: COLUMN states_master.start_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.states_master.start_date IS 'start date';


--
-- Name: COLUMN states_master.end_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.states_master.end_date IS 'end date';


--
-- Name: COLUMN states_master.state_code; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.states_master.state_code IS 'state code';


--
-- Name: COLUMN states_master.name; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.states_master.name IS 'name';


--
-- Name: COLUMN states_master.geom; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.states_master.geom IS 'geometry colume';


--
-- Name: states_master_sr_no_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.states_master_sr_no_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.states_master_sr_no_seq OWNER TO pgadmin;

--
-- Name: states_master_sr_no_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.states_master_sr_no_seq OWNED BY public.states_master.sr_no;


--
-- Name: tehsil_master; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.tehsil_master (
    mc numeric NOT NULL,
    state_code numeric NOT NULL,
    district_code numeric NOT NULL,
    name text NOT NULL,
    start_date date,
    end_date date,
    create_by text,
    update_by text,
    create_date timestamp without time zone,
    update_date timestamp without time zone,
    geom public.geometry,
    sr_no integer NOT NULL,
    hindi_name character varying
);


ALTER TABLE public.tehsil_master OWNER TO pgadmin;

--
-- Name: COLUMN tehsil_master.mc; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.tehsil_master.mc IS 'master code';


--
-- Name: COLUMN tehsil_master.state_code; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.tehsil_master.state_code IS 'state code';


--
-- Name: COLUMN tehsil_master.district_code; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.tehsil_master.district_code IS 'district code';


--
-- Name: COLUMN tehsil_master.name; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.tehsil_master.name IS 'name';


--
-- Name: COLUMN tehsil_master.start_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.tehsil_master.start_date IS 'start date';


--
-- Name: COLUMN tehsil_master.end_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.tehsil_master.end_date IS 'end date';


--
-- Name: COLUMN tehsil_master.create_by; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.tehsil_master.create_by IS 'create by';


--
-- Name: COLUMN tehsil_master.update_by; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.tehsil_master.update_by IS 'update by';


--
-- Name: COLUMN tehsil_master.create_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.tehsil_master.create_date IS 'create date';


--
-- Name: COLUMN tehsil_master.update_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.tehsil_master.update_date IS 'update date';


--
-- Name: COLUMN tehsil_master.geom; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.tehsil_master.geom IS 'geometry colume';


--
-- Name: tehsil_master_sr_no_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.tehsil_master_sr_no_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tehsil_master_sr_no_seq OWNER TO pgadmin;

--
-- Name: tehsil_master_sr_no_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.tehsil_master_sr_no_seq OWNED BY public.tehsil_master.sr_no;


--
-- Name: token_session; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.token_session (
    srno integer NOT NULL,
    device_id text,
    token_id text,
    user_id text,
    start_date timestamp without time zone,
    end_date timestamp without time zone
);


ALTER TABLE public.token_session OWNER TO pgadmin;

--
-- Name: token_session_srno_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.token_session_srno_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.token_session_srno_seq OWNER TO pgadmin;

--
-- Name: token_session_srno_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.token_session_srno_seq OWNED BY public.token_session.srno;


--
-- Name: u_id; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.u_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.u_id OWNER TO pgadmin;

--
-- Name: user_group_relation_sr_no_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.user_group_relation_sr_no_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_group_relation_sr_no_seq OWNER TO pgadmin;

--
-- Name: user_group_relation_sr_no_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.user_group_relation_sr_no_seq OWNED BY public.roles_master.role_id;


--
-- Name: user_group_relation; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.user_group_relation (
    user_id text NOT NULL,
    group_id numeric,
    start_date date,
    end_date date,
    sr_no integer DEFAULT nextval('public.user_group_relation_sr_no_seq'::regclass) NOT NULL
);


ALTER TABLE public.user_group_relation OWNER TO pgadmin;

--
-- Name: COLUMN user_group_relation.user_id; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.user_group_relation.user_id IS 'user id';


--
-- Name: COLUMN user_group_relation.group_id; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.user_group_relation.group_id IS 'group id';


--
-- Name: COLUMN user_group_relation.start_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.user_group_relation.start_date IS 'start date';


--
-- Name: COLUMN user_group_relation.end_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.user_group_relation.end_date IS 'end date';


--
-- Name: user_master; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.user_master (
    username text NOT NULL,
    name text,
    designation text,
    landline_number numeric,
    mob_number numeric,
    email text,
    address text,
    start_date date,
    end_date date,
    password text,
    sr_no integer NOT NULL,
    default_village numeric
);


ALTER TABLE public.user_master OWNER TO pgadmin;

--
-- Name: COLUMN user_master.username; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.user_master.username IS 'user id';


--
-- Name: COLUMN user_master.name; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.user_master.name IS 'user name';


--
-- Name: COLUMN user_master.designation; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.user_master.designation IS 'designation';


--
-- Name: COLUMN user_master.landline_number; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.user_master.landline_number IS 'phone number';


--
-- Name: COLUMN user_master.mob_number; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.user_master.mob_number IS 'mobile number';


--
-- Name: COLUMN user_master.email; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.user_master.email IS 'email';


--
-- Name: COLUMN user_master.address; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.user_master.address IS 'address';


--
-- Name: COLUMN user_master.start_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.user_master.start_date IS 'start date';


--
-- Name: COLUMN user_master.end_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.user_master.end_date IS 'end date';


--
-- Name: COLUMN user_master.password; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.user_master.password IS 'password';


--
-- Name: user_master_sr_no_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.user_master_sr_no_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_master_sr_no_seq OWNER TO pgadmin;

--
-- Name: user_master_sr_no_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.user_master_sr_no_seq OWNED BY public.user_master.sr_no;


--
-- Name: user_role_rel; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.user_role_rel
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_role_rel OWNER TO pgadmin;

--
-- Name: user_role_rel; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.user_role_rel OWNED BY public.user_roles_relation.sr_no;


--
-- Name: village_master; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.village_master (
    mc numeric NOT NULL,
    state_code numeric NOT NULL,
    district_code numeric NOT NULL,
    tehsil_code numeric NOT NULL,
    name text NOT NULL,
    start_date date,
    end_date date,
    create_by text,
    update_by text,
    create_date timestamp without time zone,
    update_date timestamp without time zone,
    geom public.geometry,
    sr_no integer NOT NULL,
    panchayat_code numeric,
    hindi_name character varying
);


ALTER TABLE public.village_master OWNER TO pgadmin;

--
-- Name: COLUMN village_master.mc; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.village_master.mc IS 'master code';


--
-- Name: COLUMN village_master.state_code; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.village_master.state_code IS 'state code';


--
-- Name: COLUMN village_master.district_code; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.village_master.district_code IS 'district code';


--
-- Name: COLUMN village_master.tehsil_code; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.village_master.tehsil_code IS 'tehsil code';


--
-- Name: COLUMN village_master.name; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.village_master.name IS 'name';


--
-- Name: COLUMN village_master.start_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.village_master.start_date IS 'start date';


--
-- Name: COLUMN village_master.end_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.village_master.end_date IS 'end date';


--
-- Name: COLUMN village_master.create_by; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.village_master.create_by IS 'create by';


--
-- Name: COLUMN village_master.update_by; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.village_master.update_by IS 'update by';


--
-- Name: COLUMN village_master.create_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.village_master.create_date IS 'create date';


--
-- Name: COLUMN village_master.update_date; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.village_master.update_date IS 'update date';


--
-- Name: COLUMN village_master.geom; Type: COMMENT; Schema: public; Owner: pgadmin
--

COMMENT ON COLUMN public.village_master.geom IS 'geometry colume';


--
-- Name: village_master_sr_no_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.village_master_sr_no_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.village_master_sr_no_seq OWNER TO pgadmin;

--
-- Name: village_master_sr_no_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgadmin
--

ALTER SEQUENCE public.village_master_sr_no_seq OWNED BY public.village_master.sr_no;


--
-- Name: w_id; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.w_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.w_id OWNER TO pgadmin;

--
-- Name: web_sessions_id_seq; Type: SEQUENCE; Schema: public; Owner: pgadmin
--

CREATE SEQUENCE public.web_sessions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.web_sessions_id_seq OWNER TO pgadmin;

--
-- Name: web_sessions; Type: TABLE; Schema: public; Owner: pgadmin
--

CREATE TABLE public.web_sessions (
    id integer DEFAULT nextval('public.web_sessions_id_seq'::regclass) NOT NULL,
    user_id text,
    "timestamp" timestamp with time zone DEFAULT now(),
    ip_address text,
    user_agent text
);


ALTER TABLE public.web_sessions OWNER TO pgadmin;

--
-- Name: access_policy srno; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.access_policy ALTER COLUMN srno SET DEFAULT nextval('public.access_policy_id_seq'::regclass);


--
-- Name: attributes_master id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.attributes_master ALTER COLUMN id SET DEFAULT nextval('public.att_master'::regclass);


--
-- Name: attributes_master_lang_relation srno; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.attributes_master_lang_relation ALTER COLUMN srno SET DEFAULT nextval('public.attributes_master_lang_relation_srno_seq'::regclass);


--
-- Name: error_info er_id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.error_info ALTER COLUMN er_id SET DEFAULT nextval('public.err_master'::regclass);


--
-- Name: group_master group_id; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.group_master ALTER COLUMN group_id SET DEFAULT nextval('public.groups_master'::regclass);


--
-- Name: region_allocation srno; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.region_allocation ALTER COLUMN srno SET DEFAULT nextval('public.region_allocation_srno_seq'::regclass);


--
-- Name: sms_logs sr_no; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.sms_logs ALTER COLUMN sr_no SET DEFAULT nextval('public.sms_logs_sr_no_seq'::regclass);


--
-- Name: sms_short_codes_master sr_no; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.sms_short_codes_master ALTER COLUMN sr_no SET DEFAULT nextval('public.sms_short_codes_master_sr_no_seq'::regclass);


--
-- Name: states_master sr_no; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.states_master ALTER COLUMN sr_no SET DEFAULT nextval('public.states_master_sr_no_seq'::regclass);


--
-- Name: tehsil_master sr_no; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.tehsil_master ALTER COLUMN sr_no SET DEFAULT nextval('public.tehsil_master_sr_no_seq'::regclass);


--
-- Name: token_session srno; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.token_session ALTER COLUMN srno SET DEFAULT nextval('public.token_session_srno_seq'::regclass);


--
-- Name: user_master sr_no; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.user_master ALTER COLUMN sr_no SET DEFAULT nextval('public.user_master_sr_no_seq'::regclass);


--
-- Name: user_roles_relation sr_no; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.user_roles_relation ALTER COLUMN sr_no SET DEFAULT nextval('public.user_role_rel'::regclass);


--
-- Name: village_master sr_no; Type: DEFAULT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.village_master ALTER COLUMN sr_no SET DEFAULT nextval('public.village_master_sr_no_seq'::regclass);


--
-- Data for Name: access_policy; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.access_policy (role_id, accesspolicy, srno) FROM stdin;
0	DataEntry.getNewEntryForm,DataEntry.postNewEntry,DataEntry.getVerification	1
4	DataEntry.getVerification,Reports.getSECCDashboard,Reports.getSECCGISDashboard	4
14	Scheme.getFormFieldsHH,Scheme.getNewSchemeEntry,Scheme.getClassAndAction,Sync.getClassAndAction,Reports.getSECCGISDashboard	6
2	DataEntry.postNewEntry,DataEntry.getVerification,DataEntry.postVerification,DataEntry.getAllEntries,DataEntry.getAllSECCEntries,DataEntry.postSECCEntry,User.getAdminUsers,User.getAdminUserUpdate,Scheme.getStateSchemes,Scheme.getstatesList,Scheme.getFormFields,Scheme.getSchemeDeatils,Sync.getClassAndAction,Reports.getPDF	2
3	DataEntry.getNewEntry,DataEntry.getNewHHEntry,DataEntry.getHHMembers,DataEntry.postNewEntry,DataEntry.getIdentityFields,DataEntry.getVerification,DataEntry.postVerification,DataEntry.getAllEntries,DataEntry.getAllSECCEntries,DataEntry.getSECCEntryVerify,DataEntry.getSECCEntryView,DataEntry.postSECCEntry,DataEntry.getEntitlementCard,DataEntry.getAllIndividuals,DataEntry.getAllHH,DataEntry.getIndividualsEntry,DataEntry.getHHEntry,DataEntry.postIndividualsEntry,DataEntry.postHHEntry,DataEntry.getEntitlementTracking,DataEntry.getEntitlementTrackingHH,DataEntry.getSchemesListInd,DataEntry.getSchemesListHH,DataEntry.getSchemesListIndTracking,DataEntry.getSchemesListHHTracking,DataEntry.getSchemesListAppliedInd,DataEntry.getSchemesListAvailedInd,DataEntry.getSchemesListAppliedHH,DataEntry.getSchemesListAvailedHH,DataEntry.getCheckEligibilityEC,DataEntry.getCheckEligibilityHH,DataEntry.postEntitlementTracking,DataEntry.postEntitlementTrackingHH,DataEntry.getGrievanceForm,DataEntry.getGrievanceFormHH,DataEntry.postGrievanceForm,DataEntry.postGrievanceFormHH,DataEntry.getAllGrievances,DataEntry.getAllGrievancesHH,DataEntry.getGrievanceEntryView,DataEntry.getGrievanceEntryViewHH,DataEntry.getGrievanceEntryUpdate,DataEntry.getGrievanceEntryUpdateHH,DataEntry.postGrievanceEntryUpdate,DataEntry.postGrievanceEntryUpdateHH,DataEntry.getEntitlementRegister,DataEntry.getEntitlementRegisterHH,DataEntry.getDeathRegistrations,DataEntry.getDeathRegistration,DataEntry.getIndDetails,DataEntry.postDeathRegistration,DataEntry.getDeathDetails,DataEntry.getHHSplit,DataEntry.getIndividualsListHH,DataEntry.postHHSplit,DataEntry.getClassAndAction,DataEntry.getUserAllocatedRegionsPolicy,DataEntry.getRegionHierarchy,DataEntry.getPDF,User.getLogin,User.getLogout,User.updateGroup,Scheme.getStateSchemes,Scheme.getstatesList,Scheme.getFormFields,Scheme.getFormFieldsHH,Scheme.getSchemeSearch,Scheme.getSchemeDeatils,Scheme.getNewSchemeEntry,Scheme.postEligibilityCheckCounter,Scheme.getEligibilityCheckCounter,Scheme.getClassAndAction,Scheme.getPDF,Sync.getSyncTable,Sync.getClassAndAction,Sync.getPDF,Reports.getEligibleHHlist,Contact.getJSLPS,Contact.getClassAndAction,Contact.getUserAllocatedRegionsPolicy,Contact.getRegionHierarchy,Contact.getPDF,AppUpdate.getAppUpdateInfoPublic,AppUpdate.getAppUpdateInfoEnumerator,AppUpdate.getAppUpdateInfoMonitor,AppUpdate.getLatestVersionPublic,AppUpdate.getLatestVersionEnumerator,AppUpdate.getLatestVersionMonitor	3
17	User.getLogin,User.getLoginMobile,User.getLogoutMobile,User.getAccessPolicyMobile,User.getLogout,User.getChangePassword,User.postChangePassword,Scheme.getStateSchemes,Scheme.getstatesList,Scheme.getFormFields,Scheme.getFormFieldsHH,Scheme.getSchemeSearch,Scheme.getFormFields_,Scheme.getFormFieldsHH_,Scheme.getSchemeDeatils,Scheme.getAllStateSchemesInd,Scheme.getEligibilityCheckCounter,Scheme.getClassAndAction,Contact.getJSLPS,Contact.getClassAndAction,Contact.getUserAllocatedRegionsPolicy,Contact.getRegionHierarchy,Contact.getPDF,AppUpdate.getAppUpdateInfoPublic,AppUpdate.getAppUpdateInfoEnumerator,AppUpdate.getAppUpdateInfoMonitor,AppUpdate.getLatestVersionPublic,AppUpdate.getLatestVersionEnumerator,AppUpdate.getLatestVersionMonitor	9
15	DataEntry.getDownloadIndData,DataEntry.getDownloadHHData,Scheme.getEligibilityCheckCounter,Reports.getSECCDashboard,Reports.getSECCReports,Reports.getSECCDashboardImage,Reports.getGISDashboard,Reports.getLayerCategories,Reports.getLayerSecondaryCategories,Reports.getLayerTertiaryCategories,Reports.getLayerTertiaryCategoryLayers,Reports.getLayerInfo,Reports.getSECCGISDashboard,Reports.getTrackingGISDashboard,Reports.getIndividualReports,Reports.getAvailableSchemes,Reports.getSelectedRegionsList,Reports.getStateSchemes,Reports.getPrintAvailableSchemes,Reports.getLocationReports,Reports.getGrievanceReports,Reports.getGrievanceReport,Reports.getLayerLatLon,Reports.getAllSchemeDetails,Reports.getEligibleHHlist,Reports.getEligibleIndlist,Reports.getAutoInclusionHH,Reports.getDeprivationHH,Reports.getSchemeWiseIndEligibility,Reports.getIndTrackingList,Reports.getHHTrackingList,Reports.getEnumeratorReport,Reports.getDashboard,Reports.getClassAndAction,Reports.getUserAllocatedRegionsPolicy,Reports.getRegionHierarchy,Reports.getPDF,Contact.getJSLPS,Contact.getClassAndAction,Contact.getUserAllocatedRegionsPolicy,Contact.getRegionHierarchy,Contact.getPDF,AppUpdate.getAppUpdateInfoPublic,AppUpdate.getAppUpdateInfoEnumerator,AppUpdate.getAppUpdateInfoMonitor,AppUpdate.getLatestVersionPublic,AppUpdate.getLatestVersionEnumerator,AppUpdate.getLatestVersionMonitor	7
1	User.getLogin,User.getLoginMobile,User.getLogoutMobile,User.getAccessPolicyMobile,Scheme.getStateSchemes,Scheme.getstatesList,Scheme.getFormFields,Scheme.getFormFieldsHH,Scheme.getSchemeSearch,Scheme.getFormFields_,Scheme.getFormFieldsHH_,Scheme.getSchemeDeatils,Scheme.getAllStateSchemesInd,Scheme.postEligibilityCheckCounter,Scheme.getEligibilityCheckCounter,Sync.getSyncTable,Sync.postSyncTable,Sync.getClassAndAction,Sync.getUserAllocatedRegionsPolicy,Sync.getRegionHierarchy,Reports.getSECCReports,Reports.getAvailableSchemes,Reports.getPrintAvailableSchemes,Contact.getJSLPS,Contact.getClassAndAction,Contact.getUserAllocatedRegionsPolicy,Contact.getRegionHierarchy,Contact.getPDF,AppUpdate.getAppUpdateInfoPublic,AppUpdate.getAppUpdateInfoEnumerator,AppUpdate.getAppUpdateInfoMonitor,AppUpdate.getLatestVersionPublic,AppUpdate.getLatestVersionEnumerator,AppUpdate.getLatestVersionMonitor	5
18	Scheme.getStateSchemes,Scheme.getstatesList,Scheme.getFormFields,Scheme.getFormFieldsHH,Scheme.getSchemeSearch,Scheme.getFormFields_,Scheme.getFormFieldsHH_,Scheme.getSchemeDeatils,Scheme.getNewSchemeEntry,Scheme.getAllStateSchemesInd,Scheme.getAttributeExpression,Scheme.postNewScheme,Scheme.getSchemes,Scheme.getUpdateSchemeEntry,Scheme.postUpdateScheme,Scheme.getAttributes,Scheme.getNewAttributeEntry,Scheme.postNewAttribute,Scheme.getUpdateAttributeEntry,Scheme.postUpdateAttribute,Scheme.postSchemeTranslation,Scheme.getLangSchemeEntry,Scheme.getSchemeLanguageData,Scheme.getSubjectHeads,Scheme.getNewSubjectHeadEntry,Scheme.postNewSubjectHead,Scheme.getUpdateSubjectHeadEntry,Scheme.postUpdateSubjectHead,Scheme.getUpdateEndDate,Scheme.postUpdateEndDate,Scheme.getLangAttributeEntry,Scheme.postAttributeTranslation,Scheme.getClassAndAction,Scheme.getUserAllocatedRegionsPolicy,Scheme.getRegionHierarchy,Scheme.getPDF	10
16	DataEntry.getNewEntry,DataEntry.getNewHHEntry,DataEntry.getHHMembers,DataEntry.postNewEntry,DataEntry.getIdentityFields,DataEntry.getVerification,DataEntry.postVerification,DataEntry.getAllEntries,DataEntry.getAllSECCEntries,DataEntry.getSECCEntryVerify,DataEntry.getSECCEntryView,DataEntry.postSECCEntry,DataEntry.getEntitlementCard,DataEntry.getAllIndividuals,DataEntry.getAllHH,DataEntry.getIndividualsEntry,DataEntry.getHHEntry,DataEntry.postIndividualsEntry,DataEntry.postHHEntry,DataEntry.getEntitlementTracking,DataEntry.getEntitlementTrackingHH,DataEntry.getSchemesListInd,DataEntry.getSchemesListHH,DataEntry.getSchemesListIndTracking,DataEntry.getSchemesListHHTracking,DataEntry.getSchemesListAppliedInd,DataEntry.getSchemesListAvailedInd,DataEntry.getSchemesListAppliedHH,DataEntry.getSchemesListAvailedHH,DataEntry.getCheckEligibilityEC,DataEntry.getCheckEligibilityHH,DataEntry.postEntitlementTracking,DataEntry.postEntitlementTrackingHH,DataEntry.getGrievanceForm,DataEntry.getGrievanceFormHH,DataEntry.postGrievanceForm,DataEntry.postGrievanceFormHH,DataEntry.getAllGrievances,DataEntry.getAllGrievancesHH,DataEntry.getGrievanceEntryView,DataEntry.getGrievanceEntryViewHH,DataEntry.getGrievanceEntryUpdate,DataEntry.getGrievanceEntryUpdateHH,DataEntry.postGrievanceEntryUpdate,DataEntry.postGrievanceEntryUpdateHH,DataEntry.getEntitlementRegister,DataEntry.getEntitlementRegisterHH,DataEntry.getDeathRegistrations,DataEntry.getDeathRegistration,DataEntry.getIndDetails,DataEntry.postDeathRegistration,DataEntry.getDeathDetails,DataEntry.getHHSplit,DataEntry.getIndividualsListHH,DataEntry.postHHSplit,DataEntry.getDownloadIndData,DataEntry.getDownloadHHData,DataEntry.getClassAndAction,DataEntry.getUserAllocatedRegionsPolicy,DataEntry.getRegionHierarchy,DataEntry.getPDF,User.getUserRelations,User.getLogin,User.getLoginMobile,User.getLogoutMobile,User.getAccessPolicyMobile,User.getAdminUsers,User.getAdminUserUpdate,User.getAdminDashboard,User.getAccessPolicy,User.getNewAdminUser,User.postNewUser,User.postUpdateUser,User.getNewRelations,User.getLogout,User.getUserEmail,User.getUserId,User.getRoles,User.getNewRole,User.updateRole,User.postNewRole,User.postUpdateRole,User.getGroups,User.updateGroup,User.postNewGroup,User.postUpdateGroup,User.getResetPassword,User.postResetPassword,User.getChangePassword,User.postChangePassword,User.getLocationEntries,User.getUserAllocationsList,User.getSelectedRegionsList,User.getPageCategories,User.getPageCategory,User.postUpdatePageCategory,User.getNewCategory,User.postPageCategory,User.getAllPages,User.getPage,User.postUpdatePage,User.getNewPage,User.postPage,User.getClassAndAction,User.getUserAllocatedRegionsPolicy,User.getRegionHierarchy,User.getPDF,Scheme.getStateSchemes,Scheme.getstatesList,Scheme.getFormFields,Scheme.getFormFieldsHH,Scheme.getSchemeSearch,Scheme.getFormFields_,Scheme.getFormFieldsHH_,Scheme.getSchemeDeatils,Scheme.getNewSchemeEntry,Scheme.getAllStateSchemesInd,Scheme.getAttributeExpression,Scheme.postNewScheme,Scheme.getSchemes,Scheme.getUpdateSchemeEntry,Scheme.postUpdateScheme,Scheme.getAttributes,Scheme.getNewAttributeEntry,Scheme.postNewAttribute,Scheme.getUpdateAttributeEntry,Scheme.postUpdateAttribute,Scheme.postSchemeTranslation,Scheme.getLangSchemeEntry,Scheme.getSchemeLanguageData,Scheme.getSubjectHeads,Scheme.getNewSubjectHeadEntry,Scheme.postNewSubjectHead,Scheme.getUpdateSubjectHeadEntry,Scheme.postUpdateSubjectHead,Scheme.getUpdateEndDate,Scheme.postUpdateEndDate,Scheme.getLangAttributeEntry,Scheme.postAttributeTranslation,Scheme.postEligibilityCheckCounter,Scheme.getEligibilityCheckCounter,Scheme.getClassAndAction,Scheme.getUserAllocatedRegionsPolicy,Scheme.getRegionHierarchy,Scheme.getPDF,Sync.getSyncTable,Sync.postSyncTableTest,Sync.postSyncTable,Sync.getClassAndAction,Sync.getUserAllocatedRegionsPolicy,Sync.getRegionHierarchy,Sync.getPDF,Reports.getSECCDashboard,Reports.getSECCReports,Reports.getSECCDashboardImage,Reports.getGISDashboard,Reports.getLayerCategories,Reports.getLayerSecondaryCategories,Reports.getLayerTertiaryCategories,Reports.getLayerTertiaryCategoryLayers,Reports.getLayerInfo,Reports.getSECCGISDashboard,Reports.getTrackingGISDashboard,Reports.getIndividualReports,Reports.getAvailableSchemes,Reports.getSelectedRegionsList,Reports.getStateSchemes,Reports.getPrintAvailableSchemes,Reports.getLocationReports,Reports.getGrievanceReports,Reports.getGrievanceReport,Reports.getLayerLatLon,Reports.getAllSchemeDetails,Reports.getEligibleHHlist,Reports.getEligibleIndlist,Reports.getAutoInclusionHH,Reports.getDeprivationHH,Reports.getSchemeWiseIndEligibility,Reports.getIndTrackingList,Reports.getHHTrackingList,Reports.getEnumeratorReport,Reports.getDashboard,Reports.getLoginHistory,Reports.getClassAndAction,Reports.getUserAllocatedRegionsPolicy,Reports.getRegionHierarchy,Reports.getPDF,Contact.getJSLPS,Contact.getClassAndAction,Contact.getUserAllocatedRegionsPolicy,Contact.getRegionHierarchy,Contact.getPDF,AppUpdate.getAppUpdateInfoPublic,AppUpdate.getAppUpdateInfoEnumerator,AppUpdate.getAppUpdateInfoMonitor,AppUpdate.getLatestVersionPublic,AppUpdate.getLatestVersionEnumerator,AppUpdate.getLatestVersionMonitor,AppUpdate.getClassAndAction,AppUpdate.getUserAllocatedRegionsPolicy,AppUpdate.getRegionHierarchy,AppUpdate.getPDF	8
\.


--
-- Data for Name: app_version; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.app_version (id, version, release_date, path, create_by, stable_version, dev_version, in_use, is_public, is_enumerator, is_monitor) FROM stdin;
\.


--
-- Data for Name: attributes_master; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.attributes_master (id, name, data_type, ui_element, for_hh, for_ind, start_date, end_date, create_by, update_by, create_date, update_date, possible_values, system_attr, weight) FROM stdin;
1	Area claimed	string	textfield	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
2	state	string	textfield	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
3	district	string	textfield	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	3
4	village	string	textfield	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
\.


--
-- Data for Name: attributes_master_lang_relation; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.attributes_master_lang_relation (f_id, srno, l_id, name, possible_values, create_by, update_by, create_date, update_date) FROM stdin;
\.


--
-- Data for Name: beat_master; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beat_master (name, create_by, geom, id, parent_id, "timestamp") FROM stdin;
\.


--
-- Data for Name: block_master; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.block_master (create_by, parent_id, name, id, geom, "timestamp") FROM stdin;
\.


--
-- Data for Name: circle_master; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.circle_master (id, parent_id, name, create_by, geom, "timestamp") FROM stdin;
\.


--
-- Data for Name: claim; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.claim (cl_id, g_id, status, create_by, itern, "timestamp", id) FROM stdin;
8	8	\N	akash	\N	2018-04-02 12:00:00	8
1	1	\N	akash	\N	2018-04-02 12:00:00	1
2	2	\N	akash	\N	2019-08-02 12:00:00	2
3	3	\N	akash	\N	2018-03-01 12:00:00	3
4	4	\N	akash	\N	2019-05-03 12:00:00	4
5	5	\N	akash	\N	2019-06-01 12:00:00	5
6	6	\N	akash	\N	2019-05-22 12:00:00	6
7	7	\N	akash	\N	2019-07-23 12:00:00	7
\.


--
-- Data for Name: claimant; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.claimant (c_id, name, create_by, "timestamp") FROM stdin;
1	akash	akash	\N
2	વસાવા સતીષભાઈ પારસીંગભાઈ	akash	\N
3	વસાવા જેઠાભાઈ પોહનાભાઈ	akash	\N
4	Rameshwar, H/o Piralibai, S/o Bhaku, Paharikorwa	akash	\N
5	Beera, H/o Sitiyobai, S/o Somaru, Paharikorwa	akash	\N
6	Ramlal, H/o Munnibai, S/o Amarsai, Paharikorwa	akash	\N
7	Adhinsai, H/o Khasaribai, S/o Budhu, Paharikorwa	akash	\N
8	Hajariram, H/o Sukamani, S/o Balakram, Paharikorwa	akash	\N
\.


--
-- Data for Name: compartment_master; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.compartment_master (name, create_by, geom, id, parent_id, "timestamp") FROM stdin;
\.


--
-- Data for Name: district_master; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.district_master (mc, state_code, name, create_by, update_by, create_date, update_date, start_date, end_date, geom, hindi_name) FROM stdin;
\.


--
-- Data for Name: division_master; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.division_master (id, name, create_by, parent_id, geom, "timestamp") FROM stdin;
\.


--
-- Data for Name: error_info; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.error_info (er_id, er_discription, start_date, end_date) FROM stdin;
\.


--
-- Data for Name: form_data; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.form_data (id, f_id, att_id, answer, create_by, "timestamp", grp_id, claim_id, claimant_id) FROM stdin;
1	1	1	45 hectare	akash	\N	\N	1	1
4	1	4	Hadgud	akash	\N	\N	1	1
3	1	3	Anand	akash	\N	\N	1	1
2	1	2	Gujarat	akash	\N	\N	1	1
\.


--
-- Data for Name: form_master; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.form_master (f_id, q_id, create_by, "timestamp") FROM stdin;
\.


--
-- Data for Name: geolocations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.geolocations (id, c_id, declatitude, declongitude, decaltitude, decaccuracy, dtideleted) FROM stdin;
1	1	23.0000000	45.0000000	6.0000000	7.0000000	\N
2	1	23.5000000	45.5000000	6.0000000	7.0000000	\N
3	1	24.0000000	46.0000000	6.0000000	7.0000000	\N
4	1	24.5000000	46.6000000	6.0000000	7.0000000	\N
5	1	25.0000000	46.9000000	6.0000000	7.0000000	\N
6	1	25.5000000	47.0000000	6.0000000	7.0000000	\N
7	1	26.0000000	47.2000000	6.0000000	7.0000000	\N
8	1	26.5000000	47.5000000	6.0000000	7.0000000	\N
19	1	22.5338260	72.9872390	-22.0000000	6.0000000	\N
20	1	22.5338440	72.9872650	-22.0000000	5.0000000	\N
21	1	22.5338470	72.9872700	-22.0000000	5.0000000	\N
22	1	22.5338400	72.9873020	-23.0000000	4.0000000	\N
23	1	22.5338560	72.9873290	-23.0000000	4.0000000	\N
24	1	22.5338690	72.9873680	-24.0000000	5.0000000	\N
25	1	22.5338860	72.9874050	-25.0000000	5.0000000	\N
26	1	22.5338930	72.9874470	-25.0000000	4.0000000	\N
27	1	22.5339020	72.9874840	-25.0000000	4.0000000	\N
28	1	22.5339150	72.9875220	-25.0000000	5.0000000	\N
29	1	22.5339250	72.9875590	-26.0000000	5.0000000	\N
30	1	22.5339350	72.9875960	-26.0000000	5.0000000	\N
31	1	22.5339400	72.9876280	-27.0000000	5.0000000	\N
32	1	22.5339600	72.9876690	-27.0000000	4.0000000	\N
33	1	22.5339730	72.9876980	-28.0000000	5.0000000	\N
34	1	22.5339760	72.9877440	-29.0000000	5.0000000	\N
35	1	22.5339730	72.9877650	-29.0000000	5.0000000	\N
36	1	22.5339850	72.9878070	-29.0000000	5.0000000	\N
37	1	22.5339820	72.9878490	-30.0000000	5.0000000	\N
38	1	22.5339630	72.9878710	-29.0000000	4.0000000	\N
39	1	22.5339380	72.9878960	-26.0000000	4.0000000	\N
40	1	22.5339050	72.9879180	-26.0000000	5.0000000	\N
41	1	22.5338580	72.9879400	-24.0000000	4.0000000	\N
42	1	22.5338150	72.9879620	-23.0000000	4.0000000	\N
43	1	22.5337720	72.9879810	-22.0000000	5.0000000	\N
44	1	22.5337350	72.9879810	-21.0000000	4.0000000	\N
45	1	22.5337320	72.9879410	-20.0000000	4.0000000	\N
46	1	22.5337140	72.9879030	-19.0000000	4.0000000	\N
47	1	22.5336960	72.9878610	-20.0000000	4.0000000	\N
48	1	22.5336860	72.9878210	-19.0000000	4.0000000	\N
49	1	22.5336820	72.9877910	-19.0000000	3.0000000	\N
50	1	22.5336980	72.9877690	-25.0000000	18.0000000	\N
51	1	22.5336470	72.9877190	-16.0000000	6.0000000	\N
52	1	22.5336470	72.9877190	-16.0000000	5.0000000	\N
53	1	22.5336470	72.9877190	-16.0000000	4.0000000	\N
54	1	22.5336470	72.9877190	-16.0000000	3.0000000	\N
55	1	22.5336470	72.9877190	-16.0000000	3.0000000	\N
56	1	22.5336470	72.9877190	-16.0000000	4.0000000	\N
57	1	22.5336470	72.9877190	-16.0000000	5.0000000	\N
58	1	22.5336470	72.9877190	-16.0000000	4.0000000	\N
59	1	22.5336470	72.9877190	-16.0000000	5.0000000	\N
60	1	22.5336470	72.9877190	-16.0000000	5.0000000	\N
61	1	22.5336470	72.9877190	-16.0000000	5.0000000	\N
62	1	22.5336470	72.9877190	-16.0000000	5.0000000	\N
63	1	22.5336470	72.9877190	-16.0000000	5.0000000	\N
64	1	22.5336470	72.9877190	-16.0000000	5.0000000	\N
65	1	22.5336470	72.9877190	-16.0000000	5.0000000	\N
66	1	22.5336470	72.9877190	-16.0000000	5.0000000	\N
67	1	22.5336470	72.9877190	-16.0000000	5.0000000	\N
68	1	22.5336470	72.9877190	-16.0000000	5.0000000	\N
69	1	22.5336470	72.9877190	-16.0000000	5.0000000	\N
70	1	22.5336470	72.9877190	-16.0000000	5.0000000	\N
71	1	22.5336470	72.9877190	-16.0000000	5.0000000	\N
72	1	22.5336470	72.9877190	-16.0000000	5.0000000	\N
73	1	22.5336470	72.9877190	-16.0000000	5.0000000	\N
74	1	22.5336470	72.9877190	-16.0000000	5.0000000	\N
75	1	22.5336470	72.9877190	-16.0000000	5.0000000	\N
76	1	22.5336470	72.9877190	-16.0000000	5.0000000	\N
77	1	22.5336470	72.9877190	-16.0000000	5.0000000	\N
78	1	22.5336270	72.9876440	-17.0000000	5.0000000	\N
79	1	22.5336440	72.9875870	-18.0000000	5.0000000	\N
80	1	22.5336590	72.9875450	-18.0000000	4.0000000	\N
81	1	22.5336480	72.9874130	-18.0000000	4.0000000	\N
82	1	22.5336600	72.9873410	-16.0000000	3.0000000	\N
83	1	22.5336860	72.9872640	-18.0000000	3.0000000	\N
84	1	22.5337320	72.9872420	-20.0000000	3.0000000	\N
85	1	22.5337640	72.9872420	-20.0000000	4.0000000	\N
86	1	22.5338260	72.9872390	-22.0000000	6.0000000	\N
87	1	22.5339766	72.9869543	0.0000000	31.3200000	\N
88	1	22.5339850	72.9869620	0.0000000	23.8320000	\N
89	1	22.5339850	72.9869620	0.0000000	23.8320000	\N
90	1	22.5339850	72.9869620	0.0000000	23.8320000	\N
91	1	22.5339850	72.9869620	0.0000000	23.8320000	\N
92	1	22.5339850	72.9869620	0.0000000	23.8320000	\N
93	1	22.5339850	72.9869620	0.0000000	23.8320000	\N
94	1	22.5339850	72.9869620	0.0000000	23.8320000	\N
95	1	22.5339850	72.9869620	0.0000000	23.8320000	\N
96	1	22.5339850	72.9869620	0.0000000	23.8320000	\N
97	1	22.5339850	72.9869620	0.0000000	23.8320000	\N
98	1	22.5339850	72.9869620	0.0000000	23.8320000	\N
99	1	22.5339850	72.9869620	0.0000000	23.8320000	\N
100	1	22.5339850	72.9869620	0.0000000	23.8320000	\N
101	1	22.5339850	72.9869620	0.0000000	23.8320000	\N
102	1	22.5339850	72.9869620	0.0000000	23.8320000	\N
103	1	22.5339770	72.9869544	0.0000000	26.5700000	\N
104	1	22.5339570	72.9869640	0.0000000	23.2770000	\N
105	1	22.5339570	72.9869640	0.0000000	23.2770000	\N
106	1	22.5339570	72.9869640	0.0000000	23.2770000	\N
107	1	22.5339570	72.9869640	0.0000000	23.2770000	\N
108	1	22.5339570	72.9869640	0.0000000	23.2770000	\N
109	1	22.5339570	72.9869640	0.0000000	23.2770000	\N
110	1	22.5339570	72.9869640	0.0000000	23.2770000	\N
111	1	22.5339602	72.9869580	0.0000000	21.0700000	\N
112	1	22.5339577	72.9869666	0.0000000	25.5100000	\N
113	1	22.5347409	72.9870418	-53.5700000	10.0000000	\N
114	1	22.5340085	72.9869724	0.0000000	18.8400000	\N
115	1	22.5332950	72.9860520	-19.0000000	12.0000000	\N
116	1	22.5332930	72.9860520	-18.0000000	12.0000000	\N
117	1	22.5332970	72.9860510	-18.0000000	8.0000000	\N
118	1	22.5333090	72.9860050	-17.0000000	8.0000000	\N
119	1	22.5333140	72.9859620	-17.0000000	6.0000000	\N
120	1	22.5333360	72.9859450	-16.0000000	6.0000000	\N
121	1	22.5333780	72.9859380	-16.0000000	6.0000000	\N
122	1	22.5334300	72.9859330	-15.0000000	6.0000000	\N
123	1	22.5334260	72.9859740	-15.0000000	4.0000000	\N
124	1	22.5334130	72.9860110	-15.0000000	8.0000000	\N
125	1	22.5333810	72.9860320	-15.0000000	6.0000000	\N
126	1	22.5333400	72.9860610	-16.0000000	6.0000000	\N
127	1	22.5333200	72.9860850	-17.0000000	8.0000000	\N
128	1	22.5332950	72.9860520	-19.0000000	12.0000000	\N
129	1	22.5339443	72.9869620	0.0000000	19.8900000	\N
130	1	22.5339438	72.9869613	0.0000000	18.1800000	\N
131	1	22.5339441	72.9869620	0.0000000	24.1700000	\N
132	1	22.5340180	72.9869320	-14.0000000	23.5670000	\N
133	1	22.5344340	72.9875200	0.0000000	23.5670000	\N
134	1	22.5351000	72.9877350	0.0000000	23.5670000	\N
135	1	22.5356010	72.9875390	0.0000000	23.5780000	\N
136	1	22.5358930	72.9873430	0.0000000	23.5780000	\N
137	1	22.5340170	72.9869320	-14.0000000	23.6040000	\N
138	1	22.5340180	72.9869320	-14.0000000	23.5670000	\N
139	1	22.5339417	72.9869610	0.0000000	24.0100000	\N
140	1	22.5340162	72.9869320	-14.1000000	21.8000000	\N
141	1	22.5345330	72.9866200	-13.0000000	3.2160000	\N
142	1	22.5343320	72.9866310	-17.0000000	9.6480010	\N
143	1	22.5340490	72.9866870	-16.0000000	3.2160000	\N
144	1	22.5337670	72.9867380	-18.0000000	3.2160000	\N
145	1	22.5335640	72.9867800	-15.0000000	3.2160000	\N
146	1	22.5334980	72.9865340	-13.0000000	9.6480010	\N
147	1	22.5334140	72.9862020	-15.0000000	9.6480010	\N
148	1	22.5379020	72.9812970	0.0000000	23.1890000	\N
149	1	22.5384870	72.9831270	0.0000000	23.4700000	\N
150	1	22.5397750	72.9826060	0.0000000	23.4490000	\N
151	1	22.5392890	72.9810130	0.0000000	23.5980000	\N
152	1	22.5379070	72.9812440	0.0000000	23.4530000	\N
153	1	22.5340530	72.9872100	-3.0000000	4.0000000	\N
154	1	22.5339610	72.9871650	-3.0000000	3.0000000	\N
155	1	22.5340130	72.9870760	-3.0000000	3.0000000	\N
156	1	22.5341840	72.9862300	0.0000000	6.0000000	\N
157	1	22.5340820	72.9861630	0.0000000	3.0000000	\N
158	1	22.5337200	72.9861930	0.0000000	4.0000000	\N
159	1	22.5337280	72.9866060	0.0000000	3.0000000	\N
160	1	22.5339800	72.9864580	0.0000000	4.0000000	\N
161	1	22.5340510	72.9865380	9.0000000	4.0000000	\N
162	1	22.5340890	72.9867500	0.0000000	4.0000000	\N
163	1	22.5340230	72.9860740	0.0000000	4.0000000	\N
164	1	22.5337650	72.9861420	0.0000000	4.0000000	\N
165	1	22.5337920	72.9865360	0.0000000	2.0000000	\N
166	1	22.5340400	72.9865880	-9.0000000	4.0000000	\N
167	1	22.5363640	72.9858630	0.0000000	1.9000000	\N
168	1	22.5363090	72.9856080	0.0000000	1.9000000	\N
169	1	22.5357740	72.9855730	0.0000000	1.9000000	\N
170	1	22.5353430	72.9856370	0.0000000	1.9000000	\N
171	1	22.5343590	72.9857280	0.0000000	1.9000000	\N
172	1	22.5340510	72.9865800	44.0000000	1.8000000	\N
173	1	22.5350660	72.9869940	0.0000000	1.8000000	\N
174	1	22.5358140	72.9859860	0.0000000	1.8000000	\N
175	1	22.5356820	72.9864040	0.0000000	1.1000000	\N
176	1	22.5337250	72.9866190	0.0000000	48.0000000	\N
177	1	22.5337250	72.9862380	0.0000000	48.0000000	\N
178	1	22.5341020	72.9862060	0.0000000	16.0000000	\N
179	1	22.5339980	72.9869240	-42.0000000	24.0000000	\N
180	1	22.5337230	72.9866150	0.0000000	24.0000000	\N
181	1	22.5337250	72.9866190	0.0000000	48.0000000	\N
182	1	22.5347321	72.9870449	25.1700000	51.5100000	\N
183	1	22.5339758	72.9869840	0.0000000	29.8600000	\N
184	1	22.5337625	72.9866934	-135.6900000	22.3700000	\N
185	1	25.4624840	74.8844320	0.0000000	4.0000000	\N
186	1	25.4582030	74.8786060	0.0000000	4.0000000	\N
187	1	25.4585900	74.8827580	0.0000000	4.0000000	\N
188	1	25.4625230	74.8843680	0.0000000	3.0000000	\N
189	1	22.5341507	72.9870603	-36.8600000	39.8500000	\N
190	1	22.5339882	72.9869621	-14.0000000	24.2900000	\N
191	1	22.5339856	72.9869618	-14.0000000	210.9600000	\N
192	1	22.5339812	72.9869738	-14.0000000	24.8000000	\N
193	1	22.5339789	72.9869740	-14.0000000	25.8300000	\N
194	1	22.5339842	72.9869619	-14.0000000	25.1500000	\N
195	1	22.5337378	72.9869765	-61.9500000	62.5200000	\N
196	1	22.5344680	72.9862090	0.0000000	23.2480000	\N
197	1	22.5342680	72.9862490	0.0000000	23.2480000	\N
198	1	22.5341040	72.9862620	0.0000000	23.4060000	\N
199	1	22.5341340	72.9865490	0.0000000	23.4060000	\N
200	1	22.5343150	72.9865220	0.0000000	23.3610000	\N
201	1	22.5345010	72.9864770	0.0000000	23.3200000	\N
202	1	22.5344710	72.9861740	0.0000000	23.3410000	\N
203	1	22.5336610	72.9871900	0.0000000	23.1890000	\N
204	1	22.5334950	72.9872010	0.0000000	23.1890000	\N
205	1	22.5333240	72.9871960	0.0000000	23.1150000	\N
206	1	22.5335370	72.9880750	0.0000000	23.3500000	\N
207	1	22.5337300	72.9880080	0.0000000	23.3000000	\N
208	1	22.5337750	72.9879360	0.0000000	23.5260000	\N
209	1	22.5339510	72.9879200	0.0000000	23.4050000	\N
210	1	22.5338740	72.9875820	0.0000000	23.4830000	\N
211	1	22.5338270	72.9873750	0.0000000	23.3180000	\N
212	1	22.5338270	72.9872440	0.0000000	23.2790000	\N
213	1	22.5337850	72.9871660	0.0000000	23.2380000	\N
214	1	22.5336460	72.9870750	0.0000000	23.3310000	\N
215	1	22.5336680	72.9871880	0.0000000	23.1500000	\N
216	1	22.5338510	72.9862150	-19.0000000	8.0000000	\N
217	1	22.5338690	72.9863270	0.0000000	6.0000000	\N
218	1	22.5337970	72.9863230	-20.0000000	12.0000000	\N
219	1	22.5338050	72.9861920	-18.0000000	8.0000000	\N
220	1	22.5338290	72.9862080	-19.0000000	6.0000000	\N
221	1	22.5338290	72.9862080	-19.0000000	6.0000000	\N
222	1	22.5338200	72.9863040	-16.0000000	4.0000000	\N
223	1	22.5338250	72.9863820	-17.0000000	4.0000000	\N
224	1	22.5339340	72.9863610	-14.0000000	6.0000000	\N
225	1	22.5339260	72.9862890	0.0000000	6.0000000	\N
226	1	22.5338040	72.9863100	-17.0000000	4.0000000	\N
227	1	22.5338050	72.9863110	0.0000000	4.0000000	\N
228	1	25.4735950	74.8203490	0.0000000	23.3450000	\N
229	1	25.4722970	74.8211430	0.0000000	23.3450000	\N
230	1	25.4728580	74.8235460	0.0000000	23.3450000	\N
231	1	25.4743690	74.8242970	0.0000000	23.3450000	\N
232	1	25.4747570	74.8263350	0.0000000	23.3450000	\N
233	1	25.4765970	74.8273010	0.0000000	23.3450000	\N
234	1	25.4786120	74.8248330	0.0000000	23.3450000	\N
235	1	25.4804330	74.8265930	0.0000000	23.3450000	\N
236	1	25.4836870	74.8266790	0.0000000	23.3450000	\N
237	1	25.4839580	74.8244040	0.0000000	23.3450000	\N
238	1	25.4843840	74.8234600	0.0000000	23.3450000	\N
239	1	25.4836670	74.8229240	0.0000000	23.3450000	\N
240	1	25.4824470	74.8214220	0.0000000	23.3450000	\N
241	1	25.4818470	74.8197910	0.0000000	23.3450000	\N
242	1	25.4806260	74.8180100	0.0000000	23.3450000	\N
243	1	25.4781470	74.8171730	0.0000000	23.3450000	\N
244	1	25.4772940	74.8180960	0.0000000	23.3450000	\N
245	1	25.4761130	74.8188680	0.0000000	23.3450000	\N
246	1	25.4747570	74.8195980	0.0000000	23.3450000	\N
247	1	25.4735170	74.8203700	0.0000000	23.3450000	\N
248	1	22.5339280	72.9868170	0.0000000	15.0000000	\N
249	1	22.5338390	72.9868150	0.0000000	25.0000000	\N
250	1	22.5338620	72.9870430	0.0000000	10.0000000	\N
251	1	22.5341840	72.9870050	0.0000000	10.0000000	\N
252	1	22.5341790	72.9868980	0.0000000	20.0000000	\N
253	1	22.5339530	72.9869300	0.0000000	10.0000000	\N
254	1	22.5339280	72.9868170	0.0000000	25.0000000	\N
255	1	22.5338170	72.9872310	0.0000000	20.0000000	\N
256	1	22.5336560	72.9871930	0.0000000	20.0000000	\N
257	1	22.5335000	72.9872090	0.0000000	20.0000000	\N
258	1	22.5335910	72.9875580	0.0000000	20.0000000	\N
259	1	22.5336860	72.9879760	0.0000000	20.0000000	\N
260	1	22.5336980	72.9880000	0.0000000	20.0000000	\N
261	1	22.5339660	72.9879090	0.0000000	20.0000000	\N
262	1	22.5338220	72.9872330	0.0000000	20.0000000	\N
263	1	22.5338170	72.9872310	0.0000000	20.0000000	\N
264	1	22.5335150	72.9866140	0.0000000	25.0000000	\N
265	1	22.5336860	72.9865840	0.0000000	20.0000000	\N
266	1	22.5336660	72.9863560	0.0000000	25.0000000	\N
267	1	22.5335170	72.9863750	0.0000000	10.0000000	\N
268	1	22.5335170	72.9864610	0.0000000	20.0000000	\N
269	1	22.5335300	72.9866220	0.0000000	25.0000000	\N
270	1	22.5338440	72.9864200	-11.0000000	6.0000000	\N
271	1	22.5338440	72.9864200	-11.0000000	4.0000000	\N
272	1	22.5338440	72.9864200	-11.0000000	4.0000000	\N
273	1	22.5338440	72.9864200	-11.0000000	4.0000000	\N
274	1	22.5338440	72.9864200	-11.0000000	6.0000000	\N
275	1	22.5338440	72.9864200	-11.0000000	6.0000000	\N
276	1	22.5338440	72.9864200	-11.0000000	6.0000000	\N
277	1	22.5338440	72.9864200	-11.0000000	6.0000000	\N
278	1	22.5338440	72.9864200	-11.0000000	6.0000000	\N
279	1	22.5338440	72.9864200	-11.0000000	4.0000000	\N
280	1	22.5338440	72.9864200	-11.0000000	4.0000000	\N
281	1	22.5338440	72.9864200	-11.0000000	4.0000000	\N
282	1	22.5338440	72.9864200	-11.0000000	4.0000000	\N
283	1	22.5338440	72.9864200	-11.0000000	6.0000000	\N
284	1	22.5338440	72.9864200	-11.0000000	6.0000000	\N
285	1	22.5338440	72.9864200	-11.0000000	8.0000000	\N
286	1	22.5338440	72.9864200	-11.0000000	4.0000000	\N
287	1	22.5338440	72.9864200	-11.0000000	4.0000000	\N
288	1	22.5338440	72.9864200	-11.0000000	4.0000000	\N
289	1	22.5338440	72.9864200	-10.0000000	4.0000000	\N
290	1	22.5338920	72.9864090	-10.0000000	4.0000000	\N
291	1	22.5339050	72.9864030	-10.0000000	6.0000000	\N
292	1	22.5339190	72.9863990	-10.0000000	6.0000000	\N
293	1	22.5339240	72.9863980	-10.0000000	6.0000000	\N
294	1	22.5339240	72.9863980	-10.0000000	6.0000000	\N
295	1	22.5339240	72.9863980	-10.0000000	8.0000000	\N
296	1	22.5339380	72.9864010	-10.0000000	6.0000000	\N
297	1	22.5339380	72.9864010	-10.0000000	6.0000000	\N
298	1	22.5339380	72.9864010	-10.0000000	6.0000000	\N
299	1	22.5339380	72.9864010	-10.0000000	6.0000000	\N
300	1	22.5339380	72.9864010	-10.0000000	4.0000000	\N
\.


--
-- Data for Name: gp_master; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.gp_master (state, district, tehsil, name, state_code, district_code, tehsil_code, mc, gp_code) FROM stdin;
Jharkhand	Gumla	Albert Ekka(Jari)	JARDA	912000000000000000	912036600000000000	912036602715000000	912036602715113251	113251
Jharkhand	Gumla	Albert Ekka(Jari)	GOVINDPUR	912000000000000000	912036600000000000	912036602715000000	912036602715113249	113249
Jharkhand	Gumla	Albert Ekka(Jari)	SIKRI	912000000000000000	912036600000000000	912036602715000000	912036602715113259	113259
Jharkhand	Gumla	Albert Ekka(Jari)	MERAL	912000000000000000	912036600000000000	912036602715000000	912036602715113256	113256
Jharkhand	Gumla	Albert Ekka(Jari)	SHISHI KARAMTOLI	912000000000000000	912036600000000000	912036602715000000	912036602715113258	113258
Jharkhand	Gumla	Basia	TETRA	912000000000000000	912036600000000000	912036602711000000	912036602711113213	113213
Jharkhand	Gumla	Basia	TURBUNGA	912000000000000000	912036600000000000	912036602711000000	912036602711113214	113214
Jharkhand	Gumla	Basia	MORENG	912000000000000000	912036600000000000	912036602711000000	912036602711113208	113208
Jharkhand	Gumla	Basia	ETAM	912000000000000000	912036600000000000	912036602711000000	912036602711113202	113202
Jharkhand	Gumla	Basia	AREYA	912000000000000000	912036600000000000	912036602711000000	912036602711113210	113210
Jharkhand	Gumla	Basia	KONBIR	912000000000000000	912036600000000000	912036602711000000	912036602711113204	113204
Jharkhand	Gumla	Basia	KALIGA	912000000000000000	912036600000000000	912036602711000000	912036602711113203	113203
Jharkhand	Gumla	Basia	BASIA	912000000000000000	912036600000000000	912036602711000000	912036602711113201	113201
Jharkhand	Gumla	Basia	PANTHA	912000000000000000	912036600000000000	912036602711000000	912036602711113211	113211
Jharkhand	Gumla	Basia	KUMHARI	912000000000000000	912036600000000000	912036602711000000	912036602711113205	113205
Jharkhand	Gumla	Basia	LUNGTU	912000000000000000	912036600000000000	912036602711000000	912036602711113206	113206
Jharkhand	Gumla	Basia	POKTA	912000000000000000	912036600000000000	912036602711000000	912036602711113212	113212
Jharkhand	Gumla	Basia	BANAI	912000000000000000	912036600000000000	912036602711000000	912036602711113200	113200
Jharkhand	Gumla	Basia	MAMRALA	912000000000000000	912036600000000000	912036602711000000	912036602711113207	113207
Jharkhand	Gumla	Basia	OKBA	912000000000000000	912036600000000000	912036602711000000	912036602711113209	113209
Jharkhand	Gumla	Bishunpur	GURDARI	912000000000000000	912036600000000000	912036602706000000	912036602706113232	113232
Jharkhand	Gumla	Bishunpur	HELTA	912000000000000000	912036600000000000	912036602706000000	912036602706113233	113233
Jharkhand	Gumla	Bishunpur	CHIRODIH	912000000000000000	912036600000000000	912036602706000000	912036602706113230	113230
Jharkhand	Gumla	Bishunpur	BISHUNPUR	912000000000000000	912036600000000000	912036602706000000	912036602706113229	113229
Jharkhand	Gumla	Bishunpur	AMTIPANI	912000000000000000	912036600000000000	912036602706000000	912036602706113227	113227
Jharkhand	Gumla	Bishunpur	SERKA	912000000000000000	912036600000000000	912036602706000000	912036602706113236	113236
Jharkhand	Gumla	Bishunpur	BANARI	912000000000000000	912036600000000000	912036602706000000	912036602706113228	113228
Jharkhand	Gumla	Bishunpur	NARMA	912000000000000000	912036600000000000	912036602706000000	912036602706113234	113234
Jharkhand	Gumla	Bishunpur	GHAGHRA	912000000000000000	912036600000000000	912036602706000000	912036602706113231	113231
Jharkhand	Gumla	Bishunpur	NIRASI	912000000000000000	912036600000000000	912036602706000000	912036602706113235	113235
Jharkhand	Gumla	Chainpur	BAMDA	912000000000000000	912036600000000000	912036602713000000	912036602713113237	113237
Jharkhand	Gumla	Chainpur	BARDIH	912000000000000000	912036600000000000	912036602713000000	912036602713113238	113238
Jharkhand	Gumla	Chainpur	KATING	912000000000000000	912036600000000000	912036602713000000	912036602713113244	113244
Jharkhand	Gumla	Chainpur	BENDORA	912000000000000000	912036600000000000	912036602713000000	912036602713113240	113240
Jharkhand	Gumla	Chainpur	RAMPUR	912000000000000000	912036600000000000	912036602713000000	912036602713113246	113246
Jharkhand	Gumla	Chainpur	MALAM	912000000000000000	912036600000000000	912036602713000000	912036602713113245	113245
Jharkhand	Gumla	Chainpur	CHAINPUR	912000000000000000	912036600000000000	912036602713000000	912036602713113241	113241
Jharkhand	Gumla	Chainpur	BARWENAGAR	912000000000000000	912036600000000000	912036602713000000	912036602713113239	113239
Jharkhand	Gumla	Chainpur	CHHICHHWANI	912000000000000000	912036600000000000	912036602713000000	912036602713113242	113242
Jharkhand	Gumla	Chainpur	JANAWAL	912000000000000000	912036600000000000	912036602713000000	912036602713113243	113243
Jharkhand	Gumla	Dumri	KHETALI	912000000000000000	912036600000000000	912036602714000000	912036602714113254	113254
Jharkhand	Gumla	Dumri	NAWADIH	912000000000000000	912036600000000000	912036602714000000	912036602714113257	113257
Jharkhand	Gumla	Dumri	KARNI	912000000000000000	912036600000000000	912036602714000000	912036602714113253	113253
Jharkhand	Gumla	Dumri	DUMRI	912000000000000000	912036600000000000	912036602714000000	912036602714113248	113248
Jharkhand	Gumla	Dumri	MAJHAGAON	912000000000000000	912036600000000000	912036602714000000	912036602714113255	113255
Jharkhand	Gumla	Dumri	JURMU	912000000000000000	912036600000000000	912036602714000000	912036602714113252	113252
Jharkhand	Gumla	Dumri	AKASI	912000000000000000	912036600000000000	912036602714000000	912036602714113247	113247
Jharkhand	Gumla	Dumri	JAIRAGI	912000000000000000	912036600000000000	912036602714000000	912036602714113250	113250
Jharkhand	Gumla	Dumri	UDANI	912000000000000000	912036600000000000	912036602714000000	912036602714113260	113260
Jharkhand	Gumla	Ghaghra	SEHAL	912000000000000000	912036600000000000	912036602707000000	912036602707113277	113277
Jharkhand	Gumla	Ghaghra	ARANGI	912000000000000000	912036600000000000	912036602707000000	912036602707113262	113262
Jharkhand	Gumla	Ghaghra	BELAGARA	912000000000000000	912036600000000000	912036602707000000	912036602707113264	113264
Jharkhand	Gumla	Ghaghra	KUHIPAT	912000000000000000	912036600000000000	912036602707000000	912036602707113273	113273
Jharkhand	Gumla	Ghaghra	SHIVRAJPUR	912000000000000000	912036600000000000	912036602707000000	912036602707113278	113278
Jharkhand	Gumla	Ghaghra	BIMARLA	912000000000000000	912036600000000000	912036602707000000	912036602707113265	113265
Jharkhand	Gumla	Ghaghra	CHAPKA	912000000000000000	912036600000000000	912036602707000000	912036602707113266	113266
Jharkhand	Gumla	Ghaghra	DIRGAON	912000000000000000	912036600000000000	912036602707000000	912036602707113269	113269
Jharkhand	Gumla	Ghaghra	DUKO	912000000000000000	912036600000000000	912036602707000000	912036602707113270	113270
Jharkhand	Gumla	Ghaghra	BADRI	912000000000000000	912036600000000000	912036602707000000	912036602707113263	113263
Jharkhand	Gumla	Ghaghra	RUKI	912000000000000000	912036600000000000	912036602707000000	912036602707113275	113275
Jharkhand	Gumla	Ghaghra	KUGAON	912000000000000000	912036600000000000	912036602707000000	912036602707113272	113272
Jharkhand	Gumla	Ghaghra	GHAGHRA	912000000000000000	912036600000000000	912036602707000000	912036602707113271	113271
Jharkhand	Gumla	Ghaghra	NAWDIHA	912000000000000000	912036600000000000	912036602707000000	912036602707113274	113274
Jharkhand	Gumla	Ghaghra	ADAR	912000000000000000	912036600000000000	912036602707000000	912036602707113261	113261
Jharkhand	Gumla	Ghaghra	DEVAKI	912000000000000000	912036600000000000	912036602707000000	912036602707113268	113268
Jharkhand	Gumla	Ghaghra	CHUNDARI	912000000000000000	912036600000000000	912036602707000000	912036602707113267	113267
Jharkhand	Gumla	Ghaghra	SARANGO	912000000000000000	912036600000000000	912036602707000000	912036602707113276	113276
Jharkhand	Gumla	Gumla	\N	912000000000000000	912036600000000000	912036602712000000	912036602712248057	248057
Jharkhand	Gumla	Gumla	TELGAON	912000000000000000	912036600000000000	912036602712000000	912036602712113302	113302
Jharkhand	Gumla	Gumla	MURKUNDA	912000000000000000	912036600000000000	912036602712000000	912036602712113298	113298
Jharkhand	Gumla	Gumla	BRINDA	912000000000000000	912036600000000000	912036602712000000	912036602712113284	113284
Jharkhand	Gumla	Gumla	KARAUNDI	912000000000000000	912036600000000000	912036602712000000	912036602712113290	113290
Jharkhand	Gumla	Gumla	KALIGA	912000000000000000	912036600000000000	912036602712000000	912036602712113289	113289
Jharkhand	Gumla	Gumla	AMBOA	912000000000000000	912036600000000000	912036602712000000	912036602712113279	113279
Jharkhand	Gumla	Gumla	KUMHARIYA	912000000000000000	912036600000000000	912036602712000000	912036602712113297	113297
Jharkhand	Gumla	Gumla	ARAMAI	912000000000000000	912036600000000000	912036602712000000	912036602712113281	113281
Jharkhand	Gumla	Gumla	KASIRA	912000000000000000	912036600000000000	912036602712000000	912036602712113291	113291
Jharkhand	Gumla	Gumla	FASIA	912000000000000000	912036600000000000	912036602712000000	912036602712113286	113286
Jharkhand	Gumla	Gumla	PUGU	912000000000000000	912036600000000000	912036602712000000	912036602712113300	113300
Jharkhand	Gumla	Gumla	KHORA	912000000000000000	912036600000000000	912036602712000000	912036602712113294	113294
Jharkhand	Gumla	Gumla	GHATGAON	912000000000000000	912036600000000000	912036602712000000	912036602712113288	113288
Jharkhand	Gumla	Gumla	KULABIRA	912000000000000000	912036600000000000	912036602712000000	912036602712113296	113296
Jharkhand	Gumla	Gumla	ASNI	912000000000000000	912036600000000000	912036602712000000	912036602712113282	113282
Jharkhand	Gumla	Gumla	SILAFARI	912000000000000000	912036600000000000	912036602712000000	912036602712113301	113301
Jharkhand	Gumla	Gumla	DUMARDIH	912000000000000000	912036600000000000	912036602712000000	912036602712113285	113285
Jharkhand	Gumla	Gumla	NAWADIH	912000000000000000	912036600000000000	912036602712000000	912036602712113299	113299
Jharkhand	Gumla	Gumla	TOTO	912000000000000000	912036600000000000	912036602712000000	912036602712113303	113303
Jharkhand	Gumla	Gumla	FORI	912000000000000000	912036600000000000	912036602712000000	912036602712113287	113287
Jharkhand	Gumla	Gumla	KHARKA	912000000000000000	912036600000000000	912036602712000000	912036602712113293	113293
Jharkhand	Gumla	Gumla	BASUWA	912000000000000000	912036600000000000	912036602712000000	912036602712113283	113283
Jharkhand	Gumla	Gumla	ANJAN	912000000000000000	912036600000000000	912036602712000000	912036602712113280	113280
Jharkhand	Gumla	Gumla	KOTAM	912000000000000000	912036600000000000	912036602712000000	912036602712113295	113295
Jharkhand	Gumla	Gumla	KATRI	912000000000000000	912036600000000000	912036602712000000	912036602712113292	113292
Jharkhand	Gumla	Kamdara	RAMPUR	912000000000000000	912036600000000000	912036602710000000	912036602710113307	113307
Jharkhand	Gumla	Kamdara	KULUBURU	912000000000000000	912036600000000000	912036602710000000	912036602710113306	113306
Jharkhand	Gumla	Kamdara	SARITA	912000000000000000	912036600000000000	912036602710000000	912036602710113311	113311
Jharkhand	Gumla	Kamdara	SALEGUTU	912000000000000000	912036600000000000	912036602710000000	912036602710113310	113310
Jharkhand	Gumla	Kamdara	SURHU	912000000000000000	912036600000000000	912036602710000000	912036602710113312	113312
Jharkhand	Gumla	Kamdara	TURUNDU	912000000000000000	912036600000000000	912036602710000000	912036602710113313	113313
Jharkhand	Gumla	Kamdara	KAMDARA	912000000000000000	912036600000000000	912036602710000000	912036602710113304	113304
Jharkhand	Gumla	Kamdara	RAMTOLYA	912000000000000000	912036600000000000	912036602710000000	912036602710113308	113308
Jharkhand	Gumla	Kamdara	KONSA	912000000000000000	912036600000000000	912036602710000000	912036602710113305	113305
Jharkhand	Gumla	Kamdara	RERWA	912000000000000000	912036600000000000	912036602710000000	912036602710113309	113309
Jharkhand	Gumla	Palkot	JHIKIRMA	912000000000000000	912036600000000000	912036602717000000	912036602717113319	113319
Jharkhand	Gumla	Palkot	BILINGBIRA	912000000000000000	912036600000000000	912036602717000000	912036602717113317	113317
Jharkhand	Gumla	Palkot	KULUKERA	912000000000000000	912036600000000000	912036602717000000	912036602717113321	113321
Jharkhand	Gumla	Palkot	TAPKARA	912000000000000000	912036600000000000	912036602717000000	912036602717113325	113325
Jharkhand	Gumla	Palkot	BANGRU	912000000000000000	912036600000000000	912036602717000000	912036602717113316	113316
Jharkhand	Gumla	Palkot	DAHUPANI	912000000000000000	912036600000000000	912036602717000000	912036602717113318	113318
Jharkhand	Gumla	Palkot	NATHPUR	912000000000000000	912036600000000000	912036602717000000	912036602717113322	113322
Jharkhand	Gumla	Palkot	UMRA	912000000000000000	912036600000000000	912036602717000000	912036602717113327	113327
Jharkhand	Gumla	Palkot	TENGARIYA	912000000000000000	912036600000000000	912036602717000000	912036602717113326	113326
Jharkhand	Gumla	Palkot	PALKOT NORTH	912000000000000000	912036600000000000	912036602717000000	912036602717113323	113323
Jharkhand	Gumla	Palkot	KOLENG	912000000000000000	912036600000000000	912036602717000000	912036602717113320	113320
Jharkhand	Gumla	Palkot	BAGHIMA	912000000000000000	912036600000000000	912036602717000000	912036602717113315	113315
Jharkhand	Gumla	Palkot	BAGESERA	912000000000000000	912036600000000000	912036602717000000	912036602717113314	113314
Jharkhand	Gumla	Raidih	KONDRA	912000000000000000	912036600000000000	912036602716000000	912036602716113332	113332
Jharkhand	Gumla	Raidih	KOBJA	912000000000000000	912036600000000000	912036602716000000	912036602716113331	113331
Jharkhand	Gumla	Raidih	SURSANG	912000000000000000	912036600000000000	912036602716000000	912036602716113339	113339
Jharkhand	Gumla	Raidih	JARJATTA	912000000000000000	912036600000000000	912036602716000000	912036602716113328	113328
Jharkhand	Gumla	Raidih	PIBO	912000000000000000	912036600000000000	912036602716000000	912036602716113336	113336
Jharkhand	Gumla	Raidih	KORO CHHATARPUR	912000000000000000	912036600000000000	912036602716000000	912036602716113333	113333
Jharkhand	Gumla	Raidih	NAWAGARH	912000000000000000	912036600000000000	912036602716000000	912036602716113334	113334
Jharkhand	Gumla	Raidih	SILAM	912000000000000000	912036600000000000	912036602716000000	912036602716113338	113338
Jharkhand	Gumla	Raidih	SIKOI	912000000000000000	912036600000000000	912036602716000000	912036602716113337	113337
Jharkhand	Gumla	Raidih	PARSA	912000000000000000	912036600000000000	912036602716000000	912036602716113335	113335
Jharkhand	Gumla	Raidih	UPPER KHATANGA	912000000000000000	912036600000000000	912036602716000000	912036602716113340	113340
Jharkhand	Gumla	Raidih	KEMTA	912000000000000000	912036600000000000	912036602716000000	912036602716113330	113330
Jharkhand	Gumla	Raidih	KANSIR	912000000000000000	912036600000000000	912036602716000000	912036602716113329	113329
Jharkhand	Gumla	Sisai	OLMUNDA	912000000000000000	912036600000000000	912036602708000000	912036602708113353	113353
Jharkhand	Gumla	Sisai	RERWA	912000000000000000	912036600000000000	912036602708000000	912036602708113356	113356
Jharkhand	Gumla	Sisai	NAGAR	912000000000000000	912036600000000000	912036602708000000	912036602708113352	113352
Jharkhand	Gumla	Sisai	PANDARIYA	912000000000000000	912036600000000000	912036602708000000	912036602708113354	113354
Jharkhand	Gumla	Sisai	MURGU	912000000000000000	912036600000000000	912036602708000000	912036602708113351	113351
Jharkhand	Gumla	Sisai	BHADAULI	912000000000000000	912036600000000000	912036602708000000	912036602708113343	113343
Jharkhand	Gumla	Sisai	LAKEYA	912000000000000000	912036600000000000	912036602708000000	912036602708113349	113349
Jharkhand	Gumla	Sisai	BARGAON BARTOLI	912000000000000000	912036600000000000	912036602708000000	912036602708113342	113342
Jharkhand	Gumla	Sisai	KUDRA	912000000000000000	912036600000000000	912036602708000000	912036602708113348	113348
Jharkhand	Gumla	Sisai	BARGAON	912000000000000000	912036600000000000	912036602708000000	912036602708113341	113341
Jharkhand	Gumla	Sisai	SHIVNATHPUR	912000000000000000	912036600000000000	912036602708000000	912036602708113357	113357
Jharkhand	Gumla	Sisai	CHHARDA	912000000000000000	912036600000000000	912036602708000000	912036602708113346	113346
Jharkhand	Gumla	Sisai	BHURSO	912000000000000000	912036600000000000	912036602708000000	912036602708113344	113344
Jharkhand	Gumla	Sisai	GHAGHRA	912000000000000000	912036600000000000	912036602708000000	912036602708113347	113347
Jharkhand	Gumla	Sisai	LARANGO	912000000000000000	912036600000000000	912036602708000000	912036602708113350	113350
Jharkhand	Gumla	Sisai	PUSO	912000000000000000	912036600000000000	912036602708000000	912036602708113355	113355
Jharkhand	Gumla	Sisai	BONDO	912000000000000000	912036600000000000	912036602708000000	912036602708113345	113345
Jharkhand	Gumla	Verno	KARANJ	912000000000000000	912036600000000000	912036602709000000	912036602709113220	113220
Jharkhand	Gumla	Verno	DUDIYA	912000000000000000	912036600000000000	912036602709000000	912036602709113218	113218
Jharkhand	Gumla	Verno	ATAKORA	912000000000000000	912036600000000000	912036602709000000	912036602709113216	113216
Jharkhand	Gumla	Verno	KARAUNDAJOR	912000000000000000	912036600000000000	912036602709000000	912036602709113221	113221
Jharkhand	Gumla	Verno	SUPA	912000000000000000	912036600000000000	912036602709000000	912036602709113225	113225
Jharkhand	Gumla	Verno	DOMBA	912000000000000000	912036600000000000	912036602709000000	912036602709113217	113217
Jharkhand	Gumla	Verno	MARASILI	912000000000000000	912036600000000000	912036602709000000	912036602709113222	113222
Jharkhand	Gumla	Verno	DUMBO	912000000000000000	912036600000000000	912036602709000000	912036602709113219	113219
Jharkhand	Gumla	Verno	TURIAMBA	912000000000000000	912036600000000000	912036602709000000	912036602709113226	113226
Jharkhand	Gumla	Verno	SOUTH BHARNO	912000000000000000	912036600000000000	912036602709000000	912036602709113224	113224
Jharkhand	Gumla	Verno	NORTH BHARNO	912000000000000000	912036600000000000	912036602709000000	912036602709113223	113223
Jharkhand	Gumla	Verno	AMALIYA	912000000000000000	912036600000000000	912036602709000000	912036602709113215	113215
Jharkhand	Latehar	Balumath	MASIYATU	912000000000000000	912035900000000000	912035902640000000	912035902640113979	113979
Jharkhand	Latehar	Balumath	MURPA	912000000000000000	912035900000000000	912035902640000000	912035902640113980	113980
Jharkhand	Latehar	Balumath	BHAGEYA	912000000000000000	912035900000000000	912035902640000000	912035902640113969	113969
Jharkhand	Latehar	Balumath	MARANG LOIYA	912000000000000000	912035900000000000	912035902640000000	912035902640113978	113978
Jharkhand	Latehar	Balumath	DHADHU	912000000000000000	912035900000000000	912035902640000000	912035902640113973	113973
Jharkhand	Latehar	Balumath	GONIYA	912000000000000000	912035900000000000	912035902640000000	912035902640113975	113975
Jharkhand	Latehar	Balumath	CHETAG	912000000000000000	912035900000000000	912035902640000000	912035902640113970	113970
Jharkhand	Latehar	Balumath	RAJVAR	912000000000000000	912035900000000000	912035902640000000	912035902640113982	113982
Jharkhand	Latehar	Balumath	TONTI	912000000000000000	912035900000000000	912035902640000000	912035902640113989	113989
Jharkhand	Latehar	Balumath	SHEREGARA	912000000000000000	912035900000000000	912035902640000000	912035902640113986	113986
Jharkhand	Latehar	Balumath	BASIYA	912000000000000000	912035900000000000	912035902640000000	912035902640113968	113968
Jharkhand	Latehar	Balumath	GANESHPUR	912000000000000000	912035900000000000	912035902640000000	912035902640113974	113974
Jharkhand	Latehar	Balumath	TASU	912000000000000000	912035900000000000	912035902640000000	912035902640113988	113988
Jharkhand	Latehar	Balumath	BALUMATH	912000000000000000	912035900000000000	912035902640000000	912035902640113966	113966
Jharkhand	Latehar	Balumath	BALUBHANG	912000000000000000	912035900000000000	912035902640000000	912035902640113965	113965
Jharkhand	Latehar	Balumath	JHABAR	912000000000000000	912035900000000000	912035902640000000	912035902640113977	113977
Jharkhand	Latehar	Balumath	BALU	912000000000000000	912035900000000000	912035902640000000	912035902640113964	113964
Jharkhand	Latehar	Balumath	HERHENJ	912000000000000000	912035900000000000	912035902640000000	912035902640113976	113976
Jharkhand	Latehar	Bariyatu	AMARWADIH	912000000000000000	912035900000000000	912035902641000000	912035902641113963	113963
Jharkhand	Latehar	Bariyatu	SALWAY	912000000000000000	912035900000000000	912035902641000000	912035902641113984	113984
Jharkhand	Latehar	Bariyatu	DARHA	912000000000000000	912035900000000000	912035902641000000	912035902641113972	113972
Jharkhand	Latehar	Bariyatu	TONTI	912000000000000000	912035900000000000	912035902641000000	912035902641113989	113989
Jharkhand	Latehar	Bariyatu	TASU	912000000000000000	912035900000000000	912035902641000000	912035902641113988	113988
Jharkhand	Latehar	Bariyatu	BARIYATU	912000000000000000	912035900000000000	912035902641000000	912035902641113967	113967
Jharkhand	Latehar	Bariyatu	MARANG LOIYA	912000000000000000	912035900000000000	912035902641000000	912035902641113978	113978
Jharkhand	Latehar	Bariyatu	SHIBLA	912000000000000000	912035900000000000	912035902641000000	912035902641113987	113987
Jharkhand	Latehar	Bariyatu	GONIYA	912000000000000000	912035900000000000	912035902641000000	912035902641113975	113975
Jharkhand	Latehar	Bariyatu	PHULSU	912000000000000000	912035900000000000	912035902641000000	912035902641113981	113981
Jharkhand	Latehar	Bariyatu	BALUBHANG	912000000000000000	912035900000000000	912035902641000000	912035902641113965	113965
Jharkhand	Latehar	Bariyatu	DHADHU	912000000000000000	912035900000000000	912035902641000000	912035902641113973	113973
Jharkhand	Latehar	Bariyatu	CHETAG	912000000000000000	912035900000000000	912035902641000000	912035902641113970	113970
Jharkhand	Latehar	Barwadih	LAT	912000000000000000	912035900000000000	912035902636000000	912035902636114001	114001
Jharkhand	Latehar	Barwadih	GANESHPUR	912000000000000000	912035900000000000	912035902636000000	912035902636113995	113995
Jharkhand	Latehar	Barwadih	HARATU	912000000000000000	912035900000000000	912035902636000000	912035902636113996	113996
Jharkhand	Latehar	Barwadih	MORWAI KALA	912000000000000000	912035900000000000	912035902636000000	912035902636114003	114003
Jharkhand	Latehar	Barwadih	CHUNGRU	912000000000000000	912035900000000000	912035902636000000	912035902636113994	113994
Jharkhand	Latehar	Barwadih	CHHIPADOHAR	912000000000000000	912035900000000000	912035902636000000	912035902636113993	113993
Jharkhand	Latehar	Barwadih	KUCHILA	912000000000000000	912035900000000000	912035902636000000	912035902636114000	114000
Jharkhand	Latehar	Barwadih	KER	912000000000000000	912035900000000000	912035902636000000	912035902636113997	113997
Jharkhand	Latehar	Barwadih	CHHENCHA	912000000000000000	912035900000000000	912035902636000000	912035902636113992	113992
Jharkhand	Latehar	Barwadih	KHURA	912000000000000000	912035900000000000	912035902636000000	912035902636113999	113999
Jharkhand	Latehar	Barwadih	UKAMANR	912000000000000000	912035900000000000	912035902636000000	912035902636114005	114005
Jharkhand	Latehar	Barwadih	BARWADIH	912000000000000000	912035900000000000	912035902636000000	912035902636113990	113990
Jharkhand	Latehar	Barwadih	MANGRA	912000000000000000	912035900000000000	912035902636000000	912035902636114002	114002
Jharkhand	Latehar	Barwadih	BETLA	912000000000000000	912035900000000000	912035902636000000	912035902636113991	113991
Jharkhand	Latehar	Barwadih	KETCHKI	912000000000000000	912035900000000000	912035902636000000	912035902636113998	113998
Jharkhand	Latehar	Barwadih	POKHRIKALA	912000000000000000	912035900000000000	912035902636000000	912035902636114004	114004
Jharkhand	Latehar	Chandwa	BARWATOLI	912000000000000000	912035900000000000	912035902643000000	912035902643114009	114009
Jharkhand	Latehar	Chandwa	LADHUP	912000000000000000	912035900000000000	912035902643000000	912035902643114019	114019
Jharkhand	Latehar	Chandwa	CHETAR	912000000000000000	912035900000000000	912035902643000000	912035902643114014	114014
Jharkhand	Latehar	Chandwa	HUTAP	912000000000000000	912035900000000000	912035902643000000	912035902643114016	114016
Jharkhand	Latehar	Chandwa	MALHAN	912000000000000000	912035900000000000	912035902643000000	912035902643114020	114020
Jharkhand	Latehar	Chandwa	DUMARO	912000000000000000	912035900000000000	912035902643000000	912035902643114015	114015
Jharkhand	Latehar	Chandwa	BODA	912000000000000000	912035900000000000	912035902643000000	912035902643114010	114010
Jharkhand	Latehar	Chandwa	JAMIRA	912000000000000000	912035900000000000	912035902643000000	912035902643114017	114017
Jharkhand	Latehar	Chandwa	CHANDWA EAST	912000000000000000	912035900000000000	912035902643000000	912035902643114012	114012
Jharkhand	Latehar	Chandwa	SASANG	912000000000000000	912035900000000000	912035902643000000	912035902643114021	114021
Jharkhand	Latehar	Chandwa	ALODIYA	912000000000000000	912035900000000000	912035902643000000	912035902643114006	114006
Jharkhand	Latehar	Chandwa	KAMTA	912000000000000000	912035900000000000	912035902643000000	912035902643114018	114018
Jharkhand	Latehar	Chandwa	SERAK	912000000000000000	912035900000000000	912035902643000000	912035902643114022	114022
Jharkhand	Latehar	Chandwa	BANHARDI	912000000000000000	912035900000000000	912035902643000000	912035902643114007	114007
Jharkhand	Latehar	Chandwa	BARI	912000000000000000	912035900000000000	912035902643000000	912035902643114008	114008
Jharkhand	Latehar	Chandwa	CHAKALA	912000000000000000	912035900000000000	912035902643000000	912035902643114011	114011
Jharkhand	Latehar	Garu	GHASITOLA	912000000000000000	912035900000000000	912035902638000000	912035902638114026	114026
Jharkhand	Latehar	Garu	KARWAI	912000000000000000	912035900000000000	912035902638000000	912035902638114027	114027
Jharkhand	Latehar	Garu	DHANGARTOLA	912000000000000000	912035900000000000	912035902638000000	912035902638114025	114025
Jharkhand	Latehar	Garu	RUD	912000000000000000	912035900000000000	912035902638000000	912035902638114030	114030
Jharkhand	Latehar	Garu	MAYAPUR	912000000000000000	912035900000000000	912035902638000000	912035902638114029	114029
Jharkhand	Latehar	Garu	BARESANR	912000000000000000	912035900000000000	912035902638000000	912035902638114023	114023
Jharkhand	Latehar	Garu	CHORHA	912000000000000000	912035900000000000	912035902638000000	912035902638114024	114024
Jharkhand	Latehar	Garu	KOTAM	912000000000000000	912035900000000000	912035902638000000	912035902638114028	114028
Jharkhand	Latehar	Herhanj	TASU	912000000000000000	912035900000000000	912035902642000000	912035902642113988	113988
Jharkhand	Latehar	Herhanj	SHEREGARA	912000000000000000	912035900000000000	912035902642000000	912035902642113986	113986
Jharkhand	Latehar	Herhanj	CHIRU	912000000000000000	912035900000000000	912035902642000000	912035902642113971	113971
Jharkhand	Latehar	Herhanj	JHABAR	912000000000000000	912035900000000000	912035902642000000	912035902642113977	113977
Jharkhand	Latehar	Herhanj	SERENDAG	912000000000000000	912035900000000000	912035902642000000	912035902642113985	113985
Jharkhand	Latehar	Herhanj	SALAIYA	912000000000000000	912035900000000000	912035902642000000	912035902642113983	113983
Jharkhand	Latehar	Herhanj	HERHENJ	912000000000000000	912035900000000000	912035902642000000	912035902642113976	113976
Jharkhand	Latehar	Herhanj	AMARWADIH	912000000000000000	912035900000000000	912035902642000000	912035902642113963	113963
Jharkhand	Latehar	Latehar	\N	912000000000000000	912035900000000000	912035902639000000	912035902639250326	250326
Jharkhand	Latehar	Latehar	BENDI	912000000000000000	912035900000000000	912035902639000000	912035902639114032	114032
Jharkhand	Latehar	Latehar	ICHAK	912000000000000000	912035900000000000	912035902639000000	912035902639114038	114038
Jharkhand	Latehar	Latehar	PESHRAR	912000000000000000	912035900000000000	912035902639000000	912035902639114045	114045
Jharkhand	Latehar	Latehar	NAWAGARH	912000000000000000	912035900000000000	912035902639000000	912035902639114041	114041
Jharkhand	Latehar	Latehar	DHANKARA	912000000000000000	912035900000000000	912035902639000000	912035902639114035	114035
Jharkhand	Latehar	Latehar	MONGAR	912000000000000000	912035900000000000	912035902639000000	912035902639114040	114040
Jharkhand	Latehar	Latehar	TAWADIH	912000000000000000	912035900000000000	912035902639000000	912035902639114048	114048
Jharkhand	Latehar	Latehar	AARAGUNDI	912000000000000000	912035900000000000	912035902639000000	912035902639114031	114031
Jharkhand	Latehar	Latehar	DEMU	912000000000000000	912035900000000000	912035902639000000	912035902639114034	114034
Jharkhand	Latehar	Latehar	SHISHI	912000000000000000	912035900000000000	912035902639000000	912035902639114047	114047
Jharkhand	Latehar	Latehar	BHUSUR	912000000000000000	912035900000000000	912035902639000000	912035902639114033	114033
Jharkhand	Latehar	Latehar	SASANG	912000000000000000	912035900000000000	912035902639000000	912035902639114046	114046
Jharkhand	Latehar	Latehar	JALIM KHURD	912000000000000000	912035900000000000	912035902639000000	912035902639114039	114039
Jharkhand	Latehar	Latehar	PANDEPURA	912000000000000000	912035900000000000	912035902639000000	912035902639114043	114043
Jharkhand	Latehar	Latehar	PARSAHI	912000000000000000	912035900000000000	912035902639000000	912035902639114044	114044
Jharkhand	Latehar	Latehar	DIHI	912000000000000000	912035900000000000	912035902639000000	912035902639114036	114036
Jharkhand	Latehar	Latehar	HETHPOCHRA	912000000000000000	912035900000000000	912035902639000000	912035902639114037	114037
Jharkhand	Latehar	Latehar	NEWADI	912000000000000000	912035900000000000	912035902639000000	912035902639114042	114042
Jharkhand	Latehar	Mahuadanr	CHATAKPUR	912000000000000000	912035900000000000	912035902637000000	912035902637114053	114053
Jharkhand	Latehar	Mahuadanr	CHAMPA	912000000000000000	912035900000000000	912035902637000000	912035902637114052	114052
Jharkhand	Latehar	Mahuadanr	SOHAR	912000000000000000	912035900000000000	912035902637000000	912035902637114062	114062
Jharkhand	Latehar	Mahuadanr	ORSA	912000000000000000	912035900000000000	912035902637000000	912035902637114059	114059
Jharkhand	Latehar	Mahuadanr	PARHATOLI	912000000000000000	912035900000000000	912035902637000000	912035902637114060	114060
Jharkhand	Latehar	Mahuadanr	AMWATOLI	912000000000000000	912035900000000000	912035902637000000	912035902637114050	114050
Jharkhand	Latehar	Mahuadanr	MAHUADANR	912000000000000000	912035900000000000	912035902637000000	912035902637114057	114057
Jharkhand	Latehar	Mahuadanr	RENGAI	912000000000000000	912035900000000000	912035902637000000	912035902637114061	114061
Jharkhand	Latehar	Mahuadanr	GARBUDHNI	912000000000000000	912035900000000000	912035902637000000	912035902637114055	114055
Jharkhand	Latehar	Mahuadanr	HAMI	912000000000000000	912035900000000000	912035902637000000	912035902637114056	114056
Jharkhand	Latehar	Mahuadanr	NETARHAT	912000000000000000	912035900000000000	912035902637000000	912035902637114058	114058
Jharkhand	Latehar	Mahuadanr	DURUP	912000000000000000	912035900000000000	912035902637000000	912035902637114054	114054
Jharkhand	Latehar	Mahuadanr	CHAINPUR	912000000000000000	912035900000000000	912035902637000000	912035902637114051	114051
Jharkhand	Latehar	Mahuadanr	AKSI	912000000000000000	912035900000000000	912035902637000000	912035902637114049	114049
Jharkhand	Latehar	Manika	KOPE	912000000000000000	912035900000000000	912035902635000000	912035902635114070	114070
Jharkhand	Latehar	Manika	BADKADIH	912000000000000000	912035900000000000	912035902635000000	912035902635114063	114063
Jharkhand	Latehar	Manika	SINJO	912000000000000000	912035900000000000	912035902635000000	912035902635114076	114076
Jharkhand	Latehar	Manika	VISHUNBANDH	912000000000000000	912035900000000000	912035902635000000	912035902635114077	114077
Jharkhand	Latehar	Manika	DUNDU	912000000000000000	912035900000000000	912035902635000000	912035902635114067	114067
Jharkhand	Latehar	Manika	RAWKIKALA	912000000000000000	912035900000000000	912035902635000000	912035902635114075	114075
Jharkhand	Latehar	Manika	MANIKA	912000000000000000	912035900000000000	912035902635000000	912035902635114071	114071
Jharkhand	Latehar	Manika	MATLONG	912000000000000000	912035900000000000	912035902635000000	912035902635114072	114072
Jharkhand	Latehar	Manika	NAMUDAG	912000000000000000	912035900000000000	912035902635000000	912035902635114073	114073
Jharkhand	Latehar	Manika	JANHO	912000000000000000	912035900000000000	912035902635000000	912035902635114068	114068
Jharkhand	Latehar	Manika	JUNGUR	912000000000000000	912035900000000000	912035902635000000	912035902635114069	114069
Jharkhand	Latehar	Manika	BANDUWA	912000000000000000	912035900000000000	912035902635000000	912035902635114064	114064
Jharkhand	Latehar	Manika	BARWAIYA KALA	912000000000000000	912035900000000000	912035902635000000	912035902635114065	114065
Jharkhand	Latehar	Manika	PALHEYA	912000000000000000	912035900000000000	912035902635000000	912035902635114074	114074
Jharkhand	Latehar	Manika	DONKI	912000000000000000	912035900000000000	912035902635000000	912035902635114066	114066
Jharkhand	Palamu	Bishrampur	\N	912000000000000000	912035800000000000	912035802624000000	912035802624253231	253231
Jharkhand	Palamu	Bishrampur	KETAT KALAN	912000000000000000	912035800000000000	912035802624000000	912035802624114282	114282
Jharkhand	Palamu	Bishrampur	PANJARI KALAN	912000000000000000	912035800000000000	912035802624000000	912035802624114285	114285
Jharkhand	Palamu	Bishrampur	GURI	912000000000000000	912035800000000000	912035802624000000	912035802624114277	114277
Jharkhand	Palamu	Bishrampur	LALGARH	912000000000000000	912035800000000000	912035802624000000	912035802624114284	114284
Jharkhand	Palamu	Bishrampur	TOLARA	912000000000000000	912035800000000000	912035802624000000	912035802624114290	114290
Jharkhand	Palamu	Bishrampur	GURHA KALAN	912000000000000000	912035800000000000	912035802624000000	912035802624114276	114276
Jharkhand	Palamu	Bishrampur	BHANDAR	912000000000000000	912035800000000000	912035802624000000	912035802624114274	114274
Jharkhand	Palamu	Bishrampur	SIGSIGI	912000000000000000	912035800000000000	912035802624000000	912035802624114288	114288
Jharkhand	Palamu	Bishrampur	KUMBHI  KALAN	912000000000000000	912035800000000000	912035802624000000	912035802624114283	114283
Jharkhand	Palamu	Bishrampur	BAGHMANWAN	912000000000000000	912035800000000000	912035802624000000	912035802624114272	114272
Jharkhand	Palamu	Bishrampur	GHANSIDAG	912000000000000000	912035800000000000	912035802624000000	912035802624114275	114275
Jharkhand	Palamu	Chainpur	JHARIWA	912000000000000000	912035800000000000	912035802634000000	912035802634114307	114307
Jharkhand	Palamu	Chainpur	HUTAR	912000000000000000	912035800000000000	912035802634000000	912035802634114306	114306
Jharkhand	Palamu	Chainpur	NAWADIH	912000000000000000	912035800000000000	912035802634000000	912035802634114315	114315
Jharkhand	Palamu	Chainpur	ULDANDA	912000000000000000	912035800000000000	912035802634000000	912035802634114326	114326
Jharkhand	Palamu	Chainpur	BASDIH KHURD	912000000000000000	912035800000000000	912035802634000000	912035802634114297	114297
Jharkhand	Palamu	Chainpur	RAMGARH	912000000000000000	912035800000000000	912035802634000000	912035802634114321	114321
Jharkhand	Palamu	Chainpur	CHORHAT	912000000000000000	912035800000000000	912035802634000000	912035802634114305	114305
Jharkhand	Palamu	Chainpur	AWSANE	912000000000000000	912035800000000000	912035802634000000	912035802634114292	114292
Jharkhand	Palamu	Chainpur	CHANDO	912000000000000000	912035800000000000	912035802634000000	912035802634114304	114304
Jharkhand	Palamu	Chainpur	BASARIYA KALA	912000000000000000	912035800000000000	912035802634000000	912035802634114295	114295
Jharkhand	Palamu	Chainpur	NARSINGHPUR PATHRA	912000000000000000	912035800000000000	912035802634000000	912035802634114314	114314
Jharkhand	Palamu	Chainpur	BODI	912000000000000000	912035800000000000	912035802634000000	912035802634114301	114301
Jharkhand	Palamu	Chainpur	BHABHANDI	912000000000000000	912035800000000000	912035802634000000	912035802634114299	114299
Jharkhand	Palamu	Chainpur	BEDMA BABHANDI	912000000000000000	912035800000000000	912035802634000000	912035802634114298	114298
Jharkhand	Palamu	Chainpur	NEURA	912000000000000000	912035800000000000	912035802634000000	912035802634114316	114316
Jharkhand	Palamu	Chainpur	KANKARI	912000000000000000	912035800000000000	912035802634000000	912035802634114308	114308
Jharkhand	Palamu	Chainpur	BHARGAONA	912000000000000000	912035800000000000	912035802634000000	912035802634114300	114300
Jharkhand	Palamu	Chainpur	SEMRA	912000000000000000	912035800000000000	912035802634000000	912035802634114323	114323
Jharkhand	Palamu	Chainpur	KARSO	912000000000000000	912035800000000000	912035802634000000	912035802634114309	114309
Jharkhand	Palamu	Chainpur	CHAINPUR	912000000000000000	912035800000000000	912035802634000000	912035802634114303	114303
Jharkhand	Palamu	Chainpur	SALATUA	912000000000000000	912035800000000000	912035802634000000	912035802634114322	114322
Jharkhand	Palamu	Chainpur	BUDHIBIR	912000000000000000	912035800000000000	912035802634000000	912035802634114302	114302
Jharkhand	Palamu	Chainpur	SHAHPUR (SOUTH)	912000000000000000	912035800000000000	912035802634000000	912035802634114325	114325
Jharkhand	Palamu	Chainpur	BANDUWA	912000000000000000	912035800000000000	912035802634000000	912035802634114294	114294
Jharkhand	Palamu	Chainpur	SHAHPUR (NORTH)	912000000000000000	912035800000000000	912035802634000000	912035802634114324	114324
Jharkhand	Palamu	Chainpur	ORNAR	912000000000000000	912035800000000000	912035802634000000	912035802634114317	114317
Jharkhand	Palamu	Chainpur	BASDIH	912000000000000000	912035800000000000	912035802634000000	912035802634114296	114296
Jharkhand	Palamu	Chainpur	KHURA KALA	912000000000000000	912035800000000000	912035802634000000	912035802634114310	114310
Jharkhand	Palamu	Chainpur	MAHUGAONA	912000000000000000	912035800000000000	912035802634000000	912035802634114312	114312
Jharkhand	Palamu	Chainpur	RABDA	912000000000000000	912035800000000000	912035802634000000	912035802634114320	114320
Jharkhand	Palamu	Chainpur	PURBDIHA	912000000000000000	912035800000000000	912035802634000000	912035802634114319	114319
Jharkhand	Palamu	Chainpur	MAJHIGAONA	912000000000000000	912035800000000000	912035802634000000	912035802634114313	114313
Jharkhand	Palamu	Chainpur	BAIRAO	912000000000000000	912035800000000000	912035802634000000	912035802634114293	114293
Jharkhand	Palamu	Chainpur	PATHARI A KHURD	912000000000000000	912035800000000000	912035802634000000	912035802634114318	114318
Jharkhand	Palamu	Chainpur	KOSHIYARA	912000000000000000	912035800000000000	912035802634000000	912035802634114311	114311
Jharkhand	Palamu	Chhatarpur	MURUMDAG	912000000000000000	912035800000000000	912035802620000000	912035802620114350	114350
Jharkhand	Palamu	Chhatarpur	HULSUM	912000000000000000	912035800000000000	912035802620000000	912035802620114336	114336
Jharkhand	Palamu	Chhatarpur	CHERAI(1)	912000000000000000	912035800000000000	912035802620000000	912035802620114330	114330
Jharkhand	Palamu	Chhatarpur	RUDWA	912000000000000000	912035800000000000	912035802620000000	912035802620114355	114355
Jharkhand	Palamu	Chhatarpur	DALI	912000000000000000	912035800000000000	912035802620000000	912035802620114334	114334
Jharkhand	Palamu	Chhatarpur	KHORHI	912000000000000000	912035800000000000	912035802620000000	912035802620114344	114344
Jharkhand	Palamu	Chhatarpur	HUTTUKDAG	912000000000000000	912035800000000000	912035802620000000	912035802620114337	114337
Jharkhand	Palamu	Chhatarpur	SILDAG	912000000000000000	912035800000000000	912035802620000000	912035802620114358	114358
Jharkhand	Palamu	Chhatarpur	CHIRU	912000000000000000	912035800000000000	912035802620000000	912035802620114332	114332
Jharkhand	Palamu	Chhatarpur	CHHATTARPUR	912000000000000000	912035800000000000	912035802620000000	912035802620114331	114331
Jharkhand	Palamu	Chhatarpur	KALA PAHAR	912000000000000000	912035800000000000	912035802620000000	912035802620114339	114339
Jharkhand	Palamu	Chhatarpur	MUNKERI	912000000000000000	912035800000000000	912035802620000000	912035802620114349	114349
Jharkhand	Palamu	Chhatarpur	NAUDIHA	912000000000000000	912035800000000000	912035802620000000	912035802620114352	114352
Jharkhand	Palamu	Chhatarpur	KAWAL	912000000000000000	912035800000000000	912035802620000000	912035802620114342	114342
Jharkhand	Palamu	Chhatarpur	KAUAL	912000000000000000	912035800000000000	912035802620000000	912035802620114341	114341
Jharkhand	Palamu	Chhatarpur	BARA	912000000000000000	912035800000000000	912035802620000000	912035802620114327	114327
Jharkhand	Palamu	Chhatarpur	UDAIGARH	912000000000000000	912035800000000000	912035802620000000	912035802620114361	114361
Jharkhand	Palamu	Chhatarpur	MARWA	912000000000000000	912035800000000000	912035802620000000	912035802620114347	114347
Jharkhand	Palamu	Chhatarpur	MASIHANI	912000000000000000	912035800000000000	912035802620000000	912035802620114348	114348
Jharkhand	Palamu	Chhatarpur	KACHANPUR	912000000000000000	912035800000000000	912035802620000000	912035802620114338	114338
Jharkhand	Palamu	Chhatarpur	PINDRAHI	912000000000000000	912035800000000000	912035802620000000	912035802620114354	114354
Jharkhand	Palamu	Chhatarpur	LAXMIPUR	912000000000000000	912035800000000000	912035802620000000	912035802620114346	114346
Jharkhand	Palamu	Chhatarpur	DINADAG	912000000000000000	912035800000000000	912035802620000000	912035802620114335	114335
Jharkhand	Palamu	Chhatarpur	SUSHIGANJ	912000000000000000	912035800000000000	912035802620000000	912035802620114359	114359
Jharkhand	Palamu	Haidernagar	IMMAMNAGAR BAREWA	912000000000000000	912035800000000000	912035802616000000	912035802616114388	114388
Jharkhand	Palamu	Haidernagar	BARDANDA (BARWADIH)	912000000000000000	912035800000000000	912035802616000000	912035802616114381	114381
Jharkhand	Palamu	Haidernagar	MOHKAR KALAN	912000000000000000	912035800000000000	912035802616000000	912035802616114395	114395
Jharkhand	Palamu	Haidernagar	POLDIH	912000000000000000	912035800000000000	912035802616000000	912035802616114437	114437
Jharkhand	Palamu	Haidernagar	SARAIYA	912000000000000000	912035800000000000	912035802616000000	912035802616114399	114399
Jharkhand	Palamu	Haidernagar	BILASPUR	912000000000000000	912035800000000000	912035802616000000	912035802616114383	114383
Jharkhand	Palamu	Haidernagar	BABHANDIH	912000000000000000	912035800000000000	912035802616000000	912035802616114380	114380
Jharkhand	Palamu	Haidernagar	MAHUDAND	912000000000000000	912035800000000000	912035802616000000	912035802616114434	114434
Jharkhand	Palamu	Haidernagar	PATHARA	912000000000000000	912035800000000000	912035802616000000	912035802616114435	114435
Jharkhand	Palamu	Haidernagar	KHARGADA	912000000000000000	912035800000000000	912035802616000000	912035802616114390	114390
Jharkhand	Palamu	Haidernagar	CHOUKRI	912000000000000000	912035800000000000	912035802616000000	912035802616114384	114384
Jharkhand	Palamu	Haidernagar	BELBIGHA	912000000000000000	912035800000000000	912035802616000000	912035802616114421	114421
Jharkhand	Palamu	Haidernagar	HAIDERNAGAR EAST	912000000000000000	912035800000000000	912035802616000000	912035802616114386	114386
Jharkhand	Palamu	Haidernagar	KUKHI	912000000000000000	912035800000000000	912035802616000000	912035802616114392	114392
Jharkhand	Palamu	Haidernagar	PARTA	912000000000000000	912035800000000000	912035802616000000	912035802616114397	114397
Jharkhand	Palamu	Hariharganj	KATAIYA	912000000000000000	912035800000000000	912035802618000000	912035802618114406	114406
Jharkhand	Palamu	Hariharganj	SALAIYA	912000000000000000	912035800000000000	912035802618000000	912035802618114411	114411
Jharkhand	Palamu	Hariharganj	DHAKACHA	912000000000000000	912035800000000000	912035802618000000	912035802618114405	114405
Jharkhand	Palamu	Hariharganj	SEMRWAR	912000000000000000	912035800000000000	912035802618000000	912035802618114416	114416
Jharkhand	Palamu	Hariharganj	SARSOT	912000000000000000	912035800000000000	912035802618000000	912035802618114413	114413
Jharkhand	Palamu	Hariharganj	KHADAGPUR	912000000000000000	912035800000000000	912035802618000000	912035802618114407	114407
Jharkhand	Palamu	Hariharganj	KULHIA	912000000000000000	912035800000000000	912035802618000000	912035802618114408	114408
Jharkhand	Palamu	Hariharganj	DEMA	912000000000000000	912035800000000000	912035802618000000	912035802618114404	114404
Jharkhand	Palamu	Hariharganj	BELODAR	912000000000000000	912035800000000000	912035802618000000	912035802618114402	114402
Jharkhand	Palamu	Hariharganj	ARARUA KHURD	912000000000000000	912035800000000000	912035802618000000	912035802618114400	114400
Jharkhand	Palamu	Hariharganj	PIPRA	912000000000000000	912035800000000000	912035802618000000	912035802618114410	114410
Jharkhand	Palamu	Hariharganj	SATGAWAN URF HARIHARGANJ (E)	912000000000000000	912035800000000000	912035802618000000	912035802618114414	114414
Jharkhand	Palamu	Hussainabad	\N	912000000000000000	912035800000000000	912035802615000000	912035802615250323	250323
Jharkhand	Palamu	Hussainabad	MAHUDAND	912000000000000000	912035800000000000	912035802615000000	912035802615114434	114434
Jharkhand	Palamu	Hussainabad	BILASPUR	912000000000000000	912035800000000000	912035802615000000	912035802615114383	114383
Jharkhand	Palamu	Hussainabad	DANDILA	912000000000000000	912035800000000000	912035802615000000	912035802615114423	114423
Jharkhand	Palamu	Hussainabad	PATRA KHURD	912000000000000000	912035800000000000	912035802615000000	912035802615114436	114436
Jharkhand	Palamu	Hussainabad	JAMUA	912000000000000000	912035800000000000	912035802615000000	912035802615114428	114428
Jharkhand	Palamu	Hussainabad	BELBIGHA	912000000000000000	912035800000000000	912035802615000000	912035802615114421	114421
Jharkhand	Palamu	Hussainabad	JHARGARA	912000000000000000	912035800000000000	912035802615000000	912035802615114429	114429
Jharkhand	Palamu	Hussainabad	KOSI	912000000000000000	912035800000000000	912035802615000000	912035802615114430	114430
Jharkhand	Palamu	Hussainabad	URDHWAR (MANJURAHAR)	912000000000000000	912035800000000000	912035802615000000	912035802615114439	114439
Jharkhand	Palamu	Hussainabad	BAIRAON	912000000000000000	912035800000000000	912035802615000000	912035802615114419	114419
Jharkhand	Palamu	Hussainabad	KURMIPUR	912000000000000000	912035800000000000	912035802615000000	912035802615114431	114431
Jharkhand	Palamu	Hussainabad	POLDIH	912000000000000000	912035800000000000	912035802615000000	912035802615114437	114437
Jharkhand	Palamu	Hussainabad	UPARI KALAN	912000000000000000	912035800000000000	912035802615000000	912035802615114438	114438
Jharkhand	Palamu	Hussainabad	MAHUARY	912000000000000000	912035800000000000	912035802615000000	912035802615114433	114433
Jharkhand	Palamu	Hussainabad	BARDANDA (BARWADIH)	912000000000000000	912035800000000000	912035802615000000	912035802615114381	114381
Jharkhand	Palamu	Hussainabad	PATHARA	912000000000000000	912035800000000000	912035802615000000	912035802615114435	114435
Jharkhand	Palamu	Hussainabad	BENI KALAN	912000000000000000	912035800000000000	912035802615000000	912035802615114422	114422
Jharkhand	Palamu	Hussainabad	DEORIKHURD	912000000000000000	912035800000000000	912035802615000000	912035802615114427	114427
Jharkhand	Palamu	Hussainabad	BABHANDIH	912000000000000000	912035800000000000	912035802615000000	912035802615114380	114380
Jharkhand	Palamu	Hussainabad	LOTANIA	912000000000000000	912035800000000000	912035802615000000	912035802615114432	114432
Jharkhand	Palamu	Hussainabad	DARUABENI	912000000000000000	912035800000000000	912035802615000000	912035802615114425	114425
Jharkhand	Palamu	Hussainabad	DEORIKALAN	912000000000000000	912035800000000000	912035802615000000	912035802615114426	114426
Jharkhand	Palamu	Hussainabad	BANIADIH (BARAHI)	912000000000000000	912035800000000000	912035802615000000	912035802615114420	114420
Jharkhand	Palamu	Hussainabad	BADEPUR	912000000000000000	912035800000000000	912035802615000000	912035802615114418	114418
Jharkhand	Palamu	Hussainabad	CHOUKRI	912000000000000000	912035800000000000	912035802615000000	912035802615114384	114384
Jharkhand	Palamu	Hussainabad	DANGWAR	912000000000000000	912035800000000000	912035802615000000	912035802615114424	114424
Jharkhand	Palamu	Hussainabad	KHARGADA	912000000000000000	912035800000000000	912035802615000000	912035802615114390	114390
Jharkhand	Palamu	Manatu	RANGEYA	912000000000000000	912035800000000000	912035802628000000	912035802628114469	114469
Jharkhand	Palamu	Manatu	PADAMA	912000000000000000	912035800000000000	912035802628000000	912035802628114467	114467
Jharkhand	Palamu	Manatu	DUMARI	912000000000000000	912035800000000000	912035802628000000	912035802628114459	114459
Jharkhand	Palamu	Manatu	NAUDIHA	912000000000000000	912035800000000000	912035802628000000	912035802628114465	114465
Jharkhand	Palamu	Manatu	BANSHIKHURD	912000000000000000	912035800000000000	912035802628000000	912035802628114457	114457
Jharkhand	Palamu	Manatu	ARKA	912000000000000000	912035800000000000	912035802628000000	912035802628114456	114456
Jharkhand	Palamu	Manatu	KASMAR	912000000000000000	912035800000000000	912035802628000000	912035802628114462	114462
Jharkhand	Palamu	Manatu	MANJHAULI (A)	912000000000000000	912035800000000000	912035802628000000	912035802628114463	114463
Jharkhand	Palamu	Manatu	CHAK	912000000000000000	912035800000000000	912035802628000000	912035802628114458	114458
Jharkhand	Palamu	Medininagar(Daltonganj)	LAHLAHE	912000000000000000	912035800000000000	912035802633000000	912035802633114369	114369
Jharkhand	Palamu	Medininagar(Daltonganj)	SARJA	912000000000000000	912035800000000000	912035802633000000	912035802633114375	114375
Jharkhand	Palamu	Medininagar(Daltonganj)	POLPOL KALA	912000000000000000	912035800000000000	912035802633000000	912035802633114371	114371
Jharkhand	Palamu	Medininagar(Daltonganj)	JOND	912000000000000000	912035800000000000	912035802633000000	912035802633114367	114367
Jharkhand	Palamu	Medininagar(Daltonganj)	KAUDIYA	912000000000000000	912035800000000000	912035802633000000	912035802633114368	114368
Jharkhand	Palamu	Medininagar(Daltonganj)	JHABAR	912000000000000000	912035800000000000	912035802633000000	912035802633114366	114366
Jharkhand	Palamu	Medininagar(Daltonganj)	SUA	912000000000000000	912035800000000000	912035802633000000	912035802633114377	114377
Jharkhand	Palamu	Medininagar(Daltonganj)	CHIYANKI	912000000000000000	912035800000000000	912035802633000000	912035802633114364	114364
Jharkhand	Palamu	Medininagar(Daltonganj)	REDMA (NORTH)	912000000000000000	912035800000000000	912035802633000000	912035802633114373	114373
Jharkhand	Palamu	Medininagar(Daltonganj)	JAMUNE	912000000000000000	912035800000000000	912035802633000000	912035802633114365	114365
Jharkhand	Palamu	Medininagar(Daltonganj)	POKHRAHA KHURD	912000000000000000	912035800000000000	912035802633000000	912035802633114370	114370
Jharkhand	Palamu	Medininagar(Daltonganj)	SUDNA(EAST)	912000000000000000	912035800000000000	912035802633000000	912035802633114378	114378
Jharkhand	Palamu	Medininagar(Daltonganj)	BARALOTA(NORTH)	912000000000000000	912035800000000000	912035802633000000	912035802633114362	114362
Jharkhand	Palamu	Medininagar(Daltonganj)	RAJWADIH	912000000000000000	912035800000000000	912035802633000000	912035802633114372	114372
Jharkhand	Palamu	Medininagar(Daltonganj)	SINGRA KHURD	912000000000000000	912035800000000000	912035802633000000	912035802633114376	114376
Jharkhand	Palamu	Mohammad Ganj	RAMBANDH (KOSIYARA)	912000000000000000	912035800000000000	912035802617000000	912035802617114398	114398
Jharkhand	Palamu	Mohammad Ganj	KADAL KURMI	912000000000000000	912035800000000000	912035802617000000	912035802617114389	114389
Jharkhand	Palamu	Mohammad Ganj	MOHAMMADGANJ	912000000000000000	912035800000000000	912035802617000000	912035802617114394	114394
Jharkhand	Palamu	Mohammad Ganj	BHAJANIA	912000000000000000	912035800000000000	912035802617000000	912035802617114382	114382
Jharkhand	Palamu	Mohammad Ganj	GORADIH	912000000000000000	912035800000000000	912035802617000000	912035802617114385	114385
Jharkhand	Palamu	Mohammad Ganj	LATPOURI	912000000000000000	912035800000000000	912035802617000000	912035802617114393	114393
Jharkhand	Palamu	Mohammad Ganj	KOLHUA SONBARSA	912000000000000000	912035800000000000	912035802617000000	912035802617114391	114391
Jharkhand	Palamu	Mohammad Ganj	PANSA	912000000000000000	912035800000000000	912035802617000000	912035802617114396	114396
Jharkhand	Palamu	Nawa Bazar	RABDA	912000000000000000	912035800000000000	912035802625000000	912035802625114286	114286
Jharkhand	Palamu	Nawa Bazar	RAJHARA	912000000000000000	912035800000000000	912035802625000000	912035802625114287	114287
Jharkhand	Palamu	Nawa Bazar	TUKBERA	912000000000000000	912035800000000000	912035802625000000	912035802625114291	114291
Jharkhand	Palamu	Nawa Bazar	BASANA	912000000000000000	912035800000000000	912035802625000000	912035802625114273	114273
Jharkhand	Palamu	Nawa Bazar	KUMBHI  KALAN	912000000000000000	912035800000000000	912035802625000000	912035802625114283	114283
Jharkhand	Palamu	Nawa Bazar	KANDA	912000000000000000	912035800000000000	912035802625000000	912035802625114280	114280
Jharkhand	Palamu	Nawa Bazar	SOHDAGKHURD	912000000000000000	912035800000000000	912035802625000000	912035802625114289	114289
Jharkhand	Palamu	Nawa Bazar	ITKO	912000000000000000	912035800000000000	912035802625000000	912035802625114278	114278
Jharkhand	Palamu	Nawadiha Bazar/Nawadiha*	KARKATA	912000000000000000	912035800000000000	912035802621000000	912035802621114340	114340
Jharkhand	Palamu	Nawadiha Bazar/Nawadiha*	KHAIRADOHAR	912000000000000000	912035800000000000	912035802621000000	912035802621114343	114343
Jharkhand	Palamu	Nawadiha Bazar/Nawadiha*	SAHPUR	912000000000000000	912035800000000000	912035802621000000	912035802621114356	114356
Jharkhand	Palamu	Nawadiha Bazar/Nawadiha*	DAGRA	912000000000000000	912035800000000000	912035802621000000	912035802621114333	114333
Jharkhand	Palamu	Nawadiha Bazar/Nawadiha*	CHERAI (2)	912000000000000000	912035800000000000	912035802621000000	912035802621114329	114329
Jharkhand	Palamu	Nawadiha Bazar/Nawadiha*	SARAIDIH	912000000000000000	912035800000000000	912035802621000000	912035802621114357	114357
Jharkhand	Palamu	Nawadiha Bazar/Nawadiha*	NAMUDAG	912000000000000000	912035800000000000	912035802621000000	912035802621114351	114351
Jharkhand	Palamu	Nawadiha Bazar/Nawadiha*	TARI DIH	912000000000000000	912035800000000000	912035802621000000	912035802621114360	114360
Jharkhand	Palamu	Nawadiha Bazar/Nawadiha*	LALGARA	912000000000000000	912035800000000000	912035802621000000	912035802621114345	114345
Jharkhand	Palamu	Nawadiha Bazar/Nawadiha*	NAWATAND	912000000000000000	912035800000000000	912035802621000000	912035802621114353	114353
Jharkhand	Palamu	Nawadiha Bazar/Nawadiha*	MARWA	912000000000000000	912035800000000000	912035802621000000	912035802621114347	114347
Jharkhand	Palamu	Nawadiha Bazar/Nawadiha*	BISUNPUR	912000000000000000	912035800000000000	912035802621000000	912035802621114328	114328
Jharkhand	Palamu	Nawadiha Bazar/Nawadiha*	LAXMIPUR	912000000000000000	912035800000000000	912035802621000000	912035802621114346	114346
Jharkhand	Palamu	Nilambar-Pitambarpur(Lesliganj)	HARATUA	912000000000000000	912035800000000000	912035802632000000	912035802632114443	114443
Jharkhand	Palamu	Nilambar-Pitambarpur(Lesliganj)	DABARA	912000000000000000	912035800000000000	912035802632000000	912035802632114441	114441
Jharkhand	Palamu	Nilambar-Pitambarpur(Lesliganj)	NAUDIHA	912000000000000000	912035800000000000	912035802632000000	912035802632114450	114450
Jharkhand	Palamu	Nilambar-Pitambarpur(Lesliganj)	RAJHARA	912000000000000000	912035800000000000	912035802632000000	912035802632114454	114454
Jharkhand	Palamu	Nilambar-Pitambarpur(Lesliganj)	KOTT KHAS	912000000000000000	912035800000000000	912035802632000000	912035802632114446	114446
Jharkhand	Palamu	Nilambar-Pitambarpur(Lesliganj)	PIPRAKHURD	912000000000000000	912035800000000000	912035802632000000	912035802632114452	114452
Jharkhand	Palamu	Nilambar-Pitambarpur(Lesliganj)	CHAURA	912000000000000000	912035800000000000	912035802632000000	912035802632114440	114440
Jharkhand	Palamu	Nilambar-Pitambarpur(Lesliganj)	KUNDARI	912000000000000000	912035800000000000	912035802632000000	912035802632114447	114447
Jharkhand	Palamu	Nilambar-Pitambarpur(Lesliganj)	KURAIN PATRA	912000000000000000	912035800000000000	912035802632000000	912035802632114448	114448
Jharkhand	Palamu	Nilambar-Pitambarpur(Lesliganj)	LESLIGANJ	912000000000000000	912035800000000000	912035802632000000	912035802632114449	114449
Jharkhand	Palamu	Nilambar-Pitambarpur(Lesliganj)	JAMUNDIH	912000000000000000	912035800000000000	912035802632000000	912035802632114444	114444
Jharkhand	Palamu	Nilambar-Pitambarpur(Lesliganj)	PURNADIH	912000000000000000	912035800000000000	912035802632000000	912035802632114453	114453
Jharkhand	Palamu	Nilambar-Pitambarpur(Lesliganj)	SANGBAR	912000000000000000	912035800000000000	912035802632000000	912035802632114455	114455
Jharkhand	Palamu	Nilambar-Pitambarpur(Lesliganj)	DARUDIH	912000000000000000	912035800000000000	912035802632000000	912035802632114442	114442
Jharkhand	Palamu	Nilambar-Pitambarpur(Lesliganj)	JURU	912000000000000000	912035800000000000	912035802632000000	912035802632114445	114445
Jharkhand	Palamu	Nilambar-Pitambarpur(Lesliganj)	ORIYA KALAN	912000000000000000	912035800000000000	912035802632000000	912035802632114451	114451
Jharkhand	Palamu	Padwa	KAJARI	912000000000000000	912035800000000000	912035802627000000	912035802627114519	114519
Jharkhand	Palamu	Padwa	GARI KHAS	912000000000000000	912035800000000000	912035802627000000	912035802627114516	114516
Jharkhand	Palamu	Padwa	LOHARA	912000000000000000	912035800000000000	912035802627000000	912035802627114524	114524
Jharkhand	Palamu	Padwa	MUDMA	912000000000000000	912035800000000000	912035802627000000	912035802627114529	114529
Jharkhand	Palamu	Padwa	PADWA	912000000000000000	912035800000000000	912035802627000000	912035802627114533	114533
Jharkhand	Palamu	Padwa	PATRA	912000000000000000	912035800000000000	912035802627000000	912035802627114535	114535
Jharkhand	Palamu	Padwa	MAJHIGAWAN	912000000000000000	912035800000000000	912035802627000000	912035802627114527	114527
Jharkhand	Palamu	Padwa	CHHECHAURI	912000000000000000	912035800000000000	912035802627000000	912035802627114515	114515
Jharkhand	Palamu	Pandu	TISI BAR KALAN	912000000000000000	912035800000000000	912035802622000000	912035802622114489	114489
Jharkhand	Palamu	Pandu	PANDU	912000000000000000	912035800000000000	912035802622000000	912035802622114485	114485
Jharkhand	Palamu	Pandu	KUTMU	912000000000000000	912035800000000000	912035802622000000	912035802622114478	114478
Jharkhand	Palamu	Pandu	PHULIA	912000000000000000	912035800000000000	912035802622000000	912035802622114486	114486
Jharkhand	Palamu	Pandu	MUSIKHAP	912000000000000000	912035800000000000	912035802622000000	912035802622114484	114484
Jharkhand	Palamu	Pandu	MAHUGAWAN	912000000000000000	912035800000000000	912035802622000000	912035802622114481	114481
Jharkhand	Palamu	Pandu	DALA KALAN	912000000000000000	912035800000000000	912035802622000000	912035802622114476	114476
Jharkhand	Palamu	Pandu	RATNAG	912000000000000000	912035800000000000	912035802622000000	912035802622114487	114487
Jharkhand	Palamu	Pandu	KAJARU KALAN	912000000000000000	912035800000000000	912035802622000000	912035802622114477	114477
Jharkhand	Palamu	Pandu	SILDILI	912000000000000000	912035800000000000	912035802622000000	912035802622114488	114488
Jharkhand	Palamu	Panki	SUNDI	912000000000000000	912035800000000000	912035802630000000	912035802630114512	114512
Jharkhand	Palamu	Panki	PAKARIYA	912000000000000000	912035800000000000	912035802630000000	912035802630114506	114506
Jharkhand	Palamu	Panki	SAKALDEEPA	912000000000000000	912035800000000000	912035802630000000	912035802630114511	114511
Jharkhand	Palamu	Panki	NAWADIHA	912000000000000000	912035800000000000	912035802630000000	912035802630114503	114503
Jharkhand	Palamu	Panki	TAL	912000000000000000	912035800000000000	912035802630000000	912035802630114513	114513
Jharkhand	Palamu	Panki	KEKAR GARH	912000000000000000	912035800000000000	912035802630000000	912035802630114497	114497
Jharkhand	Palamu	Panki	RATANPUR	912000000000000000	912035800000000000	912035802630000000	912035802630114509	114509
Jharkhand	Palamu	Panki	HOTAI	912000000000000000	912035800000000000	912035802630000000	912035802630114494	114494
Jharkhand	Palamu	Panki	MADAN	912000000000000000	912035800000000000	912035802630000000	912035802630114501	114501
Jharkhand	Palamu	Panki	KELAHWA	912000000000000000	912035800000000000	912035802630000000	912035802630114498	114498
Jharkhand	Palamu	Panki	NURU	912000000000000000	912035800000000000	912035802630000000	912035802630114504	114504
Jharkhand	Palamu	Panki	KARAR	912000000000000000	912035800000000000	912035802630000000	912035802630114496	114496
Jharkhand	Palamu	Panki	TETRAIN	912000000000000000	912035800000000000	912035802630000000	912035802630114514	114514
Jharkhand	Palamu	Panki	LOHARSI	912000000000000000	912035800000000000	912035802630000000	912035802630114500	114500
Jharkhand	Palamu	Panki	AMBABAR	912000000000000000	912035800000000000	912035802630000000	912035802630114490	114490
Jharkhand	Palamu	Panki	ASEHAR	912000000000000000	912035800000000000	912035802630000000	912035802630114491	114491
Jharkhand	Palamu	Panki	PANKI (WEST)	912000000000000000	912035800000000000	912035802630000000	912035802630114508	114508
Jharkhand	Palamu	Panki	DHUB	912000000000000000	912035800000000000	912035802630000000	912035802630114493	114493
Jharkhand	Palamu	Panki	HURLONG	912000000000000000	912035800000000000	912035802630000000	912035802630114495	114495
Jharkhand	Palamu	Panki	DANDAR KALAN	912000000000000000	912035800000000000	912035802630000000	912035802630114492	114492
Jharkhand	Palamu	Panki	PAGAR KHURD	912000000000000000	912035800000000000	912035802630000000	912035802630114505	114505
Jharkhand	Palamu	Panki	PANKI (EAST)	912000000000000000	912035800000000000	912035802630000000	912035802630114507	114507
Jharkhand	Palamu	Panki	NAUDIHA	912000000000000000	912035800000000000	912035802630000000	912035802630114502	114502
Jharkhand	Palamu	Panki	SAGALIM	912000000000000000	912035800000000000	912035802630000000	912035802630114510	114510
Jharkhand	Palamu	Panki	KONWAI	912000000000000000	912035800000000000	912035802630000000	912035802630114499	114499
Jharkhand	Palamu	Patan	NAWAKHAS	912000000000000000	912035800000000000	912035802626000000	912035802626114531	114531
Jharkhand	Palamu	Patan	RUNDIDIH	912000000000000000	912035800000000000	912035802626000000	912035802626114537	114537
Jharkhand	Palamu	Patan	KELHAR	912000000000000000	912035800000000000	912035802626000000	912035802626114522	114522
Jharkhand	Palamu	Patan	MAHULIA	912000000000000000	912035800000000000	912035802626000000	912035802626114526	114526
Jharkhand	Palamu	Patan	KANKE KALAN	912000000000000000	912035800000000000	912035802626000000	912035802626114520	114520
Jharkhand	Palamu	Patan	MERAL	912000000000000000	912035800000000000	912035802626000000	912035802626114528	114528
Jharkhand	Palamu	Patan	RAJHARA	912000000000000000	912035800000000000	912035802626000000	912035802626114536	114536
Jharkhand	Palamu	Patan	SOLE	912000000000000000	912035800000000000	912035802626000000	912035802626114542	114542
Jharkhand	Palamu	Patan	NAUDIHA	912000000000000000	912035800000000000	912035802626000000	912035802626114530	114530
Jharkhand	Palamu	Patan	KASWAKHAR	912000000000000000	912035800000000000	912035802626000000	912035802626114521	114521
Jharkhand	Palamu	Patan	SUNTHA	912000000000000000	912035800000000000	912035802626000000	912035802626114543	114543
Jharkhand	Palamu	Patan	HISRA BARWADIH	912000000000000000	912035800000000000	912035802626000000	912035802626114517	114517
Jharkhand	Palamu	Patan	SIRMA	912000000000000000	912035800000000000	912035802626000000	912035802626114541	114541
Jharkhand	Palamu	Patan	LOINGA	912000000000000000	912035800000000000	912035802626000000	912035802626114525	114525
Jharkhand	Palamu	Patan	KISHUNPUR	912000000000000000	912035800000000000	912035802626000000	912035802626114523	114523
Jharkhand	Palamu	Patan	PALHE KALAN	912000000000000000	912035800000000000	912035802626000000	912035802626114534	114534
Jharkhand	Palamu	Patan	SAGUNA	912000000000000000	912035800000000000	912035802626000000	912035802626114538	114538
Jharkhand	Palamu	Patan	SATAUA	912000000000000000	912035800000000000	912035802626000000	912035802626114539	114539
Jharkhand	Palamu	Patan	UTAKI	912000000000000000	912035800000000000	912035802626000000	912035802626114544	114544
Jharkhand	Palamu	Patan	JANGHASI	912000000000000000	912035800000000000	912035802626000000	912035802626114518	114518
Jharkhand	Palamu	Patan	PACHKERIYA	912000000000000000	912035800000000000	912035802626000000	912035802626114532	114532
Jharkhand	Palamu	Patan	SEMARI	912000000000000000	912035800000000000	912035802626000000	912035802626114540	114540
Jharkhand	Palamu	Patan	KUSMADIH	912000000000000000	912035800000000000	912035802626000000	912035802626245516	245516
Jharkhand	Palamu	Pipra	DALPATPUR	912000000000000000	912035800000000000	912035802619000000	912035802619114403	114403
Jharkhand	Palamu	Pipra	PIPRA	912000000000000000	912035800000000000	912035802619000000	912035802619114410	114410
Jharkhand	Palamu	Pipra	TENDUI	912000000000000000	912035800000000000	912035802619000000	912035802619114417	114417
Jharkhand	Palamu	Pipra	MADHUBANA	912000000000000000	912035800000000000	912035802619000000	912035802619114409	114409
Jharkhand	Palamu	Pipra	BABHANDIH	912000000000000000	912035800000000000	912035802619000000	912035802619114401	114401
Jharkhand	Palamu	Pipra	BELODAR	912000000000000000	912035800000000000	912035802619000000	912035802619114402	114402
Jharkhand	Palamu	Pipra	SARAIYA	912000000000000000	912035800000000000	912035802619000000	912035802619114412	114412
Jharkhand	Palamu	Satbarwa	RABADA	912000000000000000	912035800000000000	912035802631000000	912035802631114552	114552
Jharkhand	Palamu	Satbarwa	BAKORIYA	912000000000000000	912035800000000000	912035802631000000	912035802631114545	114545
Jharkhand	Palamu	Satbarwa	DULSULMA	912000000000000000	912035800000000000	912035802631000000	912035802631114549	114549
Jharkhand	Palamu	Satbarwa	GHUTUA	912000000000000000	912035800000000000	912035802631000000	912035802631114550	114550
Jharkhand	Palamu	Satbarwa	PONCHI	912000000000000000	912035800000000000	912035802631000000	912035802631114551	114551
Jharkhand	Palamu	Satbarwa	SATBARWA	912000000000000000	912035800000000000	912035802631000000	912035802631114554	114554
Jharkhand	Palamu	Satbarwa	DHAWADIH	912000000000000000	912035800000000000	912035802631000000	912035802631114548	114548
Jharkhand	Palamu	Satbarwa	BOHITA	912000000000000000	912035800000000000	912035802631000000	912035802631114547	114547
Jharkhand	Palamu	Satbarwa	REWARATU	912000000000000000	912035800000000000	912035802631000000	912035802631114553	114553
Jharkhand	Palamu	Satbarwa	BARI	912000000000000000	912035800000000000	912035802631000000	912035802631114546	114546
Jharkhand	Palamu	Tarhasi	ARKA	912000000000000000	912035800000000000	912035802629000000	912035802629114456	114456
Jharkhand	Palamu	Tarhasi	GOINDI	912000000000000000	912035800000000000	912035802629000000	912035802629114460	114460
Jharkhand	Palamu	Tarhasi	TARHANSI	912000000000000000	912035800000000000	912035802629000000	912035802629114472	114472
Jharkhand	Palamu	Tarhasi	NAWAGARH	912000000000000000	912035800000000000	912035802629000000	912035802629114466	114466
Jharkhand	Palamu	Tarhasi	UDAIPURA  - II	912000000000000000	912035800000000000	912035802629000000	912035802629114475	114475
Jharkhand	Palamu	Tarhasi	MANJHAULI (B)	912000000000000000	912035800000000000	912035802629000000	912035802629114464	114464
Jharkhand	Palamu	Tarhasi	GURHA	912000000000000000	912035800000000000	912035802629000000	912035802629114461	114461
Jharkhand	Palamu	Tarhasi	UDAIPUR A- I	912000000000000000	912035800000000000	912035802629000000	912035802629114474	114474
Jharkhand	Palamu	Tarhasi	KASMAR	912000000000000000	912035800000000000	912035802629000000	912035802629114462	114462
Jharkhand	Palamu	Tarhasi	SELARI	912000000000000000	912035800000000000	912035802629000000	912035802629114470	114470
Jharkhand	Palamu	Tarhasi	SONPURA	912000000000000000	912035800000000000	912035802629000000	912035802629114471	114471
Jharkhand	Palamu	Tarhasi	TARIYA	912000000000000000	912035800000000000	912035802629000000	912035802629114473	114473
Jharkhand	Palamu	Tarhasi	PATHAK PAGAR	912000000000000000	912035800000000000	912035802629000000	912035802629114468	114468
Jharkhand	Palamu	Tarhasi	DUMARI	912000000000000000	912035800000000000	912035802629000000	912035802629114459	114459
Jharkhand	Palamu	Untari Road	JOGA	912000000000000000	912035800000000000	912035802623000000	912035802623114279	114279
Jharkhand	Palamu	Untari Road	KARKATA	912000000000000000	912035800000000000	912035802623000000	912035802623114281	114281
Jharkhand	Palamu	Untari Road	MURMA KHURD	912000000000000000	912035800000000000	912035802623000000	912035802623114483	114483
Jharkhand	Palamu	Untari Road	LAHAR BANJARI	912000000000000000	912035800000000000	912035802623000000	912035802623114479	114479
Jharkhand	Palamu	Untari Road	MURMA KALAN	912000000000000000	912035800000000000	912035802623000000	912035802623114482	114482
Jharkhand	Palamu	Untari Road	LUMBA SATBAHINI	912000000000000000	912035800000000000	912035802623000000	912035802623114480	114480
Jharkhand	Pashchimi Singhbhum	Anandpur	RUNDHIKOCHA	912000000000000000	912036800000000000	912036802734000000	912036802734115394	115394
Jharkhand	Pashchimi Singhbhum	Anandpur	ROBOKERA	912000000000000000	912036800000000000	912036802734000000	912036802734115494	115494
Jharkhand	Pashchimi Singhbhum	Anandpur	JHARBERA	912000000000000000	912036800000000000	912036802734000000	912036802734115485	115485
Jharkhand	Pashchimi Singhbhum	Anandpur	ANANDPUR	912000000000000000	912036800000000000	912036802734000000	912036802734115475	115475
Jharkhand	Pashchimi Singhbhum	Anandpur	BEDAKENDUDA	912000000000000000	912036800000000000	912036802734000000	912036802734115477	115477
Jharkhand	Pashchimi Singhbhum	Anandpur	BINJU	912000000000000000	912036800000000000	912036802734000000	912036802734115478	115478
Jharkhand	Pashchimi Singhbhum	Anandpur	HARTA	912000000000000000	912036800000000000	912036802734000000	912036802734115389	115389
Jharkhand	Pashchimi Singhbhum	Anandpur	CHHOTANAGRA	912000000000000000	912036800000000000	912036802734000000	912036802734115479	115479
Jharkhand	Pashchimi Singhbhum	Chaibasa	TEKRAHATU	912000000000000000	912036800000000000	912036802739000000	912036802739115358	115358
Jharkhand	Pashchimi Singhbhum	Chaibasa	HARILA	912000000000000000	912036800000000000	912036802739000000	912036802739115350	115350
Jharkhand	Pashchimi Singhbhum	Chaibasa	TUIBIR	912000000000000000	912036800000000000	912036802739000000	912036802739115360	115360
Jharkhand	Pashchimi Singhbhum	Chaibasa	SIMBIYA	912000000000000000	912036800000000000	912036802739000000	912036802739115356	115356
Jharkhand	Pashchimi Singhbhum	Chaibasa	KURSI	912000000000000000	912036800000000000	912036802739000000	912036802739115352	115352
Jharkhand	Pashchimi Singhbhum	Chaibasa	KARLAJODI	912000000000000000	912036800000000000	912036802739000000	912036802739115351	115351
Jharkhand	Pashchimi Singhbhum	Chaibasa	MATKAMHATU	912000000000000000	912036800000000000	912036802739000000	912036802739115354	115354
Jharkhand	Pashchimi Singhbhum	Chaibasa	TAMARBANDH	912000000000000000	912036800000000000	912036802739000000	912036802739115357	115357
Jharkhand	Pashchimi Singhbhum	Chaibasa	BADURI	912000000000000000	912036800000000000	912036802739000000	912036802739115347	115347
Jharkhand	Pashchimi Singhbhum	Chaibasa	LUPUNGUTU	912000000000000000	912036800000000000	912036802739000000	912036802739115353	115353
Jharkhand	Pashchimi Singhbhum	Chaibasa	GAISUTI	912000000000000000	912036800000000000	912036802739000000	912036802739115349	115349
Jharkhand	Pashchimi Singhbhum	Chaibasa	NARSANDA	912000000000000000	912036800000000000	912036802739000000	912036802739115355	115355
Jharkhand	Pashchimi Singhbhum	Chaibasa	DILIAMARCHA	912000000000000000	912036800000000000	912036802739000000	912036802739115348	115348
Jharkhand	Pashchimi Singhbhum	Chaibasa	TONTO	912000000000000000	912036800000000000	912036802739000000	912036802739115359	115359
Jharkhand	Pashchimi Singhbhum	Chakradharpur	KOLCHAKDA	912000000000000000	912036800000000000	912036802731000000	912036802731115377	115377
Jharkhand	Pashchimi Singhbhum	Chakradharpur	ASANTALIA	912000000000000000	912036800000000000	912036802731000000	912036802731115361	115361
Jharkhand	Pashchimi Singhbhum	Chakradharpur	BAIPI	912000000000000000	912036800000000000	912036802731000000	912036802731115362	115362
Jharkhand	Pashchimi Singhbhum	Chakradharpur	GULKERA	912000000000000000	912036800000000000	912036802731000000	912036802731115367	115367
Jharkhand	Pashchimi Singhbhum	Chakradharpur	KULITODANG	912000000000000000	912036800000000000	912036802731000000	912036802731115378	115378
Jharkhand	Pashchimi Singhbhum	Chakradharpur	SILPHODI	912000000000000000	912036800000000000	912036802731000000	912036802731115381	115381
Jharkhand	Pashchimi Singhbhum	Chakradharpur	JAMID	912000000000000000	912036800000000000	912036802731000000	912036802731115373	115373
Jharkhand	Pashchimi Singhbhum	Chakradharpur	PADAMPUR	912000000000000000	912036800000000000	912036802731000000	912036802731115380	115380
Jharkhand	Pashchimi Singhbhum	Chakradharpur	CHANDRI	912000000000000000	912036800000000000	912036802731000000	912036802731115365	115365
Jharkhand	Pashchimi Singhbhum	Chakradharpur	CHAINPUR	912000000000000000	912036800000000000	912036802731000000	912036802731115364	115364
Jharkhand	Pashchimi Singhbhum	Chakradharpur	HOYAHATU	912000000000000000	912036800000000000	912036802731000000	912036802731115370	115370
Jharkhand	Pashchimi Singhbhum	Chakradharpur	KERA	912000000000000000	912036800000000000	912036802731000000	912036802731115376	115376
Jharkhand	Pashchimi Singhbhum	Chakradharpur	ITORE	912000000000000000	912036800000000000	912036802731000000	912036802731115372	115372
Jharkhand	Pashchimi Singhbhum	Chakradharpur	KENDO	912000000000000000	912036800000000000	912036802731000000	912036802731115374	115374
Jharkhand	Pashchimi Singhbhum	Chakradharpur	HATHIYA	912000000000000000	912036800000000000	912036802731000000	912036802731115368	115368
Jharkhand	Pashchimi Singhbhum	Chakradharpur	SIMIDIRI	912000000000000000	912036800000000000	912036802731000000	912036802731115382	115382
Jharkhand	Pashchimi Singhbhum	Chakradharpur	GOPINATHPUR	912000000000000000	912036800000000000	912036802731000000	912036802731115366	115366
Jharkhand	Pashchimi Singhbhum	Chakradharpur	KENKE	912000000000000000	912036800000000000	912036802731000000	912036802731115375	115375
Jharkhand	Pashchimi Singhbhum	Chakradharpur	HATNATODANG	912000000000000000	912036800000000000	912036802731000000	912036802731115369	115369
Jharkhand	Pashchimi Singhbhum	Chakradharpur	ITIHASA	912000000000000000	912036800000000000	912036802731000000	912036802731115371	115371
Jharkhand	Pashchimi Singhbhum	Chakradharpur	SURGURA	912000000000000000	912036800000000000	912036802731000000	912036802731115383	115383
Jharkhand	Pashchimi Singhbhum	Chakradharpur	BHARANIA	912000000000000000	912036800000000000	912036802731000000	912036802731115363	115363
Jharkhand	Pashchimi Singhbhum	Chakradharpur	NALITA	912000000000000000	912036800000000000	912036802731000000	912036802731115379	115379
Jharkhand	Pashchimi Singhbhum	Goilkera	GAMHARIA	912000000000000000	912036800000000000	912036802733000000	912036802733115387	115387
Jharkhand	Pashchimi Singhbhum	Goilkera	ARAHASA	912000000000000000	912036800000000000	912036802733000000	912036802733115384	115384
Jharkhand	Pashchimi Singhbhum	Goilkera	KAIDA	912000000000000000	912036800000000000	912036802733000000	912036802733115391	115391
Jharkhand	Pashchimi Singhbhum	Goilkera	GOILKERA	912000000000000000	912036800000000000	912036802733000000	912036802733115388	115388
Jharkhand	Pashchimi Singhbhum	Goilkera	BILA	912000000000000000	912036800000000000	912036802733000000	912036802733115386	115386
Jharkhand	Pashchimi Singhbhum	Goilkera	TARKATKOCHA	912000000000000000	912036800000000000	912036802733000000	912036802733115396	115396
Jharkhand	Pashchimi Singhbhum	Goilkera	KUIRA	912000000000000000	912036800000000000	912036802733000000	912036802733115393	115393
Jharkhand	Pashchimi Singhbhum	Goilkera	BARA	912000000000000000	912036800000000000	912036802733000000	912036802733115385	115385
Jharkhand	Pashchimi Singhbhum	Goilkera	KEBRA	912000000000000000	912036800000000000	912036802733000000	912036802733115392	115392
Jharkhand	Pashchimi Singhbhum	Goilkera	KADAMDIHA	912000000000000000	912036800000000000	912036802733000000	912036802733115390	115390
Jharkhand	Pashchimi Singhbhum	Goilkera	SARUGARA	912000000000000000	912036800000000000	912036802733000000	912036802733115395	115395
Jharkhand	Pashchimi Singhbhum	Gudri	DADIYO KAMRORA	912000000000000000	912036800000000000	912036802729000000	912036802729115520	115520
Jharkhand	Pashchimi Singhbhum	Gudri	SARUGARA	912000000000000000	912036800000000000	912036802729000000	912036802729115395	115395
Jharkhand	Pashchimi Singhbhum	Gudri	TOMDEL	912000000000000000	912036800000000000	912036802729000000	912036802729115397	115397
Jharkhand	Pashchimi Singhbhum	Gudri	GULIKERA	912000000000000000	912036800000000000	912036802729000000	912036802729115524	115524
Jharkhand	Pashchimi Singhbhum	Gudri	KEBRA	912000000000000000	912036800000000000	912036802729000000	912036802729115392	115392
Jharkhand	Pashchimi Singhbhum	Gudri	BANDU	912000000000000000	912036800000000000	912036802729000000	912036802729115515	115515
Jharkhand	Pashchimi Singhbhum	Gudri	BOIKERA	912000000000000000	912036800000000000	912036802729000000	912036802729115519	115519
Jharkhand	Pashchimi Singhbhum	Gudri	BIRKEL	912000000000000000	912036800000000000	912036802729000000	912036802729115518	115518
Jharkhand	Pashchimi Singhbhum	Gudri	GOLMUNDA	912000000000000000	912036800000000000	912036802729000000	912036802729115522	115522
Jharkhand	Pashchimi Singhbhum	Gudri	KAMRORA	912000000000000000	912036800000000000	912036802729000000	912036802729115525	115525
Jharkhand	Pashchimi Singhbhum	Hat Gamharia	KUSHMITA	912000000000000000	912036800000000000	912036802738000000	912036802738115451	115451
Jharkhand	Pashchimi Singhbhum	Hat Gamharia	KUSHMUNDA	912000000000000000	912036800000000000	912036802738000000	912036802738115452	115452
Jharkhand	Pashchimi Singhbhum	Hat Gamharia	SINDRIGOURI	912000000000000000	912036800000000000	912036802738000000	912036802738115425	115425
Jharkhand	Pashchimi Singhbhum	Hat Gamharia	DUMRIA	912000000000000000	912036800000000000	912036802738000000	912036802738115447	115447
Jharkhand	Pashchimi Singhbhum	Hat Gamharia	DIKUBALKAND	912000000000000000	912036800000000000	912036802738000000	912036802738115446	115446
Jharkhand	Pashchimi Singhbhum	Hat Gamharia	JAIPUR	912000000000000000	912036800000000000	912036802738000000	912036802738115417	115417
Jharkhand	Pashchimi Singhbhum	Hat Gamharia	KOCHDA	912000000000000000	912036800000000000	912036802738000000	912036802738115420	115420
Jharkhand	Pashchimi Singhbhum	Hat Gamharia	RUIYA	912000000000000000	912036800000000000	912036802738000000	912036802738115424	115424
Jharkhand	Pashchimi Singhbhum	Hat Gamharia	AMDIHA	912000000000000000	912036800000000000	912036802738000000	912036802738115414	115414
Jharkhand	Pashchimi Singhbhum	Hat Gamharia	JAMDIH	912000000000000000	912036800000000000	912036802738000000	912036802738115542	115542
Jharkhand	Pashchimi Singhbhum	Hat Gamharia	NURDA	912000000000000000	912036800000000000	912036802738000000	912036802738115423	115423
Jharkhand	Pashchimi Singhbhum	Jagannathpur	MUNDAI	912000000000000000	912036800000000000	912036802743000000	912036802743115410	115410
Jharkhand	Pashchimi Singhbhum	Jagannathpur	GUMURIA	912000000000000000	912036800000000000	912036802743000000	912036802743115402	115402
Jharkhand	Pashchimi Singhbhum	Jagannathpur	JAINTGARH	912000000000000000	912036800000000000	912036802743000000	912036802743115404	115404
Jharkhand	Pashchimi Singhbhum	Jagannathpur	SIYALJORA	912000000000000000	912036800000000000	912036802743000000	912036802743115412	115412
Jharkhand	Pashchimi Singhbhum	Jagannathpur	PATAJAINT	912000000000000000	912036800000000000	912036802743000000	912036802743115411	115411
Jharkhand	Pashchimi Singhbhum	Jagannathpur	BHANGAON	912000000000000000	912036800000000000	912036802743000000	912036802743115399	115399
Jharkhand	Pashchimi Singhbhum	Jagannathpur	TODANGHATU	912000000000000000	912036800000000000	912036802743000000	912036802743115413	115413
Jharkhand	Pashchimi Singhbhum	Jagannathpur	KASIRA	912000000000000000	912036800000000000	912036802743000000	912036802743115407	115407
Jharkhand	Pashchimi Singhbhum	Jagannathpur	KALEIYA	912000000000000000	912036800000000000	912036802743000000	912036802743115405	115405
Jharkhand	Pashchimi Singhbhum	Jagannathpur	CHHOTA MAHULDIHA	912000000000000000	912036800000000000	912036802743000000	912036802743115400	115400
Jharkhand	Pashchimi Singhbhum	Jagannathpur	DANGUAPOSI	912000000000000000	912036800000000000	912036802743000000	912036802743115401	115401
Jharkhand	Pashchimi Singhbhum	Jagannathpur	KARANJIA	912000000000000000	912036800000000000	912036802743000000	912036802743115406	115406
Jharkhand	Pashchimi Singhbhum	Jagannathpur	BADANANDA	912000000000000000	912036800000000000	912036802743000000	912036802743115398	115398
Jharkhand	Pashchimi Singhbhum	Jagannathpur	MALUKA	912000000000000000	912036800000000000	912036802743000000	912036802743115408	115408
Jharkhand	Pashchimi Singhbhum	Jagannathpur	MONGRA	912000000000000000	912036800000000000	912036802743000000	912036802743115409	115409
Jharkhand	Pashchimi Singhbhum	Jagannathpur	JAGANNATHPUR	912000000000000000	912036800000000000	912036802743000000	912036802743115403	115403
Jharkhand	Pashchimi Singhbhum	Jhinkpani	JODAPOKHAR	912000000000000000	912036800000000000	912036802742000000	912036802742115418	115418
Jharkhand	Pashchimi Singhbhum	Jhinkpani	NAWAGAON	912000000000000000	912036800000000000	912036802742000000	912036802742115422	115422
Jharkhand	Pashchimi Singhbhum	Jhinkpani	CHOYA	912000000000000000	912036800000000000	912036802742000000	912036802742115416	115416
Jharkhand	Pashchimi Singhbhum	Jhinkpani	KUDAHATU	912000000000000000	912036800000000000	912036802742000000	912036802742115421	115421
Jharkhand	Pashchimi Singhbhum	Jhinkpani	TUTUGUTU	912000000000000000	912036800000000000	912036802742000000	912036802742115426	115426
Jharkhand	Pashchimi Singhbhum	Jhinkpani	KELENDE	912000000000000000	912036800000000000	912036802742000000	912036802742115419	115419
Jharkhand	Pashchimi Singhbhum	Jhinkpani	ASURA	912000000000000000	912036800000000000	912036802742000000	912036802742115415	115415
Jharkhand	Pashchimi Singhbhum	Khuntpani	BARALAGIYA	912000000000000000	912036800000000000	912036802732000000	912036802732115429	115429
Jharkhand	Pashchimi Singhbhum	Khuntpani	PANDABIR	912000000000000000	912036800000000000	912036802732000000	912036802732115436	115436
Jharkhand	Pashchimi Singhbhum	Khuntpani	DOPAI	912000000000000000	912036800000000000	912036802732000000	912036802732115432	115432
Jharkhand	Pashchimi Singhbhum	Khuntpani	BARKELA	912000000000000000	912036800000000000	912036802732000000	912036802732115430	115430
Jharkhand	Pashchimi Singhbhum	Khuntpani	BARAGUNTIA	912000000000000000	912036800000000000	912036802732000000	912036802732115428	115428
Jharkhand	Pashchimi Singhbhum	Khuntpani	BARA CHIRU	912000000000000000	912036800000000000	912036802732000000	912036802732115427	115427
Jharkhand	Pashchimi Singhbhum	Khuntpani	ULIRAJABASA	912000000000000000	912036800000000000	912036802732000000	912036802732115439	115439
Jharkhand	Pashchimi Singhbhum	Khuntpani	RUIDIH	912000000000000000	912036800000000000	912036802732000000	912036802732115438	115438
Jharkhand	Pashchimi Singhbhum	Khuntpani	LOHARDA	912000000000000000	912036800000000000	912036802732000000	912036802732115434	115434
Jharkhand	Pashchimi Singhbhum	Khuntpani	MATKOBERA	912000000000000000	912036800000000000	912036802732000000	912036802732115435	115435
Jharkhand	Pashchimi Singhbhum	Khuntpani	KEYADCHALAM	912000000000000000	912036800000000000	912036802732000000	912036802732115433	115433
Jharkhand	Pashchimi Singhbhum	Khuntpani	PURUNIA	912000000000000000	912036800000000000	912036802732000000	912036802732115437	115437
Jharkhand	Pashchimi Singhbhum	Khuntpani	BHOYA	912000000000000000	912036800000000000	912036802732000000	912036802732115431	115431
Jharkhand	Pashchimi Singhbhum	Kumardungi	CHHOTA RAIKAMAN	912000000000000000	912036800000000000	912036802744000000	912036802744115445	115445
Jharkhand	Pashchimi Singhbhum	Kumardungi	ANDHARI	912000000000000000	912036800000000000	912036802744000000	912036802744115440	115440
Jharkhand	Pashchimi Singhbhum	Kumardungi	KHANDKHORI	912000000000000000	912036800000000000	912036802744000000	912036802744115448	115448
Jharkhand	Pashchimi Singhbhum	Kumardungi	BHONDA	912000000000000000	912036800000000000	912036802744000000	912036802744115444	115444
Jharkhand	Pashchimi Singhbhum	Kumardungi	BEDAMUNDUI	912000000000000000	912036800000000000	912036802744000000	912036802744115443	115443
Jharkhand	Pashchimi Singhbhum	Kumardungi	KUMIRTA	912000000000000000	912036800000000000	912036802744000000	912036802744115450	115450
Jharkhand	Pashchimi Singhbhum	Kumardungi	BARUSAI	912000000000000000	912036800000000000	912036802744000000	912036802744115442	115442
Jharkhand	Pashchimi Singhbhum	Kumardungi	BAIHATU	912000000000000000	912036800000000000	912036802744000000	912036802744115441	115441
Jharkhand	Pashchimi Singhbhum	Kumardungi	KUMARDUNGI	912000000000000000	912036800000000000	912036802744000000	912036802744115449	115449
Jharkhand	Pashchimi Singhbhum	Majhgaon	NAYAGAON	912000000000000000	912036800000000000	912036802745000000	912036802745115471	115471
Jharkhand	Pashchimi Singhbhum	Majhgaon	ADHIKARI	912000000000000000	912036800000000000	912036802745000000	912036802745115463	115463
Jharkhand	Pashchimi Singhbhum	Majhgaon	PADSA	912000000000000000	912036800000000000	912036802745000000	912036802745115472	115472
Jharkhand	Pashchimi Singhbhum	Majhgaon	TARTARIA	912000000000000000	912036800000000000	912036802745000000	912036802745115474	115474
Jharkhand	Pashchimi Singhbhum	Majhgaon	KHARPOS	912000000000000000	912036800000000000	912036802745000000	912036802745115469	115469
Jharkhand	Pashchimi Singhbhum	Majhgaon	GHORABANDHA	912000000000000000	912036800000000000	912036802745000000	912036802745115468	115468
Jharkhand	Pashchimi Singhbhum	Majhgaon	ANGARPADA	912000000000000000	912036800000000000	912036802745000000	912036802745115464	115464
Jharkhand	Pashchimi Singhbhum	Majhgaon	SONAPOSI	912000000000000000	912036800000000000	912036802745000000	912036802745115473	115473
Jharkhand	Pashchimi Singhbhum	Majhgaon	DHOBA DHOBIN	912000000000000000	912036800000000000	912036802745000000	912036802745115467	115467
Jharkhand	Pashchimi Singhbhum	Majhgaon	MAJHGAON	912000000000000000	912036800000000000	912036802745000000	912036802745115470	115470
Jharkhand	Pashchimi Singhbhum	Majhgaon	BALIAPOSI	912000000000000000	912036800000000000	912036802745000000	912036802745115466	115466
Jharkhand	Pashchimi Singhbhum	Majhgaon	ASANPAT	912000000000000000	912036800000000000	912036802745000000	912036802745115465	115465
Jharkhand	Pashchimi Singhbhum	Manjhari	PADSA	912000000000000000	912036800000000000	912036802741000000	912036802741115459	115459
Jharkhand	Pashchimi Singhbhum	Manjhari	BADA TORLO	912000000000000000	912036800000000000	912036802741000000	912036802741115454	115454
Jharkhand	Pashchimi Singhbhum	Manjhari	PANGA	912000000000000000	912036800000000000	912036802741000000	912036802741115460	115460
Jharkhand	Pashchimi Singhbhum	Manjhari	BHAGABILA	912000000000000000	912036800000000000	912036802741000000	912036802741115455	115455
Jharkhand	Pashchimi Singhbhum	Manjhari	BADA LAGDA	912000000000000000	912036800000000000	912036802741000000	912036802741115453	115453
Jharkhand	Pashchimi Singhbhum	Manjhari	PILKA	912000000000000000	912036800000000000	912036802741000000	912036802741115461	115461
Jharkhand	Pashchimi Singhbhum	Manjhari	BHARBHARIA	912000000000000000	912036800000000000	912036802741000000	912036802741115456	115456
Jharkhand	Pashchimi Singhbhum	Manjhari	MEROMHONER	912000000000000000	912036800000000000	912036802741000000	912036802741115458	115458
Jharkhand	Pashchimi Singhbhum	Manjhari	IPILSINGI	912000000000000000	912036800000000000	912036802741000000	912036802741115457	115457
Jharkhand	Pashchimi Singhbhum	Manjhari	ROLADIH	912000000000000000	912036800000000000	912036802741000000	912036802741115462	115462
Jharkhand	Pashchimi Singhbhum	Manoharpur	DIMBULI	912000000000000000	912036800000000000	912036802735000000	912036802735115483	115483
Jharkhand	Pashchimi Singhbhum	Manoharpur	DIGHA	912000000000000000	912036800000000000	912036802735000000	912036802735115482	115482
Jharkhand	Pashchimi Singhbhum	Manoharpur	ROBOKERA	912000000000000000	912036800000000000	912036802735000000	912036802735115494	115494
Jharkhand	Pashchimi Singhbhum	Manoharpur	CHHOTANAGRA	912000000000000000	912036800000000000	912036802735000000	912036802735115479	115479
Jharkhand	Pashchimi Singhbhum	Manoharpur	MAKRANDA	912000000000000000	912036800000000000	912036802735000000	912036802735115488	115488
Jharkhand	Pashchimi Singhbhum	Manoharpur	RAIDIH	912000000000000000	912036800000000000	912036802735000000	912036802735115492	115492
Jharkhand	Pashchimi Singhbhum	Manoharpur	GANGADA	912000000000000000	912036800000000000	912036802735000000	912036802735115484	115484
Jharkhand	Pashchimi Singhbhum	Manoharpur	LAILORE	912000000000000000	912036800000000000	912036802735000000	912036802735115487	115487
Jharkhand	Pashchimi Singhbhum	Manoharpur	CHIRIYA	912000000000000000	912036800000000000	912036802735000000	912036802735115480	115480
Jharkhand	Pashchimi Singhbhum	Manoharpur	KOLPOTKA	912000000000000000	912036800000000000	912036802735000000	912036802735115486	115486
Jharkhand	Pashchimi Singhbhum	Manoharpur	BARANGA	912000000000000000	912036800000000000	912036802735000000	912036802735115476	115476
Jharkhand	Pashchimi Singhbhum	Manoharpur	NANDPUR	912000000000000000	912036800000000000	912036802735000000	912036802735115491	115491
Jharkhand	Pashchimi Singhbhum	Manoharpur	MANOHARPUR (EAST)	912000000000000000	912036800000000000	912036802735000000	912036802735115489	115489
Jharkhand	Pashchimi Singhbhum	Manoharpur	RAIKERA	912000000000000000	912036800000000000	912036802735000000	912036802735115493	115493
Jharkhand	Pashchimi Singhbhum	Manoharpur	DHIPA	912000000000000000	912036800000000000	912036802735000000	912036802735115481	115481
Jharkhand	Pashchimi Singhbhum	Noamundi	DIRIBURU	912000000000000000	912036800000000000	912036802736000000	912036802736115498	115498
Jharkhand	Pashchimi Singhbhum	Noamundi	MEGHAHATUBURU (NORTH)	912000000000000000	912036800000000000	912036802736000000	912036802736115508	115508
Jharkhand	Pashchimi Singhbhum	Noamundi	KIRIBURU (WEST)	912000000000000000	912036800000000000	912036802736000000	912036802736115505	115505
Jharkhand	Pashchimi Singhbhum	Noamundi	KADAJAMDA	912000000000000000	912036800000000000	912036802736000000	912036802736115503	115503
Jharkhand	Pashchimi Singhbhum	Noamundi	KIRIBURU (EAST)	912000000000000000	912036800000000000	912036802736000000	912036802736115504	115504
Jharkhand	Pashchimi Singhbhum	Noamundi	KOTGARH	912000000000000000	912036800000000000	912036802736000000	912036802736115506	115506
Jharkhand	Pashchimi Singhbhum	Noamundi	NOAMUNDI	912000000000000000	912036800000000000	912036802736000000	912036802736115510	115510
Jharkhand	Pashchimi Singhbhum	Noamundi	DUDHBILA	912000000000000000	912036800000000000	912036802736000000	912036802736115499	115499
Jharkhand	Pashchimi Singhbhum	Noamundi	BARA JAMDA	912000000000000000	912036800000000000	912036802736000000	912036802736115497	115497
Jharkhand	Pashchimi Singhbhum	Noamundi	POKHARPI	912000000000000000	912036800000000000	912036802736000000	912036802736115512	115512
Jharkhand	Pashchimi Singhbhum	Noamundi	GUA (EAST)	912000000000000000	912036800000000000	912036802736000000	912036802736115500	115500
Jharkhand	Pashchimi Singhbhum	Noamundi	GUA (WEST)	912000000000000000	912036800000000000	912036802736000000	912036802736115501	115501
Jharkhand	Pashchimi Singhbhum	Noamundi	JETEYA	912000000000000000	912036800000000000	912036802736000000	912036802736115502	115502
Jharkhand	Pashchimi Singhbhum	Noamundi	PATAITA	912000000000000000	912036800000000000	912036802736000000	912036802736115511	115511
Jharkhand	Pashchimi Singhbhum	Noamundi	BADAPASEIYA	912000000000000000	912036800000000000	912036802736000000	912036802736115495	115495
Jharkhand	Pashchimi Singhbhum	Sonua	BALJODI	912000000000000000	912036800000000000	912036802728000000	912036802728115514	115514
Jharkhand	Pashchimi Singhbhum	Sonua	BARI	912000000000000000	912036800000000000	912036802728000000	912036802728115516	115516
Jharkhand	Pashchimi Singhbhum	Sonua	PORAHAT	912000000000000000	912036800000000000	912036802728000000	912036802728115527	115527
Jharkhand	Pashchimi Singhbhum	Sonua	DEWANBIR	912000000000000000	912036800000000000	912036802728000000	912036802728115521	115521
Jharkhand	Pashchimi Singhbhum	Sonua	BHALURUNGI	912000000000000000	912036800000000000	912036802728000000	912036802728115517	115517
Jharkhand	Pashchimi Singhbhum	Sonua	BOIKERA	912000000000000000	912036800000000000	912036802728000000	912036802728115519	115519
Jharkhand	Pashchimi Singhbhum	Sonua	SONAPOS	912000000000000000	912036800000000000	912036802728000000	912036802728115528	115528
Jharkhand	Pashchimi Singhbhum	Sonua	GOLMUNDA	912000000000000000	912036800000000000	912036802728000000	912036802728115522	115522
Jharkhand	Pashchimi Singhbhum	Sonua	BIRKEL	912000000000000000	912036800000000000	912036802728000000	912036802728115518	115518
Jharkhand	Pashchimi Singhbhum	Sonua	GOVINDPUR	912000000000000000	912036800000000000	912036802728000000	912036802728115523	115523
Jharkhand	Pashchimi Singhbhum	Sonua	ASANTALIA	912000000000000000	912036800000000000	912036802728000000	912036802728115513	115513
Jharkhand	Pashchimi Singhbhum	Sonua	LONJO	912000000000000000	912036800000000000	912036802728000000	912036802728115526	115526
Jharkhand	Pashchimi Singhbhum	Sonua	GULIKERA	912000000000000000	912036800000000000	912036802728000000	912036802728115524	115524
Jharkhand	Pashchimi Singhbhum	Tantnagar	TANTNAGAR	912000000000000000	912036800000000000	912036802740000000	912036802740115537	115537
Jharkhand	Pashchimi Singhbhum	Tantnagar	KHAS POKHRIA	912000000000000000	912036800000000000	912036802740000000	912036802740115533	115533
Jharkhand	Pashchimi Singhbhum	Tantnagar	KATHBHARI	912000000000000000	912036800000000000	912036802740000000	912036802740115532	115532
Jharkhand	Pashchimi Singhbhum	Tantnagar	CHITIMITI	912000000000000000	912036800000000000	912036802740000000	912036802740115530	115530
Jharkhand	Pashchimi Singhbhum	Tantnagar	TANGAR POKHRIA	912000000000000000	912036800000000000	912036802740000000	912036802740115536	115536
Jharkhand	Pashchimi Singhbhum	Tantnagar	TENTERA	912000000000000000	912036800000000000	912036802740000000	912036802740115538	115538
Jharkhand	Pashchimi Singhbhum	Tantnagar	PURNIA	912000000000000000	912036800000000000	912036802740000000	912036802740115535	115535
Jharkhand	Pashchimi Singhbhum	Tantnagar	KASIYA	912000000000000000	912036800000000000	912036802740000000	912036802740115531	115531
Jharkhand	Pashchimi Singhbhum	Tantnagar	ANGARDIHA	912000000000000000	912036800000000000	912036802740000000	912036802740115529	115529
Jharkhand	Pashchimi Singhbhum	Tantnagar	KOKCHO	912000000000000000	912036800000000000	912036802740000000	912036802740115534	115534
Jharkhand	Pashchimi Singhbhum	Tonto	RENGRAHATU	912000000000000000	912036800000000000	912036802737000000	912036802737115547	115547
Jharkhand	Pashchimi Singhbhum	Tonto	BUNDU	912000000000000000	912036800000000000	912036802737000000	912036802737115541	115541
Jharkhand	Pashchimi Singhbhum	Tonto	SIRINGSIYA	912000000000000000	912036800000000000	912036802737000000	912036802737115548	115548
Jharkhand	Pashchimi Singhbhum	Tonto	TONTO	912000000000000000	912036800000000000	912036802737000000	912036802737115549	115549
Jharkhand	Pashchimi Singhbhum	Tonto	BARA JHINKPANI	912000000000000000	912036800000000000	912036802737000000	912036802737115540	115540
Jharkhand	Pashchimi Singhbhum	Tonto	KENJRA	912000000000000000	912036800000000000	912036802737000000	912036802737115543	115543
Jharkhand	Pashchimi Singhbhum	Tonto	KONDWA	912000000000000000	912036800000000000	912036802737000000	912036802737115544	115544
Jharkhand	Pashchimi Singhbhum	Tonto	JODAPOKHAR	912000000000000000	912036800000000000	912036802737000000	912036802737115418	115418
Jharkhand	Pashchimi Singhbhum	Tonto	NIMDIH	912000000000000000	912036800000000000	912036802737000000	912036802737115545	115545
Jharkhand	Pashchimi Singhbhum	Tonto	PURANAPANI	912000000000000000	912036800000000000	912036802737000000	912036802737115546	115546
Jharkhand	Pashchimi Singhbhum	Tonto	BAMEBASA	912000000000000000	912036800000000000	912036802737000000	912036802737115539	115539
Odisha	Kalahandi	Biswanathpur	BANDHAPARI	912100000000000000	912139500000000000	912139503163000000	912139503163118788	118788
Odisha	Kalahandi	Biswanathpur	BEDAGAON	912100000000000000	912139500000000000	912139503163000000	912139503163275622	275622
Odisha	Kalahandi	Biswanathpur	KANKUTRU	912100000000000000	912139500000000000	912139503163000000	912139503163118800	118800
Odisha	Kalahandi	Biswanathpur	PANDAPADAR	912100000000000000	912139500000000000	912139503163000000	912139503163118806	118806
Odisha	Kalahandi	Biswanathpur	TRILOCHANPUR	912100000000000000	912139500000000000	912139503163000000	912139503163118808	118808
Odisha	Kalahandi	Biswanathpur	LANJI	912100000000000000	912139500000000000	912139503163000000	912139503163118802	118802
Odisha	Kalahandi	Biswanathpur	JUGSAIPATNA	912100000000000000	912139500000000000	912139503163000000	912139503163118607	118607
Odisha	Kalahandi	Biswanathpur	KUMKHAL	912100000000000000	912139500000000000	912139503163000000	912139503163275621	275621
Odisha	Kalahandi	Biswanathpur	LAKHABAHALI	912100000000000000	912139500000000000	912139503163000000	912139503163118801	118801
Odisha	Kalahandi	Biswanathpur	BIJEPUR	912100000000000000	912139500000000000	912139503163000000	912139503163118793	118793
Odisha	Kalahandi	Biswanathpur	BISWANATHPUR	912100000000000000	912139500000000000	912139503163000000	912139503163118794	118794
Odisha	Kalahandi	Biswanathpur	MALIJUBANGA	912100000000000000	912139500000000000	912139503163000000	912139503163118805	118805
Odisha	Kalahandi	Biswanathpur	TAL-BELGAON	912100000000000000	912139500000000000	912139503163000000	912139503163118624	118624
Odisha	Kalahandi	Biswanathpur	POKHARIBANDH	912100000000000000	912139500000000000	912139503163000000	912139503163118807	118807
Odisha	Kalahandi	Biswanathpur	CHHATRAPUR	912100000000000000	912139500000000000	912139503163000000	912139503163118796	118796
Odisha	Kalahandi	Dharamgarh	KALOPALA	912100000000000000	912139500000000000	912139503168000000	912139503168118706	118706
Odisha	Kalahandi	Dharamgarh	NANDAGAON	912100000000000000	912139500000000000	912139503168000000	912139503168118640	118640
Odisha	Kalahandi	Dharamgarh	BADBAFLA	912100000000000000	912139500000000000	912139503168000000	912139503168275647	275647
Odisha	Kalahandi	Dharamgarh	BODEN	912100000000000000	912139500000000000	912139503168000000	912139503168118628	118628
Odisha	Kalahandi	Dharamgarh	KADALIMUNDA	912100000000000000	912139500000000000	912139503168000000	912139503168275646	275646
Odisha	Kalahandi	Dharamgarh	PARLA	912100000000000000	912139500000000000	912139503168000000	912139503168118641	118641
Odisha	Kalahandi	Dharamgarh	GADIAJORE	912100000000000000	912139500000000000	912139503168000000	912139503168118635	118635
Odisha	Kalahandi	Dharamgarh	BAD GHUMER	912100000000000000	912139500000000000	912139503168000000	912139503168275644	275644
Odisha	Kalahandi	Dharamgarh	BADBASUL	912100000000000000	912139500000000000	912139503168000000	912139503168118594	118594
Odisha	Kalahandi	Dharamgarh	BAGAD	912100000000000000	912139500000000000	912139503168000000	912139503168275645	275645
Odisha	Kalahandi	Dharamgarh	KHAIRAPADAR	912100000000000000	912139500000000000	912139503168000000	912139503168118639	118639
Odisha	Kalahandi	Dharamgarh	CHHILPA	912100000000000000	912139500000000000	912139503168000000	912139503168118631	118631
Odisha	Kalahandi	Dharamgarh	BRAMHANCHHENDIA	912100000000000000	912139500000000000	912139503168000000	912139503168118629	118629
Odisha	Kalahandi	Dharamgarh	TAMBACHHADA	912100000000000000	912139500000000000	912139503168000000	912139503168118643	118643
Odisha	Kalahandi	Dharamgarh	KANGAON	912100000000000000	912139500000000000	912139503168000000	912139503168118637	118637
Odisha	Kalahandi	Dharamgarh	SANDHIKULIHARI	912100000000000000	912139500000000000	912139503168000000	912139503168118642	118642
Odisha	Kalahandi	Dharamgarh	KANKERI	912100000000000000	912139500000000000	912139503168000000	912139503168118638	118638
Odisha	Kalahandi	Dharamgarh	TIPIGUDA	912100000000000000	912139500000000000	912139503168000000	912139503168118646	118646
Odisha	Kalahandi	Dharamgarh	DUMARGUDA	912100000000000000	912139500000000000	912139503168000000	912139503168118634	118634
Odisha	Kalahandi	Golamunda	KHAMBALDI	912100000000000000	912139500000000000	912139503167000000	912139503167118662	118662
Odisha	Kalahandi	Golamunda	CHICHIA	912100000000000000	912139500000000000	912139503167000000	912139503167118652	118652
Odisha	Kalahandi	Golamunda	TEMRI	912100000000000000	912139500000000000	912139503167000000	912139503167275651	275651
Odisha	Kalahandi	Golamunda	FUNDA	912100000000000000	912139500000000000	912139503167000000	912139503167118656	118656
Odisha	Kalahandi	Golamunda	DUMARGUDA	912100000000000000	912139500000000000	912139503167000000	912139503167118634	118634
Odisha	Kalahandi	Golamunda	NAKTIKANI	912100000000000000	912139500000000000	912139503167000000	912139503167118666	118666
Odisha	Kalahandi	Golamunda	DASPUR	912100000000000000	912139500000000000	912139503167000000	912139503167118653	118653
Odisha	Kalahandi	Golamunda	FARANGA	912100000000000000	912139500000000000	912139503167000000	912139503167118655	118655
Odisha	Kalahandi	Golamunda	BORGUDA	912100000000000000	912139500000000000	912139503167000000	912139503167118648	118648
Odisha	Kalahandi	Golamunda	RENGSAPALI	912100000000000000	912139500000000000	912139503167000000	912139503167118668	118668
Odisha	Kalahandi	Golamunda	GANDEMER	912100000000000000	912139500000000000	912139503167000000	912139503167118657	118657
Odisha	Kalahandi	Golamunda	DHAMANPUR	912100000000000000	912139500000000000	912139503167000000	912139503167118654	118654
Odisha	Kalahandi	Golamunda	BRUNDABAHAL	912100000000000000	912139500000000000	912139503167000000	912139503167118649	118649
Odisha	Kalahandi	Golamunda	DHANARPUR	912100000000000000	912139500000000000	912139503167000000	912139503167118632	118632
Odisha	Kalahandi	Golamunda	MALIGUDA	912100000000000000	912139500000000000	912139503167000000	912139503167118709	118709
Odisha	Kalahandi	Golamunda	CHICHEIGUDA	912100000000000000	912139500000000000	912139503167000000	912139503167118698	118698
Odisha	Kalahandi	Golamunda	SANDHIKULIHARI	912100000000000000	912139500000000000	912139503167000000	912139503167118642	118642
Odisha	Kalahandi	Golamunda	KHALIAKANI	912100000000000000	912139500000000000	912139503167000000	912139503167118660	118660
Odisha	Kalahandi	Golamunda	DUNDEIMAL	912100000000000000	912139500000000000	912139503167000000	912139503167118701	118701
Odisha	Kalahandi	Golamunda	GOLAMUNDA	912100000000000000	912139500000000000	912139503167000000	912139503167118658	118658
Odisha	Kalahandi	Golamunda	MANJHARI	912100000000000000	912139500000000000	912139503167000000	912139503167118665	118665
Odisha	Kalahandi	Golamunda	UDESHRUNGA	912100000000000000	912139500000000000	912139503167000000	912139503167275649	275649
Odisha	Kalahandi	Jayapatna	DHANSULI	912100000000000000	912139500000000000	912139503170000000	912139503170118677	118677
Odisha	Kalahandi	Jayapatna	JAIPATNA	912100000000000000	912139500000000000	912139503170000000	912139503170118679	118679
Odisha	Kalahandi	Jayapatna	KARMEGAON	912100000000000000	912139500000000000	912139503170000000	912139503170118833	118833
Odisha	Kalahandi	Jayapatna	MANGALPUR	912100000000000000	912139500000000000	912139503170000000	912139503170118681	118681
Odisha	Kalahandi	Jayapatna	MUKHIGUDA	912100000000000000	912139500000000000	912139503170000000	912139503170118682	118682
Odisha	Kalahandi	Jayapatna	HIRAPUR	912100000000000000	912139500000000000	912139503170000000	912139503170118678	118678
Odisha	Kalahandi	Jayapatna	BADKARLAKOT	912100000000000000	912139500000000000	912139503170000000	912139503170118673	118673
Odisha	Kalahandi	Jayapatna	NUAGAON	912100000000000000	912139500000000000	912139503170000000	912139503170275637	275637
Odisha	Kalahandi	Jayapatna	PAIKKANDMUNDI	912100000000000000	912139500000000000	912139503170000000	912139503170118683	118683
Odisha	Kalahandi	Jayapatna	PRATAPPUR	912100000000000000	912139500000000000	912139503170000000	912139503170118684	118684
Odisha	Kalahandi	Jayapatna	SARGIGUDA	912100000000000000	912139500000000000	912139503170000000	912139503170118687	118687
Odisha	Kalahandi	Jayapatna	AINABHATA	912100000000000000	912139500000000000	912139503170000000	912139503170118671	118671
Odisha	Kalahandi	Jayapatna	KUCHAGAON	912100000000000000	912139500000000000	912139503170000000	912139503170118680	118680
Odisha	Kalahandi	Jayapatna	BANER	912100000000000000	912139500000000000	912139503170000000	912139503170118675	118675
Odisha	Kalahandi	Jayapatna	BIJAMARA	912100000000000000	912139500000000000	912139503170000000	912139503170118722	118722
Odisha	Kalahandi	Jayapatna	KHALIABHATA	912100000000000000	912139500000000000	912139503170000000	912139503170275638	275638
Odisha	Kalahandi	Jayapatna	BADATEMARI	912100000000000000	912139500000000000	912139503170000000	912139503170275641	275641
Odisha	Kalahandi	Jayapatna	SINDHIKAGUDA	912100000000000000	912139500000000000	912139503170000000	912139503170275639	275639
Odisha	Kalahandi	Jayapatna	BANDHAKANA	912100000000000000	912139500000000000	912139503170000000	912139503170118721	118721
Odisha	Kalahandi	Jayapatna	HERMAL	912100000000000000	912139500000000000	912139503170000000	912139503170118725	118725
Odisha	Kalahandi	Jayapatna	UCHALA	912100000000000000	912139500000000000	912139503170000000	912139503170118688	118688
Odisha	Kalahandi	Jayapatna	BADAPUJARIGUDA	912100000000000000	912139500000000000	912139503170000000	912139503170118672	118672
Odisha	Kalahandi	Jayapatna	BHAINRIPALI	912100000000000000	912139500000000000	912139503170000000	912139503170118676	118676
Odisha	Kalahandi	Jayapatna	RENGALIPALLI	912100000000000000	912139500000000000	912139503170000000	912139503170118686	118686
Odisha	Kalahandi	Jayapatna	KAKLAMPUR	912100000000000000	912139500000000000	912139503170000000	912139503170118726	118726
Odisha	Kalahandi	Jayapatna	BODKUTURU	912100000000000000	912139500000000000	912139503170000000	912139503170118723	118723
Odisha	Kalahandi	Jayapatna	PANDIGAON	912100000000000000	912139500000000000	912139503170000000	912139503170118729	118729
Odisha	Kalahandi	Jayapatna	BANDIGAON	912100000000000000	912139500000000000	912139503170000000	912139503170118674	118674
Odisha	Kalahandi	Jayapatna	MINIPUR	912100000000000000	912139500000000000	912139503170000000	912139503170118728	118728
Odisha	Kalahandi	Jayapatna	BHEJIGUDA	912100000000000000	912139500000000000	912139503170000000	912139503170275640	275640
Odisha	Kalahandi	Jayapatna	DEYPUR	912100000000000000	912139500000000000	912139503170000000	912139503170118724	118724
Odisha	Kalahandi	Jayapatna	RANAMAL	912100000000000000	912139500000000000	912139503170000000	912139503170118685	118685
Odisha	Kalahandi	Jayapatna	MANDAL	912100000000000000	912139500000000000	912139503170000000	912139503170118727	118727
Odisha	Kalahandi	Jayapatna	BALAGAON	912100000000000000	912139500000000000	912139503170000000	912139503170275636	275636
Odisha	Kalahandi	Junagarh	MERIABANDHALI	912100000000000000	912139500000000000	912139503166000000	912139503166118712	118712
Odisha	Kalahandi	Junagarh	BUDHIPADAR	912100000000000000	912139500000000000	912139503166000000	912139503166118694	118694
Odisha	Kalahandi	Junagarh	DUNDEIMAL	912100000000000000	912139500000000000	912139503166000000	912139503166118701	118701
Odisha	Kalahandi	Junagarh	DASIGAON	912100000000000000	912139500000000000	912139503166000000	912139503166118699	118699
Odisha	Kalahandi	Junagarh	GOUDCHENDIA	912100000000000000	912139500000000000	912139503166000000	912139503166118702	118702
Odisha	Kalahandi	Junagarh	MUNDARAGUDA	912100000000000000	912139500000000000	912139503166000000	912139503166118713	118713
Odisha	Kalahandi	Junagarh	HABASPUR	912100000000000000	912139500000000000	912139503166000000	912139503166118703	118703
Odisha	Kalahandi	Junagarh	RAJPUR	912100000000000000	912139500000000000	912139503166000000	912139503166118717	118717
Odisha	Kalahandi	Junagarh	BANKAPALAS	912100000000000000	912139500000000000	912139503166000000	912139503166118691	118691
Odisha	Kalahandi	Junagarh	DHANSULI	912100000000000000	912139500000000000	912139503166000000	912139503166118677	118677
Odisha	Kalahandi	Junagarh	MANIJORA	912100000000000000	912139500000000000	912139503166000000	912139503166118710	118710
Odisha	Kalahandi	Junagarh	CHINGUDISAR	912100000000000000	912139500000000000	912139503166000000	912139503166275653	275653
Odisha	Kalahandi	Junagarh	KALOPALA	912100000000000000	912139500000000000	912139503166000000	912139503166118706	118706
Odisha	Kalahandi	Junagarh	DHAMANPUR	912100000000000000	912139500000000000	912139503166000000	912139503166118654	118654
Odisha	Kalahandi	Junagarh	MAHICHAL;A	912100000000000000	912139500000000000	912139503166000000	912139503166118708	118708
Odisha	Kalahandi	Junagarh	RENGSAPALI	912100000000000000	912139500000000000	912139503166000000	912139503166118668	118668
Odisha	Kalahandi	Junagarh	MALIGUDA	912100000000000000	912139500000000000	912139503166000000	912139503166118709	118709
Odisha	Kalahandi	Junagarh	DEDARH	912100000000000000	912139500000000000	912139503166000000	912139503166118700	118700
Odisha	Kalahandi	Junagarh	SANYASIKUNDAMAL	912100000000000000	912139500000000000	912139503166000000	912139503166118718	118718
Odisha	Kalahandi	Junagarh	MATIGAON	912100000000000000	912139500000000000	912139503166000000	912139503166118711	118711
Odisha	Kalahandi	Junagarh	PALASH	912100000000000000	912139500000000000	912139503166000000	912139503166118716	118716
Odisha	Kalahandi	Junagarh	KALAIGAON	912100000000000000	912139500000000000	912139503166000000	912139503166118704	118704
Odisha	Kalahandi	Junagarh	CHARBAHATI	912100000000000000	912139500000000000	912139503166000000	912139503166118696	118696
Odisha	Kalahandi	Junagarh	CHHURIAGAON	912100000000000000	912139500000000000	912139503166000000	912139503166118697	118697
Odisha	Kalahandi	Junagarh	BALADIAMAL	912100000000000000	912139500000000000	912139503166000000	912139503166118690	118690
Odisha	Kalahandi	Junagarh	NAKTIGUDA	912100000000000000	912139500000000000	912139503166000000	912139503166118714	118714
Odisha	Kalahandi	Junagarh	CHICHEIGUDA	912100000000000000	912139500000000000	912139503166000000	912139503166118698	118698
Odisha	Kalahandi	Junagarh	BAXITULISIPALI	912100000000000000	912139500000000000	912139503166000000	912139503166118692	118692
Odisha	Kalahandi	Junagarh	PILIKIA	912100000000000000	912139500000000000	912139503166000000	912139503166275652	275652
Odisha	Kalahandi	Junagarh	\N	912100000000000000	912139500000000000	912139503166000000	912139503166250560	250560
Odisha	Kalahandi	Junagarh	TALJARING	912100000000000000	912139500000000000	912139503166000000	912139503166118720	118720
Odisha	Kalahandi	Junagarh	KALIAKUNDAL	912100000000000000	912139500000000000	912139503166000000	912139503166118705	118705
Odisha	Kalahandi	Junagarh	BHAINRIGUDA	912100000000000000	912139500000000000	912139503166000000	912139503166118693	118693
Odisha	Kalahandi	Junagarh	ASTTAGAON	912100000000000000	912139500000000000	912139503166000000	912139503166118689	118689
Odisha	Kalahandi	Junagarh	TALAMALA	912100000000000000	912139500000000000	912139503166000000	912139503166118719	118719
Odisha	Kalahandi	Junagarh	NANDOL	912100000000000000	912139500000000000	912139503166000000	912139503166118715	118715
Odisha	Kalahandi	Junagarh	KENDUPATI	912100000000000000	912139500000000000	912139503166000000	912139503166118707	118707
Odisha	Kalahandi	Kegaon	BADACHERGAON	912100000000000000	912139500000000000	912139503165000000	912139503165118647	118647
Odisha	Kalahandi	Kegaon	NUAGAON	912100000000000000	912139500000000000	912139503165000000	912139503165118667	118667
Odisha	Kalahandi	Kegaon	KHALIAPALI	912100000000000000	912139500000000000	912139503165000000	912139503165118661	118661
Odisha	Kalahandi	Kegaon	CHAPRIA	912100000000000000	912139500000000000	912139503165000000	912139503165118651	118651
Odisha	Kalahandi	Kegaon	UCHHALA	912100000000000000	912139500000000000	912139503165000000	912139503165118670	118670
Odisha	Kalahandi	Kegaon	CHAHAKA	912100000000000000	912139500000000000	912139503165000000	912139503165118650	118650
Odisha	Kalahandi	Kegaon	KEGAON	912100000000000000	912139500000000000	912139503165000000	912139503165118659	118659
Odisha	Kalahandi	Kegaon	PALNA	912100000000000000	912139500000000000	912139503165000000	912139503165118619	118619
Odisha	Kalahandi	Kegaon	ARTAL	912100000000000000	912139500000000000	912139503165000000	912139503165118593	118593
Odisha	Kalahandi	Kegaon	MAHALING	912100000000000000	912139500000000000	912139503165000000	912139503165118664	118664
Odisha	Kalahandi	Kegaon	SINAPALI	912100000000000000	912139500000000000	912139503165000000	912139503165118669	118669
Odisha	Kalahandi	Kegaon	SIKUN	912100000000000000	912139500000000000	912139503165000000	912139503165275613	275613
Odisha	Kalahandi	Kegaon	SEINPUR	912100000000000000	912139500000000000	912139503165000000	912139503165118623	118623
Odisha	Kalahandi	Kegaon	LETER	912100000000000000	912139500000000000	912139503165000000	912139503165275648	275648
Odisha	Kalahandi	Kegaon	BORDI	912100000000000000	912139500000000000	912139503165000000	912139503165275650	275650
Odisha	Kalahandi	Kegaon	DUMURIA	912100000000000000	912139500000000000	912139503165000000	912139503165118603	118603
Odisha	Kalahandi	Kegaon	GURJUNG	912100000000000000	912139500000000000	912139503165000000	912139503165118606	118606
Odisha	Kalahandi	Kegaon	KUHURA	912100000000000000	912139500000000000	912139503165000000	912139503165118663	118663
Odisha	Kalahandi	Kegaon	BORDA	912100000000000000	912139500000000000	912139503165000000	912139503165118596	118596
Odisha	Kalahandi	Kegaon	MADIGUDA	912100000000000000	912139500000000000	912139503165000000	912139503165118615	118615
Odisha	Kalahandi	Kegaon	KENDUPATI	912100000000000000	912139500000000000	912139503165000000	912139503165118612	118612
Odisha	Kalahandi	Kegaon	MATIA	912100000000000000	912139500000000000	912139503165000000	912139503165118617	118617
Odisha	Kalahandi	Kegaon	BORABHATA	912100000000000000	912139500000000000	912139503165000000	912139503165118595	118595
Odisha	Kalahandi	Kegaon	KULIAMAL	912100000000000000	912139500000000000	912139503165000000	912139503165118613	118613
Odisha	Kalahandi	Kesinga	GAIAGAON	912100000000000000	912139500000000000	912139503159000000	912139503159118748	118748
Odisha	Kalahandi	Kesinga	CHANCHER	912100000000000000	912139500000000000	912139503159000000	912139503159118746	118746
Odisha	Kalahandi	Kesinga	PALAN	912100000000000000	912139500000000000	912139503159000000	912139503159118839	118839
Odisha	Kalahandi	Kesinga	GOKULESWAR	912100000000000000	912139500000000000	912139503159000000	912139503159118749	118749
Odisha	Kalahandi	Kesinga	KANDEL	912100000000000000	912139500000000000	912139503159000000	912139503159118751	118751
Odisha	Kalahandi	Kesinga	DEOGAON	912100000000000000	912139500000000000	912139503159000000	912139503159118747	118747
Odisha	Kalahandi	Kesinga	UTKELA	912100000000000000	912139500000000000	912139503159000000	912139503159118767	118767
Odisha	Kalahandi	Kesinga	KARMEGAON	912100000000000000	912139500000000000	912139503159000000	912139503159118833	118833
Odisha	Kalahandi	Kesinga	PARALASINGA	912100000000000000	912139500000000000	912139503159000000	912139503159118759	118759
Odisha	Kalahandi	Kesinga	BALSI	912100000000000000	912139500000000000	912139503159000000	912139503159118743	118743
Odisha	Kalahandi	Kesinga	BORIA	912100000000000000	912139500000000000	912139503159000000	912139503159118745	118745
Odisha	Kalahandi	Kesinga	RUPRA	912100000000000000	912139500000000000	912139503159000000	912139503159118841	118841
Odisha	Kalahandi	Kesinga	KIKIA	912100000000000000	912139500000000000	912139503159000000	912139503159118754	118754
Odisha	Kalahandi	Kesinga	KUNDABANDH	912100000000000000	912139500000000000	912139503159000000	912139503159118755	118755
Odisha	Kalahandi	Kesinga	SIROL	912100000000000000	912139500000000000	912139503159000000	912139503159118764	118764
Odisha	Kalahandi	Kesinga	LAITARA	912100000000000000	912139500000000000	912139503159000000	912139503159118756	118756
Odisha	Kalahandi	Kesinga	KASRUPADA	912100000000000000	912139500000000000	912139503159000000	912139503159118753	118753
Odisha	Kalahandi	Kesinga	MUSKUTI	912100000000000000	912139500000000000	912139503159000000	912139503159118836	118836
Odisha	Kalahandi	Kesinga	GIGINA	912100000000000000	912139500000000000	912139503159000000	912139503159118832	118832
Odisha	Kalahandi	Kesinga	PATHARLA	912100000000000000	912139500000000000	912139503159000000	912139503159118761	118761
Odisha	Kalahandi	Kesinga	TAKARLA	912100000000000000	912139500000000000	912139503159000000	912139503159118847	118847
Odisha	Kalahandi	Kesinga	\N	912100000000000000	912139500000000000	912139503159000000	912139503159250559	250559
Odisha	Kalahandi	Kesinga	KANTESIR	912100000000000000	912139500000000000	912139503159000000	912139503159118752	118752
Odisha	Kalahandi	Kesinga	TUNDALA	912100000000000000	912139500000000000	912139503159000000	912139503159118765	118765
Odisha	Kalahandi	Kesinga	NUNMATH	912100000000000000	912139500000000000	912139503159000000	912139503159118758	118758
Odisha	Kalahandi	Kesinga	HATRIKHOJ	912100000000000000	912139500000000000	912139503159000000	912139503159118750	118750
Odisha	Kalahandi	Kesinga	TURLAKHAMAN	912100000000000000	912139500000000000	912139503159000000	912139503159118766	118766
Odisha	Kalahandi	Kesinga	SIRJAPALI	912100000000000000	912139500000000000	912139503159000000	912139503159118763	118763
Odisha	Kalahandi	Kesinga	BELAKHANDI	912100000000000000	912139500000000000	912139503159000000	912139503159118744	118744
Odisha	Kalahandi	Kokasara	BARADANGA	912100000000000000	912139500000000000	912139503169000000	912139503169118770	118770
Odisha	Kalahandi	Kokasara	AMPANI	912100000000000000	912139500000000000	912139503169000000	912139503169118768	118768
Odisha	Kalahandi	Kokasara	LADUGAON	912100000000000000	912139500000000000	912139503169000000	912139503169118780	118780
Odisha	Kalahandi	Kokasara	KANDHABUTARA	912100000000000000	912139500000000000	912139503169000000	912139503169275643	275643
Odisha	Kalahandi	Kokasara	BADAPODAGUDA	912100000000000000	912139500000000000	912139503169000000	912139503169118769	118769
Odisha	Kalahandi	Kokasara	KASHIBAHAL	912100000000000000	912139500000000000	912139503169000000	912139503169118776	118776
Odisha	Kalahandi	Kokasara	GAMBHARIGUDA	912100000000000000	912139500000000000	912139503169000000	912139503169118774	118774
Odisha	Kalahandi	Kokasara	DUDKATHENGA	912100000000000000	912139500000000000	912139503169000000	912139503169118773	118773
Odisha	Kalahandi	Kokasara	KOUDALA	912100000000000000	912139500000000000	912139503169000000	912139503169118779	118779
Odisha	Kalahandi	Kokasara	PHUPAGAON	912100000000000000	912139500000000000	912139503169000000	912139503169118785	118785
Odisha	Kalahandi	Kokasara	TEMRA	912100000000000000	912139500000000000	912139503169000000	912139503169118787	118787
Odisha	Kalahandi	Kokasara	MOHIMA	912100000000000000	912139500000000000	912139503169000000	912139503169118782	118782
Odisha	Kalahandi	Kokasara	KOKSARA	912100000000000000	912139500000000000	912139503169000000	912139503169118778	118778
Odisha	Kalahandi	Kokasara	MOTER	912100000000000000	912139500000000000	912139503169000000	912139503169118783	118783
Odisha	Kalahandi	Kokasara	RANGALIPALI	912100000000000000	912139500000000000	912139503169000000	912139503169118786	118786
Odisha	Kalahandi	Kokasara	MAJHIGUDA	912100000000000000	912139500000000000	912139503169000000	912139503169118781	118781
Odisha	Kalahandi	Kokasara	DALGUMA	912100000000000000	912139500000000000	912139503169000000	912139503169118772	118772
Odisha	Kalahandi	Kokasara	GOTAMUNDA	912100000000000000	912139500000000000	912139503169000000	912139503169118775	118775
Odisha	Kalahandi	Kokasara	DAHAGAON	912100000000000000	912139500000000000	912139503169000000	912139503169118771	118771
Odisha	Kalahandi	Kokasara	TENDAPALI	912100000000000000	912139500000000000	912139503169000000	912139503169118645	118645
Odisha	Kalahandi	Kokasara	PODAGUDA	912100000000000000	912139500000000000	912139503169000000	912139503169275642	275642
Odisha	Kalahandi	Kokasara	MUSAPALI	912100000000000000	912139500000000000	912139503169000000	912139503169118784	118784
Odisha	Kalahandi	Kokasara	TARAPUR	912100000000000000	912139500000000000	912139503169000000	912139503169118644	118644
Odisha	Kalahandi	Kokasara	CHARBAHAL	912100000000000000	912139500000000000	912139503169000000	912139503169118695	118695
Odisha	Kalahandi	Kokasara	BEHERA	912100000000000000	912139500000000000	912139503169000000	912139503169118627	118627
Odisha	Kalahandi	Kokasara	KHUNTIA	912100000000000000	912139500000000000	912139503169000000	912139503169118777	118777
Odisha	Kalahandi	Kokasara	CHHANCHANBAHALI	912100000000000000	912139500000000000	912139503169000000	912139503169118630	118630
Odisha	Kalahandi	Kokasara	JAYANTAPUR	912100000000000000	912139500000000000	912139503169000000	912139503169118636	118636
Odisha	Kalahandi	Kokasara	DHANARPUR	912100000000000000	912139500000000000	912139503169000000	912139503169118632	118632
Odisha	Kalahandi	Lanjigarh	TRILOCHANPUR	912100000000000000	912139500000000000	912139503162000000	912139503162118808	118808
Odisha	Kalahandi	Lanjigarh	LAKHABAHALI	912100000000000000	912139500000000000	912139503162000000	912139503162118801	118801
Odisha	Kalahandi	Lanjigarh	KUMKHAL	912100000000000000	912139500000000000	912139503162000000	912139503162275621	275621
Odisha	Kalahandi	Lanjigarh	BIJEPUR	912100000000000000	912139500000000000	912139503162000000	912139503162118793	118793
Odisha	Kalahandi	Lanjigarh	BENAGAON	912100000000000000	912139500000000000	912139503162000000	912139503162118790	118790
Odisha	Kalahandi	Lanjigarh	PANDAPADAR	912100000000000000	912139500000000000	912139503162000000	912139503162118806	118806
Odisha	Kalahandi	Lanjigarh	MALIJUBANGA	912100000000000000	912139500000000000	912139503162000000	912139503162118805	118805
Odisha	Kalahandi	Lanjigarh	KANKUTRU	912100000000000000	912139500000000000	912139503162000000	912139503162118800	118800
Odisha	Kalahandi	Lanjigarh	JAGANNATHPUR	912100000000000000	912139500000000000	912139503162000000	912139503162275626	275626
Odisha	Kalahandi	Lanjigarh	BEDAGAON	912100000000000000	912139500000000000	912139503162000000	912139503162275622	275622
Odisha	Kalahandi	Lanjigarh	BANDHAPARI	912100000000000000	912139500000000000	912139503162000000	912139503162118788	118788
Odisha	Kalahandi	Lanjigarh	LANJI	912100000000000000	912139500000000000	912139503162000000	912139503162118802	118802
Odisha	Kalahandi	Lanjigarh	CHHATRAPUR	912100000000000000	912139500000000000	912139503162000000	912139503162118796	118796
Odisha	Kalahandi	Lanjigarh	BASANTAPADA	912100000000000000	912139500000000000	912139503162000000	912139503162275625	275625
Odisha	Kalahandi	Lanjigarh	KADAMGUDA	912100000000000000	912139500000000000	912139503162000000	912139503162275624	275624
Odisha	Kalahandi	Lanjigarh	LANJIGARH	912100000000000000	912139500000000000	912139503162000000	912139503162118803	118803
Odisha	Kalahandi	Lanjigarh	JUGSAIPATNA	912100000000000000	912139500000000000	912139503162000000	912139503162118607	118607
Odisha	Kalahandi	Lanjigarh	BHURTIGARH	912100000000000000	912139500000000000	912139503162000000	912139503162118792	118792
Odisha	Kalahandi	Lanjigarh	BISWANATHPUR	912100000000000000	912139500000000000	912139503162000000	912139503162118794	118794
Odisha	Kalahandi	Lanjigarh	BATERIMA	912100000000000000	912139500000000000	912139503162000000	912139503162118789	118789
Odisha	Kalahandi	Lanjigarh	CHAMPADEIPUR	912100000000000000	912139500000000000	912139503162000000	912139503162118795	118795
Odisha	Kalahandi	Lanjigarh	KAMARADA	912100000000000000	912139500000000000	912139503162000000	912139503162118799	118799
Odisha	Kalahandi	Madanpur Rampur	DUDKARANJA	912100000000000000	912139500000000000	912139503160000000	912139503160275659	275659
Odisha	Kalahandi	Madanpur Rampur	MANIKERA	912100000000000000	912139500000000000	912139503160000000	912139503160118816	118816
Odisha	Kalahandi	Madanpur Rampur	GAJABAHAL	912100000000000000	912139500000000000	912139503160000000	912139503160118731	118731
Odisha	Kalahandi	Madanpur Rampur	BARABANDHA	912100000000000000	912139500000000000	912139503160000000	912139503160118811	118811
Odisha	Kalahandi	Madanpur Rampur	URLADANI	912100000000000000	912139500000000000	912139503160000000	912139503160118823	118823
Odisha	Kalahandi	Madanpur Rampur	M.RAMPUR	912100000000000000	912139500000000000	912139503160000000	912139503160118814	118814
Odisha	Kalahandi	Madanpur Rampur	LUBENGADA	912100000000000000	912139500000000000	912139503160000000	912139503160275655	275655
Odisha	Kalahandi	Madanpur Rampur	DEDSULI	912100000000000000	912139500000000000	912139503160000000	912139503160275654	275654
Odisha	Kalahandi	Madanpur Rampur	PANDAKAMAL	912100000000000000	912139500000000000	912139503160000000	912139503160118820	118820
Odisha	Kalahandi	Madanpur Rampur	NUNPUR	912100000000000000	912139500000000000	912139503160000000	912139503160118819	118819
Odisha	Kalahandi	Madanpur Rampur	SALEPALI	912100000000000000	912139500000000000	912139503160000000	912139503160118821	118821
Odisha	Kalahandi	Madanpur Rampur	MUDING	912100000000000000	912139500000000000	912139503160000000	912139503160118818	118818
Odisha	Kalahandi	Madanpur Rampur	JORADOBRA	912100000000000000	912139500000000000	912139503160000000	912139503160118732	118732
Odisha	Kalahandi	Madanpur Rampur	MOHANGIRI	912100000000000000	912139500000000000	912139503160000000	912139503160118817	118817
Odisha	Kalahandi	Madanpur Rampur	SAIDALANGA	912100000000000000	912139500000000000	912139503160000000	912139503160118822	118822
Odisha	Kalahandi	Madanpur Rampur	BAMAK	912100000000000000	912139500000000000	912139503160000000	912139503160118810	118810
Odisha	Kalahandi	Madanpur Rampur	DAMKARLAKHUNTA	912100000000000000	912139500000000000	912139503160000000	912139503160118812	118812
Odisha	Kalahandi	Madanpur Rampur	BORAPADAR	912100000000000000	912139500000000000	912139503160000000	912139503160118730	118730
Odisha	Kalahandi	Madanpur Rampur	MADANPUR	912100000000000000	912139500000000000	912139503160000000	912139503160118815	118815
Odisha	Kalahandi	Madanpur Rampur	SAPLAHARA	912100000000000000	912139500000000000	912139503160000000	912139503160118739	118739
Odisha	Kalahandi	Madanpur Rampur	SINGPUR	912100000000000000	912139500000000000	912139503160000000	912139503160275614	275614
Odisha	Kalahandi	Madanpur Rampur	REGADA	912100000000000000	912139500000000000	912139503160000000	912139503160118736	118736
Odisha	Kalahandi	Madanpur Rampur	GOCHHANDENGAON	912100000000000000	912139500000000000	912139503160000000	912139503160118813	118813
Odisha	Kalahandi	Madanpur Rampur	ALATARA	912100000000000000	912139500000000000	912139503160000000	912139503160118809	118809
Odisha	Kalahandi	Madanpur Rampur	RINJA	912100000000000000	912139500000000000	912139503160000000	912139503160118737	118737
Odisha	Kalahandi	Madanpur Rampur	RAJPUR	912100000000000000	912139500000000000	912139503160000000	912139503160118735	118735
Odisha	Kalahandi	Madanpur Rampur	SARGULMALPADA	912100000000000000	912139500000000000	912139503160000000	912139503160118740	118740
Odisha	Kalahandi	Madanpur Rampur	KARLAMUNDA	912100000000000000	912139500000000000	912139503160000000	912139503160118733	118733
Odisha	Kalahandi	Madanpur Rampur	TERESINGA	912100000000000000	912139500000000000	912139503160000000	912139503160118741	118741
Odisha	Kalahandi	Madanpur Rampur	RISIDA	912100000000000000	912139500000000000	912139503160000000	912139503160118738	118738
Odisha	Kalahandi	Madanpur Rampur	SAKOSINGHA	912100000000000000	912139500000000000	912139503160000000	912139503160115625	115625
Odisha	Kalahandi	Madanpur Rampur	POURKELA	912100000000000000	912139500000000000	912139503160000000	912139503160118734	118734
Odisha	Kalahandi	Narala	NISANGPUR	912100000000000000	912139500000000000	912139503161000000	912139503161118838	118838
Odisha	Kalahandi	Narala	BAD-DHARPUR	912100000000000000	912139500000000000	912139503161000000	912139503161118824	118824
Odisha	Kalahandi	Narala	LANJIGARH ROAD	912100000000000000	912139500000000000	912139503161000000	912139503161118804	118804
Odisha	Kalahandi	Narala	SANTPUR	912100000000000000	912139500000000000	912139503161000000	912139503161118843	118843
Odisha	Kalahandi	Narala	GOBARDHANPUR	912100000000000000	912139500000000000	912139503161000000	912139503161118797	118797
Odisha	Kalahandi	Narala	KADAMGUDA	912100000000000000	912139500000000000	912139503161000000	912139503161275624	275624
Odisha	Kalahandi	Narala	BHURTIGARH	912100000000000000	912139500000000000	912139503161000000	912139503161118792	118792
Odisha	Kalahandi	Narala	GHANTAMALA	912100000000000000	912139500000000000	912139503161000000	912139503161118830	118830
Odisha	Kalahandi	Narala	SARGIGUDA	912100000000000000	912139500000000000	912139503161000000	912139503161118844	118844
Odisha	Kalahandi	Narala	KARMEGAON	912100000000000000	912139500000000000	912139503161000000	912139503161118833	118833
Odisha	Kalahandi	Narala	KURMEL	912100000000000000	912139500000000000	912139503161000000	912139503161118834	118834
Odisha	Kalahandi	Narala	ULIKUPA	912100000000000000	912139500000000000	912139503161000000	912139503161118849	118849
Odisha	Kalahandi	Narala	NARLA	912100000000000000	912139500000000000	912139503161000000	912139503161118837	118837
Odisha	Kalahandi	Narala	KAMARADA	912100000000000000	912139500000000000	912139503161000000	912139503161118799	118799
Odisha	Kalahandi	Narala	GADEBANDHA	912100000000000000	912139500000000000	912139503161000000	912139503161118829	118829
Odisha	Kalahandi	Narala	MANDEL	912100000000000000	912139500000000000	912139503161000000	912139503161118835	118835
Odisha	Kalahandi	Narala	SARIAN	912100000000000000	912139500000000000	912139503161000000	912139503161118845	118845
Odisha	Kalahandi	Narala	RAKSHI	912100000000000000	912139500000000000	912139503161000000	912139503161118840	118840
Odisha	Kalahandi	Narala	BALISINGA	912100000000000000	912139500000000000	912139503161000000	912139503161118826	118826
Odisha	Kalahandi	Narala	RUPRA ROAD	912100000000000000	912139500000000000	912139503161000000	912139503161118842	118842
Odisha	Kalahandi	Narala	BAGPUR	912100000000000000	912139500000000000	912139503161000000	912139503161118825	118825
Odisha	Kalahandi	Narala	BHIMKELA	912100000000000000	912139500000000000	912139503161000000	912139503161118828	118828
Odisha	Kalahandi	Narala	BHANPUR	912100000000000000	912139500000000000	912139503161000000	912139503161118827	118827
Odisha	Kalahandi	Narala	GHODABANDHA	912100000000000000	912139500000000000	912139503161000000	912139503161118831	118831
Odisha	Kalahandi	Narala	SERGARH	912100000000000000	912139500000000000	912139503161000000	912139503161118846	118846
Odisha	Kalahandi	Narala	TULAPADA	912100000000000000	912139500000000000	912139503161000000	912139503161118848	118848
Odisha	Kalahandi	Sadar(P) K	JUGSAIPATNA	912100000000000000	912139500000000000	912139503164000000	912139503164118607	118607
Odisha	Kalahandi	Sadar(P) K	BEDAGAON	912100000000000000	912139500000000000	912139503164000000	912139503164275622	275622
Odisha	Kalahandi	Sadar(P) K	CHAHAGAON	912100000000000000	912139500000000000	912139503164000000	912139503164118597	118597
Odisha	Kalahandi	Sadar(P) K	BHATANGPADAR	912100000000000000	912139500000000000	912139503164000000	912139503164118791	118791
Odisha	Kalahandi	Sadar(P) K	TAL-BELGAON	912100000000000000	912139500000000000	912139503164000000	912139503164118624	118624
Odisha	Kalahandi	Sadar(P) K	SAGADA	912100000000000000	912139500000000000	912139503164000000	912139503164118622	118622
Odisha	Kalahandi	Sadar(P) K	CHANCHER	912100000000000000	912139500000000000	912139503164000000	912139503164118598	118598
Odisha	Kalahandi	Sadar(P) K	CHHELIAMAL	912100000000000000	912139500000000000	912139503164000000	912139503164118599	118599
Odisha	Kalahandi	Sadar(P) K	KANKUTRU	912100000000000000	912139500000000000	912139503164000000	912139503164118800	118800
Odisha	Kalahandi	Sadar(P) K	DUARSUNI	912100000000000000	912139500000000000	912139503164000000	912139503164118602	118602
Odisha	Kalahandi	Sadar(P) K	GAIAGAON	912100000000000000	912139500000000000	912139503164000000	912139503164118748	118748
Odisha	Kalahandi	Sadar(P) K	BENAGAON	912100000000000000	912139500000000000	912139503164000000	912139503164118790	118790
Odisha	Kalahandi	Sadar(P) K	RISIGAON	912100000000000000	912139500000000000	912139503164000000	912139503164118621	118621
Odisha	Kalahandi	Sadar(P) K	KULIAMAL	912100000000000000	912139500000000000	912139503164000000	912139503164118613	118613
Odisha	Kalahandi	Sadar(P) K	THUAPADAR	912100000000000000	912139500000000000	912139503164000000	912139503164118625	118625
Odisha	Kalahandi	Sadar(P) K	KUTRUKHAMAR	912100000000000000	912139500000000000	912139503164000000	912139503164118614	118614
Odisha	Kalahandi	Sadar(P) K	MALGAON	912100000000000000	912139500000000000	912139503164000000	912139503164118616	118616
Odisha	Kalahandi	Sadar(P) K	GUNDRI	912100000000000000	912139500000000000	912139503164000000	912139503164118798	118798
Odisha	Kalahandi	Sadar(P) K	LAXMIPUR	912100000000000000	912139500000000000	912139503164000000	912139503164275612	275612
Odisha	Kalahandi	Sadar(P) K	SRIPUR	912100000000000000	912139500000000000	912139503164000000	912139503164275656	275656
Odisha	Kalahandi	Sadar(P) K	DEYPUR	912100000000000000	912139500000000000	912139503164000000	912139503164118601	118601
Odisha	Kalahandi	Sadar(P) K	GAND-BARAJHOLA	912100000000000000	912139500000000000	912139503164000000	912139503164118604	118604
Odisha	Kalahandi	Sadar(P) K	KAMATHANA	912100000000000000	912139500000000000	912139503164000000	912139503164118609	118609
Odisha	Kalahandi	Sadar(P) K	BIJEPUR	912100000000000000	912139500000000000	912139503164000000	912139503164118793	118793
Odisha	Kalahandi	Sadar(P) K	\N	912100000000000000	912139500000000000	912139503164000000	912139503164250558	250558
Odisha	Kalahandi	Sadar(P) K	MEDINIPUR	912100000000000000	912139500000000000	912139503164000000	912139503164118618	118618
Odisha	Kalahandi	Sadar(P) K	KALAM	912100000000000000	912139500000000000	912139503164000000	912139503164118608	118608
Odisha	Kalahandi	Sadar(P) K	BORABHATA	912100000000000000	912139500000000000	912139503164000000	912139503164118595	118595
Odisha	Kalahandi	Sadar(P) K	UDEPUR	912100000000000000	912139500000000000	912139503164000000	912139503164118626	118626
Odisha	Kalahandi	Sadar(P) K	GUDIALIPADAR	912100000000000000	912139500000000000	912139503164000000	912139503164118605	118605
Odisha	Kalahandi	Sadar(P) K	DADPUR	912100000000000000	912139500000000000	912139503164000000	912139503164118600	118600
Odisha	Kalahandi	Sadar(P) K	KARLAGUDA	912100000000000000	912139500000000000	912139503164000000	912139503164118611	118611
Odisha	Kalahandi	Sadar(P) K	PALSIJHARAN	912100000000000000	912139500000000000	912139503164000000	912139503164118620	118620
Odisha	Kalahandi	Sadar(P) K	KARALAPADA	912100000000000000	912139500000000000	912139503164000000	912139503164118610	118610
Odisha	Kalahandi	Sadar(P) K	PASTIKUDI	912100000000000000	912139500000000000	912139503164000000	912139503164118760	118760
Odisha	Kalahandi	Sadar(P) K	NASIGAON	912100000000000000	912139500000000000	912139503164000000	912139503164118757	118757
Odisha	Kalahandi	Sadar(P) K	ADHAMUNDA	912100000000000000	912139500000000000	912139503164000000	912139503164118742	118742
Odisha	Kalahandi	Sadar(P) K	PHATKAMAL	912100000000000000	912139500000000000	912139503164000000	912139503164118762	118762
Odisha	Kalahandi	Sadar(P) K	NUNMATH	912100000000000000	912139500000000000	912139503164000000	912139503164118758	118758
Odisha	Kalahandi	Sadar(P) K	UTKELA	912100000000000000	912139500000000000	912139503164000000	912139503164118767	118767
Odisha	Kalahandi	Sadar(P) K	KIKIA	912100000000000000	912139500000000000	912139503164000000	912139503164118754	118754
Odisha	Kalahandi	Thuamul Rampur	GOPINATHPUR	912100000000000000	912139500000000000	912139503171000000	912139503171118854	118854
Odisha	Kalahandi	Thuamul Rampur	PODAPADAR (B)	912100000000000000	912139500000000000	912139503171000000	912139503171275632	275632
Odisha	Kalahandi	Thuamul Rampur	MAHULPATNA	912100000000000000	912139500000000000	912139503171000000	912139503171118859	118859
Odisha	Kalahandi	Thuamul Rampur	ADRI	912100000000000000	912139500000000000	912139503171000000	912139503171118850	118850
Odisha	Kalahandi	Thuamul Rampur	KARLAPAT	912100000000000000	912139500000000000	912139503171000000	912139503171118857	118857
Odisha	Kalahandi	Thuamul Rampur	SINDHIPADAR	912100000000000000	912139500000000000	912139503171000000	912139503171118863	118863
Odisha	Kalahandi	Thuamul Rampur	TALNAGI	912100000000000000	912139500000000000	912139503171000000	912139503171118864	118864
Odisha	Kalahandi	Thuamul Rampur	MALIGAON	912100000000000000	912139500000000000	912139503171000000	912139503171118860	118860
Odisha	Kalahandi	Thuamul Rampur	PADEPADAR	912100000000000000	912139500000000000	912139503171000000	912139503171118862	118862
Odisha	Kalahandi	Thuamul Rampur	BADHHATRANG	912100000000000000	912139500000000000	912139503171000000	912139503171118851	118851
Odisha	Kalahandi	Thuamul Rampur	BIRIKOT	912100000000000000	912139500000000000	912139503171000000	912139503171275683	275683
Odisha	Kalahandi	Thuamul Rampur	BADACHHATRANG	912100000000000000	912139500000000000	912139503171000000	912139503171275631	275631
Odisha	Kalahandi	Thuamul Rampur	KERPAI	912100000000000000	912139500000000000	912139503171000000	912139503171118858	118858
Odisha	Kalahandi	Thuamul Rampur	DUMERPADAR	912100000000000000	912139500000000000	912139503171000000	912139503171118852	118852
Odisha	Kalahandi	Thuamul Rampur	SILET	912100000000000000	912139500000000000	912139503171000000	912139503171275657	275657
Odisha	Kalahandi	Thuamul Rampur	TH.RAMPUR	912100000000000000	912139500000000000	912139503171000000	912139503171118865	118865
Odisha	Kalahandi	Thuamul Rampur	TALAJHAPI	912100000000000000	912139500000000000	912139503171000000	912139503171275630	275630
Odisha	Kalahandi	Thuamul Rampur	GOPALPUR	912100000000000000	912139500000000000	912139503171000000	912139503171118853	118853
Odisha	Kalahandi	Thuamul Rampur	TALA-AMBPADAR	912100000000000000	912139500000000000	912139503171000000	912139503171275627	275627
Odisha	Kalahandi	Thuamul Rampur	NAKRUNDI	912100000000000000	912139500000000000	912139503171000000	912139503171118861	118861
Odisha	Kalahandi	Thuamul Rampur	KIAPADAR	912100000000000000	912139500000000000	912139503171000000	912139503171275658	275658
Odisha	Kalahandi	Thuamul Rampur	THUAMUL	912100000000000000	912139500000000000	912139503171000000	912139503171275660	275660
Odisha	Kalahandi	Thuamul Rampur	KANIGUAMA	912100000000000000	912139500000000000	912139503171000000	912139503171118856	118856
Odisha	Kalahandi	Thuamul Rampur	GUNPUR	912100000000000000	912139500000000000	912139503171000000	912139503171118855	118855
Odisha	Koraput	Bandhugaon	KUMBHARIPUT	912100000000000000	912139800000000000	912139803205000000	912139803205119711	119711
Odisha	Koraput	Bandhugaon	PEDAVALDA	912100000000000000	912139800000000000	912139803205000000	912139803205119714	119714
Odisha	Koraput	Bandhugaon	KAPALADA	912100000000000000	912139800000000000	912139803205000000	912139803205119709	119709
Odisha	Koraput	Bandhugaon	ALMONDA	912100000000000000	912139800000000000	912139803205000000	912139803205119703	119703
Odisha	Koraput	Bandhugaon	KABERIBADI	912100000000000000	912139800000000000	912139803205000000	912139803205119707	119707
Odisha	Koraput	Bandhugaon	JAGUGUDA	912100000000000000	912139800000000000	912139803205000000	912139803205119706	119706
Odisha	Koraput	Bandhugaon	BANDHUGAON	912100000000000000	912139800000000000	912139803205000000	912139803205119704	119704
Odisha	Koraput	Bandhugaon	KUMBHAGANDA	912100000000000000	912139800000000000	912139803205000000	912139803205119710	119710
Odisha	Koraput	Bandhugaon	B ADASORPOLLI	912100000000000000	912139800000000000	912139803205000000	912139803205275162	275162
Odisha	Koraput	Bandhugaon	KANAGAM	912100000000000000	912139800000000000	912139803205000000	912139803205119708	119708
Odisha	Koraput	Bandhugaon	GARIDI	912100000000000000	912139800000000000	912139803205000000	912139803205119705	119705
Odisha	Koraput	Bandhugaon	NEELABADI	912100000000000000	912139800000000000	912139803205000000	912139803205119713	119713
Odisha	Koraput	Bandhugaon	KUTRABEDA	912100000000000000	912139800000000000	912139803205000000	912139803205119712	119712
Odisha	Koraput	Bandhugaon	KUMBHARI	912100000000000000	912139800000000000	912139803205000000	912139803205119893	119893
Odisha	Koraput	Bhairabsingipur	DANGARACHINCHI	912100000000000000	912139800000000000	912139803201000000	912139803201119782	119782
Odisha	Koraput	Bhairabsingipur	KEBIDI	912100000000000000	912139800000000000	912139803201000000	912139803201119790	119790
Odisha	Koraput	Bhairabsingipur	RANIGADA	912100000000000000	912139800000000000	912139803201000000	912139803201119796	119796
Odisha	Koraput	Bhairabsingipur	HARIDAGUDA	912100000000000000	912139800000000000	912139803201000000	912139803201119743	119743
Odisha	Koraput	Bhairabsingipur	JUJHARI	912100000000000000	912139800000000000	912139803201000000	912139803201119745	119745
Odisha	Koraput	Bhairabsingipur	GUJUNIGUDA	912100000000000000	912139800000000000	912139803201000000	912139803201119741	119741
Odisha	Koraput	Bhairabsingipur	RANASPUR	912100000000000000	912139800000000000	912139803201000000	912139803201119756	119756
Odisha	Koraput	Bhairabsingipur	BENAGAM	912100000000000000	912139800000000000	912139803201000000	912139803201119734	119734
Odisha	Koraput	Bhairabsingipur	GUMUDA	912100000000000000	912139800000000000	912139803201000000	912139803201119822	119822
Odisha	Koraput	Bhairabsingipur	BIJAPUR	912100000000000000	912139800000000000	912139803201000000	912139803201119736	119736
Odisha	Koraput	Bhairabsingipur	SEMLAGUDA	912100000000000000	912139800000000000	912139803201000000	912139803201119760	119760
Odisha	Koraput	Bhairabsingipur	BENASUR	912100000000000000	912139800000000000	912139803201000000	912139803201119735	119735
Odisha	Koraput	Bhairabsingipur	B.SINGPUR	912100000000000000	912139800000000000	912139803201000000	912139803201119733	119733
Odisha	Koraput	Bhairabsingipur	CHAMPAPADAR	912100000000000000	912139800000000000	912139803201000000	912139803201119739	119739
Odisha	Koraput	Bhairabsingipur	NARIGAM	912100000000000000	912139800000000000	912139803201000000	912139803201119753	119753
Odisha	Koraput	Bhairabsingipur	NUAGAM	912100000000000000	912139800000000000	912139803201000000	912139803201119754	119754
Odisha	Koraput	Bhairabsingipur	KATHARGADA	912100000000000000	912139800000000000	912139803201000000	912139803201119748	119748
Odisha	Koraput	Bhairabsingipur	PONDASGUDA	912100000000000000	912139800000000000	912139803201000000	912139803201119755	119755
Odisha	Koraput	Bhairabsingipur	GUMUDA	912100000000000000	912139800000000000	912139803201000000	912139803201119742	119742
Odisha	Koraput	Bhairabsingipur	KAMTA	912100000000000000	912139800000000000	912139803201000000	912139803201119747	119747
Odisha	Koraput	Bhairabsingipur	KUMULI	912100000000000000	912139800000000000	912139803201000000	912139803201119750	119750
Odisha	Koraput	Bhairabsingipur	BODIGAM	912100000000000000	912139800000000000	912139803201000000	912139803201119737	119737
Odisha	Koraput	Bhairabsingipur	MUNJA	912100000000000000	912139800000000000	912139803201000000	912139803201119752	119752
Odisha	Koraput	Boipariguda	M ATHAPADA	912100000000000000	912139800000000000	912139803211000000	912139803211275101	275101
Odisha	Koraput	Boipariguda	MAJHIGUDA	912100000000000000	912139800000000000	912139803211000000	912139803211119727	119727
Odisha	Koraput	Boipariguda	KATHAPADA	912100000000000000	912139800000000000	912139803211000000	912139803211119724	119724
Odisha	Koraput	Boipariguda	DANDABADI	912100000000000000	912139800000000000	912139803211000000	912139803211119720	119720
Odisha	Koraput	Boipariguda	KENDUGUDA	912100000000000000	912139800000000000	912139803211000000	912139803211119725	119725
Odisha	Koraput	Boipariguda	CHIPAKUR	912100000000000000	912139800000000000	912139803211000000	912139803211119719	119719
Odisha	Koraput	Boipariguda	P UJARIGUDA	912100000000000000	912139800000000000	912139803211000000	912139803211275126	275126
Odisha	Koraput	Boipariguda	BOIPARIGUDA	912100000000000000	912139800000000000	912139803211000000	912139803211119717	119717
Odisha	Koraput	Boipariguda	HALADIKUND	912100000000000000	912139800000000000	912139803211000000	912139803211119723	119723
Odisha	Koraput	Boipariguda	KOLLAR	912100000000000000	912139800000000000	912139803211000000	912139803211119726	119726
Odisha	Koraput	Boipariguda	BODAPUT	912100000000000000	912139800000000000	912139803211000000	912139803211119716	119716
Odisha	Koraput	Boipariguda	BADAJUINA	912100000000000000	912139800000000000	912139803211000000	912139803211119779	119779
Odisha	Koraput	Boipariguda	DASAMANTHPUR	912100000000000000	912139800000000000	912139803211000000	912139803211119721	119721
Odisha	Koraput	Boipariguda	P ENDAPADA	912100000000000000	912139800000000000	912139803211000000	912139803211275137	275137
Odisha	Koraput	Boipariguda	CHANDRAPADA	912100000000000000	912139800000000000	912139803211000000	912139803211119718	119718
Odisha	Koraput	Boipariguda	DORAGUDA	912100000000000000	912139800000000000	912139803211000000	912139803211119722	119722
Odisha	Koraput	Boipariguda	MOHULI	912100000000000000	912139800000000000	912139803211000000	912139803211119728	119728
Odisha	Koraput	Boipariguda	BARNIPUT	912100000000000000	912139800000000000	912139803211000000	912139803211119781	119781
Odisha	Koraput	Boipariguda	TENTULIGUMA	912100000000000000	912139800000000000	912139803211000000	912139803211119730	119730
Odisha	Koraput	Boipariguda	CH ERKA	912100000000000000	912139800000000000	912139803211000000	912139803211275104	275104
Odisha	Koraput	Boipariguda	RAMAGIRI	912100000000000000	912139800000000000	912139803211000000	912139803211119729	119729
Odisha	Koraput	Boipariguda	GHUMAR	912100000000000000	912139800000000000	912139803211000000	912139803211119834	119834
Odisha	Koraput	Boipariguda	DIGAPUR	912100000000000000	912139800000000000	912139803211000000	912139803211119833	119833
Odisha	Koraput	Boipariguda	MOSIGAM	912100000000000000	912139800000000000	912139803211000000	912139803211119838	119838
Odisha	Koraput	Boipariguda	DANGARAPAUNSI	912100000000000000	912139800000000000	912139803211000000	912139803211119832	119832
Odisha	Koraput	Boipariguda	BALIGAM	912100000000000000	912139800000000000	912139803211000000	912139803211119715	119715
Odisha	Koraput	Boriguma	MALDA	912100000000000000	912139800000000000	912139803200000000	912139803200119751	119751
Odisha	Koraput	Boriguma	KUMULIPUT	912100000000000000	912139800000000000	912139803200000000	912139803200119792	119792
Odisha	Koraput	Boriguma	JAYANTIGIRI	912100000000000000	912139800000000000	912139803200000000	912139803200119788	119788
Odisha	Koraput	Boriguma	JAMUNDA	912100000000000000	912139800000000000	912139803200000000	912139803200119787	119787
Odisha	Koraput	Boriguma	KEBIDI	912100000000000000	912139800000000000	912139803200000000	912139803200119790	119790
Odisha	Koraput	Boriguma	PUJARIPUT	912100000000000000	912139800000000000	912139803200000000	912139803200119794	119794
Odisha	Koraput	Boriguma	BIJAPUR	912100000000000000	912139800000000000	912139803200000000	912139803200119736	119736
Odisha	Koraput	Boriguma	SARGIGUDA	912100000000000000	912139800000000000	912139803200000000	912139803200119758	119758
Odisha	Koraput	Boriguma	BORIGUMMA	912100000000000000	912139800000000000	912139803200000000	912139803200119738	119738
Odisha	Koraput	Boriguma	RANASPUR	912100000000000000	912139800000000000	912139803200000000	912139803200119756	119756
Odisha	Koraput	Boriguma	B ANDIGUDA	912100000000000000	912139800000000000	912139803200000000	912139803200275089	275089
Odisha	Koraput	Boriguma	JUJHARI	912100000000000000	912139800000000000	912139803200000000	912139803200119745	119745
Odisha	Koraput	Boriguma	ANCHALA	912100000000000000	912139800000000000	912139803200000000	912139803200119731	119731
Odisha	Koraput	Boriguma	KANAGAM	912100000000000000	912139800000000000	912139803200000000	912139803200119749	119749
Odisha	Koraput	Boriguma	HORDOLI	912100000000000000	912139800000000000	912139803200000000	912139803200119744	119744
Odisha	Koraput	Boriguma	KAMARA	912100000000000000	912139800000000000	912139803200000000	912139803200119746	119746
Odisha	Koraput	Boriguma	SASAHANDI	912100000000000000	912139800000000000	912139803200000000	912139803200119759	119759
Odisha	Koraput	Boriguma	PONDASGUDA	912100000000000000	912139800000000000	912139803200000000	912139803200119755	119755
Odisha	Koraput	Boriguma	SANAPORI A	912100000000000000	912139800000000000	912139803200000000	912139803200119757	119757
Odisha	Koraput	Boriguma	KATHARGADA	912100000000000000	912139800000000000	912139803200000000	912139803200119748	119748
Odisha	Koraput	Boriguma	DENGAPADAR	912100000000000000	912139800000000000	912139803200000000	912139803200119740	119740
Odisha	Koraput	Boriguma	AUNLI	912100000000000000	912139800000000000	912139803200000000	912139803200119732	119732
Odisha	Koraput	Damonjodi	DUMURIPADAR	912100000000000000	912139800000000000	912139803217000000	912139803217119801	119801
Odisha	Koraput	Damonjodi	M ARICHMAL	912100000000000000	912139800000000000	912139803217000000	912139803217275152	275152
Odisha	Koraput	Damonjodi	MATHALPUT	912100000000000000	912139800000000000	912139803217000000	912139803217119809	119809
Odisha	Koraput	Damonjodi	LITIGUDA	912100000000000000	912139800000000000	912139803217000000	912139803217119805	119805
Odisha	Koraput	Dasamantapur	CHIKAMBA	912100000000000000	912139800000000000	912139803202000000	912139803202119763	119763
Odisha	Koraput	Dasamantapur	GODIAGUDA	912100000000000000	912139800000000000	912139803202000000	912139803202119768	119768
Odisha	Koraput	Dasamantapur	LULLA	912100000000000000	912139800000000000	912139803202000000	912139803202119769	119769
Odisha	Koraput	Dasamantapur	GIRLIGUMMA	912100000000000000	912139800000000000	912139803202000000	912139803202119767	119767
Odisha	Koraput	Dasamantapur	MURKAR	912100000000000000	912139800000000000	912139803202000000	912139803202119771	119771
Odisha	Koraput	Dasamantapur	DASMANTHPUR	912100000000000000	912139800000000000	912139803202000000	912139803202119764	119764
Odisha	Koraput	Dasamantapur	TUNPAR	912100000000000000	912139800000000000	912139803202000000	912139803202119867	119867
Odisha	Koraput	Dasamantapur	CHANABADA	912100000000000000	912139800000000000	912139803202000000	912139803202119762	119762
Odisha	Koraput	Dasamantapur	PARAJABEDAPADAR	912100000000000000	912139800000000000	912139803202000000	912139803202119774	119774
Odisha	Koraput	Dasamantapur	PINDAPADAR	912100000000000000	912139800000000000	912139803202000000	912139803202119775	119775
Odisha	Koraput	Dasamantapur	DUMBAGUDA	912100000000000000	912139800000000000	912139803202000000	912139803202119766	119766
Odisha	Koraput	Dasamantapur	DUARSUNI	912100000000000000	912139800000000000	912139803202000000	912139803202119765	119765
Odisha	Koraput	Dasamantapur	DASAMANTHPUR	912100000000000000	912139800000000000	912139803202000000	912139803202119721	119721
Odisha	Koraput	Jeypore	EKAMBA	912100000000000000	912139800000000000	912139803209000000	912139803209119784	119784
Odisha	Koraput	Jeypore	DANGARACHINCHI	912100000000000000	912139800000000000	912139803209000000	912139803209119782	119782
Odisha	Koraput	Jeypore	BARNIPUT	912100000000000000	912139800000000000	912139803209000000	912139803209119781	119781
Odisha	Koraput	Jeypore	TANKUA	912100000000000000	912139800000000000	912139803209000000	912139803209119797	119797
Odisha	Koraput	Jeypore	BALIA	912100000000000000	912139800000000000	912139803209000000	912139803209119780	119780
Odisha	Koraput	Jeypore	UMURI	912100000000000000	912139800000000000	912139803209000000	912139803209119798	119798
Odisha	Koraput	Jeypore	AMBAGUDA	912100000000000000	912139800000000000	912139803209000000	912139803209119777	119777
Odisha	Koraput	Jeypore	ANTA	912100000000000000	912139800000000000	912139803209000000	912139803209119778	119778
Odisha	Koraput	Jeypore	PHAMPUNI	912100000000000000	912139800000000000	912139803209000000	912139803209119793	119793
Odisha	Koraput	Jeypore	KEBIDI	912100000000000000	912139800000000000	912139803209000000	912139803209119790	119790
Odisha	Koraput	Jeypore	\N	912100000000000000	912139800000000000	912139803209000000	912139803209250574	250574
Odisha	Koraput	Jeypore	GADAPADARA	912100000000000000	912139800000000000	912139803209000000	912139803209119785	119785
Odisha	Koraput	Jeypore	KALIAGAM	912100000000000000	912139800000000000	912139803209000000	912139803209119789	119789
Odisha	Koraput	Jeypore	RANDAPALLI	912100000000000000	912139800000000000	912139803209000000	912139803209119795	119795
Odisha	Koraput	Jeypore	BADAJUINA	912100000000000000	912139800000000000	912139803209000000	912139803209119779	119779
Odisha	Koraput	Jeypore	DHANPUR	912100000000000000	912139800000000000	912139803209000000	912139803209119783	119783
Odisha	Koraput	Jeypore	KUMULIPUT	912100000000000000	912139800000000000	912139803209000000	912139803209119792	119792
Odisha	Koraput	Jeypore	HADIA	912100000000000000	912139800000000000	912139803209000000	912139803209119786	119786
Odisha	Koraput	Jeypore	KONGA	912100000000000000	912139800000000000	912139803209000000	912139803209119791	119791
Odisha	Koraput	Jeypore	JAMUNDA	912100000000000000	912139800000000000	912139803209000000	912139803209119787	119787
Odisha	Koraput	Jeypore	MURTHAHANDI	912100000000000000	912139800000000000	912139803209000000	912139803209119824	119824
Odisha	Koraput	Kakiriguma	ODIAPENTHA	912100000000000000	912139800000000000	912139803206000000	912139803206119863	119863
Odisha	Koraput	Kakiriguma	BHITARAGADA	912100000000000000	912139800000000000	912139803206000000	912139803206119855	119855
Odisha	Koraput	Kakiriguma	PIPALPADAR	912100000000000000	912139800000000000	912139803206000000	912139803206119865	119865
Odisha	Koraput	Kakiriguma	BURJA	912100000000000000	912139800000000000	912139803206000000	912139803206119856	119856
Odisha	Koraput	Kakiriguma	TUNPAR	912100000000000000	912139800000000000	912139803206000000	912139803206119867	119867
Odisha	Koraput	Kakiriguma	GOUDAGUDA	912100000000000000	912139800000000000	912139803206000000	912139803206119858	119858
Odisha	Koraput	Kakiriguma	NARAYANAPATNA	912100000000000000	912139800000000000	912139803206000000	912139803206119895	119895
Odisha	Koraput	Kakiriguma	PANCHADA	912100000000000000	912139800000000000	912139803206000000	912139803206119864	119864
Odisha	Koraput	Kakiriguma	KAKIRIGUMMA	912100000000000000	912139800000000000	912139803206000000	912139803206119859	119859
Odisha	Koraput	Kakiriguma	KUSUMGUDA	912100000000000000	912139800000000000	912139803206000000	912139803206119860	119860
Odisha	Koraput	Koraput	BADASUKU	912100000000000000	912139800000000000	912139803207000000	912139803207119799	119799
Odisha	Koraput	Koraput	LANKAPUT	912100000000000000	912139800000000000	912139803207000000	912139803207119804	119804
Odisha	Koraput	Koraput	KERENGA	912100000000000000	912139800000000000	912139803207000000	912139803207119803	119803
Odisha	Koraput	Koraput	MUJANGA	912100000000000000	912139800000000000	912139803207000000	912139803207119770	119770
Odisha	Koraput	Koraput	UMURI	912100000000000000	912139800000000000	912139803207000000	912139803207119811	119811
Odisha	Koraput	Koraput	PODAGADA	912100000000000000	912139800000000000	912139803207000000	912139803207119776	119776
Odisha	Koraput	Koraput	DEVIGHAT	912100000000000000	912139800000000000	912139803207000000	912139803207119800	119800
Odisha	Koraput	Koraput	MASTIPUT	912100000000000000	912139800000000000	912139803207000000	912139803207119808	119808
Odisha	Koraput	Koraput	MANBAR	912100000000000000	912139800000000000	912139803207000000	912139803207119807	119807
Odisha	Koraput	Koraput	MAHADEIPUT	912100000000000000	912139800000000000	912139803207000000	912139803207119806	119806
Odisha	Koraput	Koraput	DUARSUNI	912100000000000000	912139800000000000	912139803207000000	912139803207119765	119765
Odisha	Koraput	Koraput	GODIAGUDA	912100000000000000	912139800000000000	912139803207000000	912139803207119768	119768
Odisha	Koraput	Koraput	A.MALKANGIRI	912100000000000000	912139800000000000	912139803207000000	912139803207119761	119761
Odisha	Koraput	Koraput	KENDAR	912100000000000000	912139800000000000	912139803207000000	912139803207119802	119802
Odisha	Koraput	Koraput	PAIKPHULBEDA	912100000000000000	912139800000000000	912139803207000000	912139803207119773	119773
Odisha	Koraput	Koraput	NANDIGAM	912100000000000000	912139800000000000	912139803207000000	912139803207119772	119772
Odisha	Koraput	Koraput Town	PADMAPUR	912100000000000000	912139800000000000	912139803208000000	912139803208119810	119810
Odisha	Koraput	Koraput Town	\N	912100000000000000	912139800000000000	912139803208000000	912139803208250571	250571
Odisha	Koraput	Koraput Town	MANBAR	912100000000000000	912139800000000000	912139803208000000	912139803208119807	119807
Odisha	Koraput	Kotiya	KOTIYA	912100000000000000	912139800000000000	912139803219000000	912139803219119903	119903
Odisha	Koraput	Kotiya	NUAGAON	912100000000000000	912139800000000000	912139803219000000	912139803219119905	119905
Odisha	Koraput	Kotiya	RALEGADA	912100000000000000	912139800000000000	912139803219000000	912139803219119909	119909
Odisha	Koraput	Kotiya	UPPARKANTI	912100000000000000	912139800000000000	912139803219000000	912139803219119928	119928
Odisha	Koraput	Kotiya	SUNKI	912100000000000000	912139800000000000	912139803219000000	912139803219119911	119911
Odisha	Koraput	Kotiya	PARAJAMUTHAI	912100000000000000	912139800000000000	912139803219000000	912139803219119921	119921
Odisha	Koraput	Kotpad	CHATARLA	912100000000000000	912139800000000000	912139803199000000	912139803199119816	119816
Odisha	Koraput	Kotpad	KUSUMI	912100000000000000	912139800000000000	912139803199000000	912139803199119823	119823
Odisha	Koraput	Kotpad	S.B.NUAGAM	912100000000000000	912139800000000000	912139803199000000	912139803199119825	119825
Odisha	Koraput	Kotpad	BOBEYA	912100000000000000	912139800000000000	912139803199000000	912139803199119814	119814
Odisha	Koraput	Kotpad	BHANSULI	912100000000000000	912139800000000000	912139803199000000	912139803199119813	119813
Odisha	Koraput	Kotpad	CHITRA	912100000000000000	912139800000000000	912139803199000000	912139803199119817	119817
Odisha	Koraput	Kotpad	BATASONA	912100000000000000	912139800000000000	912139803199000000	912139803199119812	119812
Odisha	Koraput	Kotpad	MURTHAHANDI	912100000000000000	912139800000000000	912139803199000000	912139803199119824	119824
Odisha	Koraput	Kotpad	JUJHARI	912100000000000000	912139800000000000	912139803199000000	912139803199119745	119745
Odisha	Koraput	Kotpad	DHAMANAHANDI	912100000000000000	912139800000000000	912139803199000000	912139803199119818	119818
Odisha	Koraput	Kotpad	CHANDILI	912100000000000000	912139800000000000	912139803199000000	912139803199119815	119815
Odisha	Koraput	Kotpad	SASAHANDI	912100000000000000	912139800000000000	912139803199000000	912139803199119759	119759
Odisha	Koraput	Kotpad	GHUMAR	912100000000000000	912139800000000000	912139803199000000	912139803199119819	119819
Odisha	Koraput	Kotpad	S UTIPADAR	912100000000000000	912139800000000000	912139803199000000	912139803199275086	275086
Odisha	Koraput	Kotpad	SARGIGUDA	912100000000000000	912139800000000000	912139803199000000	912139803199119827	119827
Odisha	Koraput	Kotpad	SADARANGA	912100000000000000	912139800000000000	912139803199000000	912139803199119826	119826
Odisha	Koraput	Kotpad	\N	912100000000000000	912139800000000000	912139803199000000	912139803199250570	250570
Odisha	Koraput	Kotpad	SANAPORI A	912100000000000000	912139800000000000	912139803199000000	912139803199119757	119757
Odisha	Koraput	Kotpad	GIRLA	912100000000000000	912139800000000000	912139803199000000	912139803199119820	119820
Odisha	Koraput	Kotpad	GUALI	912100000000000000	912139800000000000	912139803199000000	912139803199119821	119821
Odisha	Koraput	Kotpad	GUMUDA	912100000000000000	912139800000000000	912139803199000000	912139803199119822	119822
Odisha	Koraput	Kundura	BAGDERI	912100000000000000	912139800000000000	912139803210000000	912139803210119829	119829
Odisha	Koraput	Kundura	KERIMITI	912100000000000000	912139800000000000	912139803210000000	912139803210119835	119835
Odisha	Koraput	Kundura	MOSIGAM	912100000000000000	912139800000000000	912139803210000000	912139803210119838	119838
Odisha	Koraput	Kundura	P HULABHATA	912100000000000000	912139800000000000	912139803210000000	912139803210275096	275096
Odisha	Koraput	Kundura	KUNDRA	912100000000000000	912139800000000000	912139803210000000	912139803210119836	119836
Odisha	Koraput	Kundura	BANUAGUDA	912100000000000000	912139800000000000	912139803210000000	912139803210119830	119830
Odisha	Koraput	Kundura	G UNDALA	912100000000000000	912139800000000000	912139803210000000	912139803210275092	275092
Odisha	Koraput	Kundura	LIMA	912100000000000000	912139800000000000	912139803210000000	912139803210119837	119837
Odisha	Koraput	Kundura	ASANA	912100000000000000	912139800000000000	912139803210000000	912139803210119828	119828
Odisha	Koraput	Kundura	PHUPUGAN	912100000000000000	912139800000000000	912139803210000000	912139803210275099	275099
Odisha	Koraput	Kundura	DIGAPUR	912100000000000000	912139800000000000	912139803210000000	912139803210119833	119833
Odisha	Koraput	Kundura	DANGARAPAUNSI	912100000000000000	912139800000000000	912139803210000000	912139803210119832	119832
Odisha	Koraput	Kundura	P AKHANAGUDA	912100000000000000	912139800000000000	912139803210000000	912139803210275097	275097
Odisha	Koraput	Kundura	BHUSANGAGUDA	912100000000000000	912139800000000000	912139803210000000	912139803210119831	119831
Odisha	Koraput	Kundura	RANIGADA	912100000000000000	912139800000000000	912139803210000000	912139803210119839	119839
Odisha	Koraput	Lakshmipur	ODIAPENTHA	912100000000000000	912139800000000000	912139803203000000	912139803203119863	119863
Odisha	Koraput	Lakshmipur	PIPALPADAR	912100000000000000	912139800000000000	912139803203000000	912139803203119865	119865
Odisha	Koraput	Lakshmipur	TOYAPUT	912100000000000000	912139800000000000	912139803203000000	912139803203119866	119866
Odisha	Koraput	Lakshmipur	KUSUMGUDA	912100000000000000	912139800000000000	912139803203000000	912139803203119860	119860
Odisha	Koraput	Lakshmipur	LAXMIPUR	912100000000000000	912139800000000000	912139803203000000	912139803203119862	119862
Odisha	Koraput	Lakshmipur	BURJA	912100000000000000	912139800000000000	912139803203000000	912139803203119856	119856
Odisha	Koraput	Lakshmipur	KUTINGA	912100000000000000	912139800000000000	912139803203000000	912139803203119861	119861
Odisha	Koraput	Lakshmipur	CHAMPI	912100000000000000	912139800000000000	912139803203000000	912139803203119857	119857
Odisha	Koraput	Lakshmipur	BHITARAGADA	912100000000000000	912139800000000000	912139803203000000	912139803203119855	119855
Odisha	Koraput	Machh Kund	BILAPUT	912100000000000000	912139800000000000	912139803212000000	912139803212119872	119872
Odisha	Koraput	Machh Kund	JALAHANJAR	912100000000000000	912139800000000000	912139803212000000	912139803212119847	119847
Odisha	Koraput	Machh Kund	KULARSING	912100000000000000	912139800000000000	912139803212000000	912139803212119881	119881
Odisha	Koraput	Machh Kund	THUSUBA	912100000000000000	912139800000000000	912139803212000000	912139803212119852	119852
Odisha	Koraput	Machh Kund	BALLEL	912100000000000000	912139800000000000	912139803212000000	912139803212119841	119841
Odisha	Koraput	Machh Kund	ONKADELLI	912100000000000000	912139800000000000	912139803212000000	912139803212119850	119850
Odisha	Koraput	Machh Kund	CHIKENPUT	912100000000000000	912139800000000000	912139803212000000	912139803212119843	119843
Odisha	Koraput	Machh Kund	GODIHANJAR	912100000000000000	912139800000000000	912139803212000000	912139803212119845	119845
Odisha	Koraput	Machh Kund	BADEL	912100000000000000	912139800000000000	912139803212000000	912139803212119869	119869
Odisha	Koraput	Machh Kund	BADIGADA	912100000000000000	912139800000000000	912139803212000000	912139803212119840	119840
Odisha	Koraput	Machh Kund	GUNEIPADA	912100000000000000	912139800000000000	912139803212000000	912139803212119846	119846
Odisha	Koraput	Machh Kund	LAMTAPUT	912100000000000000	912139800000000000	912139803212000000	912139803212119849	119849
Odisha	Koraput	Machh Kund	TIKARPADA	912100000000000000	912139800000000000	912139803212000000	912139803212119853	119853
Odisha	Koraput	Machh Kund	DABUGUDA	912100000000000000	912139800000000000	912139803212000000	912139803212119844	119844
Odisha	Koraput	Machh Kund	PETA	912100000000000000	912139800000000000	912139803212000000	912139803212119851	119851
Odisha	Koraput	Nandapur(P)	BADEL	912100000000000000	912139800000000000	912139803214000000	912139803214119869	119869
Odisha	Koraput	Nandapur(P)	K HINBAR	912100000000000000	912139800000000000	912139803214000000	912139803214275171	275171
Odisha	Koraput	Nandapur(P)	NANDAKA	912100000000000000	912139800000000000	912139803214000000	912139803214119883	119883
Odisha	Koraput	Nandapur(P)	KASANDI	912100000000000000	912139800000000000	912139803214000000	912139803214119877	119877
Odisha	Koraput	Nandapur(P)	RAISING	912100000000000000	912139800000000000	912139803214000000	912139803214119888	119888
Odisha	Koraput	Nandapur(P)	HIKIMPUT	912100000000000000	912139800000000000	912139803214000000	912139803214119876	119876
Odisha	Koraput	Nandapur(P)	KHEMUNDUGUDA	912100000000000000	912139800000000000	912139803214000000	912139803214119878	119878
Odisha	Koraput	Nandapur(P)	BILAPUT	912100000000000000	912139800000000000	912139803214000000	912139803214119872	119872
Odisha	Koraput	Nandapur(P)	THUBA	912100000000000000	912139800000000000	912139803214000000	912139803214119889	119889
Odisha	Koraput	Nandapur(P)	PARAJAMUTHAI	912100000000000000	912139800000000000	912139803214000000	912139803214119921	119921
Odisha	Koraput	Nandapur(P)	HATAGUDA	912100000000000000	912139800000000000	912139803214000000	912139803214119917	119917
Odisha	Koraput	Nandapur(P)	BHEJA	912100000000000000	912139800000000000	912139803214000000	912139803214119871	119871
Odisha	Koraput	Nandapur(P)	NANDAPUR	912100000000000000	912139800000000000	912139803214000000	912139803214119884	119884
Odisha	Koraput	Nandapur(P)	MALIBELGAON	912100000000000000	912139800000000000	912139803214000000	912139803214119882	119882
Odisha	Koraput	Nandapur(P)	BALDA	912100000000000000	912139800000000000	912139803214000000	912139803214119870	119870
Odisha	Koraput	Nandapur(P)	TIKARPADA	912100000000000000	912139800000000000	912139803214000000	912139803214119853	119853
Odisha	Koraput	Nandapur(P)	KHURJI	912100000000000000	912139800000000000	912139803214000000	912139803214119879	119879
Odisha	Koraput	Nandapur(P)	GUNTHAPUT	912100000000000000	912139800000000000	912139803214000000	912139803214119916	119916
Odisha	Koraput	Nandapur(P)	RENGA	912100000000000000	912139800000000000	912139803214000000	912139803214119924	119924
Odisha	Koraput	Nandapur(P)	BANAMALIPUT	912100000000000000	912139800000000000	912139803214000000	912139803214119842	119842
Odisha	Koraput	Nandapur(P)	KUMARAGANDHANA	912100000000000000	912139800000000000	912139803214000000	912139803214119848	119848
Odisha	Koraput	Nandapur(P)	SUBAI	912100000000000000	912139800000000000	912139803214000000	912139803214119927	119927
Odisha	Koraput	Nandapur(P)	DABUGUDA	912100000000000000	912139800000000000	912139803214000000	912139803214119844	119844
Odisha	Koraput	Nandapur(P)	UMBEL	912100000000000000	912139800000000000	912139803214000000	912139803214119854	119854
Odisha	Koraput	Nandapur(P)	PITAGUDA	912100000000000000	912139800000000000	912139803214000000	912139803214119922	119922
Odisha	Koraput	Nandapur(P)	RAJPUT	912100000000000000	912139800000000000	912139803214000000	912139803214119923	119923
Odisha	Koraput	Nandapur(P)	PETA	912100000000000000	912139800000000000	912139803214000000	912139803214119851	119851
Odisha	Koraput	Narayanpatana	TALAGUMANDI	912100000000000000	912139800000000000	912139803204000000	912139803204119897	119897
Odisha	Koraput	Narayanpatana	BIJAGHATI	912100000000000000	912139800000000000	912139803204000000	912139803204119891	119891
Odisha	Koraput	Narayanpatana	LANGALAPADA	912100000000000000	912139800000000000	912139803204000000	912139803204119894	119894
Odisha	Koraput	Narayanpatana	TENTULIPADAR	912100000000000000	912139800000000000	912139803204000000	912139803204119898	119898
Odisha	Koraput	Narayanpatana	BORIGI	912100000000000000	912139800000000000	912139803204000000	912139803204119892	119892
Odisha	Koraput	Narayanpatana	PODAPADAR	912100000000000000	912139800000000000	912139803204000000	912139803204119896	119896
Odisha	Koraput	Narayanpatana	KUMBHARI	912100000000000000	912139800000000000	912139803204000000	912139803204119893	119893
Odisha	Koraput	Narayanpatana	BALIPETA	912100000000000000	912139800000000000	912139803204000000	912139803204119890	119890
Odisha	Koraput	Narayanpatana	NARAYANAPATNA	912100000000000000	912139800000000000	912139803204000000	912139803204119895	119895
Odisha	Koraput	Narayanpatana	KANAGAM	912100000000000000	912139800000000000	912139803204000000	912139803204119708	119708
Odisha	Koraput	Padua	PANTHULUNG	912100000000000000	912139800000000000	912139803213000000	912139803213119886	119886
Odisha	Koraput	Padua	GOLLURU	912100000000000000	912139800000000000	912139803213000000	912139803213119874	119874
Odisha	Koraput	Padua	KULARSING	912100000000000000	912139800000000000	912139803213000000	912139803213119881	119881
Odisha	Koraput	Padua	BILAPUT	912100000000000000	912139800000000000	912139803213000000	912139803213119872	119872
Odisha	Koraput	Padua	KASANDI	912100000000000000	912139800000000000	912139803213000000	912139803213119877	119877
Odisha	Koraput	Padua	PADUA	912100000000000000	912139800000000000	912139803213000000	912139803213119885	119885
Odisha	Koraput	Padua	CHATUA	912100000000000000	912139800000000000	912139803213000000	912139803213119873	119873
Odisha	Koraput	Padua	PARAJABEDAPADA	912100000000000000	912139800000000000	912139803213000000	912139803213119887	119887
Odisha	Koraput	Padua	ATTANDA	912100000000000000	912139800000000000	912139803213000000	912139803213119868	119868
Odisha	Koraput	Padua	KULABIR	912100000000000000	912139800000000000	912139803213000000	912139803213119880	119880
Odisha	Koraput	Padua	HATIBARI	912100000000000000	912139800000000000	912139803213000000	912139803213119875	119875
Odisha	Koraput	Padua	BALDA	912100000000000000	912139800000000000	912139803213000000	912139803213119870	119870
Odisha	Koraput	Padua	THUBA	912100000000000000	912139800000000000	912139803213000000	912139803213119889	119889
Odisha	Koraput	Padua	MALIBELGAON	912100000000000000	912139800000000000	912139803213000000	912139803213119882	119882
Odisha	Koraput	Padua	RAISING	912100000000000000	912139800000000000	912139803213000000	912139803213119888	119888
Odisha	Koraput	Padua	NANDAKA	912100000000000000	912139800000000000	912139803213000000	912139803213119883	119883
Odisha	Koraput	Padua	BHEJA	912100000000000000	912139800000000000	912139803213000000	912139803213119871	119871
Odisha	Koraput	Padua	BADEL	912100000000000000	912139800000000000	912139803213000000	912139803213119869	119869
Odisha	Koraput	Padua	HIKIMPUT	912100000000000000	912139800000000000	912139803213000000	912139803213119876	119876
Odisha	Koraput	Padua	KHURJI	912100000000000000	912139800000000000	912139803213000000	912139803213119879	119879
Odisha	Koraput	Pottangi	TALAGOLURU	912100000000000000	912139800000000000	912139803218000000	912139803218119912	119912
Odisha	Koraput	Pottangi	NUAGAON	912100000000000000	912139800000000000	912139803218000000	912139803218119905	119905
Odisha	Koraput	Pottangi	AMPAVALLI	912100000000000000	912139800000000000	912139803218000000	912139803218119899	119899
Odisha	Koraput	Pottangi	PETTERU	912100000000000000	912139800000000000	912139803218000000	912139803218119906	119906
Odisha	Koraput	Pottangi	SAMBAI	912100000000000000	912139800000000000	912139803218000000	912139803218119910	119910
Odisha	Koraput	Pottangi	KOTIYA	912100000000000000	912139800000000000	912139803218000000	912139803218119903	119903
Odisha	Koraput	Pottangi	SUNKI	912100000000000000	912139800000000000	912139803218000000	912139803218119911	119911
Odisha	Koraput	Pottangi	CHANDAKA	912100000000000000	912139800000000000	912139803218000000	912139803218119900	119900
Odisha	Koraput	Pottangi	PUKALI	912100000000000000	912139800000000000	912139803218000000	912139803218119908	119908
Odisha	Koraput	Pottangi	SADAM	912100000000000000	912139800000000000	912139803218000000	912139803218119925	119925
Odisha	Koraput	Pottangi	GANGARAJPUR	912100000000000000	912139800000000000	912139803218000000	912139803218119902	119902
Odisha	Koraput	Pottangi	NANDAKA	912100000000000000	912139800000000000	912139803218000000	912139803218119883	119883
Odisha	Koraput	Pottangi	DEOPATTANGI	912100000000000000	912139800000000000	912139803218000000	912139803218119901	119901
Odisha	Koraput	Pottangi	POTTANGI	912100000000000000	912139800000000000	912139803218000000	912139803218119907	119907
Odisha	Koraput	Pottangi	RALEGADA	912100000000000000	912139800000000000	912139803218000000	912139803218119909	119909
Odisha	Koraput	Pottangi	SUBAI	912100000000000000	912139800000000000	912139803218000000	912139803218119927	119927
Odisha	Koraput	Pottangi	MALIPUT	912100000000000000	912139800000000000	912139803218000000	912139803218119904	119904
Odisha	Koraput	Pottangi	RENGA	912100000000000000	912139800000000000	912139803218000000	912139803218119924	119924
Odisha	Koraput	Pottangi	DUMURIPADAR	912100000000000000	912139800000000000	912139803218000000	912139803218119801	119801
Odisha	Koraput	Pottangi	PARAJAMUTHAI	912100000000000000	912139800000000000	912139803218000000	912139803218119921	119921
Odisha	Koraput	Pottangi	HATAGUDA	912100000000000000	912139800000000000	912139803218000000	912139803218119917	119917
Odisha	Koraput	Pottangi	GUNTHAPUT	912100000000000000	912139800000000000	912139803218000000	912139803218119916	119916
Odisha	Koraput	Pottangi	KUNDULI	912100000000000000	912139800000000000	912139803218000000	912139803218119919	119919
Odisha	Koraput	Pottangi	KHUDI	912100000000000000	912139800000000000	912139803218000000	912139803218119918	119918
Odisha	Koraput	Pottangi	SORISPADAR	912100000000000000	912139800000000000	912139803218000000	912139803218119926	119926
Odisha	Koraput	Pottangi	UPPARKANTI	912100000000000000	912139800000000000	912139803218000000	912139803218119928	119928
Odisha	Koraput	Pottangi	DUDHARI	912100000000000000	912139800000000000	912139803218000000	912139803218119915	119915
Odisha	Koraput	Similiguda	PITAGUDA	912100000000000000	912139800000000000	912139803215000000	912139803215119922	119922
Odisha	Koraput	Similiguda	DOLAIGUDA	912100000000000000	912139800000000000	912139803215000000	912139803215119914	119914
Odisha	Koraput	Similiguda	GUNTHAPUT	912100000000000000	912139800000000000	912139803215000000	912139803215119916	119916
Odisha	Koraput	Similiguda	RAJPUT	912100000000000000	912139800000000000	912139803215000000	912139803215119923	119923
Odisha	Koraput	Similiguda	PAKJHOLA	912100000000000000	912139800000000000	912139803215000000	912139803215119920	119920
Odisha	Koraput	Similiguda	SAKOSINGHA	912100000000000000	912139800000000000	912139803215000000	912139803215115625	115625
Odisha	Koraput	Similiguda	DUDHARI	912100000000000000	912139800000000000	912139803215000000	912139803215119915	119915
Odisha	Koraput	Sunabeda	RAJPUT	912100000000000000	912139800000000000	912139803216000000	912139803216119923	119923
Odisha	Koraput	Sunabeda	R AJPALMA	912100000000000000	912139800000000000	912139803216000000	912139803216275157	275157
Odisha	Koraput	Sunabeda	CHARANGUL	912100000000000000	912139800000000000	912139803216000000	912139803216119913	119913
Odisha	Koraput	Sunabeda	\N	912100000000000000	912139800000000000	912139803216000000	912139803216250573	250573
Odisha	Koraput	Sunabeda	PADMAPUR	912100000000000000	912139800000000000	912139803216000000	912139803216119810	119810
Odisha	Malkangiri	Chitrakonda	NAKAMAMUDI	912100000000000000	912139900000000000	912139903223000000	912139903223119988	119988
Odisha	Malkangiri	Chitrakonda	GANGALA	912100000000000000	912139900000000000	912139903223000000	912139903223119995	119995
Odisha	Malkangiri	Chitrakonda	JODAMBA	912100000000000000	912139900000000000	912139903223000000	912139903223119986	119986
Odisha	Malkangiri	Chitrakonda	BADAPADAR	912100000000000000	912139900000000000	912139903223000000	912139903223119985	119985
Odisha	Malkangiri	Chitrakonda	NUAGUDA	912100000000000000	912139900000000000	912139903223000000	912139903223119974	119974
Odisha	Malkangiri	Chitrakonda	KAPATUTI	912100000000000000	912139900000000000	912139903223000000	912139903223119968	119968
Odisha	Malkangiri	Chitrakonda	GUNTHAWADA	912100000000000000	912139900000000000	912139903223000000	912139903223119966	119966
Odisha	Malkangiri	Chitrakonda	DORAGUDA	912100000000000000	912139900000000000	912139903223000000	912139903223119963	119963
Odisha	Malkangiri	Chitrakonda	CHITRAKONDA	912100000000000000	912139900000000000	912139903223000000	912139903223119962	119962
Odisha	Malkangiri	Chitrakonda	RALEGADA	912100000000000000	912139900000000000	912139903223000000	912139903223119992	119992
Odisha	Malkangiri	Chitrakonda	PAPERMETLA	912100000000000000	912139900000000000	912139903223000000	912139903223119990	119990
Odisha	Malkangiri	Chitrakonda	BADAPADA	912100000000000000	912139900000000000	912139903223000000	912139903223119984	119984
Odisha	Malkangiri	Chitrakonda	TUNNEL-CAMP	912100000000000000	912139900000000000	912139903223000000	912139903223119982	119982
Odisha	Malkangiri	Chitrakonda	NILAKAMBERU	912100000000000000	912139900000000000	912139903223000000	912139903223119973	119973
Odisha	Malkangiri	Jodamba	PANASPUT	912100000000000000	912139900000000000	912139903225000000	912139903225119989	119989
Odisha	Malkangiri	Jodamba	ANDRAPALLI	912100000000000000	912139900000000000	912139903225000000	912139903225119983	119983
Odisha	Malkangiri	Jodamba	PAPERMETLA	912100000000000000	912139900000000000	912139903225000000	912139903225119990	119990
Odisha	Malkangiri	Jodamba	JODAMBA	912100000000000000	912139900000000000	912139903225000000	912139903225119986	119986
Odisha	Malkangiri	Jodamba	BADAPADA	912100000000000000	912139900000000000	912139903225000000	912139903225119984	119984
Odisha	Malkangiri	Jodamba	BADAPADAR	912100000000000000	912139900000000000	912139903225000000	912139903225119985	119985
Odisha	Malkangiri	Jodamba	RALEGADA	912100000000000000	912139900000000000	912139903225000000	912139903225119992	119992
Odisha	Malkangiri	Jodamba	NAKAMAMUDI	912100000000000000	912139900000000000	912139903225000000	912139903225119988	119988
Odisha	Malkangiri	Jodamba	DORAGUDA	912100000000000000	912139900000000000	912139903225000000	912139903225119963	119963
Odisha	Malkangiri	Kalimela	MANYAMKUNDA	912100000000000000	912139900000000000	912139903227000000	912139903227119943	119943
Odisha	Malkangiri	Kalimela	POPULUR	912100000000000000	912139900000000000	912139903227000000	912139903227119945	119945
Odisha	Malkangiri	Kalimela	TELERAI	912100000000000000	912139900000000000	912139903227000000	912139903227119946	119946
Odisha	Malkangiri	Kalimela	KURUMANURU	912100000000000000	912139900000000000	912139903227000000	912139903227119939	119939
Odisha	Malkangiri	Kalimela	BODIGETTA	912100000000000000	912139900000000000	912139903227000000	912139903227119930	119930
Odisha	Malkangiri	Kalimela	BHEJANGIWADA	912100000000000000	912139900000000000	912139903227000000	912139903227119929	119929
Odisha	Malkangiri	Kalimela	KALDAPALLI	912100000000000000	912139900000000000	912139903227000000	912139903227120029	120029
Odisha	Malkangiri	Kalimela	KOIMETLA	912100000000000000	912139900000000000	912139903227000000	912139903227119938	119938
Odisha	Malkangiri	Kalimela	kALIMELA	912100000000000000	912139900000000000	912139903227000000	912139903227119936	119936
Odisha	Malkangiri	Kalimela	GOMPAKUNDA	912100000000000000	912139900000000000	912139903227000000	912139903227119934	119934
Odisha	Malkangiri	Kalimela	NILIGUDA	912100000000000000	912139900000000000	912139903227000000	912139903227120032	120032
Odisha	Malkangiri	Kalimela	CHINTALWADA	912100000000000000	912139900000000000	912139903227000000	912139903227119931	119931
Odisha	Malkangiri	Kalimela	UNDRUKUNDA	912100000000000000	912139900000000000	912139903227000000	912139903227119948	119948
Odisha	Malkangiri	Kalimela	VENKATAPALLAM	912100000000000000	912139900000000000	912139903227000000	912139903227119949	119949
Odisha	Malkangiri	Kalimela	BAPANPALLI	912100000000000000	912139900000000000	912139903227000000	912139903227120027	120027
Odisha	Malkangiri	Kalimela	OLD-CHIMITAPALLI	912100000000000000	912139900000000000	912139903227000000	912139903227119975	119975
Odisha	Malkangiri	Kalimela	KANGURUKUNDA	912100000000000000	912139900000000000	912139903227000000	912139903227119937	119937
Odisha	Malkangiri	Kalimela	CHITRANGAPALLI	912100000000000000	912139900000000000	912139903227000000	912139903227119932	119932
Odisha	Malkangiri	Kalimela	MATERU	912100000000000000	912139900000000000	912139903227000000	912139903227120030	120030
Odisha	Malkangiri	Kalimela	GUMUKA	912100000000000000	912139900000000000	912139903227000000	912139903227119935	119935
Odisha	Malkangiri	Kalimela	BADILI	912100000000000000	912139900000000000	912139903227000000	912139903227119959	119959
Odisha	Malkangiri	Kalimela	MANDAPALLI	912100000000000000	912139900000000000	912139903227000000	912139903227119970	119970
Odisha	Malkangiri	M.V. 79	TANDBAI	912100000000000000	912139900000000000	912139903229000000	912139903229120036	120036
Odisha	Malkangiri	M.V. 79	MAHARAJAPALLI	912100000000000000	912139900000000000	912139903229000000	912139903229119941	119941
Odisha	Malkangiri	M.V. 79	TELERAI	912100000000000000	912139900000000000	912139903229000000	912139903229119946	119946
Odisha	Malkangiri	M.V. 79	MANYAMKUNDA	912100000000000000	912139900000000000	912139903229000000	912139903229119943	119943
Odisha	Malkangiri	M.V. 79	NALAGUNTHI	912100000000000000	912139900000000000	912139903229000000	912139903229119944	119944
Odisha	Malkangiri	M.V. 79	BHUBANPALLI	912100000000000000	912139900000000000	912139903229000000	912139903229120028	120028
Odisha	Malkangiri	M.V. 79	SIMILIBANCHA	912100000000000000	912139900000000000	912139903229000000	912139903229120035	120035
Odisha	Malkangiri	M.V. 79	TIGAL	912100000000000000	912139900000000000	912139903229000000	912139903229119947	119947
Odisha	Malkangiri	M.V. 79	GIRKANPALLI	912100000000000000	912139900000000000	912139903229000000	912139903229119933	119933
Odisha	Malkangiri	M.V. 79	KALDAPALLI	912100000000000000	912139900000000000	912139903229000000	912139903229120029	120029
Odisha	Malkangiri	Malkangiri	TUMUSPALLI	912100000000000000	912139900000000000	912139903220000000	912139903220119981	119981
Odisha	Malkangiri	Malkangiri	SINDRIMAL	912100000000000000	912139900000000000	912139903220000000	912139903220120004	120004
Odisha	Malkangiri	Malkangiri	SIKHAPALLI	912100000000000000	912139900000000000	912139903220000000	912139903220119977	119977
Odisha	Malkangiri	Malkangiri	KORUKONDA	912100000000000000	912139900000000000	912139903220000000	912139903220119969	119969
Odisha	Malkangiri	Malkangiri	PEDAWADA	912100000000000000	912139900000000000	912139903220000000	912139903220120002	120002
Odisha	Malkangiri	Malkangiri	TARLAKOTTA	912100000000000000	912139900000000000	912139903220000000	912139903220119980	119980
Odisha	Malkangiri	Malkangiri	NUAGUDA	912100000000000000	912139900000000000	912139903220000000	912139903220119974	119974
Odisha	Malkangiri	Malkangiri	TAMASA	912100000000000000	912139900000000000	912139903220000000	912139903220120005	120005
Odisha	Malkangiri	Malkangiri	TANDAPALLI	912100000000000000	912139900000000000	912139903220000000	912139903220119978	119978
Odisha	Malkangiri	Malkangiri	BHANDARIPANGAM	912100000000000000	912139900000000000	912139903220000000	912139903220120008	120008
Odisha	Malkangiri	Malkangiri	BADILI	912100000000000000	912139900000000000	912139903220000000	912139903220119959	119959
Odisha	Malkangiri	Malkangiri	MATAPAKA	912100000000000000	912139900000000000	912139903220000000	912139903220119972	119972
Odisha	Malkangiri	Malkangiri	BHEJAGUDA	912100000000000000	912139900000000000	912139903220000000	912139903220120009	120009
Odisha	Malkangiri	Malkangiri	GOUDAGUDA	912100000000000000	912139900000000000	912139903220000000	912139903220119996	119996
Odisha	Malkangiri	Malkangiri	CHALLANGUDA	912100000000000000	912139900000000000	912139903220000000	912139903220119960	119960
Odisha	Malkangiri	Malkangiri	TANDIKI	912100000000000000	912139900000000000	912139903220000000	912139903220119979	119979
Odisha	Malkangiri	Malkangiri	BIRLAXMANPUR	912100000000000000	912139900000000000	912139903220000000	912139903220119994	119994
Odisha	Malkangiri	Malkangiri	GANGALA	912100000000000000	912139900000000000	912139903220000000	912139903220119995	119995
Odisha	Malkangiri	Malkangiri	\N	912100000000000000	912139900000000000	912139903220000000	912139903220263345	263345
Odisha	Malkangiri	Malkangiri	PANDRIPANI	912100000000000000	912139900000000000	912139903220000000	912139903220120000	120000
Odisha	Malkangiri	Malkangiri	MARKAPALLI	912100000000000000	912139900000000000	912139903220000000	912139903220119998	119998
Odisha	Malkangiri	Malkangiri	PADMAGIRI	912100000000000000	912139900000000000	912139903220000000	912139903220119999	119999
Odisha	Malkangiri	Malkangiri	JHARAPALLI	912100000000000000	912139900000000000	912139903220000000	912139903220119997	119997
Odisha	Malkangiri	Malkangiri	UDUPA	912100000000000000	912139900000000000	912139903220000000	912139903220120006	120006
Odisha	Malkangiri	Malkangiri	NAIKGUDA	912100000000000000	912139900000000000	912139903220000000	912139903220120022	120022
Odisha	Malkangiri	Malkangiri	SERPALLI	912100000000000000	912139900000000000	912139903220000000	912139903220120003	120003
Odisha	Malkangiri	Malkangiri	PEDAKUNDA	912100000000000000	912139900000000000	912139903220000000	912139903220120001	120001
Odisha	Malkangiri	Malkangiri	DALAPATIGUDA	912100000000000000	912139900000000000	912139903220000000	912139903220120012	120012
Odisha	Malkangiri	Malkangiri	MANDAPALLI	912100000000000000	912139900000000000	912139903220000000	912139903220119970	119970
Odisha	Malkangiri	Malkangiri	DUDAMETTA	912100000000000000	912139900000000000	912139903220000000	912139903220119964	119964
Odisha	Malkangiri	Malkangiri	MARIWADA	912100000000000000	912139900000000000	912139903220000000	912139903220119971	119971
Odisha	Malkangiri	Malkangiri	GORAKHUNTA	912100000000000000	912139900000000000	912139903220000000	912139903220119965	119965
Odisha	Malkangiri	Malkangiri	NAKAMAMUDI	912100000000000000	912139900000000000	912139903220000000	912139903220119988	119988
Odisha	Malkangiri	Mathili	CHAULAMENDI	912100000000000000	912139900000000000	912139903221000000	912139903221120010	120010
Odisha	Malkangiri	Mathili	MECCA	912100000000000000	912139900000000000	912139903221000000	912139903221120020	120020
Odisha	Malkangiri	Mathili	PANGAM	912100000000000000	912139900000000000	912139903221000000	912139903221120023	120023
Odisha	Malkangiri	Mathili	NAIKGUDA	912100000000000000	912139900000000000	912139903221000000	912139903221120022	120022
Odisha	Malkangiri	Mathili	AMBAGUDA	912100000000000000	912139900000000000	912139903221000000	912139903221120007	120007
Odisha	Malkangiri	Mathili	BHANDARIPANGAM	912100000000000000	912139900000000000	912139903221000000	912139903221120008	120008
Odisha	Malkangiri	Mathili	CHEDENGA	912100000000000000	912139900000000000	912139903221000000	912139903221120011	120011
Odisha	Malkangiri	Mathili	DHUNGIAPUT	912100000000000000	912139900000000000	912139903221000000	912139903221120013	120013
Odisha	Malkangiri	Mathili	MATHILI	912100000000000000	912139900000000000	912139903221000000	912139903221120019	120019
Odisha	Malkangiri	Mathili	UDULIBEDA	912100000000000000	912139900000000000	912139903221000000	912139903221120026	120026
Odisha	Malkangiri	Mathili	DALAPATIGUDA	912100000000000000	912139900000000000	912139903221000000	912139903221120012	120012
Odisha	Malkangiri	Mathili	KATAPALLI	912100000000000000	912139900000000000	912139903221000000	912139903221120016	120016
Odisha	Malkangiri	Mathili	MADKAPADAR	912100000000000000	912139900000000000	912139903221000000	912139903221119955	119955
Odisha	Malkangiri	Mathili	KUTUNIPALLI	912100000000000000	912139900000000000	912139903221000000	912139903221120018	120018
Odisha	Malkangiri	Mathili	TEMURUPALLI	912100000000000000	912139900000000000	912139903221000000	912139903221120025	120025
Odisha	Malkangiri	Mathili	GOVINDAPALLI	912100000000000000	912139900000000000	912139903221000000	912139903221119952	119952
Odisha	Malkangiri	Mathili	KIANGO	912100000000000000	912139900000000000	912139903221000000	912139903221120017	120017
Odisha	Malkangiri	Mathili	SALIMI	912100000000000000	912139900000000000	912139903221000000	912139903221120024	120024
Odisha	Malkangiri	Mathili	MOHUPADAR	912100000000000000	912139900000000000	912139903221000000	912139903221120021	120021
Odisha	Malkangiri	Mathili	BHEJAGUDA	912100000000000000	912139900000000000	912139903221000000	912139903221120009	120009
Odisha	Malkangiri	Mathili	KAMARPALLI	912100000000000000	912139900000000000	912139903221000000	912139903221120014	120014
Odisha	Malkangiri	Mathili	KARATANPALLI	912100000000000000	912139900000000000	912139903221000000	912139903221120015	120015
Odisha	Malkangiri	Motu	PUSUGUDA	912100000000000000	912139900000000000	912139903230000000	912139903230120034	120034
Odisha	Malkangiri	Motu	MALAVARAM	912100000000000000	912139900000000000	912139903230000000	912139903230119942	119942
Odisha	Malkangiri	Motu	MOTTU	912100000000000000	912139900000000000	912139903230000000	912139903230120031	120031
Odisha	Malkangiri	Motu	MANYAMKUNDA	912100000000000000	912139900000000000	912139903230000000	912139903230119943	119943
Odisha	Malkangiri	Motu	LUGEL	912100000000000000	912139900000000000	912139903230000000	912139903230119940	119940
Odisha	Malkangiri	Motu	MAHARAJAPALLI	912100000000000000	912139900000000000	912139903230000000	912139903230119941	119941
Odisha	Malkangiri	Mudulipada	MUDULIPODA	912100000000000000	912139900000000000	912139903222000000	912139903222119956	119956
Odisha	Malkangiri	Mudulipada	ANDRAHAL	912100000000000000	912139900000000000	912139903222000000	912139903222119950	119950
Odisha	Malkangiri	Mudulipada	KHAIRAPUT	912100000000000000	912139900000000000	912139903222000000	912139903222119954	119954
Odisha	Malkangiri	Mudulipada	KIANGO	912100000000000000	912139900000000000	912139903222000000	912139903222120017	120017
Odisha	Malkangiri	Mudulipada	RASBEDA	912100000000000000	912139900000000000	912139903222000000	912139903222119958	119958
Odisha	Malkangiri	Mudulipada	KADAMGUDA	912100000000000000	912139900000000000	912139903222000000	912139903222119953	119953
Odisha	Malkangiri	Mudulipada	PODAGHAT	912100000000000000	912139900000000000	912139903222000000	912139903222119957	119957
Odisha	Malkangiri	Mudulipada	BADODURAL	912100000000000000	912139900000000000	912139903222000000	912139903222119951	119951
Odisha	Malkangiri	Mudulipada	MATHILI	912100000000000000	912139900000000000	912139903222000000	912139903222120019	120019
Odisha	Malkangiri	Mudulipada	UDULIBEDA	912100000000000000	912139900000000000	912139903222000000	912139903222120026	120026
Odisha	Malkangiri	Mudulipada	GOVINDAPALLI	912100000000000000	912139900000000000	912139903222000000	912139903222119952	119952
Odisha	Malkangiri	Mudulipada	MADKAPADAR	912100000000000000	912139900000000000	912139903222000000	912139903222119955	119955
Odisha	Malkangiri	Orkel	SOMANATHPUR	912100000000000000	912139900000000000	912139903226000000	912139903226119993	119993
Odisha	Malkangiri	Orkel	CHITAPARI-III	912100000000000000	912139900000000000	912139903226000000	912139903226119961	119961
Odisha	Malkangiri	Orkel	TARLAKOTTA	912100000000000000	912139900000000000	912139903226000000	912139903226119980	119980
Odisha	Malkangiri	Orkel	POTREL	912100000000000000	912139900000000000	912139903226000000	912139903226119976	119976
Odisha	Malkangiri	Orkel	NAKAMAMUDI	912100000000000000	912139900000000000	912139903226000000	912139903226119988	119988
Odisha	Malkangiri	Orkel	KUDUMULUGUMMA	912100000000000000	912139900000000000	912139903226000000	912139903226119987	119987
Odisha	Malkangiri	Orkel	PARKHANMALLA	912100000000000000	912139900000000000	912139903226000000	912139903226119991	119991
Odisha	Malkangiri	Orkel	PAPERMETLA	912100000000000000	912139900000000000	912139903226000000	912139903226119990	119990
Odisha	Malkangiri	Orkel	GANGALA	912100000000000000	912139900000000000	912139903226000000	912139903226119995	119995
Odisha	Malkangiri	Orkel	PEDAWADA	912100000000000000	912139900000000000	912139903226000000	912139903226120002	120002
Odisha	Malkangiri	Orkel	RASBEDA	912100000000000000	912139900000000000	912139903226000000	912139903226119958	119958
Odisha	Malkangiri	Orkel	PODAGHAT	912100000000000000	912139900000000000	912139903226000000	912139903226119957	119957
Odisha	Malkangiri	Orkel	KAMWADA	912100000000000000	912139900000000000	912139903226000000	912139903226119967	119967
Odisha	Malkangiri	Orkel	OLD-CHIMITAPALLI	912100000000000000	912139900000000000	912139903226000000	912139903226119975	119975
Odisha	Malkangiri	Orkel	\N	912100000000000000	912139900000000000	912139903226000000	912139903226250576	250576
Odisha	Malkangiri	Orkel	NILAKAMBERU	912100000000000000	912139900000000000	912139903226000000	912139903226119973	119973
Odisha	Malkangiri	Orkel	KORUKONDA	912100000000000000	912139900000000000	912139903226000000	912139903226119969	119969
Odisha	Malkangiri	Paparmetla	BADAPADA	912100000000000000	912139900000000000	912139903224000000	912139903224119984	119984
Odisha	Malkangiri	Paparmetla	RALEGADA	912100000000000000	912139900000000000	912139903224000000	912139903224119992	119992
Odisha	Malkangiri	Paparmetla	KAPATUTI	912100000000000000	912139900000000000	912139903224000000	912139903224119968	119968
Odisha	Malkangiri	Paparmetla	NAKAMAMUDI	912100000000000000	912139900000000000	912139903224000000	912139903224119988	119988
Odisha	Malkangiri	Paparmetla	PANDRIPANI	912100000000000000	912139900000000000	912139903224000000	912139903224120000	120000
Odisha	Malkangiri	Paparmetla	GAJALMAMUDI	912100000000000000	912139900000000000	912139903224000000	912139903224275082	275082
Odisha	Malkangiri	Paparmetla	BADAPADAR	912100000000000000	912139900000000000	912139903224000000	912139903224119985	119985
Odisha	Malkangiri	Paparmetla	PAPERMETLA	912100000000000000	912139900000000000	912139903224000000	912139903224119990	119990
Odisha	Malkangiri	Paparmetla	DHULIPUT	912100000000000000	912139900000000000	912139903224000000	912139903224275077	275077
Odisha	Malkangiri	Podia	GOMPAKUNDA	912100000000000000	912139900000000000	912139903228000000	912139903228119934	119934
Odisha	Malkangiri	Podia	NILIGUDA	912100000000000000	912139900000000000	912139903228000000	912139903228120032	120032
Odisha	Malkangiri	Podia	GIRKANPALLI	912100000000000000	912139900000000000	912139903228000000	912139903228119933	119933
Odisha	Malkangiri	Podia	SIMILIBANCHA	912100000000000000	912139900000000000	912139903228000000	912139903228120035	120035
Odisha	Malkangiri	Podia	KALDAPALLI	912100000000000000	912139900000000000	912139903228000000	912139903228120029	120029
Odisha	Malkangiri	Podia	BAPANPALLI	912100000000000000	912139900000000000	912139903228000000	912139903228120027	120027
Odisha	Malkangiri	Podia	PODIA	912100000000000000	912139900000000000	912139903228000000	912139903228120033	120033
Odisha	Malkangiri	Podia	BHEJANGIWADA	912100000000000000	912139900000000000	912139903228000000	912139903228119929	119929
Odisha	Malkangiri	Podia	CHITRANGAPALLI	912100000000000000	912139900000000000	912139903228000000	912139903228119932	119932
Odisha	Malkangiri	Podia	MATERU	912100000000000000	912139900000000000	912139903228000000	912139903228120030	120030
Odisha	Malkangiri	Podia	GUMUKA	912100000000000000	912139900000000000	912139903228000000	912139903228119935	119935
Odisha	Nuapada	Boden	KARANGAMAL	912100000000000000	912139400000000000	912139403157000000	912139403157120773	120773
Odisha	Nuapada	Boden	BARGAON	912100000000000000	912139400000000000	912139403157000000	912139403157120854	120854
Odisha	Nuapada	Boden	LARKA	912100000000000000	912139400000000000	912139403157000000	912139403157120776	120776
Odisha	Nuapada	Boden	BABEBIR	912100000000000000	912139400000000000	912139403157000000	912139403157120767	120767
Odisha	Nuapada	Boden	LITISARGI	912100000000000000	912139400000000000	912139403157000000	912139403157120777	120777
Odisha	Nuapada	Boden	ROKAL	912100000000000000	912139400000000000	912139403157000000	912139403157120779	120779
Odisha	Nuapada	Boden	BOIRGAON	912100000000000000	912139400000000000	912139403157000000	912139403157120770	120770
Odisha	Nuapada	Boden	FARSARA	912100000000000000	912139400000000000	912139403157000000	912139403157120772	120772
Odisha	Nuapada	Boden	BODEN	912100000000000000	912139400000000000	912139403157000000	912139403157120769	120769
Odisha	Nuapada	Boden	BHAINSADANI	912100000000000000	912139400000000000	912139403157000000	912139403157120768	120768
Odisha	Nuapada	Boden	SUNAPUR	912100000000000000	912139400000000000	912139403157000000	912139403157120780	120780
Odisha	Nuapada	Boden	KHAIRA	912100000000000000	912139400000000000	912139403157000000	912139403157120775	120775
Odisha	Nuapada	Boden	PALSADA	912100000000000000	912139400000000000	912139403157000000	912139403157275094	275094
Odisha	Nuapada	Boden	DAMJHAR	912100000000000000	912139400000000000	912139403157000000	912139403157120771	120771
Odisha	Nuapada	Boden	NAGPADA	912100000000000000	912139400000000000	912139403157000000	912139403157120778	120778
Odisha	Nuapada	Boden	KARLAKOTE	912100000000000000	912139400000000000	912139403157000000	912139403157120774	120774
Odisha	Nuapada	Jonk	SALIHA	912100000000000000	912139400000000000	912139403153000000	912139403153120851	120851
Odisha	Nuapada	Jonk	DARLIMUNDA	912100000000000000	912139400000000000	912139403153000000	912139403153120836	120836
Odisha	Nuapada	Jonk	KODOMERI	912100000000000000	912139400000000000	912139403153000000	912139403153120844	120844
Odisha	Nuapada	Jonk	BOIRBHADI	912100000000000000	912139400000000000	912139403153000000	912139403153120833	120833
Odisha	Nuapada	Jonk	JAMPANI	912100000000000000	912139400000000000	912139403153000000	912139403153120840	120840
Odisha	Nuapada	Jonk	AMSENA	912100000000000000	912139400000000000	912139403153000000	912139403153120827	120827
Odisha	Nuapada	Jonk	DUMERPANI	912100000000000000	912139400000000000	912139403153000000	912139403153120838	120838
Odisha	Nuapada	Jonk	Bhainsatal	912100000000000000	912139400000000000	912139403153000000	912139403153275458	275458
Odisha	Nuapada	Jonk	BIROMAL	912100000000000000	912139400000000000	912139403153000000	912139403153120831	120831
Odisha	Nuapada	Jonk	BUDHIPALLI	912100000000000000	912139400000000000	912139403153000000	912139403153120834	120834
Odisha	Nuapada	Jonk	KULIABANDHA	912100000000000000	912139400000000000	912139403153000000	912139403153120846	120846
Odisha	Nuapada	Jonk	\N	912100000000000000	912139400000000000	912139403153000000	912139403153250556	250556
Odisha	Nuapada	Jonk	PARKOD	912100000000000000	912139400000000000	912139403153000000	912139403153120849	120849
Odisha	Nuapada	Jonk	Bhanpur	912100000000000000	912139400000000000	912139403153000000	912139403153275437	275437
Odisha	Nuapada	Jonk	BISORA	912100000000000000	912139400000000000	912139403153000000	912139403153120832	120832
Odisha	Nuapada	Jonk	Kadomeri	912100000000000000	912139400000000000	912139403153000000	912139403153275457	275457
Odisha	Nuapada	Jonk	BELTUKRI	912100000000000000	912139400000000000	912139403153000000	912139403153120828	120828
Odisha	Nuapada	Jonk	BHALESWAR	912100000000000000	912139400000000000	912139403153000000	912139403153120829	120829
Odisha	Nuapada	Khariar	DABRI	912100000000000000	912139400000000000	912139403156000000	912139403156120788	120788
Odisha	Nuapada	Khariar	SUNARI SIKUAN	912100000000000000	912139400000000000	912139403156000000	912139403156275685	275685
Odisha	Nuapada	Khariar	KIRIKITA	912100000000000000	912139400000000000	912139403156000000	912139403156120792	120792
Odisha	Nuapada	Khariar	KHASBAHAL	912100000000000000	912139400000000000	912139403156000000	912139403156120790	120790
Odisha	Nuapada	Khariar	RANIMUNDA	912100000000000000	912139400000000000	912139403156000000	912139403156120795	120795
Odisha	Nuapada	Khariar	DUAJHAR	912100000000000000	912139400000000000	912139403156000000	912139403156120789	120789
Odisha	Nuapada	Khariar	SARDHAPUR	912100000000000000	912139400000000000	912139403156000000	912139403156120797	120797
Odisha	Nuapada	Khariar	LANJI	912100000000000000	912139400000000000	912139403156000000	912139403156120793	120793
Odisha	Nuapada	Khariar	BADDOHEL	912100000000000000	912139400000000000	912139403156000000	912139403156275489	275489
Odisha	Nuapada	Khariar	DOHELPADA	912100000000000000	912139400000000000	912139403156000000	912139403156275669	275669
Odisha	Nuapada	Khariar	LARKA	912100000000000000	912139400000000000	912139403156000000	912139403156120776	120776
Odisha	Nuapada	Khariar	NEHENA	912100000000000000	912139400000000000	912139403156000000	912139403156120794	120794
Odisha	Nuapada	Khariar	MANDOSIL	912100000000000000	912139400000000000	912139403156000000	912139403156275487	275487
Odisha	Nuapada	Khariar	BHOJPUR	912100000000000000	912139400000000000	912139403156000000	912139403156120784	120784
Odisha	Nuapada	Khariar	BIRIGHAT	912100000000000000	912139400000000000	912139403156000000	912139403156120786	120786
Odisha	Nuapada	Khariar	BADI	912100000000000000	912139400000000000	912139403156000000	912139403156120782	120782
Odisha	Nuapada	Khariar	RISIGAON	912100000000000000	912139400000000000	912139403156000000	912139403156275492	275492
Odisha	Nuapada	Khariar	PALSADA	912100000000000000	912139400000000000	912139403156000000	912139403156275094	275094
Odisha	Nuapada	Khariar	KENDUPATI	912100000000000000	912139400000000000	912139403156000000	912139403156275493	275493
Odisha	Nuapada	Khariar	TUKLA	912100000000000000	912139400000000000	912139403156000000	912139403156120798	120798
Odisha	Nuapada	Khariar	CHINDAGUDA	912100000000000000	912139400000000000	912139403156000000	912139403156120787	120787
Odisha	Nuapada	Khariar	KHUDPEJ	912100000000000000	912139400000000000	912139403156000000	912139403156120791	120791
Odisha	Nuapada	Khariar	\N	912100000000000000	912139400000000000	912139403156000000	912139403156250557	250557
Odisha	Nuapada	Khariar	GADRAMUNDA	912100000000000000	912139400000000000	912139403156000000	912139403156275486	275486
Odisha	Nuapada	Khariar	AREDA	912100000000000000	912139400000000000	912139403156000000	912139403156120781	120781
Odisha	Nuapada	Khariar	BARGAON	912100000000000000	912139400000000000	912139403156000000	912139403156120783	120783
Odisha	Nuapada	Khariar	CHANABEDA	912100000000000000	912139400000000000	912139403156000000	912139403156275491	275491
Odisha	Nuapada	Khariar	BHULIASIKUAN	912100000000000000	912139400000000000	912139403156000000	912139403156120785	120785
Odisha	Nuapada	Khariar	SANMAHESWAR	912100000000000000	912139400000000000	912139403156000000	912139403156120796	120796
Odisha	Nuapada	Komna	SUNABEDA	912100000000000000	912139400000000000	912139403155000000	912139403155120822	120822
Odisha	Nuapada	Komna	KARLAKOTE	912100000000000000	912139400000000000	912139403155000000	912139403155120774	120774
Odisha	Nuapada	Komna	NAGPADA	912100000000000000	912139400000000000	912139403155000000	912139403155120778	120778
Odisha	Nuapada	Komna	PNENDRAWAN	912100000000000000	912139400000000000	912139403155000000	912139403155120816	120816
Odisha	Nuapada	Komna	TIKRAPADA	912100000000000000	912139400000000000	912139403155000000	912139403155120825	120825
Odisha	Nuapada	Komna	RAJNA	912100000000000000	912139400000000000	912139403155000000	912139403155120817	120817
Odisha	Nuapada	Komna	KONABIRA	912100000000000000	912139400000000000	912139403155000000	912139403155120809	120809
Odisha	Nuapada	Komna	SIALATI	912100000000000000	912139400000000000	912139403155000000	912139403155120819	120819
Odisha	Nuapada	Komna	DEODHARA	912100000000000000	912139400000000000	912139403155000000	912139403155275462	275462
Odisha	Nuapada	Komna	SOSENG	912100000000000000	912139400000000000	912139403155000000	912139403155120821	120821
Odisha	Nuapada	Komna	AGREN	912100000000000000	912139400000000000	912139403155000000	912139403155120799	120799
Odisha	Nuapada	Komna	KOMNA	912100000000000000	912139400000000000	912139403155000000	912139403155120808	120808
Odisha	Nuapada	Komna	Dhorlamunda	912100000000000000	912139400000000000	912139403155000000	912139403155275461	275461
Odisha	Nuapada	Komna	JADAMUNDA	912100000000000000	912139400000000000	912139403155000000	912139403155120804	120804
Odisha	Nuapada	Komna	MICHHAPALLI	912100000000000000	912139400000000000	912139403155000000	912139403155120813	120813
Odisha	Nuapada	Komna	BHELA	912100000000000000	912139400000000000	912139403155000000	912139403155120800	120800
Odisha	Nuapada	Komna	SAMARSING	912100000000000000	912139400000000000	912139403155000000	912139403155120818	120818
Odisha	Nuapada	Komna	Poinr	912100000000000000	912139400000000000	912139403155000000	912139403155275460	275460
Odisha	Nuapada	Komna	KURESWAR	912100000000000000	912139400000000000	912139403155000000	912139403155120810	120810
Odisha	Nuapada	Komna	JHAGRAHI	912100000000000000	912139400000000000	912139403155000000	912139403155120806	120806
Odisha	Nuapada	Komna	BUDHIKOMNA	912100000000000000	912139400000000000	912139403155000000	912139403155120801	120801
Odisha	Nuapada	Komna	THAGAPALI	912100000000000000	912139400000000000	912139403155000000	912139403155120824	120824
Odisha	Nuapada	Komna	Maniguda	912100000000000000	912139400000000000	912139403155000000	912139403155275459	275459
Odisha	Nuapada	Komna	NUAGAON	912100000000000000	912139400000000000	912139403155000000	912139403155120815	120815
Odisha	Nuapada	Komna	DARLIPADA	912100000000000000	912139400000000000	912139403155000000	912139403155120802	120802
Odisha	Nuapada	Komna	KANDETARA	912100000000000000	912139400000000000	912139403155000000	912139403155120807	120807
Odisha	Nuapada	Komna	GANDAMER	912100000000000000	912139400000000000	912139403155000000	912139403155120803	120803
Odisha	Nuapada	Komna	TARBOD	912100000000000000	912139400000000000	912139403155000000	912139403155120823	120823
Odisha	Nuapada	Komna	JATGARH	912100000000000000	912139400000000000	912139403155000000	912139403155120805	120805
Odisha	Nuapada	Komna	MUNDAPALA	912100000000000000	912139400000000000	912139403155000000	912139403155120814	120814
Odisha	Nuapada	Komna	KURUMPUR	912100000000000000	912139400000000000	912139403155000000	912139403155120811	120811
Odisha	Nuapada	Komna	LAKHANA	912100000000000000	912139400000000000	912139403155000000	912139403155120812	120812
Odisha	Nuapada	Komna	PALASIPANI	912100000000000000	912139400000000000	912139403155000000	912139403155275604	275604
Odisha	Nuapada	Komna	SILVA	912100000000000000	912139400000000000	912139403155000000	912139403155120820	120820
Odisha	Nuapada	Nuapada	DHARAMBANDHA	912100000000000000	912139400000000000	912139403154000000	912139403154120837	120837
Odisha	Nuapada	Nuapada	BHARUAMUNDA	912100000000000000	912139400000000000	912139403154000000	912139403154120830	120830
Odisha	Nuapada	Nuapada	KOTENCHUAN	912100000000000000	912139400000000000	912139403154000000	912139403154120845	120845
Odisha	Nuapada	Nuapada	KERMELI	912100000000000000	912139400000000000	912139403154000000	912139403154120842	120842
Odisha	Nuapada	Nuapada	KURUMPUR	912100000000000000	912139400000000000	912139403154000000	912139403154120811	120811
Odisha	Nuapada	Nuapada	AMANARA	912100000000000000	912139400000000000	912139403154000000	912139403154120826	120826
Odisha	Nuapada	Nuapada	SILVA	912100000000000000	912139400000000000	912139403154000000	912139403154120820	120820
Odisha	Nuapada	Nuapada	TANWAT	912100000000000000	912139400000000000	912139403154000000	912139403154120853	120853
Odisha	Nuapada	Nuapada	CHULABHAT	912100000000000000	912139400000000000	912139403154000000	912139403154120835	120835
Odisha	Nuapada	Nuapada	LAKHANA	912100000000000000	912139400000000000	912139403154000000	912139403154120812	120812
Odisha	Nuapada	Nuapada	SAMARSING	912100000000000000	912139400000000000	912139403154000000	912139403154120818	120818
Odisha	Nuapada	Nuapada	KHUTMANBHERA	912100000000000000	912139400000000000	912139403154000000	912139403154120843	120843
Odisha	Nuapada	Nuapada	MUNDAPALA	912100000000000000	912139400000000000	912139403154000000	912139403154120814	120814
Odisha	Nuapada	Nuapada	MOTANUAPADA	912100000000000000	912139400000000000	912139403154000000	912139403154120847	120847
Odisha	Nuapada	Nuapada	GODFULA	912100000000000000	912139400000000000	912139403154000000	912139403154120839	120839
Odisha	Nuapada	Nuapada	SUNARI SIKUAN	912100000000000000	912139400000000000	912139403154000000	912139403154275685	275685
Odisha	Nuapada	Nuapada	KHAIRANI	912100000000000000	912139400000000000	912139403154000000	912139403154275603	275603
Odisha	Nuapada	Nuapada	SIALATI	912100000000000000	912139400000000000	912139403154000000	912139403154120819	120819
Odisha	Nuapada	Nuapada	SARABANG	912100000000000000	912139400000000000	912139403154000000	912139403154120852	120852
Odisha	Nuapada	Nuapada	DARLIMUNDA	912100000000000000	912139400000000000	912139403154000000	912139403154120836	120836
Odisha	Nuapada	Nuapada	KENDUBAHARA	912100000000000000	912139400000000000	912139403154000000	912139403154120841	120841
Odisha	Nuapada	Nuapada	SAIPALA	912100000000000000	912139400000000000	912139403154000000	912139403154120850	120850
Odisha	Nuapada	Nuapada	DUMERPANI	912100000000000000	912139400000000000	912139403154000000	912139403154120838	120838
Odisha	Nuapada	Sinapali	CHATIAGUDA	912100000000000000	912139400000000000	912139403158000000	912139403158120856	120856
Odisha	Nuapada	Sinapali	JHARBANDH	912100000000000000	912139400000000000	912139403158000000	912139403158120863	120863
Odisha	Nuapada	Sinapali	NUAPADA	912100000000000000	912139400000000000	912139403158000000	912139403158120872	120872
Odisha	Nuapada	Sinapali	KUSUMJORE	912100000000000000	912139400000000000	912139403158000000	912139403158120867	120867
Odisha	Nuapada	Sinapali	SINGJHAR	912100000000000000	912139400000000000	912139403158000000	912139403158120874	120874
Odisha	Nuapada	Sinapali	KARANBAHALI	912100000000000000	912139400000000000	912139403158000000	912139403158120864	120864
Odisha	Nuapada	Sinapali	NUAMALPADA	912100000000000000	912139400000000000	912139403158000000	912139403158275464	275464
Odisha	Nuapada	Sinapali	KAINTAPADAR	912100000000000000	912139400000000000	912139403158000000	912139403158275485	275485
Odisha	Nuapada	Sinapali	GORLA	912100000000000000	912139400000000000	912139403158000000	912139403158120861	120861
Odisha	Nuapada	Sinapali	KENDUMUNDA	912100000000000000	912139400000000000	912139403158000000	912139403158120865	120865
Odisha	Nuapada	Sinapali	GODAL	912100000000000000	912139400000000000	912139403158000000	912139403158120860	120860
Odisha	Nuapada	Sinapali	BHARUAMUNDA	912100000000000000	912139400000000000	912139403158000000	912139403158120855	120855
Odisha	Nuapada	Sinapali	LITIGUDA	912100000000000000	912139400000000000	912139403158000000	912139403158120868	120868
Odisha	Nuapada	Sinapali	NANGALBOD	912100000000000000	912139400000000000	912139403158000000	912139403158120870	120870
Odisha	Nuapada	Sinapali	GHATMAL	912100000000000000	912139400000000000	912139403158000000	912139403158120859	120859
Odisha	Nuapada	Sinapali	HATIBANDHA	912100000000000000	912139400000000000	912139403158000000	912139403158120862	120862
Odisha	Nuapada	Sinapali	GHANTIGUDA	912100000000000000	912139400000000000	912139403158000000	912139403158120858	120858
Odisha	Nuapada	Sinapali	GHUCHAGUDA	912100000000000000	912139400000000000	912139403158000000	912139403158275463	275463
Odisha	Nuapada	Sinapali	SINAPALI	912100000000000000	912139400000000000	912139403158000000	912139403158120873	120873
Odisha	Nuapada	Sinapali	NILJEE	912100000000000000	912139400000000000	912139403158000000	912139403158120871	120871
Odisha	Nuapada	Sinapali	KHAIRABHADI	912100000000000000	912139400000000000	912139403158000000	912139403158120866	120866
Odisha	Nuapada	Sinapali	MAKHAPADAR	912100000000000000	912139400000000000	912139403158000000	912139403158120869	120869
Odisha	Nuapada	Sinapali	GANDABAHALI	912100000000000000	912139400000000000	912139403158000000	912139403158120857	120857
Odisha	Nuapada	Sinapali	KHARSEL	912100000000000000	912139400000000000	912139403158000000	912139403158275465	275465
Odisha	Nuapada	Sinapali	TIMANPUR	912100000000000000	912139400000000000	912139403158000000	912139403158120875	120875
Odisha	Nuapada	Sinapali	RANIMUNDA	912100000000000000	912139400000000000	912139403158000000	912139403158120795	120795
Odisha	Nuapada	Sinapali	RANIMUNDA	912100000000000000	912139400000000000	912139403158000000	912139403158275682	275682
RAJASTHAN	UDAIPUR	GOGUNDA	CHITRAWAS	910800000000000000	910813000000000000	910813000686000000	91081300068642410	42410
RAJASTHAN	UDAIPUR	GOGUNDA	KARECH	910800000000000000	910813000000000000	910813000686000000	910813000686262409	262409
KARNATAKA	CHIKBALLAPUR	CHIKKABALLAPURA	THIPPANAHALLI	912900000000000000	912958200000000000	912958205596000000	912958205596218845	218845
RAJASTHAN	PRATAPGARH	PRATAPGARH	MERIYA KHEDI	910800000000000000	910813100000000000	910813100699000000	91081310069936745	36745
RAJASTHAN	BHILWARA	MANDALGARH	MANGTHALA	910800000000000000	910812200000000000	910812200637000000	91081220063735850	35850
KARNATAKA	KOLAR	SRINIVASPUR	KURIGEPALLI	912900000000000000	912958100000000000	912958105590000000	912958105590219055	219055
RAJASTHAN	PRATAPGARH	PEEPALKHOONT	VEERPUR	910800000000000000	910813100000000000	910813100697000000	91081310069736761	36761
RAJASTHAN	BHILWARA	MANDAL	BHAGWANPURA	910800000000000000	910812200000000000	910812200631000000	91081220063135792	35792
RAJASTHAN	BHILWARA	MANDAL	THANA	910800000000000000	910812200000000000	910812200631000000	91081220063135823	35823
RAJASTHAN	BHILWARA	MANDALGARH	SHYAMGARH	910800000000000000	910812200000000000	910812200637000000	91081220063735862	35862
RAJASTHAN	UDAIPUR	GOGUNDA	CHATIYA KHEDI	910800000000000000	910813000000000000	910813000686000000	91081300068642409	42409
RAJASTHAN	UDAIPUR	GOGUNDA	BRAHMANON KA KALWANA	910800000000000000	910813000000000000	910813000686000000	91081300068642407	42407
RAJASTHAN	PRATAPGARH	PEEPALKHOONT	SEMALIYA	910800000000000000	910813100000000000	910813100697000000	91081310069734624	34624
RAJASTHAN	BHILWARA	MANDALGARH	JHANJHOLA	910800000000000000	910812200000000000	910812200637000000	91081220063735839	35839
KARNATAKA	CHIKBALLAPUR	BAGEPALLI	GULUR	912900000000000000	912958200000000000	912958205598000000	912958205598218769	218769
KARNATAKA	KOLAR	SRINIVASPUR	GOWNIPALLI	912900000000000000	912958100000000000	912958105590000000	912958105590219050	219050
KARNATAKA	KOLAR	KOLAR	HUTTUR	912900000000000000	912958100000000000	912958105591000000	912958105591218936	218936
KARNATAKA	CHIKBALLAPUR	BAGEPALLI	JOOLAPALYA	912900000000000000	912958200000000000	912958205598000000	912958205598218770	218770
KARNATAKA	CHIKBALLAPUR	BAGEPALLI	MARGANUKUNTE	912900000000000000	912958200000000000	912958205598000000	912958205598218773	218773
RAJASTHAN	BHILWARA	MANDALGARH	JASSU JI KA KHERA	910800000000000000	910812200000000000	910812200637000000	91081220063735838	35838
RAJASTHAN	UDAIPUR	GOGUNDA	BHANPURA	910800000000000000	910813000000000000	910813000686000000	91081300068642405	42405
RAJASTHAN	BHILWARA	MANDAL	NARELI	910800000000000000	910812200000000000	910812200631000000	91081220063135813	35813
RAJASTHAN	BHILWARA	KOTRI	BIRDHOL	910800000000000000	910812200000000000	910812200635000000	91081220063535756	35756
RAJASTHAN	PRATAPGARH	PRATAPGARH	KHEROT	910800000000000000	910813100000000000	910813100699000000	91081310069936737	36737
KARNATAKA	KOLAR	KOLAR	HOLUR	912900000000000000	912958100000000000	912958105591000000	912958105591218935	218935
RAJASTHAN	BHILWARA	MANDAL	LADUWAS	910800000000000000	910812200000000000	910812200631000000	910812200631244608	244608
RAJASTHAN	BHILWARA	MANDAL	BHABHANA	910800000000000000	910812200000000000	910812200631000000	91081220063135790	35790
RAJASTHAN	BHILWARA	MANDALGARH	MOHANPURA	910800000000000000	910812200000000000	910812200637000000	91081220063735852	35852
RAJASTHAN	CHITTAURGARH	BEGUN	BICHHOR	910800000000000000	910812600000000000	910812600657000000	91081260065736450	36450
KARNATAKA	KOLAR	SRINIVASPUR	BYRAGANAHALLI	912900000000000000	912958100000000000	912958105590000000	912958105590219047	219047
RAJASTHAN	BHILWARA	KOTRI	PAROLI	910800000000000000	910812200000000000	910812200635000000	91081220063535774	35774
KARNATAKA	KOLAR	SRINIVASPUR	ADDAGAL	912900000000000000	912958100000000000	912958105590000000	912958105590219045	219045
RAJASTHAN	BHILWARA	MANDALGARH	LADPURA	910800000000000000	910812200000000000	910812200637000000	91081220063735846	35846
KARNATAKA	CHIKBALLAPUR	SIDLAGHATTA	BASETTYHALLI	912900000000000000	912958200000000000	912958205599000000	912958205599219020	219020
KARNATAKA	CHIKBALLAPUR	BAGEPALLI	PATHAPALYA	912900000000000000	912958200000000000	912958205598000000	912958205598218780	218780
RAJASTHAN	BHILWARA	MANDALGARH	BARUNDNI	910800000000000000	910812200000000000	910812200637000000	91081220063735826	35826
RAJASTHAN	CHITTAURGARH	CHITTAURGARH	AMARPURA	910800000000000000	910812600000000000	910812600659000000	91081260065936570	36570
RAJASTHAN	UDAIPUR	GOGUNDA	BOKHADA	910800000000000000	910813000000000000	910813000686000000	91081300068642406	42406
RAJASTHAN	CHITTAURGARH	BEGUN	DORAI	910800000000000000	910812600000000000	910812600657000000	91081260065736453	36453
RAJASTHAN	CHITTAURGARH	BEGUN	ANOPPURA	910800000000000000	910812600000000000	910812600657000000	91081260065736447	36447
KARNATAKA	KOLAR	KOLAR	AMMANALLUR	912900000000000000	912958100000000000	912958105591000000	912958105591218924	218924
RAJASTHAN	BHILWARA	MANDALGARH	SRINAGAR	910800000000000000	910812200000000000	910812200637000000	91081220063735865	35865
RAJASTHAN	PALI	SOJAT	MEV	910800000000000000	910811800000000000	910811800599000000	91081180059941076	41076
RAJASTHAN	CHITTAURGARH	BEGUN	KERPURA	910800000000000000	910812600000000000	910812600657000000	91081260065736461	36461
RAJASTHAN	PRATAPGARH	PRATAPGARH	KHORIYA	910800000000000000	910813100000000000	910813100699000000	91081310069936738	36738
RAJASTHAN	BHILWARA	MANDALGARH	MOTORON KA KHERA	910800000000000000	910812200000000000	910812200637000000	91081220063735853	35853
RAJASTHAN	PRATAPGARH	PRATAPGARH	DEVGARH	910800000000000000	910813100000000000	910813100699000000	91081310069936726	36726
RAJASTHAN	BHILWARA	MANDALGARH	SARANA	910800000000000000	910812200000000000	910812200637000000	91081220063735860	35860
RAJASTHAN	PRATAPGARH	PRATAPGARH	MADHURA TALAB	910800000000000000	910813100000000000	910813100699000000	91081310069936743	36743
RAJASTHAN	BHILWARA	MANDAL	DHUWALA(KAREDA)	910800000000000000	910812200000000000	910812200631000000	91081220063135800	35800
KARNATAKA	CHIKBALLAPUR	CHINTAMANI	KORLAPARTHI	912900000000000000	912958200000000000	912958205600000000	912958205600218861	218861
KARNATAKA	CHIKBALLAPUR	BAGEPALLI	THOLLAPALLI	912900000000000000	912958200000000000	912958205598000000	912958205598218785	218785
RAJASTHAN	PRATAPGARH	PRATAPGARH	SARI PEEPLI	910800000000000000	910813100000000000	910813100699000000	91081310069936756	36756
RAJASTHAN	BHILWARA	MANDAL	KIDIMAL	910800000000000000	910812200000000000	910812200631000000	91081220063135809	35809
RAJASTHAN	BHILWARA	KOTRI	KOTRI	910800000000000000	910812200000000000	910812200635000000	91081220063535770	35770
KARNATAKA	CHIKBALLAPUR	BAGEPALLI	KOTHAKOTE	912900000000000000	912958200000000000	912958205598000000	912958205598218772	218772
RAJASTHAN	PALI	SOJAT	SOJAT ROAD	910800000000000000	910811800000000000	910811800599000000	91081180059941086	41086
KARNATAKA	KOLAR	KOLAR	SHAPUR	912900000000000000	912958100000000000	912958105591000000	912958105591218951	218951
RAJASTHAN	PRATAPGARH	PRATAPGARH	NARAYAN  KHEDA	910800000000000000	910813100000000000	910813100699000000	91081310069936748	36748
RAJASTHAN	PRATAPGARH	PEEPALKHOONT	MOTI KHERI	910800000000000000	910813100000000000	910813100697000000	91081310069736746	36746
RAJASTHAN	BHILWARA	MANDALGARH	DOLPURA	910800000000000000	910812200000000000	910812200637000000	91081220063735834	35834
RAJASTHAN	PRATAPGARH	PEEPALKHOONT	KACHOTIYA	910800000000000000	910813100000000000	910813100697000000	91081310069736734	36734
RAJASTHAN	BHILWARA	MANDAL	MOTA KA KHEDA	910800000000000000	910812200000000000	910812200631000000	91081220063135812	35812
RAJASTHAN	PRATAPGARH	PRATAPGARH	BARI LANKH	910800000000000000	910813100000000000	910813100699000000	91081310069936719	36719
KARNATAKA	CHIKBALLAPUR	SIDLAGHATTA	GANJIGUNTE	912900000000000000	912958200000000000	912958205599000000	912958205599219026	219026
KARNATAKA	CHIKBALLAPUR	SIDLAGHATTA	E.THIMMASANDRA	912900000000000000	912958200000000000	912958205599000000	912958205599219041	219041
RAJASTHAN	CHITTAURGARH	BEGUN	MADHOPURA	910800000000000000	910812600000000000	910812600657000000	91081260065736463	36463
RAJASTHAN	CHITTAURGARH	BEGUN	SUWANIYA	910800000000000000	910812600000000000	910812600657000000	91081260065736476	36476
RAJASTHAN	PRATAPGARH	PRATAPGARH	ACHAL PUR	910800000000000000	910813100000000000	910813100699000000	91081310069936713	36713
RAJASTHAN	CHITTAURGARH	BEGUN	KHERI	910800000000000000	910812600000000000	910812600657000000	91081260065736462	36462
RAJASTHAN	PRATAPGARH	PEEPALKHOONT	PEEPALKHOONT	910800000000000000	910813100000000000	910813100697000000	91081310069734620	34620
RAJASTHAN	BHILWARA	MANDALGARH	SHYAMPURA	910800000000000000	910812200000000000	910812200637000000	91081220063735863	35863
KARNATAKA	KOLAR	SRINIVASPUR	MUDIMADUGU	912900000000000000	912958100000000000	912958105590000000	912958105590219059	219059
RAJASTHAN	PRATAPGARH	PEEPALKHOONT	CHHARI	910800000000000000	910813100000000000	910813100697000000	91081310069734595	34595
RAJASTHAN	BHILWARA	MANDAL	CHITAMBA	910800000000000000	910812200000000000	910812200631000000	91081220063135798	35798
RAJASTHAN	PRATAPGARH	PRATAPGARH	PANMODI	910800000000000000	910813100000000000	910813100699000000	91081310069936751	36751
KARNATAKA	CHIKBALLAPUR	SIDLAGHATTA	TALAKAYALABETTA	912900000000000000	912958200000000000	912958205599000000	912958205599272733	272733
RAJASTHAN	UDAIPUR	GOGUNDA	SEMAD	910800000000000000	910813000000000000	910813000686000000	91081300068642438	42438
RAJASTHAN	UDAIPUR	GOGUNDA	GOGUNDA	910800000000000000	910813000000000000	910813000686000000	91081300068642416	42416
RAJASTHAN	CHITTAURGARH	BEGUN	DHAMANCHA	910800000000000000	910812600000000000	910812600657000000	91081260065736452	36452
RAJASTHAN	BHILWARA	KOTRI	NANDRIA	910800000000000000	910812200000000000	910812200635000000	91081220063535773	35773
RAJASTHAN	BHILWARA	MANDAL	GOVERDHANPURA	910800000000000000	910812200000000000	910812200631000000	91081220063135804	35804
RAJASTHAN	PRATAPGARH	PEEPALKHOONT	MOTA DHAMANIYA	910800000000000000	910813100000000000	910813100697000000	91081310069736413	36413
RAJASTHAN	BHILWARA	MANDALGARH	BIKRAN	910800000000000000	910812200000000000	910812200637000000	91081220063735830	35830
RAJASTHAN	PRATAPGARH	PRATAPGARH	GYASPUR	910800000000000000	910813100000000000	910813100699000000	91081310069936731	36731
KARNATAKA	KOLAR	SRINIVASPUR	RAYALAPADU	912900000000000000	912958100000000000	912958105590000000	912958105590219064	219064
KARNATAKA	KOLAR	SRINIVASPUR	YARRAMVARIPALLI	912900000000000000	912958100000000000	912958105590000000	912958105590219069	219069
RAJASTHAN	PRATAPGARH	PEEPALKHOONT	SUHAG PURA	910800000000000000	910813100000000000	910813100697000000	91081310069736758	36758
KARNATAKA	CHIKBALLAPUR	BAGEPALLI	NAREMADDEPALLI	912900000000000000	912958200000000000	912958205598000000	912958205598218777	218777
RAJASTHAN	CHITTAURGARH	BEGUN	MANDAWARI	910800000000000000	910812600000000000	910812600657000000	91081260065736464	36464
RAJASTHAN	UDAIPUR	GOGUNDA	JEMLI	910800000000000000	910813000000000000	910813000686000000	910813000686262408	262408
RAJASTHAN	BHILWARA	MANDALGARH	MAL KA KHERA	910800000000000000	910812200000000000	910812200637000000	91081220063735849	35849
KARNATAKA	KOLAR	KOLAR	VOKKALERI	912900000000000000	912958100000000000	912958105591000000	912958105591218959	218959
RAJASTHAN	PRATAPGARH	PEEPALKHOONT	RATAN PURIYA	910800000000000000	910813100000000000	910813100697000000	91081310069736754	36754
RAJASTHAN	PRATAPGARH	PRATAPGARH	DHAMOTAR	910800000000000000	910813100000000000	910813100699000000	91081310069936727	36727
RAJASTHAN	UDAIPUR	GOGUNDA	SAYRA	910800000000000000	910813000000000000	910813000686000000	91081300068642437	42437
RAJASTHAN	BHILWARA	KOTRI	RASED	910800000000000000	910812200000000000	910812200635000000	91081220063535775	35775
RAJASTHAN	CHITTAURGARH	BEGUN	MEGHPURA	910800000000000000	910812600000000000	910812600657000000	91081260065736466	36466
RAJASTHAN	PRATAPGARH	PEEPALKHOONT	RAM PURIYA	910800000000000000	910813100000000000	910813100697000000	91081310069736753	36753
RAJASTHAN	CHITTAURGARH	BEGUN	GOPAL PURA	910800000000000000	910812600000000000	910812600657000000	91081260065736456	36456
KARNATAKA	CHIKBALLAPUR	SIDLAGHATTA	KUNDALAGURKI	912900000000000000	912958200000000000	912958205599000000	912958205599219032	219032
RAJASTHAN	UDAIPUR	GOGUNDA	DIYAN	910800000000000000	910813000000000000	910813000686000000	91081300068642415	42415
KARNATAKA	CHIKBALLAPUR	SIDLAGHATTA	SADALI	912900000000000000	912958200000000000	912958205599000000	912958205599219039	219039
RAJASTHAN	PRATAPGARH	PRATAPGARH	NAKOR	910800000000000000	910813100000000000	910813100699000000	91081310069936747	36747
RAJASTHAN	UDAIPUR	GOGUNDA	RAWACH	910800000000000000	910813000000000000	910813000686000000	91081300068642434	42434
RAJASTHAN	PRATAPGARH	PEEPALKHOONT	KESHARPURA	910800000000000000	910813100000000000	910813100697000000	91081310069734610	34610
RAJASTHAN	CHITTAURGARH	BEGUN	JAI NAGAR	910800000000000000	910812600000000000	910812600657000000	91081260065736459	36459
KARNATAKA	CHIKBALLAPUR	BAGEPALLI	GORTHAPALLI	912900000000000000	912958200000000000	912958205598000000	912958205598218768	218768
KARNATAKA	CHIKBALLAPUR	BAGEPALLI	SOMANATHAPURA	912900000000000000	912958200000000000	912958205598000000	912958205598218783	218783
RAJASTHAN	BHILWARA	MANDAL	GYANGARH	910800000000000000	910812200000000000	910812200631000000	91081220063135803	35803
RAJASTHAN	CHITTAURGARH	BEGUN	NANDWAI	910800000000000000	910812600000000000	910812600657000000	91081260065736468	36468
RAJASTHAN	PALI	SOJAT	REPRAWAS	910800000000000000	910811800000000000	910811800599000000	91081180059941080	41080
RAJASTHAN	BHILWARA	KOTRI	KANTI	910800000000000000	910812200000000000	910812200635000000	91081220063535767	35767
RAJASTHAN	PRATAPGARH	PEEPALKHOONT	SODALPUR	910800000000000000	910813100000000000	910813100697000000	91081310069734625	34625
KARNATAKA	CHIKBALLAPUR	SIDLAGHATTA	PALICHERLU	912900000000000000	912958200000000000	912958205599000000	912958205599219037	219037
RAJASTHAN	PALI	SOJAT	BIRAWAS	910800000000000000	910811800000000000	910811800599000000	91081180059941054	41054
RAJASTHAN	BHILWARA	KOTRI	BISANIYA	910800000000000000	910812200000000000	910812200635000000	91081220063535757	35757
RAJASTHAN	PRATAPGARH	PRATAPGARH	KERWAS	910800000000000000	910813100000000000	910813100699000000	91081310069936736	36736
KARNATAKA	CHIKBALLAPUR	CHINTAMANI	KOTAGAL	912900000000000000	912958200000000000	912958205600000000	912958205600218862	218862
RAJASTHAN	PALI	SOJAT	GAGRAU	910800000000000000	910811800000000000	910811800599000000	91081180059941063	41063
KARNATAKA	KOLAR	SRINIVASPUR	LAKSHMIPURA	912900000000000000	912958100000000000	912958105590000000	912958105590219056	219056
RAJASTHAN	BHILWARA	KOTRI	KAKROLIYA GHATI	910800000000000000	910812200000000000	910812200635000000	91081220063535766	35766
RAJASTHAN	BHILWARA	MANDALGARH	SARTHALA	910800000000000000	910812200000000000	910812200637000000	91081220063735861	35861
KARNATAKA	CHIKBALLAPUR	SIDLAGHATTA	S.DEVAGANAHALLI	912900000000000000	912958200000000000	912958205599000000	912958205599219038	219038
RAJASTHAN	BHILWARA	KOTRI	KOTHAJ	910800000000000000	910812200000000000	910812200635000000	91081220063535769	35769
RAJASTHAN	BHILWARA	MANDALGARH	SINGOLI	910800000000000000	910812200000000000	910812200637000000	91081220063735864	35864
RAJASTHAN	CHITTAURGARH	BEGUN	KATUNDA	910800000000000000	910812600000000000	910812600657000000	91081260065736460	36460
RAJASTHAN	UDAIPUR	GOGUNDA	VISAMA	910800000000000000	910813000000000000	910813000686000000	91081300068642443	42443
RAJASTHAN	BHILWARA	MANDAL	SANUNDA	910800000000000000	910812200000000000	910812200631000000	91081220063135818	35818
RAJASTHAN	PRATAPGARH	PRATAPGARH	JOLAR	910800000000000000	910813100000000000	910813100699000000	91081310069936733	36733
KARNATAKA	KOLAR	KOLAR	ARABI KOTHANUR	912900000000000000	912958100000000000	912958105591000000	912958105591218925	218925
KARNATAKA	KOLAR	SRINIVASPUR	KODIPALLI	912900000000000000	912958100000000000	912958105590000000	912958105590219053	219053
RAJASTHAN	UDAIPUR	GOGUNDA	SINGHADA	910800000000000000	910813000000000000	910813000686000000	91081300068642439	42439
KARNATAKA	CHIKBALLAPUR	CHINTAMANI	SETTYHALLI	912900000000000000	912958200000000000	912958205600000000	912958205600218875	218875
RAJASTHAN	BHILWARA	MANDAL	NIMBAHERA JATAN	910800000000000000	910812200000000000	910812200631000000	91081220063135814	35814
KARNATAKA	CHIKBALLAPUR	BAGEPALLI	THIMMAMPALLI	912900000000000000	912958200000000000	912958205598000000	912958205598218784	218784
KARNATAKA	CHIKBALLAPUR	SIDLAGHATTA	DIBBURAHALLI	912900000000000000	912958200000000000	912958205599000000	912958205599219024	219024
RAJASTHAN	BHILWARA	MANDAL	DHUWALA(MANDAL)	910800000000000000	910812200000000000	910812200631000000	91081220063135801	35801
KARNATAKA	CHIKBALLAPUR	SIDLAGHATTA	THIMMANAYAKANA HALLI	912900000000000000	912958200000000000	912958205599000000	912958205599219040	219040
RAJASTHAN	UDAIPUR	GOGUNDA	KAMOL	910800000000000000	910813000000000000	910813000686000000	91081300068642421	42421
KARNATAKA	KOLAR	KOLAR	BELAMARANAHALLI	912900000000000000	912958100000000000	912958105591000000	912958105591218928	218928
\.


--
-- Data for Name: group_master; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.group_master (group_id, group_name, group_desc, start_date, end_date, parent_group_id, create_by, update_by, create_date, update_date) FROM stdin;
\.


--
-- Data for Name: language_master; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.language_master (id, name, "desc", created_by, updated_by, date, status, language_code, create_date, update_date, column1) FROM stdin;
1	English	English translation	test_user	test_user	\N	t	en	\N	\N	\N
2	Hindi	hindi translation	test_user	test_user	\N	\N	hi	\N	\N	\N
3	Odia	Odia Translation	test_user	test_user	\N	\N	or	\N	\N	\N
4	Kannada	Kannada Translation	test_user	test_user	\N	\N	kn	\N	\N	\N
5	Marathi	Marathi Translation	test_user	test_user	\N	\N	mr	\N	\N	\N
\.


--
-- Data for Name: layer_info; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.layer_info (l_id, c_id, workspace, store, server_url, layer_name, layer_date, layer_displayname, layer_metadata, is_temporal, "TIMESTAMP", field_name, field_filter, for_hh, for_ind) FROM stdin;
\.


--
-- Data for Name: layer_primary_category; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.layer_primary_category (c_id, c_name, c_discription, "TIMESTAMP", create_by, update_by, create_date, update_date) FROM stdin;
\.


--
-- Data for Name: org_master; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.org_master (id, name, description, start_date, end_date) FROM stdin;
1	fes	\N	\N	\N
\.


--
-- Data for Name: page_category_master; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.page_category_master (page_category_id, page_category_name, weight) FROM stdin;
2	Schemes Eligibility 	4
5	Entitlement Tracking	2
6	Entitlement Grievances	3
7	User Management	8
9	Schemes Administration	7
3	Reports	5
8	GIS Dashboard	6
1	Data Entry	1
10	Site configuration	12
\.


--
-- Data for Name: page_master; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.page_master (page_id, page_category_id, page_link, page_name, for_mobile, mobile_action) FROM stdin;
13	7	/User/getRoles	Roles	\N	\N
11	7	/User/getAdminUsers	Users	\N	\N
10	7	/User/getAccessPolicy	Privillages	\N	\N
1	1	/DataEntry/getAllSECCEntries	SECC Data	t	viewSECCRecords
2	1	/DataEntry/getAllIndividuals	Entitlement Card Data	t	viewEntitlementCardRecords
4	2	/Scheme/getSchemeSearch	Check Eligibility	t	viewSchemeSearch
16	2	/Scheme/getAvailableSchemes	Schemes Information	\N	\N
3	1	/DataEntry/getNewEntry	Add New Record	t	addNewEntry
6	5	/DataEntry/getEntitlementTracking	Individuals	t	viewEntitlementTrackingIndividual
19	5	/DataEntry/getEntitlementTrackingHH	Households	t	viewEntitlementTrackingHousehold
9	6	/DataEntry/getAllGrievances	Individuals	t	viewGrievanceRecordsIndividual
18	6	/DataEntry/getAllGrievancesHH	Households	t	viewEntitlementGrievancesHousehold
20	3	/Reports/getGrievanceReport	Grievances	\N	\N
5	3	/Reports/getSECCReports	SECC Basic	t	viewSECCDashboard
7	8	/Reports/getGISDashboard	Map viewer	f	viewSECCGISDashboard
24	9	/Scheme/getAttributes	Attributes	f	[NULL]
25	3	/Reports/getSchemeWiseIndEligibility	Scheme wise eligibility - Individuals	\N	
15	9	/Scheme/getSchemes	Schemes Configuration	\N	\N
27	3	/Reports/getIndTrackingList	Tracking - Individuals	f	
28	3	/Reports/getHHTrackingList	Tracking - Household	f	
26	3	/Reports/getEligibleIndlist	Eligibility - Individuals	\N	
21	3	/Reports/getEligibleHHlist	Eligibility - Households	\N	\N
29	9	/Scheme/getSubjectHeads	Subject Heads (Attributes' Categories)	f	
30	3	/Reports/getEnumeratorReport	Enumerator report - Data Collection	f	
31	10	/User/getPageCategories	Categories - Administration Menu	f	\N
32	10	/User/getAllPages	Pages - Administration menu	f	\N
33	3	/Reports/getLoginHistory	Login history	f	\N
34	3	/Reports/getDashboard	Dashboard	f	\N
\.


--
-- Data for Name: range_master; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.range_master (parent_id, name, create_by, geom, id, "timestamp") FROM stdin;
\.


--
-- Data for Name: region_allocation; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.region_allocation (user_id, region_type, state_code, district_code, tehsil_code, village_code, start_date, end_date, create_by, update_by, create_date, update_date, srno) FROM stdin;
\.


--
-- Data for Name: roles_master; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.roles_master (role_id, role_name, role_desc, start_date, end_date, create_by, update_by, create_date, update_date) FROM stdin;
\.


--
-- Data for Name: sms_logs; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.sms_logs (id, db_query, sms_body, recipient_user_id, recipient_phone_number, status, sent_date, sr_no) FROM stdin;
\.


--
-- Data for Name: sms_short_codes_master; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.sms_short_codes_master (code_id, code_number, response, start_date, end_date, create_by, update_by, create_date, update_date, sr_no) FROM stdin;
\.


--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) FROM stdin;
\.


--
-- Data for Name: states_master; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.states_master (mc, create_by, update_by, create_date, update_date, start_date, end_date, state_code, name, geom, sr_no) FROM stdin;
\.


--
-- Data for Name: tehsil_master; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.tehsil_master (mc, state_code, district_code, name, start_date, end_date, create_by, update_by, create_date, update_date, geom, sr_no, hindi_name) FROM stdin;
\.


--
-- Data for Name: token_session; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.token_session (srno, device_id, token_id, user_id, start_date, end_date) FROM stdin;
\.


--
-- Data for Name: user_group_relation; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.user_group_relation (user_id, group_id, start_date, end_date, sr_no) FROM stdin;
\.


--
-- Data for Name: user_master; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.user_master (username, name, designation, landline_number, mob_number, email, address, start_date, end_date, password, sr_no, default_village) FROM stdin;
akash	akash	designation	\N	9033556241	akash@fes.org.in	FES	\N	\N	techno	1	\N
\.


--
-- Data for Name: user_roles_relation; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.user_roles_relation (user_id, role_id, start_date, end_date, sr_no) FROM stdin;
\.


--
-- Data for Name: village_master; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.village_master (mc, state_code, district_code, tehsil_code, name, start_date, end_date, create_by, update_by, create_date, update_date, geom, sr_no, panchayat_code, hindi_name) FROM stdin;
\.


--
-- Data for Name: web_sessions; Type: TABLE DATA; Schema: public; Owner: pgadmin
--

COPY public.web_sessions (id, user_id, "timestamp", ip_address, user_agent) FROM stdin;
\.


--
-- Data for Name: topology; Type: TABLE DATA; Schema: topology; Owner: postgres
--

COPY topology.topology (id, name, srid, "precision", hasz) FROM stdin;
\.


--
-- Data for Name: layer; Type: TABLE DATA; Schema: topology; Owner: postgres
--

COPY topology.layer (topology_id, layer_id, schema_name, table_name, feature_column, feature_type, level, child_id) FROM stdin;
\.


--
-- Name: access_policy_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.access_policy_id_seq', 10, true);


--
-- Name: app_version_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.app_version_id_seq', 3, true);


--
-- Name: att_master; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.att_master', 1, false);


--
-- Name: attributes_master_lang_relation_srno_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.attributes_master_lang_relation_srno_seq', 1, false);


--
-- Name: bh_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.bh_id_seq', 14, true);


--
-- Name: c_id; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.c_id', 5, true);


--
-- Name: eligibility_check_counter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.eligibility_check_counter_id_seq', 1, true);


--
-- Name: err_master; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.err_master', 1, false);


--
-- Name: groups_master; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.groups_master', 1, false);


--
-- Name: l_id; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.l_id', 8, true);


--
-- Name: page_category_master_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.page_category_master_seq', 10, true);


--
-- Name: page_master_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.page_master_seq', 34, true);


--
-- Name: region_allocation_srno_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.region_allocation_srno_seq', 1, false);


--
-- Name: roles_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.roles_seq', 1, false);


--
-- Name: s_id; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.s_id', 1, false);


--
-- Name: sc_id; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.sc_id', 3, true);


--
-- Name: schemes_master_sc_id_no_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.schemes_master_sc_id_no_seq', 211, true);


--
-- Name: shg_master_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.shg_master_id_seq', 2079, true);


--
-- Name: shg_master_ind_relation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.shg_master_ind_relation_id_seq', 1661, true);


--
-- Name: sms_logs_sr_no_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.sms_logs_sr_no_seq', 1, false);


--
-- Name: sms_short_codes_master_sr_no_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.sms_short_codes_master_sr_no_seq', 1, false);


--
-- Name: sr_no; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.sr_no', 79876, true);


--
-- Name: states_master_sr_no_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.states_master_sr_no_seq', 1, false);


--
-- Name: tehsil_master_sr_no_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.tehsil_master_sr_no_seq', 1, false);


--
-- Name: token_session_srno_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.token_session_srno_seq', 1, false);


--
-- Name: u_id; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.u_id', 1, false);


--
-- Name: user_group_relation_sr_no_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.user_group_relation_sr_no_seq', 1, false);


--
-- Name: user_master_sr_no_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.user_master_sr_no_seq', 2, true);


--
-- Name: user_role_rel; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.user_role_rel', 1, false);


--
-- Name: village_master_sr_no_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.village_master_sr_no_seq', 1, false);


--
-- Name: w_id; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.w_id', 1, false);


--
-- Name: web_sessions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgadmin
--

SELECT pg_catalog.setval('public.web_sessions_id_seq', 460, true);


--
-- Name: access_policy access_policy_pk_id; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.access_policy
    ADD CONSTRAINT access_policy_pk_id PRIMARY KEY (srno);


--
-- Name: app_version app_version_pk_id; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.app_version
    ADD CONSTRAINT app_version_pk_id PRIMARY KEY (id);


--
-- Name: attributes_master_lang_relation attributes_master_lang_relation_pk_srno; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.attributes_master_lang_relation
    ADD CONSTRAINT attributes_master_lang_relation_pk_srno PRIMARY KEY (srno);


--
-- Name: attributes_master attributes_master_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.attributes_master
    ADD CONSTRAINT attributes_master_pkey PRIMARY KEY (id);


--
-- Name: beat_master beat_master_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beat_master
    ADD CONSTRAINT beat_master_pk PRIMARY KEY (id);


--
-- Name: block_master block_master_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.block_master
    ADD CONSTRAINT block_master_pk PRIMARY KEY (id);


--
-- Name: circle_master circle_master_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.circle_master
    ADD CONSTRAINT circle_master_pk PRIMARY KEY (id);


--
-- Name: claim claim_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.claim
    ADD CONSTRAINT claim_pk PRIMARY KEY (cl_id);


--
-- Name: compartment_master compartment_master_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.compartment_master
    ADD CONSTRAINT compartment_master_pkey PRIMARY KEY (id);


--
-- Name: division_master devision_master_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.division_master
    ADD CONSTRAINT devision_master_pk PRIMARY KEY (id);


--
-- Name: district_master district_master_pk; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.district_master
    ADD CONSTRAINT district_master_pk PRIMARY KEY (mc);


--
-- Name: geolocations geolocations_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.geolocations
    ADD CONSTRAINT geolocations_pk PRIMARY KEY (id);


--
-- Name: gp_master gp_master_pk; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.gp_master
    ADD CONSTRAINT gp_master_pk PRIMARY KEY (mc);


--
-- Name: group_master group_master_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.group_master
    ADD CONSTRAINT group_master_pkey PRIMARY KEY (group_id);


--
-- Name: layer_primary_category layer_category_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.layer_primary_category
    ADD CONSTRAINT layer_category_pkey PRIMARY KEY (c_id);


--
-- Name: layer_info layer_info_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.layer_info
    ADD CONSTRAINT layer_info_pkey PRIMARY KEY (l_id);


--
-- Name: language_master mast_lang_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.language_master
    ADD CONSTRAINT mast_lang_pkey PRIMARY KEY (id);


--
-- Name: page_master page_master_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.page_master
    ADD CONSTRAINT page_master_pkey PRIMARY KEY (page_id);


--
-- Name: page_category_master pk_page_category_id; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.page_category_master
    ADD CONSTRAINT pk_page_category_id PRIMARY KEY (page_category_id);


--
-- Name: range_master range_master_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.range_master
    ADD CONSTRAINT range_master_pk PRIMARY KEY (id);


--
-- Name: region_allocation region_allocation_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.region_allocation
    ADD CONSTRAINT region_allocation_pkey PRIMARY KEY (srno);


--
-- Name: roles_master roles_master_pk; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.roles_master
    ADD CONSTRAINT roles_master_pk PRIMARY KEY (role_id);


--
-- Name: sms_logs sms_logs_id_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.sms_logs
    ADD CONSTRAINT sms_logs_id_pkey PRIMARY KEY (id);


--
-- Name: sms_short_codes_master sms_short_codes_master_code_number_key; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.sms_short_codes_master
    ADD CONSTRAINT sms_short_codes_master_code_number_key UNIQUE (code_number);


--
-- Name: sms_short_codes_master sms_short_codes_master_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.sms_short_codes_master
    ADD CONSTRAINT sms_short_codes_master_pkey PRIMARY KEY (code_id);


--
-- Name: states_master states_master_pk; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.states_master
    ADD CONSTRAINT states_master_pk PRIMARY KEY (mc);


--
-- Name: tehsil_master tehsil_master_pk; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.tehsil_master
    ADD CONSTRAINT tehsil_master_pk PRIMARY KEY (mc);


--
-- Name: token_session token_session_pk_srno; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.token_session
    ADD CONSTRAINT token_session_pk_srno PRIMARY KEY (srno);


--
-- Name: user_master user_master_pkey; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.user_master
    ADD CONSTRAINT user_master_pkey PRIMARY KEY (username);


--
-- Name: village_master village_master_pk; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.village_master
    ADD CONSTRAINT village_master_pk PRIMARY KEY (mc);


--
-- Name: web_sessions web_sessions_id_pk; Type: CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.web_sessions
    ADD CONSTRAINT web_sessions_id_pk PRIMARY KEY (id);


--
-- Name: codes; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX codes ON public.village_master USING btree (state_code, district_code, tehsil_code, mc);


--
-- Name: mc; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX mc ON public.village_master USING btree (mc);


--
-- Name: vill_name; Type: INDEX; Schema: public; Owner: pgadmin
--

CREATE INDEX vill_name ON public.village_master USING btree (name);


--
-- Name: app_version app_version_fk_create_by; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.app_version
    ADD CONSTRAINT app_version_fk_create_by FOREIGN KEY (create_by) REFERENCES public.user_master(username);


--
-- Name: attributes_master attributes_master_added_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.attributes_master
    ADD CONSTRAINT attributes_master_added_by_fkey FOREIGN KEY (create_by) REFERENCES public.user_master(username);


--
-- Name: attributes_master_lang_relation attributes_master_lang_relation_fk_f_id; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.attributes_master_lang_relation
    ADD CONSTRAINT attributes_master_lang_relation_fk_f_id FOREIGN KEY (f_id) REFERENCES public.attributes_master(id);


--
-- Name: attributes_master_lang_relation attributes_master_lang_relation_fk_l_id; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.attributes_master_lang_relation
    ADD CONSTRAINT attributes_master_lang_relation_fk_l_id FOREIGN KEY (l_id) REFERENCES public.language_master(id);


--
-- Name: attributes_master attributes_master_updated_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.attributes_master
    ADD CONSTRAINT attributes_master_updated_by_fkey FOREIGN KEY (update_by) REFERENCES public.user_master(username);


--
-- Name: district_master district_master_create_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.district_master
    ADD CONSTRAINT district_master_create_by_fkey FOREIGN KEY (create_by) REFERENCES public.user_master(username);


--
-- Name: district_master district_master_states_master_fk; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.district_master
    ADD CONSTRAINT district_master_states_master_fk FOREIGN KEY (state_code) REFERENCES public.states_master(mc);


--
-- Name: district_master district_master_update_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.district_master
    ADD CONSTRAINT district_master_update_by_fkey FOREIGN KEY (update_by) REFERENCES public.user_master(username);


--
-- Name: geolocations geolocations_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.geolocations
    ADD CONSTRAINT geolocations_fk FOREIGN KEY (c_id) REFERENCES public.claim(cl_id);


--
-- Name: group_master group_master_create_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.group_master
    ADD CONSTRAINT group_master_create_by_fkey FOREIGN KEY (create_by) REFERENCES public.user_master(username);


--
-- Name: group_master group_master_update_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.group_master
    ADD CONSTRAINT group_master_update_by_fkey FOREIGN KEY (update_by) REFERENCES public.user_master(username);


--
-- Name: layer_primary_category layer_category_user_master_fk; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.layer_primary_category
    ADD CONSTRAINT layer_category_user_master_fk FOREIGN KEY (create_by) REFERENCES public.user_master(username);


--
-- Name: layer_primary_category layer_category_user_master_update_fk; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.layer_primary_category
    ADD CONSTRAINT layer_category_user_master_update_fk FOREIGN KEY (update_by) REFERENCES public.user_master(username);


--
-- Name: layer_info layer_info_layer_primary_category_fk; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.layer_info
    ADD CONSTRAINT layer_info_layer_primary_category_fk FOREIGN KEY (c_id) REFERENCES public.layer_primary_category(c_id);


--
-- Name: region_allocation region_allocation_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.region_allocation
    ADD CONSTRAINT region_allocation_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.user_master(username);


--
-- Name: roles_master roles_master_create_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.roles_master
    ADD CONSTRAINT roles_master_create_by_fkey FOREIGN KEY (create_by) REFERENCES public.user_master(username);


--
-- Name: roles_master roles_master_update_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.roles_master
    ADD CONSTRAINT roles_master_update_by_fkey FOREIGN KEY (update_by) REFERENCES public.user_master(username);


--
-- Name: sms_logs sms_logs_sent_to_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.sms_logs
    ADD CONSTRAINT sms_logs_sent_to_fkey FOREIGN KEY (recipient_user_id) REFERENCES public.user_master(username);


--
-- Name: sms_short_codes_master sms_short_codes_master_create_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.sms_short_codes_master
    ADD CONSTRAINT sms_short_codes_master_create_by_fkey FOREIGN KEY (create_by) REFERENCES public.user_master(username);


--
-- Name: sms_short_codes_master sms_short_codes_master_update_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.sms_short_codes_master
    ADD CONSTRAINT sms_short_codes_master_update_by_fkey FOREIGN KEY (update_by) REFERENCES public.user_master(username);


--
-- Name: states_master states_master_create_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.states_master
    ADD CONSTRAINT states_master_create_by_fkey FOREIGN KEY (create_by) REFERENCES public.user_master(username);


--
-- Name: states_master states_master_update_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.states_master
    ADD CONSTRAINT states_master_update_by_fkey FOREIGN KEY (update_by) REFERENCES public.user_master(username);


--
-- Name: tehsil_master tehsil_master_created_by_user_master_fk; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.tehsil_master
    ADD CONSTRAINT tehsil_master_created_by_user_master_fk FOREIGN KEY (create_by) REFERENCES public.user_master(username);


--
-- Name: tehsil_master tehsil_master_district_master_fk; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.tehsil_master
    ADD CONSTRAINT tehsil_master_district_master_fk FOREIGN KEY (district_code) REFERENCES public.district_master(mc);


--
-- Name: tehsil_master tehsil_master_states_master_fk; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.tehsil_master
    ADD CONSTRAINT tehsil_master_states_master_fk FOREIGN KEY (state_code) REFERENCES public.states_master(mc);


--
-- Name: tehsil_master tehsil_master_updated_by_user_master_fk; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.tehsil_master
    ADD CONSTRAINT tehsil_master_updated_by_user_master_fk FOREIGN KEY (update_by) REFERENCES public.user_master(username);


--
-- Name: token_session token_session_fk_user_id; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.token_session
    ADD CONSTRAINT token_session_fk_user_id FOREIGN KEY (user_id) REFERENCES public.user_master(username);


--
-- Name: user_group_relation user_group_relation_group_master_fk; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.user_group_relation
    ADD CONSTRAINT user_group_relation_group_master_fk FOREIGN KEY (group_id) REFERENCES public.group_master(group_id);


--
-- Name: user_group_relation user_group_relation_user_master_fk; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.user_group_relation
    ADD CONSTRAINT user_group_relation_user_master_fk FOREIGN KEY (user_id) REFERENCES public.user_master(username);


--
-- Name: user_roles_relation user_roles_relation_roles_master_fk; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.user_roles_relation
    ADD CONSTRAINT user_roles_relation_roles_master_fk FOREIGN KEY (role_id) REFERENCES public.roles_master(role_id);


--
-- Name: user_roles_relation user_roles_relation_user_master_fk; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.user_roles_relation
    ADD CONSTRAINT user_roles_relation_user_master_fk FOREIGN KEY (user_id) REFERENCES public.user_master(username);


--
-- Name: village_master village_master_update_by_user_master_fk; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.village_master
    ADD CONSTRAINT village_master_update_by_user_master_fk FOREIGN KEY (update_by) REFERENCES public.user_master(username);


--
-- Name: village_master village_master_user_master_fk; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.village_master
    ADD CONSTRAINT village_master_user_master_fk FOREIGN KEY (create_by) REFERENCES public.user_master(username);


--
-- Name: web_sessions web_sessions_user_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: pgadmin
--

ALTER TABLE ONLY public.web_sessions
    ADD CONSTRAINT web_sessions_user_id_fk FOREIGN KEY (user_id) REFERENCES public.user_master(username);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO pgadmin;
GRANT ALL ON SCHEMA public TO fra_user;


--
-- Name: FUNCTION raster_in(cstring); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_in(cstring) TO geet;
GRANT ALL ON FUNCTION public.raster_in(cstring) TO geet_web;


--
-- Name: FUNCTION raster_out(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_out(public.raster) TO geet;
GRANT ALL ON FUNCTION public.raster_out(public.raster) TO geet_web;


--
-- Name: FUNCTION box2d_in(cstring); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box2d_in(cstring) TO geet;
GRANT ALL ON FUNCTION public.box2d_in(cstring) TO geet_web;


--
-- Name: FUNCTION box2d_out(public.box2d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box2d_out(public.box2d) TO geet;
GRANT ALL ON FUNCTION public.box2d_out(public.box2d) TO geet_web;


--
-- Name: FUNCTION box2df_in(cstring); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box2df_in(cstring) TO geet;
GRANT ALL ON FUNCTION public.box2df_in(cstring) TO geet_web;


--
-- Name: FUNCTION box2df_out(public.box2df); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box2df_out(public.box2df) TO geet;
GRANT ALL ON FUNCTION public.box2df_out(public.box2df) TO geet_web;


--
-- Name: FUNCTION box3d_in(cstring); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box3d_in(cstring) TO geet;
GRANT ALL ON FUNCTION public.box3d_in(cstring) TO geet_web;


--
-- Name: FUNCTION box3d_out(public.box3d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box3d_out(public.box3d) TO geet;
GRANT ALL ON FUNCTION public.box3d_out(public.box3d) TO geet_web;


--
-- Name: FUNCTION geography_analyze(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_analyze(internal) TO geet;
GRANT ALL ON FUNCTION public.geography_analyze(internal) TO geet_web;


--
-- Name: FUNCTION geography_in(cstring, oid, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_in(cstring, oid, integer) TO geet;
GRANT ALL ON FUNCTION public.geography_in(cstring, oid, integer) TO geet_web;


--
-- Name: FUNCTION geography_out(public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_out(public.geography) TO geet;
GRANT ALL ON FUNCTION public.geography_out(public.geography) TO geet_web;


--
-- Name: FUNCTION geography_recv(internal, oid, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_recv(internal, oid, integer) TO geet;
GRANT ALL ON FUNCTION public.geography_recv(internal, oid, integer) TO geet_web;


--
-- Name: FUNCTION geography_send(public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_send(public.geography) TO geet;
GRANT ALL ON FUNCTION public.geography_send(public.geography) TO geet_web;


--
-- Name: FUNCTION geography_typmod_in(cstring[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_typmod_in(cstring[]) TO geet;
GRANT ALL ON FUNCTION public.geography_typmod_in(cstring[]) TO geet_web;


--
-- Name: FUNCTION geography_typmod_out(integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_typmod_out(integer) TO geet;
GRANT ALL ON FUNCTION public.geography_typmod_out(integer) TO geet_web;


--
-- Name: FUNCTION geometry_analyze(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_analyze(internal) TO geet;
GRANT ALL ON FUNCTION public.geometry_analyze(internal) TO geet_web;


--
-- Name: FUNCTION geometry_in(cstring); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_in(cstring) TO geet;
GRANT ALL ON FUNCTION public.geometry_in(cstring) TO geet_web;


--
-- Name: FUNCTION geometry_out(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_out(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_out(public.geometry) TO geet_web;


--
-- Name: FUNCTION geometry_recv(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_recv(internal) TO geet;
GRANT ALL ON FUNCTION public.geometry_recv(internal) TO geet_web;


--
-- Name: FUNCTION geometry_send(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_send(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_send(public.geometry) TO geet_web;


--
-- Name: FUNCTION geometry_typmod_in(cstring[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_typmod_in(cstring[]) TO geet;
GRANT ALL ON FUNCTION public.geometry_typmod_in(cstring[]) TO geet_web;


--
-- Name: FUNCTION geometry_typmod_out(integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_typmod_out(integer) TO geet;
GRANT ALL ON FUNCTION public.geometry_typmod_out(integer) TO geet_web;


--
-- Name: FUNCTION gidx_in(cstring); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.gidx_in(cstring) TO geet;
GRANT ALL ON FUNCTION public.gidx_in(cstring) TO geet_web;


--
-- Name: FUNCTION gidx_out(public.gidx); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.gidx_out(public.gidx) TO geet;
GRANT ALL ON FUNCTION public.gidx_out(public.gidx) TO geet_web;


--
-- Name: FUNCTION pgis_abs_in(cstring); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_abs_in(cstring) TO geet;
GRANT ALL ON FUNCTION public.pgis_abs_in(cstring) TO geet_web;


--
-- Name: FUNCTION pgis_abs_out(public.pgis_abs); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_abs_out(public.pgis_abs) TO geet;
GRANT ALL ON FUNCTION public.pgis_abs_out(public.pgis_abs) TO geet_web;


--
-- Name: FUNCTION spheroid_in(cstring); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.spheroid_in(cstring) TO geet;
GRANT ALL ON FUNCTION public.spheroid_in(cstring) TO geet_web;


--
-- Name: FUNCTION spheroid_out(public.spheroid); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.spheroid_out(public.spheroid) TO geet;
GRANT ALL ON FUNCTION public.spheroid_out(public.spheroid) TO geet_web;


--
-- Name: FUNCTION __st_countagg_transfn(agg public.agg_count, rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.__st_countagg_transfn(agg public.agg_count, rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO geet;
GRANT ALL ON FUNCTION public.__st_countagg_transfn(agg public.agg_count, rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO geet_web;


--
-- Name: FUNCTION _add_overview_constraint(ovschema name, ovtable name, ovcolumn name, refschema name, reftable name, refcolumn name, factor integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_overview_constraint(ovschema name, ovtable name, ovcolumn name, refschema name, reftable name, refcolumn name, factor integer) TO geet;
GRANT ALL ON FUNCTION public._add_overview_constraint(ovschema name, ovtable name, ovcolumn name, refschema name, reftable name, refcolumn name, factor integer) TO geet_web;


--
-- Name: FUNCTION _add_raster_constraint(cn name, sql text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_raster_constraint(cn name, sql text) TO geet;
GRANT ALL ON FUNCTION public._add_raster_constraint(cn name, sql text) TO geet_web;


--
-- Name: FUNCTION _add_raster_constraint_alignment(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_raster_constraint_alignment(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._add_raster_constraint_alignment(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _add_raster_constraint_blocksize(rastschema name, rasttable name, rastcolumn name, axis text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_raster_constraint_blocksize(rastschema name, rasttable name, rastcolumn name, axis text) TO geet;
GRANT ALL ON FUNCTION public._add_raster_constraint_blocksize(rastschema name, rasttable name, rastcolumn name, axis text) TO geet_web;


--
-- Name: FUNCTION _add_raster_constraint_coverage_tile(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_raster_constraint_coverage_tile(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._add_raster_constraint_coverage_tile(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _add_raster_constraint_extent(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_raster_constraint_extent(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._add_raster_constraint_extent(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _add_raster_constraint_nodata_values(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_raster_constraint_nodata_values(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._add_raster_constraint_nodata_values(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _add_raster_constraint_num_bands(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_raster_constraint_num_bands(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._add_raster_constraint_num_bands(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _add_raster_constraint_out_db(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_raster_constraint_out_db(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._add_raster_constraint_out_db(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _add_raster_constraint_pixel_types(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_raster_constraint_pixel_types(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._add_raster_constraint_pixel_types(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _add_raster_constraint_scale(rastschema name, rasttable name, rastcolumn name, axis character); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_raster_constraint_scale(rastschema name, rasttable name, rastcolumn name, axis character) TO geet;
GRANT ALL ON FUNCTION public._add_raster_constraint_scale(rastschema name, rasttable name, rastcolumn name, axis character) TO geet_web;


--
-- Name: FUNCTION _add_raster_constraint_spatially_unique(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_raster_constraint_spatially_unique(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._add_raster_constraint_spatially_unique(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _add_raster_constraint_srid(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._add_raster_constraint_srid(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._add_raster_constraint_srid(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _drop_overview_constraint(ovschema name, ovtable name, ovcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_overview_constraint(ovschema name, ovtable name, ovcolumn name) TO geet;
GRANT ALL ON FUNCTION public._drop_overview_constraint(ovschema name, ovtable name, ovcolumn name) TO geet_web;


--
-- Name: FUNCTION _drop_raster_constraint(rastschema name, rasttable name, cn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint(rastschema name, rasttable name, cn name) TO geet;
GRANT ALL ON FUNCTION public._drop_raster_constraint(rastschema name, rasttable name, cn name) TO geet_web;


--
-- Name: FUNCTION _drop_raster_constraint_alignment(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_alignment(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._drop_raster_constraint_alignment(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _drop_raster_constraint_blocksize(rastschema name, rasttable name, rastcolumn name, axis text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_blocksize(rastschema name, rasttable name, rastcolumn name, axis text) TO geet;
GRANT ALL ON FUNCTION public._drop_raster_constraint_blocksize(rastschema name, rasttable name, rastcolumn name, axis text) TO geet_web;


--
-- Name: FUNCTION _drop_raster_constraint_coverage_tile(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_coverage_tile(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._drop_raster_constraint_coverage_tile(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _drop_raster_constraint_extent(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_extent(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._drop_raster_constraint_extent(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _drop_raster_constraint_nodata_values(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_nodata_values(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._drop_raster_constraint_nodata_values(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _drop_raster_constraint_num_bands(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_num_bands(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._drop_raster_constraint_num_bands(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _drop_raster_constraint_out_db(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_out_db(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._drop_raster_constraint_out_db(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _drop_raster_constraint_pixel_types(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_pixel_types(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._drop_raster_constraint_pixel_types(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _drop_raster_constraint_regular_blocking(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_regular_blocking(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._drop_raster_constraint_regular_blocking(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _drop_raster_constraint_scale(rastschema name, rasttable name, rastcolumn name, axis character); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_scale(rastschema name, rasttable name, rastcolumn name, axis character) TO geet;
GRANT ALL ON FUNCTION public._drop_raster_constraint_scale(rastschema name, rasttable name, rastcolumn name, axis character) TO geet_web;


--
-- Name: FUNCTION _drop_raster_constraint_spatially_unique(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_spatially_unique(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._drop_raster_constraint_spatially_unique(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _drop_raster_constraint_srid(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_srid(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._drop_raster_constraint_srid(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _overview_constraint(ov public.raster, factor integer, refschema name, reftable name, refcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._overview_constraint(ov public.raster, factor integer, refschema name, reftable name, refcolumn name) TO geet;
GRANT ALL ON FUNCTION public._overview_constraint(ov public.raster, factor integer, refschema name, reftable name, refcolumn name) TO geet_web;


--
-- Name: FUNCTION _overview_constraint_info(ovschema name, ovtable name, ovcolumn name, OUT refschema name, OUT reftable name, OUT refcolumn name, OUT factor integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._overview_constraint_info(ovschema name, ovtable name, ovcolumn name, OUT refschema name, OUT reftable name, OUT refcolumn name, OUT factor integer) TO geet;
GRANT ALL ON FUNCTION public._overview_constraint_info(ovschema name, ovtable name, ovcolumn name, OUT refschema name, OUT reftable name, OUT refcolumn name, OUT factor integer) TO geet_web;


--
-- Name: FUNCTION _postgis_deprecate(oldname text, newname text, version text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._postgis_deprecate(oldname text, newname text, version text) TO geet;
GRANT ALL ON FUNCTION public._postgis_deprecate(oldname text, newname text, version text) TO geet_web;


--
-- Name: FUNCTION _postgis_join_selectivity(regclass, text, regclass, text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._postgis_join_selectivity(regclass, text, regclass, text, text) TO geet;
GRANT ALL ON FUNCTION public._postgis_join_selectivity(regclass, text, regclass, text, text) TO geet_web;


--
-- Name: FUNCTION _postgis_selectivity(tbl regclass, att_name text, geom public.geometry, mode text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._postgis_selectivity(tbl regclass, att_name text, geom public.geometry, mode text) TO geet;
GRANT ALL ON FUNCTION public._postgis_selectivity(tbl regclass, att_name text, geom public.geometry, mode text) TO geet_web;


--
-- Name: FUNCTION _postgis_stats(tbl regclass, att_name text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._postgis_stats(tbl regclass, att_name text, text) TO geet;
GRANT ALL ON FUNCTION public._postgis_stats(tbl regclass, att_name text, text) TO geet_web;


--
-- Name: FUNCTION _raster_constraint_info_alignment(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_alignment(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._raster_constraint_info_alignment(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _raster_constraint_info_blocksize(rastschema name, rasttable name, rastcolumn name, axis text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_blocksize(rastschema name, rasttable name, rastcolumn name, axis text) TO geet;
GRANT ALL ON FUNCTION public._raster_constraint_info_blocksize(rastschema name, rasttable name, rastcolumn name, axis text) TO geet_web;


--
-- Name: FUNCTION _raster_constraint_info_coverage_tile(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_coverage_tile(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._raster_constraint_info_coverage_tile(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _raster_constraint_info_extent(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_extent(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._raster_constraint_info_extent(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _raster_constraint_info_index(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_index(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._raster_constraint_info_index(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _raster_constraint_info_nodata_values(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_nodata_values(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._raster_constraint_info_nodata_values(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _raster_constraint_info_num_bands(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_num_bands(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._raster_constraint_info_num_bands(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _raster_constraint_info_out_db(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_out_db(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._raster_constraint_info_out_db(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _raster_constraint_info_pixel_types(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_pixel_types(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._raster_constraint_info_pixel_types(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _raster_constraint_info_regular_blocking(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_regular_blocking(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._raster_constraint_info_regular_blocking(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _raster_constraint_info_scale(rastschema name, rasttable name, rastcolumn name, axis character); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_scale(rastschema name, rasttable name, rastcolumn name, axis character) TO geet;
GRANT ALL ON FUNCTION public._raster_constraint_info_scale(rastschema name, rasttable name, rastcolumn name, axis character) TO geet_web;


--
-- Name: FUNCTION _raster_constraint_info_spatially_unique(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_spatially_unique(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._raster_constraint_info_spatially_unique(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _raster_constraint_info_srid(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_info_srid(rastschema name, rasttable name, rastcolumn name) TO geet;
GRANT ALL ON FUNCTION public._raster_constraint_info_srid(rastschema name, rasttable name, rastcolumn name) TO geet_web;


--
-- Name: FUNCTION _raster_constraint_nodata_values(rast public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_nodata_values(rast public.raster) TO geet;
GRANT ALL ON FUNCTION public._raster_constraint_nodata_values(rast public.raster) TO geet_web;


--
-- Name: FUNCTION _raster_constraint_out_db(rast public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_out_db(rast public.raster) TO geet;
GRANT ALL ON FUNCTION public._raster_constraint_out_db(rast public.raster) TO geet_web;


--
-- Name: FUNCTION _raster_constraint_pixel_types(rast public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._raster_constraint_pixel_types(rast public.raster) TO geet;
GRANT ALL ON FUNCTION public._raster_constraint_pixel_types(rast public.raster) TO geet_web;


--
-- Name: FUNCTION _st_3ddfullywithin(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_3ddfullywithin(geom1 public.geometry, geom2 public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public._st_3ddfullywithin(geom1 public.geometry, geom2 public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION _st_3ddwithin(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_3ddwithin(geom1 public.geometry, geom2 public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public._st_3ddwithin(geom1 public.geometry, geom2 public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION _st_3dintersects(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_3dintersects(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public._st_3dintersects(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION _st_asgeojson(integer, public.geography, integer, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_asgeojson(integer, public.geography, integer, integer) TO geet;
GRANT ALL ON FUNCTION public._st_asgeojson(integer, public.geography, integer, integer) TO geet_web;


--
-- Name: FUNCTION _st_asgeojson(integer, public.geometry, integer, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_asgeojson(integer, public.geometry, integer, integer) TO geet;
GRANT ALL ON FUNCTION public._st_asgeojson(integer, public.geometry, integer, integer) TO geet_web;


--
-- Name: FUNCTION _st_asgml(integer, public.geography, integer, integer, text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_asgml(integer, public.geography, integer, integer, text, text) TO geet;
GRANT ALL ON FUNCTION public._st_asgml(integer, public.geography, integer, integer, text, text) TO geet_web;


--
-- Name: FUNCTION _st_asgml(integer, public.geometry, integer, integer, text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_asgml(integer, public.geometry, integer, integer, text, text) TO geet;
GRANT ALL ON FUNCTION public._st_asgml(integer, public.geometry, integer, integer, text, text) TO geet_web;


--
-- Name: FUNCTION _st_askml(integer, public.geography, integer, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_askml(integer, public.geography, integer, text) TO geet;
GRANT ALL ON FUNCTION public._st_askml(integer, public.geography, integer, text) TO geet_web;


--
-- Name: FUNCTION _st_askml(integer, public.geometry, integer, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_askml(integer, public.geometry, integer, text) TO geet;
GRANT ALL ON FUNCTION public._st_askml(integer, public.geometry, integer, text) TO geet_web;


--
-- Name: FUNCTION _st_aspect4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_aspect4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet;
GRANT ALL ON FUNCTION public._st_aspect4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet_web;


--
-- Name: FUNCTION _st_asraster(geom public.geometry, scalex double precision, scaley double precision, width integer, height integer, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_asraster(geom public.geometry, scalex double precision, scaley double precision, width integer, height integer, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, touched boolean) TO geet;
GRANT ALL ON FUNCTION public._st_asraster(geom public.geometry, scalex double precision, scaley double precision, width integer, height integer, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, touched boolean) TO geet_web;


--
-- Name: FUNCTION _st_asx3d(integer, public.geometry, integer, integer, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_asx3d(integer, public.geometry, integer, integer, text) TO geet;
GRANT ALL ON FUNCTION public._st_asx3d(integer, public.geometry, integer, integer, text) TO geet_web;


--
-- Name: FUNCTION _st_bestsrid(public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_bestsrid(public.geography) TO geet;
GRANT ALL ON FUNCTION public._st_bestsrid(public.geography) TO geet_web;


--
-- Name: FUNCTION _st_bestsrid(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_bestsrid(public.geography, public.geography) TO geet;
GRANT ALL ON FUNCTION public._st_bestsrid(public.geography, public.geography) TO geet_web;


--
-- Name: FUNCTION _st_buffer(public.geometry, double precision, cstring); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_buffer(public.geometry, double precision, cstring) TO geet;
GRANT ALL ON FUNCTION public._st_buffer(public.geometry, double precision, cstring) TO geet_web;


--
-- Name: FUNCTION _st_clip(rast public.raster, nband integer[], geom public.geometry, nodataval double precision[], crop boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_clip(rast public.raster, nband integer[], geom public.geometry, nodataval double precision[], crop boolean) TO geet;
GRANT ALL ON FUNCTION public._st_clip(rast public.raster, nband integer[], geom public.geometry, nodataval double precision[], crop boolean) TO geet_web;


--
-- Name: FUNCTION _st_colormap(rast public.raster, nband integer, colormap text, method text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_colormap(rast public.raster, nband integer, colormap text, method text) TO geet;
GRANT ALL ON FUNCTION public._st_colormap(rast public.raster, nband integer, colormap text, method text) TO geet_web;


--
-- Name: FUNCTION _st_concavehull(param_inputgeom public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_concavehull(param_inputgeom public.geometry) TO geet;
GRANT ALL ON FUNCTION public._st_concavehull(param_inputgeom public.geometry) TO geet_web;


--
-- Name: FUNCTION _st_contains(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_contains(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public._st_contains(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION _st_contains(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_contains(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet;
GRANT ALL ON FUNCTION public._st_contains(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet_web;


--
-- Name: FUNCTION _st_containsproperly(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_containsproperly(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public._st_containsproperly(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION _st_containsproperly(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_containsproperly(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet;
GRANT ALL ON FUNCTION public._st_containsproperly(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet_web;


--
-- Name: FUNCTION _st_convertarray4ma(value double precision[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_convertarray4ma(value double precision[]) TO geet;
GRANT ALL ON FUNCTION public._st_convertarray4ma(value double precision[]) TO geet_web;


--
-- Name: FUNCTION _st_count(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_count(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO geet;
GRANT ALL ON FUNCTION public._st_count(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO geet_web;


--
-- Name: FUNCTION _st_count(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_count(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO geet;
GRANT ALL ON FUNCTION public._st_count(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO geet_web;


--
-- Name: FUNCTION _st_countagg_finalfn(agg public.agg_count); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_countagg_finalfn(agg public.agg_count) TO geet;
GRANT ALL ON FUNCTION public._st_countagg_finalfn(agg public.agg_count) TO geet_web;


--
-- Name: FUNCTION _st_countagg_transfn(agg public.agg_count, rast public.raster, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_countagg_transfn(agg public.agg_count, rast public.raster, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public._st_countagg_transfn(agg public.agg_count, rast public.raster, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION _st_countagg_transfn(agg public.agg_count, rast public.raster, nband integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_countagg_transfn(agg public.agg_count, rast public.raster, nband integer, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public._st_countagg_transfn(agg public.agg_count, rast public.raster, nband integer, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION _st_countagg_transfn(agg public.agg_count, rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_countagg_transfn(agg public.agg_count, rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO geet;
GRANT ALL ON FUNCTION public._st_countagg_transfn(agg public.agg_count, rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO geet_web;


--
-- Name: FUNCTION _st_coveredby(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_coveredby(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public._st_coveredby(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION _st_coveredby(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_coveredby(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet;
GRANT ALL ON FUNCTION public._st_coveredby(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet_web;


--
-- Name: FUNCTION _st_covers(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_covers(public.geography, public.geography) TO geet;
GRANT ALL ON FUNCTION public._st_covers(public.geography, public.geography) TO geet_web;


--
-- Name: FUNCTION _st_covers(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_covers(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public._st_covers(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION _st_covers(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_covers(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet;
GRANT ALL ON FUNCTION public._st_covers(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet_web;


--
-- Name: FUNCTION _st_crosses(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_crosses(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public._st_crosses(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION _st_dfullywithin(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_dfullywithin(geom1 public.geometry, geom2 public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public._st_dfullywithin(geom1 public.geometry, geom2 public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION _st_dfullywithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_dfullywithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision) TO geet;
GRANT ALL ON FUNCTION public._st_dfullywithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision) TO geet_web;


--
-- Name: FUNCTION _st_distance(public.geography, public.geography, double precision, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_distance(public.geography, public.geography, double precision, boolean) TO geet;
GRANT ALL ON FUNCTION public._st_distance(public.geography, public.geography, double precision, boolean) TO geet_web;


--
-- Name: FUNCTION _st_distancetree(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_distancetree(public.geography, public.geography) TO geet;
GRANT ALL ON FUNCTION public._st_distancetree(public.geography, public.geography) TO geet_web;


--
-- Name: FUNCTION _st_distancetree(public.geography, public.geography, double precision, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_distancetree(public.geography, public.geography, double precision, boolean) TO geet;
GRANT ALL ON FUNCTION public._st_distancetree(public.geography, public.geography, double precision, boolean) TO geet_web;


--
-- Name: FUNCTION _st_distanceuncached(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_distanceuncached(public.geography, public.geography) TO geet;
GRANT ALL ON FUNCTION public._st_distanceuncached(public.geography, public.geography) TO geet_web;


--
-- Name: FUNCTION _st_distanceuncached(public.geography, public.geography, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_distanceuncached(public.geography, public.geography, boolean) TO geet;
GRANT ALL ON FUNCTION public._st_distanceuncached(public.geography, public.geography, boolean) TO geet_web;


--
-- Name: FUNCTION _st_distanceuncached(public.geography, public.geography, double precision, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_distanceuncached(public.geography, public.geography, double precision, boolean) TO geet;
GRANT ALL ON FUNCTION public._st_distanceuncached(public.geography, public.geography, double precision, boolean) TO geet_web;


--
-- Name: FUNCTION _st_dumppoints(the_geom public.geometry, cur_path integer[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_dumppoints(the_geom public.geometry, cur_path integer[]) TO geet;
GRANT ALL ON FUNCTION public._st_dumppoints(the_geom public.geometry, cur_path integer[]) TO geet_web;


--
-- Name: FUNCTION _st_dwithin(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_dwithin(geom1 public.geometry, geom2 public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public._st_dwithin(geom1 public.geometry, geom2 public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION _st_dwithin(public.geography, public.geography, double precision, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_dwithin(public.geography, public.geography, double precision, boolean) TO geet;
GRANT ALL ON FUNCTION public._st_dwithin(public.geography, public.geography, double precision, boolean) TO geet_web;


--
-- Name: FUNCTION _st_dwithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_dwithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision) TO geet;
GRANT ALL ON FUNCTION public._st_dwithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision) TO geet_web;


--
-- Name: FUNCTION _st_dwithinuncached(public.geography, public.geography, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_dwithinuncached(public.geography, public.geography, double precision) TO geet;
GRANT ALL ON FUNCTION public._st_dwithinuncached(public.geography, public.geography, double precision) TO geet_web;


--
-- Name: FUNCTION _st_dwithinuncached(public.geography, public.geography, double precision, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_dwithinuncached(public.geography, public.geography, double precision, boolean) TO geet;
GRANT ALL ON FUNCTION public._st_dwithinuncached(public.geography, public.geography, double precision, boolean) TO geet_web;


--
-- Name: FUNCTION _st_equals(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_equals(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public._st_equals(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION _st_expand(public.geography, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_expand(public.geography, double precision) TO geet;
GRANT ALL ON FUNCTION public._st_expand(public.geography, double precision) TO geet_web;


--
-- Name: FUNCTION _st_gdalwarp(rast public.raster, algorithm text, maxerr double precision, srid integer, scalex double precision, scaley double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, width integer, height integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_gdalwarp(rast public.raster, algorithm text, maxerr double precision, srid integer, scalex double precision, scaley double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, width integer, height integer) TO geet;
GRANT ALL ON FUNCTION public._st_gdalwarp(rast public.raster, algorithm text, maxerr double precision, srid integer, scalex double precision, scaley double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, width integer, height integer) TO geet_web;


--
-- Name: FUNCTION _st_geomfromgml(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_geomfromgml(text, integer) TO geet;
GRANT ALL ON FUNCTION public._st_geomfromgml(text, integer) TO geet_web;


--
-- Name: FUNCTION _st_hillshade4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_hillshade4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet;
GRANT ALL ON FUNCTION public._st_hillshade4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet_web;


--
-- Name: FUNCTION _st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public._st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION _st_histogram(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, min double precision, max double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_histogram(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, min double precision, max double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public._st_histogram(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, min double precision, max double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION _st_intersects(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_intersects(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public._st_intersects(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION _st_intersects(geom public.geometry, rast public.raster, nband integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_intersects(geom public.geometry, rast public.raster, nband integer) TO geet;
GRANT ALL ON FUNCTION public._st_intersects(geom public.geometry, rast public.raster, nband integer) TO geet_web;


--
-- Name: FUNCTION _st_intersects(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_intersects(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet;
GRANT ALL ON FUNCTION public._st_intersects(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet_web;


--
-- Name: FUNCTION _st_linecrossingdirection(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_linecrossingdirection(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public._st_linecrossingdirection(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION _st_longestline(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_longestline(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public._st_longestline(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION _st_mapalgebra(rastbandargset public.rastbandarg[], expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_mapalgebra(rastbandargset public.rastbandarg[], expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO geet;
GRANT ALL ON FUNCTION public._st_mapalgebra(rastbandargset public.rastbandarg[], expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO geet_web;


--
-- Name: FUNCTION _st_mapalgebra(rastbandargset public.rastbandarg[], callbackfunc regprocedure, pixeltype text, distancex integer, distancey integer, extenttype text, customextent public.raster, mask double precision[], weighted boolean, VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_mapalgebra(rastbandargset public.rastbandarg[], callbackfunc regprocedure, pixeltype text, distancex integer, distancey integer, extenttype text, customextent public.raster, mask double precision[], weighted boolean, VARIADIC userargs text[]) TO geet;
GRANT ALL ON FUNCTION public._st_mapalgebra(rastbandargset public.rastbandarg[], callbackfunc regprocedure, pixeltype text, distancex integer, distancey integer, extenttype text, customextent public.raster, mask double precision[], weighted boolean, VARIADIC userargs text[]) TO geet_web;


--
-- Name: FUNCTION _st_maxdistance(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_maxdistance(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public._st_maxdistance(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION _st_neighborhood(rast public.raster, band integer, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_neighborhood(rast public.raster, band integer, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public._st_neighborhood(rast public.raster, band integer, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION _st_orderingequals(geometrya public.geometry, geometryb public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_orderingequals(geometrya public.geometry, geometryb public.geometry) TO geet;
GRANT ALL ON FUNCTION public._st_orderingequals(geometrya public.geometry, geometryb public.geometry) TO geet_web;


--
-- Name: FUNCTION _st_overlaps(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_overlaps(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public._st_overlaps(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION _st_overlaps(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_overlaps(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet;
GRANT ALL ON FUNCTION public._st_overlaps(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet_web;


--
-- Name: FUNCTION _st_pixelaspolygons(rast public.raster, band integer, columnx integer, rowy integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_pixelaspolygons(rast public.raster, band integer, columnx integer, rowy integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer) TO geet;
GRANT ALL ON FUNCTION public._st_pixelaspolygons(rast public.raster, band integer, columnx integer, rowy integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer) TO geet_web;


--
-- Name: FUNCTION _st_pointoutside(public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_pointoutside(public.geography) TO geet;
GRANT ALL ON FUNCTION public._st_pointoutside(public.geography) TO geet_web;


--
-- Name: FUNCTION _st_quantile(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_quantile(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet;
GRANT ALL ON FUNCTION public._st_quantile(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet_web;


--
-- Name: FUNCTION _st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet;
GRANT ALL ON FUNCTION public._st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet_web;


--
-- Name: FUNCTION _st_rastertoworldcoord(rast public.raster, columnx integer, rowy integer, OUT longitude double precision, OUT latitude double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_rastertoworldcoord(rast public.raster, columnx integer, rowy integer, OUT longitude double precision, OUT latitude double precision) TO geet;
GRANT ALL ON FUNCTION public._st_rastertoworldcoord(rast public.raster, columnx integer, rowy integer, OUT longitude double precision, OUT latitude double precision) TO geet_web;


--
-- Name: FUNCTION _st_reclass(rast public.raster, VARIADIC reclassargset public.reclassarg[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_reclass(rast public.raster, VARIADIC reclassargset public.reclassarg[]) TO geet;
GRANT ALL ON FUNCTION public._st_reclass(rast public.raster, VARIADIC reclassargset public.reclassarg[]) TO geet_web;


--
-- Name: FUNCTION _st_roughness4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_roughness4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet;
GRANT ALL ON FUNCTION public._st_roughness4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet_web;


--
-- Name: FUNCTION _st_samealignment_finalfn(agg public.agg_samealignment); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_samealignment_finalfn(agg public.agg_samealignment) TO geet;
GRANT ALL ON FUNCTION public._st_samealignment_finalfn(agg public.agg_samealignment) TO geet_web;


--
-- Name: FUNCTION _st_samealignment_transfn(agg public.agg_samealignment, rast public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_samealignment_transfn(agg public.agg_samealignment, rast public.raster) TO geet;
GRANT ALL ON FUNCTION public._st_samealignment_transfn(agg public.agg_samealignment, rast public.raster) TO geet_web;


--
-- Name: FUNCTION _st_setvalues(rast public.raster, nband integer, x integer, y integer, newvalueset double precision[], noset boolean[], hasnosetvalue boolean, nosetvalue double precision, keepnodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_setvalues(rast public.raster, nband integer, x integer, y integer, newvalueset double precision[], noset boolean[], hasnosetvalue boolean, nosetvalue double precision, keepnodata boolean) TO geet;
GRANT ALL ON FUNCTION public._st_setvalues(rast public.raster, nband integer, x integer, y integer, newvalueset double precision[], noset boolean[], hasnosetvalue boolean, nosetvalue double precision, keepnodata boolean) TO geet_web;


--
-- Name: FUNCTION _st_slope4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_slope4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet;
GRANT ALL ON FUNCTION public._st_slope4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet_web;


--
-- Name: FUNCTION _st_summarystats(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_summarystats(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO geet;
GRANT ALL ON FUNCTION public._st_summarystats(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO geet_web;


--
-- Name: FUNCTION _st_summarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_summarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO geet;
GRANT ALL ON FUNCTION public._st_summarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO geet_web;


--
-- Name: FUNCTION _st_summarystats_finalfn(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_summarystats_finalfn(internal) TO geet;
GRANT ALL ON FUNCTION public._st_summarystats_finalfn(internal) TO geet_web;


--
-- Name: FUNCTION _st_summarystats_transfn(internal, public.raster, boolean, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_summarystats_transfn(internal, public.raster, boolean, double precision) TO geet;
GRANT ALL ON FUNCTION public._st_summarystats_transfn(internal, public.raster, boolean, double precision) TO geet_web;


--
-- Name: FUNCTION _st_summarystats_transfn(internal, public.raster, integer, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_summarystats_transfn(internal, public.raster, integer, boolean) TO geet;
GRANT ALL ON FUNCTION public._st_summarystats_transfn(internal, public.raster, integer, boolean) TO geet_web;


--
-- Name: FUNCTION _st_summarystats_transfn(internal, public.raster, integer, boolean, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_summarystats_transfn(internal, public.raster, integer, boolean, double precision) TO geet;
GRANT ALL ON FUNCTION public._st_summarystats_transfn(internal, public.raster, integer, boolean, double precision) TO geet_web;


--
-- Name: FUNCTION _st_tile(rast public.raster, width integer, height integer, nband integer[], padwithnodata boolean, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_tile(rast public.raster, width integer, height integer, nband integer[], padwithnodata boolean, nodataval double precision) TO geet;
GRANT ALL ON FUNCTION public._st_tile(rast public.raster, width integer, height integer, nband integer[], padwithnodata boolean, nodataval double precision) TO geet_web;


--
-- Name: FUNCTION _st_touches(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_touches(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public._st_touches(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION _st_touches(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_touches(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet;
GRANT ALL ON FUNCTION public._st_touches(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet_web;


--
-- Name: FUNCTION _st_tpi4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_tpi4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet;
GRANT ALL ON FUNCTION public._st_tpi4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet_web;


--
-- Name: FUNCTION _st_tri4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_tri4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet;
GRANT ALL ON FUNCTION public._st_tri4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet_web;


--
-- Name: FUNCTION _st_union_finalfn(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_union_finalfn(internal) TO geet;
GRANT ALL ON FUNCTION public._st_union_finalfn(internal) TO geet_web;


--
-- Name: FUNCTION _st_union_transfn(internal, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_union_transfn(internal, public.raster) TO geet;
GRANT ALL ON FUNCTION public._st_union_transfn(internal, public.raster) TO geet_web;


--
-- Name: FUNCTION _st_union_transfn(internal, public.raster, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_union_transfn(internal, public.raster, integer) TO geet;
GRANT ALL ON FUNCTION public._st_union_transfn(internal, public.raster, integer) TO geet_web;


--
-- Name: FUNCTION _st_union_transfn(internal, public.raster, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_union_transfn(internal, public.raster, text) TO geet;
GRANT ALL ON FUNCTION public._st_union_transfn(internal, public.raster, text) TO geet_web;


--
-- Name: FUNCTION _st_union_transfn(internal, public.raster, public.unionarg[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_union_transfn(internal, public.raster, public.unionarg[]) TO geet;
GRANT ALL ON FUNCTION public._st_union_transfn(internal, public.raster, public.unionarg[]) TO geet_web;


--
-- Name: FUNCTION _st_union_transfn(internal, public.raster, integer, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_union_transfn(internal, public.raster, integer, text) TO geet;
GRANT ALL ON FUNCTION public._st_union_transfn(internal, public.raster, integer, text) TO geet_web;


--
-- Name: FUNCTION _st_valuecount(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_valuecount(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public._st_valuecount(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION _st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public._st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION _st_voronoi(g1 public.geometry, clip public.geometry, tolerance double precision, return_polygons boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_voronoi(g1 public.geometry, clip public.geometry, tolerance double precision, return_polygons boolean) TO geet;
GRANT ALL ON FUNCTION public._st_voronoi(g1 public.geometry, clip public.geometry, tolerance double precision, return_polygons boolean) TO geet_web;


--
-- Name: FUNCTION _st_within(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_within(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public._st_within(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION _st_within(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_within(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet;
GRANT ALL ON FUNCTION public._st_within(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet_web;


--
-- Name: FUNCTION _st_worldtorastercoord(rast public.raster, longitude double precision, latitude double precision, OUT columnx integer, OUT rowy integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._st_worldtorastercoord(rast public.raster, longitude double precision, latitude double precision, OUT columnx integer, OUT rowy integer) TO geet;
GRANT ALL ON FUNCTION public._st_worldtorastercoord(rast public.raster, longitude double precision, latitude double precision, OUT columnx integer, OUT rowy integer) TO geet_web;


--
-- Name: FUNCTION _updaterastersrid(schema_name name, table_name name, column_name name, new_srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public._updaterastersrid(schema_name name, table_name name, column_name name, new_srid integer) TO geet;
GRANT ALL ON FUNCTION public._updaterastersrid(schema_name name, table_name name, column_name name, new_srid integer) TO geet_web;


--
-- Name: FUNCTION addauth(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.addauth(text) TO geet;
GRANT ALL ON FUNCTION public.addauth(text) TO geet_web;


--
-- Name: FUNCTION addgeometrycolumn(table_name character varying, column_name character varying, new_srid integer, new_type character varying, new_dim integer, use_typmod boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.addgeometrycolumn(table_name character varying, column_name character varying, new_srid integer, new_type character varying, new_dim integer, use_typmod boolean) TO geet;
GRANT ALL ON FUNCTION public.addgeometrycolumn(table_name character varying, column_name character varying, new_srid integer, new_type character varying, new_dim integer, use_typmod boolean) TO geet_web;


--
-- Name: FUNCTION addgeometrycolumn(schema_name character varying, table_name character varying, column_name character varying, new_srid integer, new_type character varying, new_dim integer, use_typmod boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.addgeometrycolumn(schema_name character varying, table_name character varying, column_name character varying, new_srid integer, new_type character varying, new_dim integer, use_typmod boolean) TO geet;
GRANT ALL ON FUNCTION public.addgeometrycolumn(schema_name character varying, table_name character varying, column_name character varying, new_srid integer, new_type character varying, new_dim integer, use_typmod boolean) TO geet_web;


--
-- Name: FUNCTION addgeometrycolumn(catalog_name character varying, schema_name character varying, table_name character varying, column_name character varying, new_srid_in integer, new_type character varying, new_dim integer, use_typmod boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.addgeometrycolumn(catalog_name character varying, schema_name character varying, table_name character varying, column_name character varying, new_srid_in integer, new_type character varying, new_dim integer, use_typmod boolean) TO geet;
GRANT ALL ON FUNCTION public.addgeometrycolumn(catalog_name character varying, schema_name character varying, table_name character varying, column_name character varying, new_srid_in integer, new_type character varying, new_dim integer, use_typmod boolean) TO geet_web;


--
-- Name: FUNCTION addoverviewconstraints(ovtable name, ovcolumn name, reftable name, refcolumn name, ovfactor integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.addoverviewconstraints(ovtable name, ovcolumn name, reftable name, refcolumn name, ovfactor integer) TO geet;
GRANT ALL ON FUNCTION public.addoverviewconstraints(ovtable name, ovcolumn name, reftable name, refcolumn name, ovfactor integer) TO geet_web;


--
-- Name: FUNCTION addoverviewconstraints(ovschema name, ovtable name, ovcolumn name, refschema name, reftable name, refcolumn name, ovfactor integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.addoverviewconstraints(ovschema name, ovtable name, ovcolumn name, refschema name, reftable name, refcolumn name, ovfactor integer) TO geet;
GRANT ALL ON FUNCTION public.addoverviewconstraints(ovschema name, ovtable name, ovcolumn name, refschema name, reftable name, refcolumn name, ovfactor integer) TO geet_web;


--
-- Name: FUNCTION addrasterconstraints(rasttable name, rastcolumn name, VARIADIC constraints text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.addrasterconstraints(rasttable name, rastcolumn name, VARIADIC constraints text[]) TO geet;
GRANT ALL ON FUNCTION public.addrasterconstraints(rasttable name, rastcolumn name, VARIADIC constraints text[]) TO geet_web;


--
-- Name: FUNCTION addrasterconstraints(rastschema name, rasttable name, rastcolumn name, VARIADIC constraints text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.addrasterconstraints(rastschema name, rasttable name, rastcolumn name, VARIADIC constraints text[]) TO geet;
GRANT ALL ON FUNCTION public.addrasterconstraints(rastschema name, rasttable name, rastcolumn name, VARIADIC constraints text[]) TO geet_web;


--
-- Name: FUNCTION addrasterconstraints(rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.addrasterconstraints(rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean) TO geet;
GRANT ALL ON FUNCTION public.addrasterconstraints(rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean) TO geet_web;


--
-- Name: FUNCTION addrasterconstraints(rastschema name, rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.addrasterconstraints(rastschema name, rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean) TO geet;
GRANT ALL ON FUNCTION public.addrasterconstraints(rastschema name, rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean) TO geet_web;


--
-- Name: FUNCTION availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric) TO geet;
GRANT ALL ON FUNCTION public.availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric) TO geet_web;


--
-- Name: FUNCTION availed_ed_id(param_region_type text, param_scheme_id numeric, param_district_code numeric); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.availed_ed_id(param_region_type text, param_scheme_id numeric, param_district_code numeric) TO geet;
GRANT ALL ON FUNCTION public.availed_ed_id(param_region_type text, param_scheme_id numeric, param_district_code numeric) TO geet_web;


--
-- Name: FUNCTION box(public.box3d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box(public.box3d) TO geet;
GRANT ALL ON FUNCTION public.box(public.box3d) TO geet_web;


--
-- Name: FUNCTION box(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.box(public.geometry) TO geet_web;


--
-- Name: FUNCTION box2d(public.box3d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box2d(public.box3d) TO geet;
GRANT ALL ON FUNCTION public.box2d(public.box3d) TO geet_web;


--
-- Name: FUNCTION box2d(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box2d(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.box2d(public.geometry) TO geet_web;


--
-- Name: FUNCTION box3d(public.box2d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box3d(public.box2d) TO geet;
GRANT ALL ON FUNCTION public.box3d(public.box2d) TO geet_web;


--
-- Name: FUNCTION box3d(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box3d(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.box3d(public.geometry) TO geet_web;


--
-- Name: FUNCTION box3d(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box3d(public.raster) TO geet;
GRANT ALL ON FUNCTION public.box3d(public.raster) TO geet_web;


--
-- Name: FUNCTION box3dtobox(public.box3d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.box3dtobox(public.box3d) TO geet;
GRANT ALL ON FUNCTION public.box3dtobox(public.box3d) TO geet_web;


--
-- Name: FUNCTION bytea(public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.bytea(public.geography) TO geet;
GRANT ALL ON FUNCTION public.bytea(public.geography) TO geet_web;


--
-- Name: FUNCTION bytea(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.bytea(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.bytea(public.geometry) TO geet_web;


--
-- Name: FUNCTION bytea(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.bytea(public.raster) TO geet;
GRANT ALL ON FUNCTION public.bytea(public.raster) TO geet_web;


--
-- Name: FUNCTION bytea_import(p_path text, OUT p_result bytea); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.bytea_import(p_path text, OUT p_result bytea) TO geet;
GRANT ALL ON FUNCTION public.bytea_import(p_path text, OUT p_result bytea) TO geet_web;


--
-- Name: FUNCTION checkauth(text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.checkauth(text, text) TO geet;
GRANT ALL ON FUNCTION public.checkauth(text, text) TO geet_web;


--
-- Name: FUNCTION checkauth(text, text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.checkauth(text, text, text) TO geet;
GRANT ALL ON FUNCTION public.checkauth(text, text, text) TO geet_web;


--
-- Name: FUNCTION checkauthtrigger(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.checkauthtrigger() TO geet;
GRANT ALL ON FUNCTION public.checkauthtrigger() TO geet_web;


--
-- Name: FUNCTION clone_schema(source_schema text, dest_schema text); Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON FUNCTION public.clone_schema(source_schema text, dest_schema text) TO geet;
GRANT ALL ON FUNCTION public.clone_schema(source_schema text, dest_schema text) TO geet_web;


--
-- Name: FUNCTION contains_2d(public.box2df, public.box2df); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.contains_2d(public.box2df, public.box2df) TO geet;
GRANT ALL ON FUNCTION public.contains_2d(public.box2df, public.box2df) TO geet_web;


--
-- Name: FUNCTION contains_2d(public.box2df, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.contains_2d(public.box2df, public.geometry) TO geet;
GRANT ALL ON FUNCTION public.contains_2d(public.box2df, public.geometry) TO geet_web;


--
-- Name: FUNCTION contains_2d(public.geometry, public.box2df); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.contains_2d(public.geometry, public.box2df) TO geet;
GRANT ALL ON FUNCTION public.contains_2d(public.geometry, public.box2df) TO geet_web;


--
-- Name: FUNCTION create_matview(name, name); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.create_matview(name, name) TO geet;
GRANT ALL ON FUNCTION public.create_matview(name, name) TO geet_web;


--
-- Name: FUNCTION dblink(text); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink(text) TO geet;
GRANT ALL ON FUNCTION public.dblink(text) TO geet_web;


--
-- Name: FUNCTION dblink(text, boolean); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink(text, boolean) TO geet;
GRANT ALL ON FUNCTION public.dblink(text, boolean) TO geet_web;


--
-- Name: FUNCTION dblink(text, text); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink(text, text) TO geet;
GRANT ALL ON FUNCTION public.dblink(text, text) TO geet_web;


--
-- Name: FUNCTION dblink(text, text, boolean); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink(text, text, boolean) TO geet;
GRANT ALL ON FUNCTION public.dblink(text, text, boolean) TO geet_web;


--
-- Name: FUNCTION dblink_build_sql_delete(text, int2vector, integer, text[]); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_build_sql_delete(text, int2vector, integer, text[]) TO geet;
GRANT ALL ON FUNCTION public.dblink_build_sql_delete(text, int2vector, integer, text[]) TO geet_web;


--
-- Name: FUNCTION dblink_build_sql_insert(text, int2vector, integer, text[], text[]); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_build_sql_insert(text, int2vector, integer, text[], text[]) TO geet;
GRANT ALL ON FUNCTION public.dblink_build_sql_insert(text, int2vector, integer, text[], text[]) TO geet_web;


--
-- Name: FUNCTION dblink_build_sql_update(text, int2vector, integer, text[], text[]); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_build_sql_update(text, int2vector, integer, text[], text[]) TO geet;
GRANT ALL ON FUNCTION public.dblink_build_sql_update(text, int2vector, integer, text[], text[]) TO geet_web;


--
-- Name: FUNCTION dblink_cancel_query(text); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_cancel_query(text) TO geet;
GRANT ALL ON FUNCTION public.dblink_cancel_query(text) TO geet_web;


--
-- Name: FUNCTION dblink_close(text); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_close(text) TO geet;
GRANT ALL ON FUNCTION public.dblink_close(text) TO geet_web;


--
-- Name: FUNCTION dblink_close(text, boolean); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_close(text, boolean) TO geet;
GRANT ALL ON FUNCTION public.dblink_close(text, boolean) TO geet_web;


--
-- Name: FUNCTION dblink_close(text, text); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_close(text, text) TO geet;
GRANT ALL ON FUNCTION public.dblink_close(text, text) TO geet_web;


--
-- Name: FUNCTION dblink_close(text, text, boolean); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_close(text, text, boolean) TO geet;
GRANT ALL ON FUNCTION public.dblink_close(text, text, boolean) TO geet_web;


--
-- Name: FUNCTION dblink_connect(text); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_connect(text) TO geet;
GRANT ALL ON FUNCTION public.dblink_connect(text) TO geet_web;


--
-- Name: FUNCTION dblink_connect(text, text); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_connect(text, text) TO geet;
GRANT ALL ON FUNCTION public.dblink_connect(text, text) TO geet_web;


--
-- Name: FUNCTION dblink_connect_u(text); Type: ACL; Schema: public; Owner: arpit
--

REVOKE ALL ON FUNCTION public.dblink_connect_u(text) FROM postgres;
GRANT ALL ON FUNCTION public.dblink_connect_u(text) TO arpit;
GRANT ALL ON FUNCTION public.dblink_connect_u(text) TO geet;
GRANT ALL ON FUNCTION public.dblink_connect_u(text) TO geet_web;


--
-- Name: FUNCTION dblink_connect_u(text, text); Type: ACL; Schema: public; Owner: arpit
--

REVOKE ALL ON FUNCTION public.dblink_connect_u(text, text) FROM postgres;
GRANT ALL ON FUNCTION public.dblink_connect_u(text, text) TO arpit;
GRANT ALL ON FUNCTION public.dblink_connect_u(text, text) TO geet;
GRANT ALL ON FUNCTION public.dblink_connect_u(text, text) TO geet_web;


--
-- Name: FUNCTION dblink_current_query(); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_current_query() TO geet;
GRANT ALL ON FUNCTION public.dblink_current_query() TO geet_web;


--
-- Name: FUNCTION dblink_disconnect(); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_disconnect() TO geet;
GRANT ALL ON FUNCTION public.dblink_disconnect() TO geet_web;


--
-- Name: FUNCTION dblink_disconnect(text); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_disconnect(text) TO geet;
GRANT ALL ON FUNCTION public.dblink_disconnect(text) TO geet_web;


--
-- Name: FUNCTION dblink_error_message(text); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_error_message(text) TO geet;
GRANT ALL ON FUNCTION public.dblink_error_message(text) TO geet_web;


--
-- Name: FUNCTION dblink_exec(text); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_exec(text) TO geet;
GRANT ALL ON FUNCTION public.dblink_exec(text) TO geet_web;


--
-- Name: FUNCTION dblink_exec(text, boolean); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_exec(text, boolean) TO geet;
GRANT ALL ON FUNCTION public.dblink_exec(text, boolean) TO geet_web;


--
-- Name: FUNCTION dblink_exec(text, text); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_exec(text, text) TO geet;
GRANT ALL ON FUNCTION public.dblink_exec(text, text) TO geet_web;


--
-- Name: FUNCTION dblink_exec(text, text, boolean); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_exec(text, text, boolean) TO geet;
GRANT ALL ON FUNCTION public.dblink_exec(text, text, boolean) TO geet_web;


--
-- Name: FUNCTION dblink_fdw_validator(options text[], catalog oid); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_fdw_validator(options text[], catalog oid) TO geet;
GRANT ALL ON FUNCTION public.dblink_fdw_validator(options text[], catalog oid) TO geet_web;


--
-- Name: FUNCTION dblink_fetch(text, integer); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_fetch(text, integer) TO geet;
GRANT ALL ON FUNCTION public.dblink_fetch(text, integer) TO geet_web;


--
-- Name: FUNCTION dblink_fetch(text, integer, boolean); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_fetch(text, integer, boolean) TO geet;
GRANT ALL ON FUNCTION public.dblink_fetch(text, integer, boolean) TO geet_web;


--
-- Name: FUNCTION dblink_fetch(text, text, integer); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_fetch(text, text, integer) TO geet;
GRANT ALL ON FUNCTION public.dblink_fetch(text, text, integer) TO geet_web;


--
-- Name: FUNCTION dblink_fetch(text, text, integer, boolean); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_fetch(text, text, integer, boolean) TO geet;
GRANT ALL ON FUNCTION public.dblink_fetch(text, text, integer, boolean) TO geet_web;


--
-- Name: FUNCTION dblink_get_connections(); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_get_connections() TO geet;
GRANT ALL ON FUNCTION public.dblink_get_connections() TO geet_web;


--
-- Name: FUNCTION dblink_get_notify(OUT notify_name text, OUT be_pid integer, OUT extra text); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_get_notify(OUT notify_name text, OUT be_pid integer, OUT extra text) TO geet;
GRANT ALL ON FUNCTION public.dblink_get_notify(OUT notify_name text, OUT be_pid integer, OUT extra text) TO geet_web;


--
-- Name: FUNCTION dblink_get_notify(conname text, OUT notify_name text, OUT be_pid integer, OUT extra text); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_get_notify(conname text, OUT notify_name text, OUT be_pid integer, OUT extra text) TO geet;
GRANT ALL ON FUNCTION public.dblink_get_notify(conname text, OUT notify_name text, OUT be_pid integer, OUT extra text) TO geet_web;


--
-- Name: FUNCTION dblink_get_pkey(text); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_get_pkey(text) TO geet;
GRANT ALL ON FUNCTION public.dblink_get_pkey(text) TO geet_web;


--
-- Name: FUNCTION dblink_get_result(text); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_get_result(text) TO geet;
GRANT ALL ON FUNCTION public.dblink_get_result(text) TO geet_web;


--
-- Name: FUNCTION dblink_get_result(text, boolean); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_get_result(text, boolean) TO geet;
GRANT ALL ON FUNCTION public.dblink_get_result(text, boolean) TO geet_web;


--
-- Name: FUNCTION dblink_is_busy(text); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_is_busy(text) TO geet;
GRANT ALL ON FUNCTION public.dblink_is_busy(text) TO geet_web;


--
-- Name: FUNCTION dblink_open(text, text); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_open(text, text) TO geet;
GRANT ALL ON FUNCTION public.dblink_open(text, text) TO geet_web;


--
-- Name: FUNCTION dblink_open(text, text, boolean); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_open(text, text, boolean) TO geet;
GRANT ALL ON FUNCTION public.dblink_open(text, text, boolean) TO geet_web;


--
-- Name: FUNCTION dblink_open(text, text, text); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_open(text, text, text) TO geet;
GRANT ALL ON FUNCTION public.dblink_open(text, text, text) TO geet_web;


--
-- Name: FUNCTION dblink_open(text, text, text, boolean); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_open(text, text, text, boolean) TO geet;
GRANT ALL ON FUNCTION public.dblink_open(text, text, text, boolean) TO geet_web;


--
-- Name: FUNCTION dblink_send_query(text, text); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dblink_send_query(text, text) TO geet;
GRANT ALL ON FUNCTION public.dblink_send_query(text, text) TO geet_web;


--
-- Name: FUNCTION disablelongtransactions(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.disablelongtransactions() TO geet;
GRANT ALL ON FUNCTION public.disablelongtransactions() TO geet_web;


--
-- Name: FUNCTION drop_matview(name); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.drop_matview(name) TO geet;
GRANT ALL ON FUNCTION public.drop_matview(name) TO geet_web;


--
-- Name: FUNCTION dropgeometrycolumn(table_name character varying, column_name character varying); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.dropgeometrycolumn(table_name character varying, column_name character varying) TO geet;
GRANT ALL ON FUNCTION public.dropgeometrycolumn(table_name character varying, column_name character varying) TO geet_web;


--
-- Name: FUNCTION dropgeometrycolumn(schema_name character varying, table_name character varying, column_name character varying); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.dropgeometrycolumn(schema_name character varying, table_name character varying, column_name character varying) TO geet;
GRANT ALL ON FUNCTION public.dropgeometrycolumn(schema_name character varying, table_name character varying, column_name character varying) TO geet_web;


--
-- Name: FUNCTION dropgeometrycolumn(catalog_name character varying, schema_name character varying, table_name character varying, column_name character varying); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.dropgeometrycolumn(catalog_name character varying, schema_name character varying, table_name character varying, column_name character varying) TO geet;
GRANT ALL ON FUNCTION public.dropgeometrycolumn(catalog_name character varying, schema_name character varying, table_name character varying, column_name character varying) TO geet_web;


--
-- Name: FUNCTION dropgeometrytable(table_name character varying); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.dropgeometrytable(table_name character varying) TO geet;
GRANT ALL ON FUNCTION public.dropgeometrytable(table_name character varying) TO geet_web;


--
-- Name: FUNCTION dropgeometrytable(schema_name character varying, table_name character varying); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.dropgeometrytable(schema_name character varying, table_name character varying) TO geet;
GRANT ALL ON FUNCTION public.dropgeometrytable(schema_name character varying, table_name character varying) TO geet_web;


--
-- Name: FUNCTION dropgeometrytable(catalog_name character varying, schema_name character varying, table_name character varying); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.dropgeometrytable(catalog_name character varying, schema_name character varying, table_name character varying) TO geet;
GRANT ALL ON FUNCTION public.dropgeometrytable(catalog_name character varying, schema_name character varying, table_name character varying) TO geet_web;


--
-- Name: FUNCTION dropoverviewconstraints(ovtable name, ovcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.dropoverviewconstraints(ovtable name, ovcolumn name) TO geet;
GRANT ALL ON FUNCTION public.dropoverviewconstraints(ovtable name, ovcolumn name) TO geet_web;


--
-- Name: FUNCTION dropoverviewconstraints(ovschema name, ovtable name, ovcolumn name); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.dropoverviewconstraints(ovschema name, ovtable name, ovcolumn name) TO geet;
GRANT ALL ON FUNCTION public.dropoverviewconstraints(ovschema name, ovtable name, ovcolumn name) TO geet_web;


--
-- Name: FUNCTION droprasterconstraints(rasttable name, rastcolumn name, VARIADIC constraints text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.droprasterconstraints(rasttable name, rastcolumn name, VARIADIC constraints text[]) TO geet;
GRANT ALL ON FUNCTION public.droprasterconstraints(rasttable name, rastcolumn name, VARIADIC constraints text[]) TO geet_web;


--
-- Name: FUNCTION droprasterconstraints(rastschema name, rasttable name, rastcolumn name, VARIADIC constraints text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.droprasterconstraints(rastschema name, rasttable name, rastcolumn name, VARIADIC constraints text[]) TO geet;
GRANT ALL ON FUNCTION public.droprasterconstraints(rastschema name, rasttable name, rastcolumn name, VARIADIC constraints text[]) TO geet_web;


--
-- Name: FUNCTION droprasterconstraints(rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.droprasterconstraints(rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean) TO geet;
GRANT ALL ON FUNCTION public.droprasterconstraints(rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean) TO geet_web;


--
-- Name: FUNCTION droprasterconstraints(rastschema name, rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.droprasterconstraints(rastschema name, rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean) TO geet;
GRANT ALL ON FUNCTION public.droprasterconstraints(rastschema name, rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean) TO geet_web;


--
-- Name: FUNCTION dynamic_view(); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.dynamic_view() TO geet;
GRANT ALL ON FUNCTION public.dynamic_view() TO geet_web;


--
-- Name: FUNCTION ec_id_ai(); Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON FUNCTION public.ec_id_ai() TO geet;
GRANT ALL ON FUNCTION public.ec_id_ai() TO geet_web;


--
-- Name: FUNCTION enablelongtransactions(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.enablelongtransactions() TO geet;
GRANT ALL ON FUNCTION public.enablelongtransactions() TO geet_web;


--
-- Name: FUNCTION equals(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.equals(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.equals(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION file_fdw_handler(); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.file_fdw_handler() TO geet;
GRANT ALL ON FUNCTION public.file_fdw_handler() TO geet_web;


--
-- Name: FUNCTION file_fdw_validator(text[], oid); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.file_fdw_validator(text[], oid) TO geet;
GRANT ALL ON FUNCTION public.file_fdw_validator(text[], oid) TO geet_web;


--
-- Name: FUNCTION find_srid(character varying, character varying, character varying); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.find_srid(character varying, character varying, character varying) TO geet;
GRANT ALL ON FUNCTION public.find_srid(character varying, character varying, character varying) TO geet_web;


--
-- Name: FUNCTION func1(a integer, b timestamp without time zone, c integer[]); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.func1(a integer, b timestamp without time zone, c integer[]) TO geet;
GRANT ALL ON FUNCTION public.func1(a integer, b timestamp without time zone, c integer[]) TO geet_web;


--
-- Name: FUNCTION geog_brin_inclusion_add_value(internal, internal, internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geog_brin_inclusion_add_value(internal, internal, internal, internal) TO geet;
GRANT ALL ON FUNCTION public.geog_brin_inclusion_add_value(internal, internal, internal, internal) TO geet_web;


--
-- Name: FUNCTION geography(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography(bytea) TO geet;
GRANT ALL ON FUNCTION public.geography(bytea) TO geet_web;


--
-- Name: FUNCTION geography(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geography(public.geometry) TO geet_web;


--
-- Name: FUNCTION geography(public.geography, integer, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography(public.geography, integer, boolean) TO geet;
GRANT ALL ON FUNCTION public.geography(public.geography, integer, boolean) TO geet_web;


--
-- Name: FUNCTION geography_cmp(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_cmp(public.geography, public.geography) TO geet;
GRANT ALL ON FUNCTION public.geography_cmp(public.geography, public.geography) TO geet_web;


--
-- Name: FUNCTION geography_distance_knn(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_distance_knn(public.geography, public.geography) TO geet;
GRANT ALL ON FUNCTION public.geography_distance_knn(public.geography, public.geography) TO geet_web;


--
-- Name: FUNCTION geography_eq(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_eq(public.geography, public.geography) TO geet;
GRANT ALL ON FUNCTION public.geography_eq(public.geography, public.geography) TO geet_web;


--
-- Name: FUNCTION geography_ge(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_ge(public.geography, public.geography) TO geet;
GRANT ALL ON FUNCTION public.geography_ge(public.geography, public.geography) TO geet_web;


--
-- Name: FUNCTION geography_gist_compress(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_gist_compress(internal) TO geet;
GRANT ALL ON FUNCTION public.geography_gist_compress(internal) TO geet_web;


--
-- Name: FUNCTION geography_gist_consistent(internal, public.geography, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_gist_consistent(internal, public.geography, integer) TO geet;
GRANT ALL ON FUNCTION public.geography_gist_consistent(internal, public.geography, integer) TO geet_web;


--
-- Name: FUNCTION geography_gist_decompress(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_gist_decompress(internal) TO geet;
GRANT ALL ON FUNCTION public.geography_gist_decompress(internal) TO geet_web;


--
-- Name: FUNCTION geography_gist_distance(internal, public.geography, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_gist_distance(internal, public.geography, integer) TO geet;
GRANT ALL ON FUNCTION public.geography_gist_distance(internal, public.geography, integer) TO geet_web;


--
-- Name: FUNCTION geography_gist_penalty(internal, internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_gist_penalty(internal, internal, internal) TO geet;
GRANT ALL ON FUNCTION public.geography_gist_penalty(internal, internal, internal) TO geet_web;


--
-- Name: FUNCTION geography_gist_picksplit(internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_gist_picksplit(internal, internal) TO geet;
GRANT ALL ON FUNCTION public.geography_gist_picksplit(internal, internal) TO geet_web;


--
-- Name: FUNCTION geography_gist_same(public.box2d, public.box2d, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_gist_same(public.box2d, public.box2d, internal) TO geet;
GRANT ALL ON FUNCTION public.geography_gist_same(public.box2d, public.box2d, internal) TO geet_web;


--
-- Name: FUNCTION geography_gist_union(bytea, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_gist_union(bytea, internal) TO geet;
GRANT ALL ON FUNCTION public.geography_gist_union(bytea, internal) TO geet_web;


--
-- Name: FUNCTION geography_gt(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_gt(public.geography, public.geography) TO geet;
GRANT ALL ON FUNCTION public.geography_gt(public.geography, public.geography) TO geet_web;


--
-- Name: FUNCTION geography_le(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_le(public.geography, public.geography) TO geet;
GRANT ALL ON FUNCTION public.geography_le(public.geography, public.geography) TO geet_web;


--
-- Name: FUNCTION geography_lt(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_lt(public.geography, public.geography) TO geet;
GRANT ALL ON FUNCTION public.geography_lt(public.geography, public.geography) TO geet_web;


--
-- Name: FUNCTION geography_overlaps(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geography_overlaps(public.geography, public.geography) TO geet;
GRANT ALL ON FUNCTION public.geography_overlaps(public.geography, public.geography) TO geet_web;


--
-- Name: FUNCTION geom2d_brin_inclusion_add_value(internal, internal, internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geom2d_brin_inclusion_add_value(internal, internal, internal, internal) TO geet;
GRANT ALL ON FUNCTION public.geom2d_brin_inclusion_add_value(internal, internal, internal, internal) TO geet_web;


--
-- Name: FUNCTION geom3d_brin_inclusion_add_value(internal, internal, internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geom3d_brin_inclusion_add_value(internal, internal, internal, internal) TO geet;
GRANT ALL ON FUNCTION public.geom3d_brin_inclusion_add_value(internal, internal, internal, internal) TO geet_web;


--
-- Name: FUNCTION geom4d_brin_inclusion_add_value(internal, internal, internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geom4d_brin_inclusion_add_value(internal, internal, internal, internal) TO geet;
GRANT ALL ON FUNCTION public.geom4d_brin_inclusion_add_value(internal, internal, internal, internal) TO geet_web;


--
-- Name: FUNCTION geom_scheme_availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.geom_scheme_availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric) TO geet;
GRANT ALL ON FUNCTION public.geom_scheme_availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric) TO geet_web;


--
-- Name: FUNCTION geometry(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry(bytea) TO geet;
GRANT ALL ON FUNCTION public.geometry(bytea) TO geet_web;


--
-- Name: FUNCTION geometry(path); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry(path) TO geet;
GRANT ALL ON FUNCTION public.geometry(path) TO geet_web;


--
-- Name: FUNCTION geometry(point); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry(point) TO geet;
GRANT ALL ON FUNCTION public.geometry(point) TO geet_web;


--
-- Name: FUNCTION geometry(polygon); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry(polygon) TO geet;
GRANT ALL ON FUNCTION public.geometry(polygon) TO geet_web;


--
-- Name: FUNCTION geometry(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry(text) TO geet;
GRANT ALL ON FUNCTION public.geometry(text) TO geet_web;


--
-- Name: FUNCTION geometry(public.box2d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry(public.box2d) TO geet;
GRANT ALL ON FUNCTION public.geometry(public.box2d) TO geet_web;


--
-- Name: FUNCTION geometry(public.box3d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry(public.box3d) TO geet;
GRANT ALL ON FUNCTION public.geometry(public.box3d) TO geet_web;


--
-- Name: FUNCTION geometry(public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry(public.geography) TO geet;
GRANT ALL ON FUNCTION public.geometry(public.geography) TO geet_web;


--
-- Name: FUNCTION geometry(public.geometry, integer, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry(public.geometry, integer, boolean) TO geet;
GRANT ALL ON FUNCTION public.geometry(public.geometry, integer, boolean) TO geet_web;


--
-- Name: FUNCTION geometry_above(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_above(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_above(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION geometry_below(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_below(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_below(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION geometry_cmp(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_cmp(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_cmp(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION geometry_contained_by_raster(public.geometry, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_contained_by_raster(public.geometry, public.raster) TO geet;
GRANT ALL ON FUNCTION public.geometry_contained_by_raster(public.geometry, public.raster) TO geet_web;


--
-- Name: FUNCTION geometry_contains(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_contains(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_contains(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION geometry_distance_box(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_distance_box(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_distance_box(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION geometry_distance_centroid(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_distance_centroid(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_distance_centroid(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION geometry_distance_centroid_nd(public.geometry, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_distance_centroid_nd(public.geometry, public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_distance_centroid_nd(public.geometry, public.geometry) TO geet_web;


--
-- Name: FUNCTION geometry_distance_cpa(public.geometry, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_distance_cpa(public.geometry, public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_distance_cpa(public.geometry, public.geometry) TO geet_web;


--
-- Name: FUNCTION geometry_eq(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_eq(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_eq(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION geometry_ge(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_ge(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_ge(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION geometry_gist_compress_2d(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_compress_2d(internal) TO geet;
GRANT ALL ON FUNCTION public.geometry_gist_compress_2d(internal) TO geet_web;


--
-- Name: FUNCTION geometry_gist_compress_nd(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_compress_nd(internal) TO geet;
GRANT ALL ON FUNCTION public.geometry_gist_compress_nd(internal) TO geet_web;


--
-- Name: FUNCTION geometry_gist_consistent_2d(internal, public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_consistent_2d(internal, public.geometry, integer) TO geet;
GRANT ALL ON FUNCTION public.geometry_gist_consistent_2d(internal, public.geometry, integer) TO geet_web;


--
-- Name: FUNCTION geometry_gist_consistent_nd(internal, public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_consistent_nd(internal, public.geometry, integer) TO geet;
GRANT ALL ON FUNCTION public.geometry_gist_consistent_nd(internal, public.geometry, integer) TO geet_web;


--
-- Name: FUNCTION geometry_gist_decompress_2d(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_decompress_2d(internal) TO geet;
GRANT ALL ON FUNCTION public.geometry_gist_decompress_2d(internal) TO geet_web;


--
-- Name: FUNCTION geometry_gist_decompress_nd(internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_decompress_nd(internal) TO geet;
GRANT ALL ON FUNCTION public.geometry_gist_decompress_nd(internal) TO geet_web;


--
-- Name: FUNCTION geometry_gist_distance_2d(internal, public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_distance_2d(internal, public.geometry, integer) TO geet;
GRANT ALL ON FUNCTION public.geometry_gist_distance_2d(internal, public.geometry, integer) TO geet_web;


--
-- Name: FUNCTION geometry_gist_distance_nd(internal, public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_distance_nd(internal, public.geometry, integer) TO geet;
GRANT ALL ON FUNCTION public.geometry_gist_distance_nd(internal, public.geometry, integer) TO geet_web;


--
-- Name: FUNCTION geometry_gist_penalty_2d(internal, internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_penalty_2d(internal, internal, internal) TO geet;
GRANT ALL ON FUNCTION public.geometry_gist_penalty_2d(internal, internal, internal) TO geet_web;


--
-- Name: FUNCTION geometry_gist_penalty_nd(internal, internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_penalty_nd(internal, internal, internal) TO geet;
GRANT ALL ON FUNCTION public.geometry_gist_penalty_nd(internal, internal, internal) TO geet_web;


--
-- Name: FUNCTION geometry_gist_picksplit_2d(internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_picksplit_2d(internal, internal) TO geet;
GRANT ALL ON FUNCTION public.geometry_gist_picksplit_2d(internal, internal) TO geet_web;


--
-- Name: FUNCTION geometry_gist_picksplit_nd(internal, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_picksplit_nd(internal, internal) TO geet;
GRANT ALL ON FUNCTION public.geometry_gist_picksplit_nd(internal, internal) TO geet_web;


--
-- Name: FUNCTION geometry_gist_same_2d(geom1 public.geometry, geom2 public.geometry, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_same_2d(geom1 public.geometry, geom2 public.geometry, internal) TO geet;
GRANT ALL ON FUNCTION public.geometry_gist_same_2d(geom1 public.geometry, geom2 public.geometry, internal) TO geet_web;


--
-- Name: FUNCTION geometry_gist_same_nd(public.geometry, public.geometry, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_same_nd(public.geometry, public.geometry, internal) TO geet;
GRANT ALL ON FUNCTION public.geometry_gist_same_nd(public.geometry, public.geometry, internal) TO geet_web;


--
-- Name: FUNCTION geometry_gist_union_2d(bytea, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_union_2d(bytea, internal) TO geet;
GRANT ALL ON FUNCTION public.geometry_gist_union_2d(bytea, internal) TO geet_web;


--
-- Name: FUNCTION geometry_gist_union_nd(bytea, internal); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gist_union_nd(bytea, internal) TO geet;
GRANT ALL ON FUNCTION public.geometry_gist_union_nd(bytea, internal) TO geet_web;


--
-- Name: FUNCTION geometry_gt(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_gt(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_gt(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION geometry_le(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_le(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_le(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION geometry_left(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_left(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_left(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION geometry_lt(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_lt(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_lt(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION geometry_overabove(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_overabove(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_overabove(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION geometry_overbelow(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_overbelow(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_overbelow(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION geometry_overlaps(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_overlaps(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_overlaps(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION geometry_overlaps_nd(public.geometry, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_overlaps_nd(public.geometry, public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_overlaps_nd(public.geometry, public.geometry) TO geet_web;


--
-- Name: FUNCTION geometry_overleft(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_overleft(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_overleft(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION geometry_overright(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_overright(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_overright(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION geometry_raster_contain(public.geometry, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_raster_contain(public.geometry, public.raster) TO geet;
GRANT ALL ON FUNCTION public.geometry_raster_contain(public.geometry, public.raster) TO geet_web;


--
-- Name: FUNCTION geometry_raster_overlap(public.geometry, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_raster_overlap(public.geometry, public.raster) TO geet;
GRANT ALL ON FUNCTION public.geometry_raster_overlap(public.geometry, public.raster) TO geet_web;


--
-- Name: FUNCTION geometry_right(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_right(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_right(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION geometry_same(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_same(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_same(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION geometry_within(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometry_within(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometry_within(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION geometrytype(public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometrytype(public.geography) TO geet;
GRANT ALL ON FUNCTION public.geometrytype(public.geography) TO geet_web;


--
-- Name: FUNCTION geometrytype(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geometrytype(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.geometrytype(public.geometry) TO geet_web;


--
-- Name: FUNCTION geomfromewkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geomfromewkb(bytea) TO geet;
GRANT ALL ON FUNCTION public.geomfromewkb(bytea) TO geet_web;


--
-- Name: FUNCTION geomfromewkt(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.geomfromewkt(text) TO geet;
GRANT ALL ON FUNCTION public.geomfromewkt(text) TO geet_web;


--
-- Name: FUNCTION get_proj4_from_srid(integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.get_proj4_from_srid(integer) TO geet;
GRANT ALL ON FUNCTION public.get_proj4_from_srid(integer) TO geet_web;


--
-- Name: FUNCTION geteciddistrict(ec_id numeric); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.geteciddistrict(ec_id numeric) TO geet;
GRANT ALL ON FUNCTION public.geteciddistrict(ec_id numeric) TO geet_web;


--
-- Name: FUNCTION geteciddistrict(param_scheme_id numeric, param_district_code numeric); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.geteciddistrict(param_scheme_id numeric, param_district_code numeric) TO geet;
GRANT ALL ON FUNCTION public.geteciddistrict(param_scheme_id numeric, param_district_code numeric) TO geet_web;


--
-- Name: FUNCTION geteciddistrict(param_region_type text, param_scheme_id numeric, param_district_code numeric); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.geteciddistrict(param_region_type text, param_scheme_id numeric, param_district_code numeric) TO geet;
GRANT ALL ON FUNCTION public.geteciddistrict(param_region_type text, param_scheme_id numeric, param_district_code numeric) TO geet_web;


--
-- Name: FUNCTION gettransactionid(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.gettransactionid() TO geet;
GRANT ALL ON FUNCTION public.gettransactionid() TO geet_web;


--
-- Name: FUNCTION gserialized_gist_joinsel_2d(internal, oid, internal, smallint); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.gserialized_gist_joinsel_2d(internal, oid, internal, smallint) TO geet;
GRANT ALL ON FUNCTION public.gserialized_gist_joinsel_2d(internal, oid, internal, smallint) TO geet_web;


--
-- Name: FUNCTION gserialized_gist_joinsel_nd(internal, oid, internal, smallint); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.gserialized_gist_joinsel_nd(internal, oid, internal, smallint) TO geet;
GRANT ALL ON FUNCTION public.gserialized_gist_joinsel_nd(internal, oid, internal, smallint) TO geet_web;


--
-- Name: FUNCTION gserialized_gist_sel_2d(internal, oid, internal, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.gserialized_gist_sel_2d(internal, oid, internal, integer) TO geet;
GRANT ALL ON FUNCTION public.gserialized_gist_sel_2d(internal, oid, internal, integer) TO geet_web;


--
-- Name: FUNCTION gserialized_gist_sel_nd(internal, oid, internal, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.gserialized_gist_sel_nd(internal, oid, internal, integer) TO geet;
GRANT ALL ON FUNCTION public.gserialized_gist_sel_nd(internal, oid, internal, integer) TO geet_web;


--
-- Name: FUNCTION hh_copy(); Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON FUNCTION public.hh_copy() TO geet;
GRANT ALL ON FUNCTION public.hh_copy() TO geet_web;


--
-- Name: FUNCTION hh_id_ai(); Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON FUNCTION public.hh_id_ai() TO geet;
GRANT ALL ON FUNCTION public.hh_id_ai() TO geet_web;


--
-- Name: FUNCTION insert_into_hh_if_not_exists(hh_head_tin numeric, state_code numeric, district_code numeric, tehsil_code numeric, village_code numeric, create_by text); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.insert_into_hh_if_not_exists(hh_head_tin numeric, state_code numeric, district_code numeric, tehsil_code numeric, village_code numeric, create_by text) TO geet;
GRANT ALL ON FUNCTION public.insert_into_hh_if_not_exists(hh_head_tin numeric, state_code numeric, district_code numeric, tehsil_code numeric, village_code numeric, create_by text) TO geet_web;


--
-- Name: FUNCTION is_contained_2d(public.box2df, public.box2df); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.is_contained_2d(public.box2df, public.box2df) TO geet;
GRANT ALL ON FUNCTION public.is_contained_2d(public.box2df, public.box2df) TO geet_web;


--
-- Name: FUNCTION is_contained_2d(public.box2df, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.is_contained_2d(public.box2df, public.geometry) TO geet;
GRANT ALL ON FUNCTION public.is_contained_2d(public.box2df, public.geometry) TO geet_web;


--
-- Name: FUNCTION is_contained_2d(public.geometry, public.box2df); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.is_contained_2d(public.geometry, public.box2df) TO geet;
GRANT ALL ON FUNCTION public.is_contained_2d(public.geometry, public.box2df) TO geet_web;


--
-- Name: FUNCTION "left"(s character varying); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public."left"(s character varying) TO geet;
GRANT ALL ON FUNCTION public."left"(s character varying) TO geet_web;


--
-- Name: FUNCTION load_csv_file(target_table text, csv_path text, col_count integer); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.load_csv_file(target_table text, csv_path text, col_count integer) TO geet;
GRANT ALL ON FUNCTION public.load_csv_file(target_table text, csv_path text, col_count integer) TO geet_web;


--
-- Name: FUNCTION lockrow(text, text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.lockrow(text, text, text) TO geet;
GRANT ALL ON FUNCTION public.lockrow(text, text, text) TO geet_web;


--
-- Name: FUNCTION lockrow(text, text, text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.lockrow(text, text, text, text) TO geet;
GRANT ALL ON FUNCTION public.lockrow(text, text, text, text) TO geet_web;


--
-- Name: FUNCTION lockrow(text, text, text, timestamp without time zone); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.lockrow(text, text, text, timestamp without time zone) TO geet;
GRANT ALL ON FUNCTION public.lockrow(text, text, text, timestamp without time zone) TO geet_web;


--
-- Name: FUNCTION lockrow(text, text, text, text, timestamp without time zone); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.lockrow(text, text, text, text, timestamp without time zone) TO geet;
GRANT ALL ON FUNCTION public.lockrow(text, text, text, text, timestamp without time zone) TO geet_web;


--
-- Name: FUNCTION longtransactionsenabled(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.longtransactionsenabled() TO geet;
GRANT ALL ON FUNCTION public.longtransactionsenabled() TO geet_web;


--
-- Name: FUNCTION overlaps_2d(public.box2df, public.box2df); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.overlaps_2d(public.box2df, public.box2df) TO geet;
GRANT ALL ON FUNCTION public.overlaps_2d(public.box2df, public.box2df) TO geet_web;


--
-- Name: FUNCTION overlaps_2d(public.box2df, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.overlaps_2d(public.box2df, public.geometry) TO geet;
GRANT ALL ON FUNCTION public.overlaps_2d(public.box2df, public.geometry) TO geet_web;


--
-- Name: FUNCTION overlaps_2d(public.geometry, public.box2df); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.overlaps_2d(public.geometry, public.box2df) TO geet;
GRANT ALL ON FUNCTION public.overlaps_2d(public.geometry, public.box2df) TO geet_web;


--
-- Name: FUNCTION overlaps_geog(public.geography, public.gidx); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.overlaps_geog(public.geography, public.gidx) TO geet;
GRANT ALL ON FUNCTION public.overlaps_geog(public.geography, public.gidx) TO geet_web;


--
-- Name: FUNCTION overlaps_geog(public.gidx, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.overlaps_geog(public.gidx, public.geography) TO geet;
GRANT ALL ON FUNCTION public.overlaps_geog(public.gidx, public.geography) TO geet_web;


--
-- Name: FUNCTION overlaps_geog(public.gidx, public.gidx); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.overlaps_geog(public.gidx, public.gidx) TO geet;
GRANT ALL ON FUNCTION public.overlaps_geog(public.gidx, public.gidx) TO geet_web;


--
-- Name: FUNCTION overlaps_nd(public.geometry, public.gidx); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.overlaps_nd(public.geometry, public.gidx) TO geet;
GRANT ALL ON FUNCTION public.overlaps_nd(public.geometry, public.gidx) TO geet_web;


--
-- Name: FUNCTION overlaps_nd(public.gidx, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.overlaps_nd(public.gidx, public.geometry) TO geet;
GRANT ALL ON FUNCTION public.overlaps_nd(public.gidx, public.geometry) TO geet_web;


--
-- Name: FUNCTION overlaps_nd(public.gidx, public.gidx); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.overlaps_nd(public.gidx, public.gidx) TO geet;
GRANT ALL ON FUNCTION public.overlaps_nd(public.gidx, public.gidx) TO geet_web;


--
-- Name: FUNCTION path(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.path(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.path(public.geometry) TO geet_web;


--
-- Name: FUNCTION pgis_geometry_accum_finalfn(public.pgis_abs); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_geometry_accum_finalfn(public.pgis_abs) TO geet;
GRANT ALL ON FUNCTION public.pgis_geometry_accum_finalfn(public.pgis_abs) TO geet_web;


--
-- Name: FUNCTION pgis_geometry_accum_transfn(public.pgis_abs, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_geometry_accum_transfn(public.pgis_abs, public.geometry) TO geet;
GRANT ALL ON FUNCTION public.pgis_geometry_accum_transfn(public.pgis_abs, public.geometry) TO geet_web;


--
-- Name: FUNCTION pgis_geometry_accum_transfn(public.pgis_abs, public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_geometry_accum_transfn(public.pgis_abs, public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public.pgis_geometry_accum_transfn(public.pgis_abs, public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION pgis_geometry_accum_transfn(public.pgis_abs, public.geometry, double precision, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_geometry_accum_transfn(public.pgis_abs, public.geometry, double precision, integer) TO geet;
GRANT ALL ON FUNCTION public.pgis_geometry_accum_transfn(public.pgis_abs, public.geometry, double precision, integer) TO geet_web;


--
-- Name: FUNCTION pgis_geometry_clusterintersecting_finalfn(public.pgis_abs); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_geometry_clusterintersecting_finalfn(public.pgis_abs) TO geet;
GRANT ALL ON FUNCTION public.pgis_geometry_clusterintersecting_finalfn(public.pgis_abs) TO geet_web;


--
-- Name: FUNCTION pgis_geometry_clusterwithin_finalfn(public.pgis_abs); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_geometry_clusterwithin_finalfn(public.pgis_abs) TO geet;
GRANT ALL ON FUNCTION public.pgis_geometry_clusterwithin_finalfn(public.pgis_abs) TO geet_web;


--
-- Name: FUNCTION pgis_geometry_collect_finalfn(public.pgis_abs); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_geometry_collect_finalfn(public.pgis_abs) TO geet;
GRANT ALL ON FUNCTION public.pgis_geometry_collect_finalfn(public.pgis_abs) TO geet_web;


--
-- Name: FUNCTION pgis_geometry_makeline_finalfn(public.pgis_abs); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_geometry_makeline_finalfn(public.pgis_abs) TO geet;
GRANT ALL ON FUNCTION public.pgis_geometry_makeline_finalfn(public.pgis_abs) TO geet_web;


--
-- Name: FUNCTION pgis_geometry_polygonize_finalfn(public.pgis_abs); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_geometry_polygonize_finalfn(public.pgis_abs) TO geet;
GRANT ALL ON FUNCTION public.pgis_geometry_polygonize_finalfn(public.pgis_abs) TO geet_web;


--
-- Name: FUNCTION pgis_geometry_union_finalfn(public.pgis_abs); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.pgis_geometry_union_finalfn(public.pgis_abs) TO geet;
GRANT ALL ON FUNCTION public.pgis_geometry_union_finalfn(public.pgis_abs) TO geet_web;


--
-- Name: FUNCTION point(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.point(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.point(public.geometry) TO geet_web;


--
-- Name: FUNCTION polygon(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.polygon(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.polygon(public.geometry) TO geet_web;


--
-- Name: FUNCTION populate_geometry_columns(use_typmod boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.populate_geometry_columns(use_typmod boolean) TO geet;
GRANT ALL ON FUNCTION public.populate_geometry_columns(use_typmod boolean) TO geet_web;


--
-- Name: FUNCTION populate_geometry_columns(tbl_oid oid, use_typmod boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.populate_geometry_columns(tbl_oid oid, use_typmod boolean) TO geet;
GRANT ALL ON FUNCTION public.populate_geometry_columns(tbl_oid oid, use_typmod boolean) TO geet_web;


--
-- Name: FUNCTION postgis_addbbox(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_addbbox(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.postgis_addbbox(public.geometry) TO geet_web;


--
-- Name: FUNCTION postgis_cache_bbox(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_cache_bbox() TO geet;
GRANT ALL ON FUNCTION public.postgis_cache_bbox() TO geet_web;


--
-- Name: FUNCTION postgis_constraint_dims(geomschema text, geomtable text, geomcolumn text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_constraint_dims(geomschema text, geomtable text, geomcolumn text) TO geet;
GRANT ALL ON FUNCTION public.postgis_constraint_dims(geomschema text, geomtable text, geomcolumn text) TO geet_web;


--
-- Name: FUNCTION postgis_constraint_srid(geomschema text, geomtable text, geomcolumn text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_constraint_srid(geomschema text, geomtable text, geomcolumn text) TO geet;
GRANT ALL ON FUNCTION public.postgis_constraint_srid(geomschema text, geomtable text, geomcolumn text) TO geet_web;


--
-- Name: FUNCTION postgis_constraint_type(geomschema text, geomtable text, geomcolumn text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_constraint_type(geomschema text, geomtable text, geomcolumn text) TO geet;
GRANT ALL ON FUNCTION public.postgis_constraint_type(geomschema text, geomtable text, geomcolumn text) TO geet_web;


--
-- Name: FUNCTION postgis_dropbbox(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_dropbbox(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.postgis_dropbbox(public.geometry) TO geet_web;


--
-- Name: FUNCTION postgis_full_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_full_version() TO geet;
GRANT ALL ON FUNCTION public.postgis_full_version() TO geet_web;


--
-- Name: FUNCTION postgis_gdal_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_gdal_version() TO geet;
GRANT ALL ON FUNCTION public.postgis_gdal_version() TO geet_web;


--
-- Name: FUNCTION postgis_geos_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_geos_version() TO geet;
GRANT ALL ON FUNCTION public.postgis_geos_version() TO geet_web;


--
-- Name: FUNCTION postgis_getbbox(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_getbbox(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.postgis_getbbox(public.geometry) TO geet_web;


--
-- Name: FUNCTION postgis_hasbbox(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_hasbbox(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.postgis_hasbbox(public.geometry) TO geet_web;


--
-- Name: FUNCTION postgis_lib_build_date(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_lib_build_date() TO geet;
GRANT ALL ON FUNCTION public.postgis_lib_build_date() TO geet_web;


--
-- Name: FUNCTION postgis_lib_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_lib_version() TO geet;
GRANT ALL ON FUNCTION public.postgis_lib_version() TO geet_web;


--
-- Name: FUNCTION postgis_libjson_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_libjson_version() TO geet;
GRANT ALL ON FUNCTION public.postgis_libjson_version() TO geet_web;


--
-- Name: FUNCTION postgis_liblwgeom_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_liblwgeom_version() TO geet;
GRANT ALL ON FUNCTION public.postgis_liblwgeom_version() TO geet_web;


--
-- Name: FUNCTION postgis_libxml_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_libxml_version() TO geet;
GRANT ALL ON FUNCTION public.postgis_libxml_version() TO geet_web;


--
-- Name: FUNCTION postgis_noop(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_noop(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.postgis_noop(public.geometry) TO geet_web;


--
-- Name: FUNCTION postgis_noop(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_noop(public.raster) TO geet;
GRANT ALL ON FUNCTION public.postgis_noop(public.raster) TO geet_web;


--
-- Name: FUNCTION postgis_proj_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_proj_version() TO geet;
GRANT ALL ON FUNCTION public.postgis_proj_version() TO geet_web;


--
-- Name: FUNCTION postgis_raster_lib_build_date(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_raster_lib_build_date() TO geet;
GRANT ALL ON FUNCTION public.postgis_raster_lib_build_date() TO geet_web;


--
-- Name: FUNCTION postgis_raster_lib_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_raster_lib_version() TO geet;
GRANT ALL ON FUNCTION public.postgis_raster_lib_version() TO geet_web;


--
-- Name: FUNCTION postgis_raster_scripts_installed(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_raster_scripts_installed() TO geet;
GRANT ALL ON FUNCTION public.postgis_raster_scripts_installed() TO geet_web;


--
-- Name: FUNCTION postgis_scripts_build_date(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_scripts_build_date() TO geet;
GRANT ALL ON FUNCTION public.postgis_scripts_build_date() TO geet_web;


--
-- Name: FUNCTION postgis_scripts_installed(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_scripts_installed() TO geet;
GRANT ALL ON FUNCTION public.postgis_scripts_installed() TO geet_web;


--
-- Name: FUNCTION postgis_scripts_released(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_scripts_released() TO geet;
GRANT ALL ON FUNCTION public.postgis_scripts_released() TO geet_web;


--
-- Name: FUNCTION postgis_svn_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_svn_version() TO geet;
GRANT ALL ON FUNCTION public.postgis_svn_version() TO geet_web;


--
-- Name: FUNCTION postgis_transform_geometry(public.geometry, text, text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_transform_geometry(public.geometry, text, text, integer) TO geet;
GRANT ALL ON FUNCTION public.postgis_transform_geometry(public.geometry, text, text, integer) TO geet_web;


--
-- Name: FUNCTION postgis_type_name(geomname character varying, coord_dimension integer, use_new_name boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_type_name(geomname character varying, coord_dimension integer, use_new_name boolean) TO geet;
GRANT ALL ON FUNCTION public.postgis_type_name(geomname character varying, coord_dimension integer, use_new_name boolean) TO geet_web;


--
-- Name: FUNCTION postgis_typmod_dims(integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_typmod_dims(integer) TO geet;
GRANT ALL ON FUNCTION public.postgis_typmod_dims(integer) TO geet_web;


--
-- Name: FUNCTION postgis_typmod_srid(integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_typmod_srid(integer) TO geet;
GRANT ALL ON FUNCTION public.postgis_typmod_srid(integer) TO geet_web;


--
-- Name: FUNCTION postgis_typmod_type(integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_typmod_type(integer) TO geet;
GRANT ALL ON FUNCTION public.postgis_typmod_type(integer) TO geet_web;


--
-- Name: FUNCTION postgis_version(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.postgis_version() TO geet;
GRANT ALL ON FUNCTION public.postgis_version() TO geet_web;


--
-- Name: FUNCTION postgres_fdw_handler(); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.postgres_fdw_handler() TO geet;
GRANT ALL ON FUNCTION public.postgres_fdw_handler() TO geet_web;


--
-- Name: FUNCTION postgres_fdw_validator(text[], oid); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.postgres_fdw_validator(text[], oid) TO geet;
GRANT ALL ON FUNCTION public.postgres_fdw_validator(text[], oid) TO geet_web;


--
-- Name: FUNCTION raster_above(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_above(public.raster, public.raster) TO geet;
GRANT ALL ON FUNCTION public.raster_above(public.raster, public.raster) TO geet_web;


--
-- Name: FUNCTION raster_below(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_below(public.raster, public.raster) TO geet;
GRANT ALL ON FUNCTION public.raster_below(public.raster, public.raster) TO geet_web;


--
-- Name: FUNCTION raster_contain(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_contain(public.raster, public.raster) TO geet;
GRANT ALL ON FUNCTION public.raster_contain(public.raster, public.raster) TO geet_web;


--
-- Name: FUNCTION raster_contained(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_contained(public.raster, public.raster) TO geet;
GRANT ALL ON FUNCTION public.raster_contained(public.raster, public.raster) TO geet_web;


--
-- Name: FUNCTION raster_contained_by_geometry(public.raster, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_contained_by_geometry(public.raster, public.geometry) TO geet;
GRANT ALL ON FUNCTION public.raster_contained_by_geometry(public.raster, public.geometry) TO geet_web;


--
-- Name: FUNCTION raster_eq(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_eq(public.raster, public.raster) TO geet;
GRANT ALL ON FUNCTION public.raster_eq(public.raster, public.raster) TO geet_web;


--
-- Name: FUNCTION raster_geometry_contain(public.raster, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_geometry_contain(public.raster, public.geometry) TO geet;
GRANT ALL ON FUNCTION public.raster_geometry_contain(public.raster, public.geometry) TO geet_web;


--
-- Name: FUNCTION raster_geometry_overlap(public.raster, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_geometry_overlap(public.raster, public.geometry) TO geet;
GRANT ALL ON FUNCTION public.raster_geometry_overlap(public.raster, public.geometry) TO geet_web;


--
-- Name: FUNCTION raster_hash(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_hash(public.raster) TO geet;
GRANT ALL ON FUNCTION public.raster_hash(public.raster) TO geet_web;


--
-- Name: FUNCTION raster_left(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_left(public.raster, public.raster) TO geet;
GRANT ALL ON FUNCTION public.raster_left(public.raster, public.raster) TO geet_web;


--
-- Name: FUNCTION raster_overabove(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_overabove(public.raster, public.raster) TO geet;
GRANT ALL ON FUNCTION public.raster_overabove(public.raster, public.raster) TO geet_web;


--
-- Name: FUNCTION raster_overbelow(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_overbelow(public.raster, public.raster) TO geet;
GRANT ALL ON FUNCTION public.raster_overbelow(public.raster, public.raster) TO geet_web;


--
-- Name: FUNCTION raster_overlap(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_overlap(public.raster, public.raster) TO geet;
GRANT ALL ON FUNCTION public.raster_overlap(public.raster, public.raster) TO geet_web;


--
-- Name: FUNCTION raster_overleft(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_overleft(public.raster, public.raster) TO geet;
GRANT ALL ON FUNCTION public.raster_overleft(public.raster, public.raster) TO geet_web;


--
-- Name: FUNCTION raster_overright(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_overright(public.raster, public.raster) TO geet;
GRANT ALL ON FUNCTION public.raster_overright(public.raster, public.raster) TO geet_web;


--
-- Name: FUNCTION raster_right(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_right(public.raster, public.raster) TO geet;
GRANT ALL ON FUNCTION public.raster_right(public.raster, public.raster) TO geet_web;


--
-- Name: FUNCTION raster_same(public.raster, public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.raster_same(public.raster, public.raster) TO geet;
GRANT ALL ON FUNCTION public.raster_same(public.raster, public.raster) TO geet_web;


--
-- Name: FUNCTION refresh_matview(name); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.refresh_matview(name) TO geet;
GRANT ALL ON FUNCTION public.refresh_matview(name) TO geet_web;


--
-- Name: FUNCTION regions_selection(param_state_code numeric, param_district_code numeric, param_tehsil_code numeric, param_village_code numeric); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.regions_selection(param_state_code numeric, param_district_code numeric, param_tehsil_code numeric, param_village_code numeric) TO geet;
GRANT ALL ON FUNCTION public.regions_selection(param_state_code numeric, param_district_code numeric, param_tehsil_code numeric, param_village_code numeric) TO geet_web;


--
-- Name: FUNCTION regions_selection_dynamic(param_region_type text, param_state_code numeric, param_district_code numeric, param_tehsil_code numeric, param_village_code numeric); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.regions_selection_dynamic(param_region_type text, param_state_code numeric, param_district_code numeric, param_tehsil_code numeric, param_village_code numeric) TO geet;
GRANT ALL ON FUNCTION public.regions_selection_dynamic(param_region_type text, param_state_code numeric, param_district_code numeric, param_tehsil_code numeric, param_village_code numeric) TO geet_web;


--
-- Name: FUNCTION rel_description(p_relname character varying, p_schemaname character varying); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.rel_description(p_relname character varying, p_schemaname character varying) TO geet;
GRANT ALL ON FUNCTION public.rel_description(p_relname character varying, p_schemaname character varying) TO geet_web;


--
-- Name: FUNCTION scheme_availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.scheme_availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric) TO geet;
GRANT ALL ON FUNCTION public.scheme_availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric) TO geet_web;


--
-- Name: FUNCTION scheme_availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric, param_geom public.geometry); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.scheme_availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric, param_geom public.geometry) TO geet;
GRANT ALL ON FUNCTION public.scheme_availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric, param_geom public.geometry) TO geet_web;


--
-- Name: FUNCTION scheme_availed_hh_id(param_region_type text, param_scheme_id numeric, param_district_code numeric); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.scheme_availed_hh_id(param_region_type text, param_scheme_id numeric, param_district_code numeric) TO geet;
GRANT ALL ON FUNCTION public.scheme_availed_hh_id(param_region_type text, param_scheme_id numeric, param_district_code numeric) TO geet_web;


--
-- Name: FUNCTION schme_availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric); Type: ACL; Schema: public; Owner: arpit
--

GRANT ALL ON FUNCTION public.schme_availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric) TO geet;
GRANT ALL ON FUNCTION public.schme_availed_ec_id(param_region_type text, param_scheme_id numeric, param_district_code numeric) TO geet_web;


--
-- Name: FUNCTION sortdata(); Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON FUNCTION public.sortdata() TO geet;
GRANT ALL ON FUNCTION public.sortdata() TO geet_web;


--
-- Name: FUNCTION st_3dclosestpoint(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3dclosestpoint(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_3dclosestpoint(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_3ddfullywithin(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3ddfullywithin(geom1 public.geometry, geom2 public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_3ddfullywithin(geom1 public.geometry, geom2 public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION st_3ddistance(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3ddistance(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_3ddistance(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_3ddwithin(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3ddwithin(geom1 public.geometry, geom2 public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_3ddwithin(geom1 public.geometry, geom2 public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION st_3dintersects(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3dintersects(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_3dintersects(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_3dlength(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3dlength(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_3dlength(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_3dlength_spheroid(public.geometry, public.spheroid); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3dlength_spheroid(public.geometry, public.spheroid) TO geet;
GRANT ALL ON FUNCTION public.st_3dlength_spheroid(public.geometry, public.spheroid) TO geet_web;


--
-- Name: FUNCTION st_3dlongestline(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3dlongestline(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_3dlongestline(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_3dmakebox(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3dmakebox(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_3dmakebox(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_3dmaxdistance(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3dmaxdistance(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_3dmaxdistance(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_3dperimeter(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3dperimeter(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_3dperimeter(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_3dshortestline(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3dshortestline(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_3dshortestline(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_addband(rast public.raster, addbandargset public.addbandarg[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_addband(rast public.raster, addbandargset public.addbandarg[]) TO geet;
GRANT ALL ON FUNCTION public.st_addband(rast public.raster, addbandargset public.addbandarg[]) TO geet_web;


--
-- Name: FUNCTION st_addband(rast public.raster, pixeltype text, initialvalue double precision, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_addband(rast public.raster, pixeltype text, initialvalue double precision, nodataval double precision) TO geet;
GRANT ALL ON FUNCTION public.st_addband(rast public.raster, pixeltype text, initialvalue double precision, nodataval double precision) TO geet_web;


--
-- Name: FUNCTION st_addband(torast public.raster, fromrasts public.raster[], fromband integer, torastindex integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_addband(torast public.raster, fromrasts public.raster[], fromband integer, torastindex integer) TO geet;
GRANT ALL ON FUNCTION public.st_addband(torast public.raster, fromrasts public.raster[], fromband integer, torastindex integer) TO geet_web;


--
-- Name: FUNCTION st_addband(torast public.raster, fromrast public.raster, fromband integer, torastindex integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_addband(torast public.raster, fromrast public.raster, fromband integer, torastindex integer) TO geet;
GRANT ALL ON FUNCTION public.st_addband(torast public.raster, fromrast public.raster, fromband integer, torastindex integer) TO geet_web;


--
-- Name: FUNCTION st_addband(rast public.raster, index integer, outdbfile text, outdbindex integer[], nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_addband(rast public.raster, index integer, outdbfile text, outdbindex integer[], nodataval double precision) TO geet;
GRANT ALL ON FUNCTION public.st_addband(rast public.raster, index integer, outdbfile text, outdbindex integer[], nodataval double precision) TO geet_web;


--
-- Name: FUNCTION st_addband(rast public.raster, index integer, pixeltype text, initialvalue double precision, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_addband(rast public.raster, index integer, pixeltype text, initialvalue double precision, nodataval double precision) TO geet;
GRANT ALL ON FUNCTION public.st_addband(rast public.raster, index integer, pixeltype text, initialvalue double precision, nodataval double precision) TO geet_web;


--
-- Name: FUNCTION st_addband(rast public.raster, outdbfile text, outdbindex integer[], index integer, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_addband(rast public.raster, outdbfile text, outdbindex integer[], index integer, nodataval double precision) TO geet;
GRANT ALL ON FUNCTION public.st_addband(rast public.raster, outdbfile text, outdbindex integer[], index integer, nodataval double precision) TO geet_web;


--
-- Name: FUNCTION st_addmeasure(public.geometry, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_addmeasure(public.geometry, double precision, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_addmeasure(public.geometry, double precision, double precision) TO geet_web;


--
-- Name: FUNCTION st_addpoint(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_addpoint(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_addpoint(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_addpoint(geom1 public.geometry, geom2 public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_addpoint(geom1 public.geometry, geom2 public.geometry, integer) TO geet;
GRANT ALL ON FUNCTION public.st_addpoint(geom1 public.geometry, geom2 public.geometry, integer) TO geet_web;


--
-- Name: FUNCTION st_affine(public.geometry, double precision, double precision, double precision, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_affine(public.geometry, double precision, double precision, double precision, double precision, double precision, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_affine(public.geometry, double precision, double precision, double precision, double precision, double precision, double precision) TO geet_web;


--
-- Name: FUNCTION st_affine(public.geometry, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_affine(public.geometry, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_affine(public.geometry, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision) TO geet_web;


--
-- Name: FUNCTION st_approxcount(rast public.raster, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxcount(rast public.raster, sample_percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxcount(rast public.raster, sample_percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxcount(rastertable text, rastercolumn text, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxcount(rastertable text, rastercolumn text, sample_percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxcount(rastertable text, rastercolumn text, sample_percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxcount(rast public.raster, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxcount(rast public.raster, exclude_nodata_value boolean, sample_percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxcount(rast public.raster, exclude_nodata_value boolean, sample_percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxcount(rast public.raster, nband integer, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxcount(rast public.raster, nband integer, sample_percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxcount(rast public.raster, nband integer, sample_percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxcount(rastertable text, rastercolumn text, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxcount(rastertable text, rastercolumn text, exclude_nodata_value boolean, sample_percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxcount(rastertable text, rastercolumn text, exclude_nodata_value boolean, sample_percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxcount(rastertable text, rastercolumn text, nband integer, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxcount(rastertable text, rastercolumn text, nband integer, sample_percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxcount(rastertable text, rastercolumn text, nband integer, sample_percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxcount(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxcount(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxcount(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxcount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxcount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxcount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxhistogram(rast public.raster, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rast public.raster, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxhistogram(rast public.raster, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxhistogram(rastertable text, rastercolumn text, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rastertable text, rastercolumn text, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxhistogram(rastertable text, rastercolumn text, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxhistogram(rast public.raster, nband integer, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rast public.raster, nband integer, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxhistogram(rast public.raster, nband integer, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxhistogram(rast public.raster, nband integer, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rast public.raster, nband integer, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxhistogram(rast public.raster, nband integer, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxhistogram(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxhistogram(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxhistogram(rast public.raster, nband integer, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rast public.raster, nband integer, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxhistogram(rast public.raster, nband integer, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxhistogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxhistogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxhistogram(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxhistogram(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxhistogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxhistogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxquantile(rast public.raster, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet_web;


--
-- Name: FUNCTION st_approxquantile(rast public.raster, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, quantile double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, quantile double precision) TO geet_web;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet_web;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, quantile double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, quantile double precision) TO geet_web;


--
-- Name: FUNCTION st_approxquantile(rast public.raster, exclude_nodata_value boolean, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, exclude_nodata_value boolean, quantile double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, exclude_nodata_value boolean, quantile double precision) TO geet_web;


--
-- Name: FUNCTION st_approxquantile(rast public.raster, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet_web;


--
-- Name: FUNCTION st_approxquantile(rast public.raster, sample_percent double precision, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, sample_percent double precision, quantile double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, sample_percent double precision, quantile double precision) TO geet_web;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, exclude_nodata_value boolean, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, exclude_nodata_value boolean, quantile double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, exclude_nodata_value boolean, quantile double precision) TO geet_web;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet_web;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, sample_percent double precision, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, sample_percent double precision, quantile double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, sample_percent double precision, quantile double precision) TO geet_web;


--
-- Name: FUNCTION st_approxquantile(rast public.raster, nband integer, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, nband integer, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, nband integer, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet_web;


--
-- Name: FUNCTION st_approxquantile(rast public.raster, nband integer, sample_percent double precision, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, nband integer, sample_percent double precision, quantile double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, nband integer, sample_percent double precision, quantile double precision) TO geet_web;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, nband integer, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, nband integer, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, nband integer, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet_web;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, nband integer, sample_percent double precision, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, nband integer, sample_percent double precision, quantile double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, nband integer, sample_percent double precision, quantile double precision) TO geet_web;


--
-- Name: FUNCTION st_approxquantile(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet_web;


--
-- Name: FUNCTION st_approxquantile(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantile double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantile double precision) TO geet_web;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet_web;


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantile double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantile double precision) TO geet_web;


--
-- Name: FUNCTION st_approxsummarystats(rast public.raster, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxsummarystats(rast public.raster, sample_percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxsummarystats(rast public.raster, sample_percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxsummarystats(rastertable text, rastercolumn text, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxsummarystats(rastertable text, rastercolumn text, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public.st_approxsummarystats(rastertable text, rastercolumn text, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION st_approxsummarystats(rastertable text, rastercolumn text, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxsummarystats(rastertable text, rastercolumn text, sample_percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxsummarystats(rastertable text, rastercolumn text, sample_percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxsummarystats(rast public.raster, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxsummarystats(rast public.raster, exclude_nodata_value boolean, sample_percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxsummarystats(rast public.raster, exclude_nodata_value boolean, sample_percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxsummarystats(rast public.raster, nband integer, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxsummarystats(rast public.raster, nband integer, sample_percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxsummarystats(rast public.raster, nband integer, sample_percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxsummarystats(rastertable text, rastercolumn text, nband integer, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxsummarystats(rastertable text, rastercolumn text, nband integer, sample_percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxsummarystats(rastertable text, rastercolumn text, nband integer, sample_percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxsummarystats(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxsummarystats(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxsummarystats(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO geet_web;


--
-- Name: FUNCTION st_approxsummarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_approxsummarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_approxsummarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO geet_web;


--
-- Name: FUNCTION st_area(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_area(text) TO geet;
GRANT ALL ON FUNCTION public.st_area(text) TO geet_web;


--
-- Name: FUNCTION st_area(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_area(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_area(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_area(geog public.geography, use_spheroid boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_area(geog public.geography, use_spheroid boolean) TO geet;
GRANT ALL ON FUNCTION public.st_area(geog public.geography, use_spheroid boolean) TO geet_web;


--
-- Name: FUNCTION st_area2d(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_area2d(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_area2d(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_asbinary(public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asbinary(public.geography) TO geet;
GRANT ALL ON FUNCTION public.st_asbinary(public.geography) TO geet_web;


--
-- Name: FUNCTION st_asbinary(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asbinary(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_asbinary(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_asbinary(public.geography, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asbinary(public.geography, text) TO geet;
GRANT ALL ON FUNCTION public.st_asbinary(public.geography, text) TO geet_web;


--
-- Name: FUNCTION st_asbinary(public.geometry, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asbinary(public.geometry, text) TO geet;
GRANT ALL ON FUNCTION public.st_asbinary(public.geometry, text) TO geet_web;


--
-- Name: FUNCTION st_asbinary(public.raster, outasin boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asbinary(public.raster, outasin boolean) TO geet;
GRANT ALL ON FUNCTION public.st_asbinary(public.raster, outasin boolean) TO geet_web;


--
-- Name: FUNCTION st_asencodedpolyline(geom public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asencodedpolyline(geom public.geometry, integer) TO geet;
GRANT ALL ON FUNCTION public.st_asencodedpolyline(geom public.geometry, integer) TO geet_web;


--
-- Name: FUNCTION st_asewkb(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asewkb(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_asewkb(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_asewkb(public.geometry, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asewkb(public.geometry, text) TO geet;
GRANT ALL ON FUNCTION public.st_asewkb(public.geometry, text) TO geet_web;


--
-- Name: FUNCTION st_asewkt(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asewkt(text) TO geet;
GRANT ALL ON FUNCTION public.st_asewkt(text) TO geet_web;


--
-- Name: FUNCTION st_asewkt(public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asewkt(public.geography) TO geet;
GRANT ALL ON FUNCTION public.st_asewkt(public.geography) TO geet_web;


--
-- Name: FUNCTION st_asewkt(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asewkt(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_asewkt(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_asgdalraster(rast public.raster, format text, options text[], srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asgdalraster(rast public.raster, format text, options text[], srid integer) TO geet;
GRANT ALL ON FUNCTION public.st_asgdalraster(rast public.raster, format text, options text[], srid integer) TO geet_web;


--
-- Name: FUNCTION st_asgeojson(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asgeojson(text) TO geet;
GRANT ALL ON FUNCTION public.st_asgeojson(text) TO geet_web;


--
-- Name: FUNCTION st_asgeojson(geog public.geography, maxdecimaldigits integer, options integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asgeojson(geog public.geography, maxdecimaldigits integer, options integer) TO geet;
GRANT ALL ON FUNCTION public.st_asgeojson(geog public.geography, maxdecimaldigits integer, options integer) TO geet_web;


--
-- Name: FUNCTION st_asgeojson(geom public.geometry, maxdecimaldigits integer, options integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asgeojson(geom public.geometry, maxdecimaldigits integer, options integer) TO geet;
GRANT ALL ON FUNCTION public.st_asgeojson(geom public.geometry, maxdecimaldigits integer, options integer) TO geet_web;


--
-- Name: FUNCTION st_asgeojson(gj_version integer, geog public.geography, maxdecimaldigits integer, options integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asgeojson(gj_version integer, geog public.geography, maxdecimaldigits integer, options integer) TO geet;
GRANT ALL ON FUNCTION public.st_asgeojson(gj_version integer, geog public.geography, maxdecimaldigits integer, options integer) TO geet_web;


--
-- Name: FUNCTION st_asgeojson(gj_version integer, geom public.geometry, maxdecimaldigits integer, options integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asgeojson(gj_version integer, geom public.geometry, maxdecimaldigits integer, options integer) TO geet;
GRANT ALL ON FUNCTION public.st_asgeojson(gj_version integer, geom public.geometry, maxdecimaldigits integer, options integer) TO geet_web;


--
-- Name: FUNCTION st_asgml(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asgml(text) TO geet;
GRANT ALL ON FUNCTION public.st_asgml(text) TO geet_web;


--
-- Name: FUNCTION st_asgml(geog public.geography, maxdecimaldigits integer, options integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asgml(geog public.geography, maxdecimaldigits integer, options integer) TO geet;
GRANT ALL ON FUNCTION public.st_asgml(geog public.geography, maxdecimaldigits integer, options integer) TO geet_web;


--
-- Name: FUNCTION st_asgml(geom public.geometry, maxdecimaldigits integer, options integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asgml(geom public.geometry, maxdecimaldigits integer, options integer) TO geet;
GRANT ALL ON FUNCTION public.st_asgml(geom public.geometry, maxdecimaldigits integer, options integer) TO geet_web;


--
-- Name: FUNCTION st_asgml(version integer, geog public.geography, maxdecimaldigits integer, options integer, nprefix text, id text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asgml(version integer, geog public.geography, maxdecimaldigits integer, options integer, nprefix text, id text) TO geet;
GRANT ALL ON FUNCTION public.st_asgml(version integer, geog public.geography, maxdecimaldigits integer, options integer, nprefix text, id text) TO geet_web;


--
-- Name: FUNCTION st_asgml(version integer, geom public.geometry, maxdecimaldigits integer, options integer, nprefix text, id text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asgml(version integer, geom public.geometry, maxdecimaldigits integer, options integer, nprefix text, id text) TO geet;
GRANT ALL ON FUNCTION public.st_asgml(version integer, geom public.geometry, maxdecimaldigits integer, options integer, nprefix text, id text) TO geet_web;


--
-- Name: FUNCTION st_ashexewkb(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_ashexewkb(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_ashexewkb(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_ashexewkb(public.geometry, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_ashexewkb(public.geometry, text) TO geet;
GRANT ALL ON FUNCTION public.st_ashexewkb(public.geometry, text) TO geet_web;


--
-- Name: FUNCTION st_asjpeg(rast public.raster, options text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asjpeg(rast public.raster, options text[]) TO geet;
GRANT ALL ON FUNCTION public.st_asjpeg(rast public.raster, options text[]) TO geet_web;


--
-- Name: FUNCTION st_asjpeg(rast public.raster, nbands integer[], options text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asjpeg(rast public.raster, nbands integer[], options text[]) TO geet;
GRANT ALL ON FUNCTION public.st_asjpeg(rast public.raster, nbands integer[], options text[]) TO geet_web;


--
-- Name: FUNCTION st_asjpeg(rast public.raster, nbands integer[], quality integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asjpeg(rast public.raster, nbands integer[], quality integer) TO geet;
GRANT ALL ON FUNCTION public.st_asjpeg(rast public.raster, nbands integer[], quality integer) TO geet_web;


--
-- Name: FUNCTION st_asjpeg(rast public.raster, nband integer, options text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asjpeg(rast public.raster, nband integer, options text[]) TO geet;
GRANT ALL ON FUNCTION public.st_asjpeg(rast public.raster, nband integer, options text[]) TO geet_web;


--
-- Name: FUNCTION st_asjpeg(rast public.raster, nband integer, quality integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asjpeg(rast public.raster, nband integer, quality integer) TO geet;
GRANT ALL ON FUNCTION public.st_asjpeg(rast public.raster, nband integer, quality integer) TO geet_web;


--
-- Name: FUNCTION st_askml(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_askml(text) TO geet;
GRANT ALL ON FUNCTION public.st_askml(text) TO geet_web;


--
-- Name: FUNCTION st_askml(geog public.geography, maxdecimaldigits integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_askml(geog public.geography, maxdecimaldigits integer) TO geet;
GRANT ALL ON FUNCTION public.st_askml(geog public.geography, maxdecimaldigits integer) TO geet_web;


--
-- Name: FUNCTION st_askml(geom public.geometry, maxdecimaldigits integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_askml(geom public.geometry, maxdecimaldigits integer) TO geet;
GRANT ALL ON FUNCTION public.st_askml(geom public.geometry, maxdecimaldigits integer) TO geet_web;


--
-- Name: FUNCTION st_askml(version integer, geog public.geography, maxdecimaldigits integer, nprefix text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_askml(version integer, geog public.geography, maxdecimaldigits integer, nprefix text) TO geet;
GRANT ALL ON FUNCTION public.st_askml(version integer, geog public.geography, maxdecimaldigits integer, nprefix text) TO geet_web;


--
-- Name: FUNCTION st_askml(version integer, geom public.geometry, maxdecimaldigits integer, nprefix text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_askml(version integer, geom public.geometry, maxdecimaldigits integer, nprefix text) TO geet;
GRANT ALL ON FUNCTION public.st_askml(version integer, geom public.geometry, maxdecimaldigits integer, nprefix text) TO geet_web;


--
-- Name: FUNCTION st_aslatlontext(geom public.geometry, tmpl text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_aslatlontext(geom public.geometry, tmpl text) TO geet;
GRANT ALL ON FUNCTION public.st_aslatlontext(geom public.geometry, tmpl text) TO geet_web;


--
-- Name: FUNCTION st_aspect(rast public.raster, nband integer, pixeltype text, units text, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_aspect(rast public.raster, nband integer, pixeltype text, units text, interpolate_nodata boolean) TO geet;
GRANT ALL ON FUNCTION public.st_aspect(rast public.raster, nband integer, pixeltype text, units text, interpolate_nodata boolean) TO geet_web;


--
-- Name: FUNCTION st_aspect(rast public.raster, nband integer, customextent public.raster, pixeltype text, units text, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_aspect(rast public.raster, nband integer, customextent public.raster, pixeltype text, units text, interpolate_nodata boolean) TO geet;
GRANT ALL ON FUNCTION public.st_aspect(rast public.raster, nband integer, customextent public.raster, pixeltype text, units text, interpolate_nodata boolean) TO geet_web;


--
-- Name: FUNCTION st_aspng(rast public.raster, options text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_aspng(rast public.raster, options text[]) TO geet;
GRANT ALL ON FUNCTION public.st_aspng(rast public.raster, options text[]) TO geet_web;


--
-- Name: FUNCTION st_aspng(rast public.raster, nbands integer[], options text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_aspng(rast public.raster, nbands integer[], options text[]) TO geet;
GRANT ALL ON FUNCTION public.st_aspng(rast public.raster, nbands integer[], options text[]) TO geet_web;


--
-- Name: FUNCTION st_aspng(rast public.raster, nbands integer[], compression integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_aspng(rast public.raster, nbands integer[], compression integer) TO geet;
GRANT ALL ON FUNCTION public.st_aspng(rast public.raster, nbands integer[], compression integer) TO geet_web;


--
-- Name: FUNCTION st_aspng(rast public.raster, nband integer, options text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_aspng(rast public.raster, nband integer, options text[]) TO geet;
GRANT ALL ON FUNCTION public.st_aspng(rast public.raster, nband integer, options text[]) TO geet_web;


--
-- Name: FUNCTION st_aspng(rast public.raster, nband integer, compression integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_aspng(rast public.raster, nband integer, compression integer) TO geet;
GRANT ALL ON FUNCTION public.st_aspng(rast public.raster, nband integer, compression integer) TO geet_web;


--
-- Name: FUNCTION st_asraster(geom public.geometry, ref public.raster, pixeltype text[], value double precision[], nodataval double precision[], touched boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, ref public.raster, pixeltype text[], value double precision[], nodataval double precision[], touched boolean) TO geet;
GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, ref public.raster, pixeltype text[], value double precision[], nodataval double precision[], touched boolean) TO geet_web;


--
-- Name: FUNCTION st_asraster(geom public.geometry, ref public.raster, pixeltype text, value double precision, nodataval double precision, touched boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, ref public.raster, pixeltype text, value double precision, nodataval double precision, touched boolean) TO geet;
GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, ref public.raster, pixeltype text, value double precision, nodataval double precision, touched boolean) TO geet_web;


--
-- Name: FUNCTION st_asraster(geom public.geometry, scalex double precision, scaley double precision, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, scalex double precision, scaley double precision, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean) TO geet;
GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, scalex double precision, scaley double precision, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean) TO geet_web;


--
-- Name: FUNCTION st_asraster(geom public.geometry, scalex double precision, scaley double precision, gridx double precision, gridy double precision, pixeltype text[], value double precision[], nodataval double precision[], skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, scalex double precision, scaley double precision, gridx double precision, gridy double precision, pixeltype text[], value double precision[], nodataval double precision[], skewx double precision, skewy double precision, touched boolean) TO geet;
GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, scalex double precision, scaley double precision, gridx double precision, gridy double precision, pixeltype text[], value double precision[], nodataval double precision[], skewx double precision, skewy double precision, touched boolean) TO geet_web;


--
-- Name: FUNCTION st_asraster(geom public.geometry, scalex double precision, scaley double precision, gridx double precision, gridy double precision, pixeltype text, value double precision, nodataval double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, scalex double precision, scaley double precision, gridx double precision, gridy double precision, pixeltype text, value double precision, nodataval double precision, skewx double precision, skewy double precision, touched boolean) TO geet;
GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, scalex double precision, scaley double precision, gridx double precision, gridy double precision, pixeltype text, value double precision, nodataval double precision, skewx double precision, skewy double precision, touched boolean) TO geet_web;


--
-- Name: FUNCTION st_asraster(geom public.geometry, scalex double precision, scaley double precision, pixeltype text, value double precision, nodataval double precision, upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, scalex double precision, scaley double precision, pixeltype text, value double precision, nodataval double precision, upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean) TO geet;
GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, scalex double precision, scaley double precision, pixeltype text, value double precision, nodataval double precision, upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean) TO geet_web;


--
-- Name: FUNCTION st_asraster(geom public.geometry, width integer, height integer, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, width integer, height integer, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean) TO geet;
GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, width integer, height integer, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean) TO geet_web;


--
-- Name: FUNCTION st_asraster(geom public.geometry, width integer, height integer, gridx double precision, gridy double precision, pixeltype text[], value double precision[], nodataval double precision[], skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, width integer, height integer, gridx double precision, gridy double precision, pixeltype text[], value double precision[], nodataval double precision[], skewx double precision, skewy double precision, touched boolean) TO geet;
GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, width integer, height integer, gridx double precision, gridy double precision, pixeltype text[], value double precision[], nodataval double precision[], skewx double precision, skewy double precision, touched boolean) TO geet_web;


--
-- Name: FUNCTION st_asraster(geom public.geometry, width integer, height integer, gridx double precision, gridy double precision, pixeltype text, value double precision, nodataval double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, width integer, height integer, gridx double precision, gridy double precision, pixeltype text, value double precision, nodataval double precision, skewx double precision, skewy double precision, touched boolean) TO geet;
GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, width integer, height integer, gridx double precision, gridy double precision, pixeltype text, value double precision, nodataval double precision, skewx double precision, skewy double precision, touched boolean) TO geet_web;


--
-- Name: FUNCTION st_asraster(geom public.geometry, width integer, height integer, pixeltype text, value double precision, nodataval double precision, upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, width integer, height integer, pixeltype text, value double precision, nodataval double precision, upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean) TO geet;
GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, width integer, height integer, pixeltype text, value double precision, nodataval double precision, upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean) TO geet_web;


--
-- Name: FUNCTION st_assvg(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_assvg(text) TO geet;
GRANT ALL ON FUNCTION public.st_assvg(text) TO geet_web;


--
-- Name: FUNCTION st_assvg(geog public.geography, rel integer, maxdecimaldigits integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_assvg(geog public.geography, rel integer, maxdecimaldigits integer) TO geet;
GRANT ALL ON FUNCTION public.st_assvg(geog public.geography, rel integer, maxdecimaldigits integer) TO geet_web;


--
-- Name: FUNCTION st_assvg(geom public.geometry, rel integer, maxdecimaldigits integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_assvg(geom public.geometry, rel integer, maxdecimaldigits integer) TO geet;
GRANT ALL ON FUNCTION public.st_assvg(geom public.geometry, rel integer, maxdecimaldigits integer) TO geet_web;


--
-- Name: FUNCTION st_astext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_astext(text) TO geet;
GRANT ALL ON FUNCTION public.st_astext(text) TO geet_web;


--
-- Name: FUNCTION st_astext(public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_astext(public.geography) TO geet;
GRANT ALL ON FUNCTION public.st_astext(public.geography) TO geet_web;


--
-- Name: FUNCTION st_astext(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_astext(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_astext(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_astiff(rast public.raster, options text[], srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_astiff(rast public.raster, options text[], srid integer) TO geet;
GRANT ALL ON FUNCTION public.st_astiff(rast public.raster, options text[], srid integer) TO geet_web;


--
-- Name: FUNCTION st_astiff(rast public.raster, compression text, srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_astiff(rast public.raster, compression text, srid integer) TO geet;
GRANT ALL ON FUNCTION public.st_astiff(rast public.raster, compression text, srid integer) TO geet_web;


--
-- Name: FUNCTION st_astiff(rast public.raster, nbands integer[], options text[], srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_astiff(rast public.raster, nbands integer[], options text[], srid integer) TO geet;
GRANT ALL ON FUNCTION public.st_astiff(rast public.raster, nbands integer[], options text[], srid integer) TO geet_web;


--
-- Name: FUNCTION st_astiff(rast public.raster, nbands integer[], compression text, srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_astiff(rast public.raster, nbands integer[], compression text, srid integer) TO geet;
GRANT ALL ON FUNCTION public.st_astiff(rast public.raster, nbands integer[], compression text, srid integer) TO geet_web;


--
-- Name: FUNCTION st_astwkb(geom public.geometry, prec integer, prec_z integer, prec_m integer, with_sizes boolean, with_boxes boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_astwkb(geom public.geometry, prec integer, prec_z integer, prec_m integer, with_sizes boolean, with_boxes boolean) TO geet;
GRANT ALL ON FUNCTION public.st_astwkb(geom public.geometry, prec integer, prec_z integer, prec_m integer, with_sizes boolean, with_boxes boolean) TO geet_web;


--
-- Name: FUNCTION st_astwkb(geom public.geometry[], ids bigint[], prec integer, prec_z integer, prec_m integer, with_sizes boolean, with_boxes boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_astwkb(geom public.geometry[], ids bigint[], prec integer, prec_z integer, prec_m integer, with_sizes boolean, with_boxes boolean) TO geet;
GRANT ALL ON FUNCTION public.st_astwkb(geom public.geometry[], ids bigint[], prec integer, prec_z integer, prec_m integer, with_sizes boolean, with_boxes boolean) TO geet_web;


--
-- Name: FUNCTION st_asx3d(geom public.geometry, maxdecimaldigits integer, options integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_asx3d(geom public.geometry, maxdecimaldigits integer, options integer) TO geet;
GRANT ALL ON FUNCTION public.st_asx3d(geom public.geometry, maxdecimaldigits integer, options integer) TO geet_web;


--
-- Name: FUNCTION st_azimuth(geog1 public.geography, geog2 public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_azimuth(geog1 public.geography, geog2 public.geography) TO geet;
GRANT ALL ON FUNCTION public.st_azimuth(geog1 public.geography, geog2 public.geography) TO geet_web;


--
-- Name: FUNCTION st_azimuth(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_azimuth(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_azimuth(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_band(rast public.raster, nbands integer[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_band(rast public.raster, nbands integer[]) TO geet;
GRANT ALL ON FUNCTION public.st_band(rast public.raster, nbands integer[]) TO geet_web;


--
-- Name: FUNCTION st_band(rast public.raster, nband integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_band(rast public.raster, nband integer) TO geet;
GRANT ALL ON FUNCTION public.st_band(rast public.raster, nband integer) TO geet_web;


--
-- Name: FUNCTION st_band(rast public.raster, nbands text, delimiter character); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_band(rast public.raster, nbands text, delimiter character) TO geet;
GRANT ALL ON FUNCTION public.st_band(rast public.raster, nbands text, delimiter character) TO geet_web;


--
-- Name: FUNCTION st_bandisnodata(rast public.raster, forcechecking boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_bandisnodata(rast public.raster, forcechecking boolean) TO geet;
GRANT ALL ON FUNCTION public.st_bandisnodata(rast public.raster, forcechecking boolean) TO geet_web;


--
-- Name: FUNCTION st_bandisnodata(rast public.raster, band integer, forcechecking boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_bandisnodata(rast public.raster, band integer, forcechecking boolean) TO geet;
GRANT ALL ON FUNCTION public.st_bandisnodata(rast public.raster, band integer, forcechecking boolean) TO geet_web;


--
-- Name: FUNCTION st_bandmetadata(rast public.raster, band integer[], OUT bandnum integer, OUT pixeltype text, OUT nodatavalue double precision, OUT isoutdb boolean, OUT path text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_bandmetadata(rast public.raster, band integer[], OUT bandnum integer, OUT pixeltype text, OUT nodatavalue double precision, OUT isoutdb boolean, OUT path text) TO geet;
GRANT ALL ON FUNCTION public.st_bandmetadata(rast public.raster, band integer[], OUT bandnum integer, OUT pixeltype text, OUT nodatavalue double precision, OUT isoutdb boolean, OUT path text) TO geet_web;


--
-- Name: FUNCTION st_bandmetadata(rast public.raster, band integer, OUT pixeltype text, OUT nodatavalue double precision, OUT isoutdb boolean, OUT path text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_bandmetadata(rast public.raster, band integer, OUT pixeltype text, OUT nodatavalue double precision, OUT isoutdb boolean, OUT path text) TO geet;
GRANT ALL ON FUNCTION public.st_bandmetadata(rast public.raster, band integer, OUT pixeltype text, OUT nodatavalue double precision, OUT isoutdb boolean, OUT path text) TO geet_web;


--
-- Name: FUNCTION st_bandnodatavalue(rast public.raster, band integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_bandnodatavalue(rast public.raster, band integer) TO geet;
GRANT ALL ON FUNCTION public.st_bandnodatavalue(rast public.raster, band integer) TO geet_web;


--
-- Name: FUNCTION st_bandpath(rast public.raster, band integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_bandpath(rast public.raster, band integer) TO geet;
GRANT ALL ON FUNCTION public.st_bandpath(rast public.raster, band integer) TO geet_web;


--
-- Name: FUNCTION st_bandpixeltype(rast public.raster, band integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_bandpixeltype(rast public.raster, band integer) TO geet;
GRANT ALL ON FUNCTION public.st_bandpixeltype(rast public.raster, band integer) TO geet_web;


--
-- Name: FUNCTION st_bdmpolyfromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_bdmpolyfromtext(text, integer) TO geet;
GRANT ALL ON FUNCTION public.st_bdmpolyfromtext(text, integer) TO geet_web;


--
-- Name: FUNCTION st_bdpolyfromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_bdpolyfromtext(text, integer) TO geet;
GRANT ALL ON FUNCTION public.st_bdpolyfromtext(text, integer) TO geet_web;


--
-- Name: FUNCTION st_boundary(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_boundary(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_boundary(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_boundingdiagonal(geom public.geometry, fits boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_boundingdiagonal(geom public.geometry, fits boolean) TO geet;
GRANT ALL ON FUNCTION public.st_boundingdiagonal(geom public.geometry, fits boolean) TO geet_web;


--
-- Name: FUNCTION st_box2dfromgeohash(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_box2dfromgeohash(text, integer) TO geet;
GRANT ALL ON FUNCTION public.st_box2dfromgeohash(text, integer) TO geet_web;


--
-- Name: FUNCTION st_buffer(text, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_buffer(text, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_buffer(text, double precision) TO geet_web;


--
-- Name: FUNCTION st_buffer(public.geography, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_buffer(public.geography, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_buffer(public.geography, double precision) TO geet_web;


--
-- Name: FUNCTION st_buffer(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_buffer(public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_buffer(public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION st_buffer(text, double precision, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_buffer(text, double precision, integer) TO geet;
GRANT ALL ON FUNCTION public.st_buffer(text, double precision, integer) TO geet_web;


--
-- Name: FUNCTION st_buffer(text, double precision, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_buffer(text, double precision, text) TO geet;
GRANT ALL ON FUNCTION public.st_buffer(text, double precision, text) TO geet_web;


--
-- Name: FUNCTION st_buffer(public.geography, double precision, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_buffer(public.geography, double precision, integer) TO geet;
GRANT ALL ON FUNCTION public.st_buffer(public.geography, double precision, integer) TO geet_web;


--
-- Name: FUNCTION st_buffer(public.geography, double precision, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_buffer(public.geography, double precision, text) TO geet;
GRANT ALL ON FUNCTION public.st_buffer(public.geography, double precision, text) TO geet_web;


--
-- Name: FUNCTION st_buffer(public.geometry, double precision, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_buffer(public.geometry, double precision, integer) TO geet;
GRANT ALL ON FUNCTION public.st_buffer(public.geometry, double precision, integer) TO geet_web;


--
-- Name: FUNCTION st_buffer(public.geometry, double precision, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_buffer(public.geometry, double precision, text) TO geet;
GRANT ALL ON FUNCTION public.st_buffer(public.geometry, double precision, text) TO geet_web;


--
-- Name: FUNCTION st_buildarea(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_buildarea(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_buildarea(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_centroid(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_centroid(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_centroid(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_cleangeometry(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_cleangeometry(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_cleangeometry(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_clip(rast public.raster, geom public.geometry, crop boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clip(rast public.raster, geom public.geometry, crop boolean) TO geet;
GRANT ALL ON FUNCTION public.st_clip(rast public.raster, geom public.geometry, crop boolean) TO geet_web;


--
-- Name: FUNCTION st_clip(rast public.raster, nband integer, geom public.geometry, crop boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clip(rast public.raster, nband integer, geom public.geometry, crop boolean) TO geet;
GRANT ALL ON FUNCTION public.st_clip(rast public.raster, nband integer, geom public.geometry, crop boolean) TO geet_web;


--
-- Name: FUNCTION st_clip(rast public.raster, geom public.geometry, nodataval double precision[], crop boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clip(rast public.raster, geom public.geometry, nodataval double precision[], crop boolean) TO geet;
GRANT ALL ON FUNCTION public.st_clip(rast public.raster, geom public.geometry, nodataval double precision[], crop boolean) TO geet_web;


--
-- Name: FUNCTION st_clip(rast public.raster, geom public.geometry, nodataval double precision, crop boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clip(rast public.raster, geom public.geometry, nodataval double precision, crop boolean) TO geet;
GRANT ALL ON FUNCTION public.st_clip(rast public.raster, geom public.geometry, nodataval double precision, crop boolean) TO geet_web;


--
-- Name: FUNCTION st_clip(rast public.raster, nband integer[], geom public.geometry, nodataval double precision[], crop boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clip(rast public.raster, nband integer[], geom public.geometry, nodataval double precision[], crop boolean) TO geet;
GRANT ALL ON FUNCTION public.st_clip(rast public.raster, nband integer[], geom public.geometry, nodataval double precision[], crop boolean) TO geet_web;


--
-- Name: FUNCTION st_clip(rast public.raster, nband integer, geom public.geometry, nodataval double precision, crop boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clip(rast public.raster, nband integer, geom public.geometry, nodataval double precision, crop boolean) TO geet;
GRANT ALL ON FUNCTION public.st_clip(rast public.raster, nband integer, geom public.geometry, nodataval double precision, crop boolean) TO geet_web;


--
-- Name: FUNCTION st_clipbybox2d(geom public.geometry, box public.box2d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clipbybox2d(geom public.geometry, box public.box2d) TO geet;
GRANT ALL ON FUNCTION public.st_clipbybox2d(geom public.geometry, box public.box2d) TO geet_web;


--
-- Name: FUNCTION st_closestpoint(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_closestpoint(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_closestpoint(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_closestpointofapproach(public.geometry, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_closestpointofapproach(public.geometry, public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_closestpointofapproach(public.geometry, public.geometry) TO geet_web;


--
-- Name: FUNCTION st_clusterdbscan(public.geometry, eps double precision, minpoints integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clusterdbscan(public.geometry, eps double precision, minpoints integer) TO geet;
GRANT ALL ON FUNCTION public.st_clusterdbscan(public.geometry, eps double precision, minpoints integer) TO geet_web;


--
-- Name: FUNCTION st_clusterintersecting(public.geometry[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clusterintersecting(public.geometry[]) TO geet;
GRANT ALL ON FUNCTION public.st_clusterintersecting(public.geometry[]) TO geet_web;


--
-- Name: FUNCTION st_clusterkmeans(geom public.geometry, k integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clusterkmeans(geom public.geometry, k integer) TO geet;
GRANT ALL ON FUNCTION public.st_clusterkmeans(geom public.geometry, k integer) TO geet_web;


--
-- Name: FUNCTION st_clusterwithin(public.geometry[], double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clusterwithin(public.geometry[], double precision) TO geet;
GRANT ALL ON FUNCTION public.st_clusterwithin(public.geometry[], double precision) TO geet_web;


--
-- Name: FUNCTION st_collect(public.geometry[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_collect(public.geometry[]) TO geet;
GRANT ALL ON FUNCTION public.st_collect(public.geometry[]) TO geet_web;


--
-- Name: FUNCTION st_collect(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_collect(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_collect(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_collectionextract(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_collectionextract(public.geometry, integer) TO geet;
GRANT ALL ON FUNCTION public.st_collectionextract(public.geometry, integer) TO geet_web;


--
-- Name: FUNCTION st_collectionhomogenize(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_collectionhomogenize(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_collectionhomogenize(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_colormap(rast public.raster, colormap text, method text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_colormap(rast public.raster, colormap text, method text) TO geet;
GRANT ALL ON FUNCTION public.st_colormap(rast public.raster, colormap text, method text) TO geet_web;


--
-- Name: FUNCTION st_colormap(rast public.raster, nband integer, colormap text, method text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_colormap(rast public.raster, nband integer, colormap text, method text) TO geet;
GRANT ALL ON FUNCTION public.st_colormap(rast public.raster, nband integer, colormap text, method text) TO geet_web;


--
-- Name: FUNCTION st_combine_bbox(public.box2d, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_combine_bbox(public.box2d, public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_combine_bbox(public.box2d, public.geometry) TO geet_web;


--
-- Name: FUNCTION st_combine_bbox(public.box3d, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_combine_bbox(public.box3d, public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_combine_bbox(public.box3d, public.geometry) TO geet_web;


--
-- Name: FUNCTION st_combinebbox(public.box2d, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_combinebbox(public.box2d, public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_combinebbox(public.box2d, public.geometry) TO geet_web;


--
-- Name: FUNCTION st_combinebbox(public.box3d, public.box3d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_combinebbox(public.box3d, public.box3d) TO geet;
GRANT ALL ON FUNCTION public.st_combinebbox(public.box3d, public.box3d) TO geet_web;


--
-- Name: FUNCTION st_combinebbox(public.box3d, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_combinebbox(public.box3d, public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_combinebbox(public.box3d, public.geometry) TO geet_web;


--
-- Name: FUNCTION st_concavehull(param_geom public.geometry, param_pctconvex double precision, param_allow_holes boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_concavehull(param_geom public.geometry, param_pctconvex double precision, param_allow_holes boolean) TO geet;
GRANT ALL ON FUNCTION public.st_concavehull(param_geom public.geometry, param_pctconvex double precision, param_allow_holes boolean) TO geet_web;


--
-- Name: FUNCTION st_contains(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_contains(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_contains(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_contains(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_contains(rast1 public.raster, rast2 public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_contains(rast1 public.raster, rast2 public.raster) TO geet_web;


--
-- Name: FUNCTION st_contains(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_contains(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet;
GRANT ALL ON FUNCTION public.st_contains(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet_web;


--
-- Name: FUNCTION st_containsproperly(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_containsproperly(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_containsproperly(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_containsproperly(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_containsproperly(rast1 public.raster, rast2 public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_containsproperly(rast1 public.raster, rast2 public.raster) TO geet_web;


--
-- Name: FUNCTION st_containsproperly(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_containsproperly(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet;
GRANT ALL ON FUNCTION public.st_containsproperly(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet_web;


--
-- Name: FUNCTION st_convexhull(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_convexhull(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_convexhull(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_convexhull(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_convexhull(public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_convexhull(public.raster) TO geet_web;


--
-- Name: FUNCTION st_coorddim(geometry public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_coorddim(geometry public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_coorddim(geometry public.geometry) TO geet_web;


--
-- Name: FUNCTION st_count(rast public.raster, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_count(rast public.raster, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public.st_count(rast public.raster, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION st_count(rastertable text, rastercolumn text, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_count(rastertable text, rastercolumn text, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public.st_count(rastertable text, rastercolumn text, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION st_count(rast public.raster, nband integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_count(rast public.raster, nband integer, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public.st_count(rast public.raster, nband integer, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION st_count(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_count(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public.st_count(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION st_coveredby(text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_coveredby(text, text) TO geet;
GRANT ALL ON FUNCTION public.st_coveredby(text, text) TO geet_web;


--
-- Name: FUNCTION st_coveredby(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_coveredby(public.geography, public.geography) TO geet;
GRANT ALL ON FUNCTION public.st_coveredby(public.geography, public.geography) TO geet_web;


--
-- Name: FUNCTION st_coveredby(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_coveredby(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_coveredby(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_coveredby(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_coveredby(rast1 public.raster, rast2 public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_coveredby(rast1 public.raster, rast2 public.raster) TO geet_web;


--
-- Name: FUNCTION st_coveredby(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_coveredby(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet;
GRANT ALL ON FUNCTION public.st_coveredby(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet_web;


--
-- Name: FUNCTION st_covers(text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_covers(text, text) TO geet;
GRANT ALL ON FUNCTION public.st_covers(text, text) TO geet_web;


--
-- Name: FUNCTION st_covers(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_covers(public.geography, public.geography) TO geet;
GRANT ALL ON FUNCTION public.st_covers(public.geography, public.geography) TO geet_web;


--
-- Name: FUNCTION st_covers(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_covers(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_covers(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_covers(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_covers(rast1 public.raster, rast2 public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_covers(rast1 public.raster, rast2 public.raster) TO geet_web;


--
-- Name: FUNCTION st_covers(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_covers(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet;
GRANT ALL ON FUNCTION public.st_covers(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet_web;


--
-- Name: FUNCTION st_cpawithin(public.geometry, public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_cpawithin(public.geometry, public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_cpawithin(public.geometry, public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION st_createoverview(tab regclass, col name, factor integer, algo text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_createoverview(tab regclass, col name, factor integer, algo text) TO geet;
GRANT ALL ON FUNCTION public.st_createoverview(tab regclass, col name, factor integer, algo text) TO geet_web;


--
-- Name: FUNCTION st_crosses(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_crosses(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_crosses(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_curvetoline(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_curvetoline(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_curvetoline(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_curvetoline(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_curvetoline(public.geometry, integer) TO geet;
GRANT ALL ON FUNCTION public.st_curvetoline(public.geometry, integer) TO geet_web;


--
-- Name: FUNCTION st_delaunaytriangles(g1 public.geometry, tolerance double precision, flags integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_delaunaytriangles(g1 public.geometry, tolerance double precision, flags integer) TO geet;
GRANT ALL ON FUNCTION public.st_delaunaytriangles(g1 public.geometry, tolerance double precision, flags integer) TO geet_web;


--
-- Name: FUNCTION st_dfullywithin(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dfullywithin(geom1 public.geometry, geom2 public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_dfullywithin(geom1 public.geometry, geom2 public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION st_dfullywithin(rast1 public.raster, rast2 public.raster, distance double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dfullywithin(rast1 public.raster, rast2 public.raster, distance double precision) TO geet;
GRANT ALL ON FUNCTION public.st_dfullywithin(rast1 public.raster, rast2 public.raster, distance double precision) TO geet_web;


--
-- Name: FUNCTION st_dfullywithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dfullywithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision) TO geet;
GRANT ALL ON FUNCTION public.st_dfullywithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision) TO geet_web;


--
-- Name: FUNCTION st_difference(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_difference(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_difference(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_dimension(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dimension(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_dimension(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_disjoint(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_disjoint(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_disjoint(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_disjoint(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_disjoint(rast1 public.raster, rast2 public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_disjoint(rast1 public.raster, rast2 public.raster) TO geet_web;


--
-- Name: FUNCTION st_disjoint(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_disjoint(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet;
GRANT ALL ON FUNCTION public.st_disjoint(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet_web;


--
-- Name: FUNCTION st_distance(text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_distance(text, text) TO geet;
GRANT ALL ON FUNCTION public.st_distance(text, text) TO geet_web;


--
-- Name: FUNCTION st_distance(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_distance(public.geography, public.geography) TO geet;
GRANT ALL ON FUNCTION public.st_distance(public.geography, public.geography) TO geet_web;


--
-- Name: FUNCTION st_distance(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_distance(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_distance(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_distance(public.geography, public.geography, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_distance(public.geography, public.geography, boolean) TO geet;
GRANT ALL ON FUNCTION public.st_distance(public.geography, public.geography, boolean) TO geet_web;


--
-- Name: FUNCTION st_distance_sphere(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_distance_sphere(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_distance_sphere(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_distance_spheroid(geom1 public.geometry, geom2 public.geometry, public.spheroid); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_distance_spheroid(geom1 public.geometry, geom2 public.geometry, public.spheroid) TO geet;
GRANT ALL ON FUNCTION public.st_distance_spheroid(geom1 public.geometry, geom2 public.geometry, public.spheroid) TO geet_web;


--
-- Name: FUNCTION st_distancecpa(public.geometry, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_distancecpa(public.geometry, public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_distancecpa(public.geometry, public.geometry) TO geet_web;


--
-- Name: FUNCTION st_distancesphere(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_distancesphere(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_distancesphere(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_distancespheroid(geom1 public.geometry, geom2 public.geometry, public.spheroid); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_distancespheroid(geom1 public.geometry, geom2 public.geometry, public.spheroid) TO geet;
GRANT ALL ON FUNCTION public.st_distancespheroid(geom1 public.geometry, geom2 public.geometry, public.spheroid) TO geet_web;


--
-- Name: FUNCTION st_distinct4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_distinct4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet;
GRANT ALL ON FUNCTION public.st_distinct4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet_web;


--
-- Name: FUNCTION st_distinct4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_distinct4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO geet;
GRANT ALL ON FUNCTION public.st_distinct4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO geet_web;


--
-- Name: FUNCTION st_dump(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dump(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_dump(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_dumpaspolygons(rast public.raster, band integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dumpaspolygons(rast public.raster, band integer, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public.st_dumpaspolygons(rast public.raster, band integer, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION st_dumppoints(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dumppoints(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_dumppoints(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_dumprings(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dumprings(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_dumprings(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_dumpvalues(rast public.raster, nband integer[], exclude_nodata_value boolean, OUT nband integer, OUT valarray double precision[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dumpvalues(rast public.raster, nband integer[], exclude_nodata_value boolean, OUT nband integer, OUT valarray double precision[]) TO geet;
GRANT ALL ON FUNCTION public.st_dumpvalues(rast public.raster, nband integer[], exclude_nodata_value boolean, OUT nband integer, OUT valarray double precision[]) TO geet_web;


--
-- Name: FUNCTION st_dumpvalues(rast public.raster, nband integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dumpvalues(rast public.raster, nband integer, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public.st_dumpvalues(rast public.raster, nband integer, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION st_dwithin(text, text, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dwithin(text, text, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_dwithin(text, text, double precision) TO geet_web;


--
-- Name: FUNCTION st_dwithin(public.geography, public.geography, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dwithin(public.geography, public.geography, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_dwithin(public.geography, public.geography, double precision) TO geet_web;


--
-- Name: FUNCTION st_dwithin(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dwithin(geom1 public.geometry, geom2 public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_dwithin(geom1 public.geometry, geom2 public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION st_dwithin(rast1 public.raster, rast2 public.raster, distance double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dwithin(rast1 public.raster, rast2 public.raster, distance double precision) TO geet;
GRANT ALL ON FUNCTION public.st_dwithin(rast1 public.raster, rast2 public.raster, distance double precision) TO geet_web;


--
-- Name: FUNCTION st_dwithin(public.geography, public.geography, double precision, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dwithin(public.geography, public.geography, double precision, boolean) TO geet;
GRANT ALL ON FUNCTION public.st_dwithin(public.geography, public.geography, double precision, boolean) TO geet_web;


--
-- Name: FUNCTION st_dwithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_dwithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision) TO geet;
GRANT ALL ON FUNCTION public.st_dwithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision) TO geet_web;


--
-- Name: FUNCTION st_endpoint(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_endpoint(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_endpoint(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_envelope(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_envelope(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_envelope(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_envelope(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_envelope(public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_envelope(public.raster) TO geet_web;


--
-- Name: FUNCTION st_equals(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_equals(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_equals(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_estimated_extent(text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_estimated_extent(text, text) TO geet;
GRANT ALL ON FUNCTION public.st_estimated_extent(text, text) TO geet_web;


--
-- Name: FUNCTION st_estimated_extent(text, text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_estimated_extent(text, text, text) TO geet;
GRANT ALL ON FUNCTION public.st_estimated_extent(text, text, text) TO geet_web;


--
-- Name: FUNCTION st_estimatedextent(text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_estimatedextent(text, text) TO geet;
GRANT ALL ON FUNCTION public.st_estimatedextent(text, text) TO geet_web;


--
-- Name: FUNCTION st_estimatedextent(text, text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_estimatedextent(text, text, text) TO geet;
GRANT ALL ON FUNCTION public.st_estimatedextent(text, text, text) TO geet_web;


--
-- Name: FUNCTION st_estimatedextent(text, text, text, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_estimatedextent(text, text, text, boolean) TO geet;
GRANT ALL ON FUNCTION public.st_estimatedextent(text, text, text, boolean) TO geet_web;


--
-- Name: FUNCTION st_expand(public.box2d, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_expand(public.box2d, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_expand(public.box2d, double precision) TO geet_web;


--
-- Name: FUNCTION st_expand(public.box3d, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_expand(public.box3d, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_expand(public.box3d, double precision) TO geet_web;


--
-- Name: FUNCTION st_expand(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_expand(public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_expand(public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION st_expand(box public.box2d, dx double precision, dy double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_expand(box public.box2d, dx double precision, dy double precision) TO geet;
GRANT ALL ON FUNCTION public.st_expand(box public.box2d, dx double precision, dy double precision) TO geet_web;


--
-- Name: FUNCTION st_expand(box public.box3d, dx double precision, dy double precision, dz double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_expand(box public.box3d, dx double precision, dy double precision, dz double precision) TO geet;
GRANT ALL ON FUNCTION public.st_expand(box public.box3d, dx double precision, dy double precision, dz double precision) TO geet_web;


--
-- Name: FUNCTION st_expand(geom public.geometry, dx double precision, dy double precision, dz double precision, dm double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_expand(geom public.geometry, dx double precision, dy double precision, dz double precision, dm double precision) TO geet;
GRANT ALL ON FUNCTION public.st_expand(geom public.geometry, dx double precision, dy double precision, dz double precision, dm double precision) TO geet_web;


--
-- Name: FUNCTION st_exteriorring(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_exteriorring(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_exteriorring(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_find_extent(text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_find_extent(text, text) TO geet;
GRANT ALL ON FUNCTION public.st_find_extent(text, text) TO geet_web;


--
-- Name: FUNCTION st_find_extent(text, text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_find_extent(text, text, text) TO geet;
GRANT ALL ON FUNCTION public.st_find_extent(text, text, text) TO geet_web;


--
-- Name: FUNCTION st_findextent(text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_findextent(text, text) TO geet;
GRANT ALL ON FUNCTION public.st_findextent(text, text) TO geet_web;


--
-- Name: FUNCTION st_findextent(text, text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_findextent(text, text, text) TO geet;
GRANT ALL ON FUNCTION public.st_findextent(text, text, text) TO geet_web;


--
-- Name: FUNCTION st_flipcoordinates(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_flipcoordinates(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_flipcoordinates(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_force2d(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_force2d(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_force2d(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_force3d(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_force3d(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_force3d(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_force3dm(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_force3dm(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_force3dm(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_force3dz(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_force3dz(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_force3dz(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_force4d(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_force4d(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_force4d(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_force_2d(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_force_2d(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_force_2d(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_force_3d(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_force_3d(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_force_3d(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_force_3dm(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_force_3dm(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_force_3dm(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_force_3dz(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_force_3dz(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_force_3dz(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_force_4d(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_force_4d(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_force_4d(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_force_collection(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_force_collection(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_force_collection(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_forcecollection(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_forcecollection(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_forcecollection(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_forcecurve(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_forcecurve(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_forcecurve(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_forcerhr(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_forcerhr(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_forcerhr(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_forcesfs(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_forcesfs(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_forcesfs(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_forcesfs(public.geometry, version text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_forcesfs(public.geometry, version text) TO geet;
GRANT ALL ON FUNCTION public.st_forcesfs(public.geometry, version text) TO geet_web;


--
-- Name: FUNCTION st_fromgdalraster(gdaldata bytea, srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_fromgdalraster(gdaldata bytea, srid integer) TO geet;
GRANT ALL ON FUNCTION public.st_fromgdalraster(gdaldata bytea, srid integer) TO geet_web;


--
-- Name: FUNCTION st_gdaldrivers(OUT idx integer, OUT short_name text, OUT long_name text, OUT create_options text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_gdaldrivers(OUT idx integer, OUT short_name text, OUT long_name text, OUT create_options text) TO geet;
GRANT ALL ON FUNCTION public.st_gdaldrivers(OUT idx integer, OUT short_name text, OUT long_name text, OUT create_options text) TO geet_web;


--
-- Name: FUNCTION st_generatepoints(area public.geometry, npoints numeric); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_generatepoints(area public.geometry, npoints numeric) TO geet;
GRANT ALL ON FUNCTION public.st_generatepoints(area public.geometry, npoints numeric) TO geet_web;


--
-- Name: FUNCTION st_geogfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geogfromtext(text) TO geet;
GRANT ALL ON FUNCTION public.st_geogfromtext(text) TO geet_web;


--
-- Name: FUNCTION st_geogfromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geogfromwkb(bytea) TO geet;
GRANT ALL ON FUNCTION public.st_geogfromwkb(bytea) TO geet_web;


--
-- Name: FUNCTION st_geographyfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geographyfromtext(text) TO geet;
GRANT ALL ON FUNCTION public.st_geographyfromtext(text) TO geet_web;


--
-- Name: FUNCTION st_geohash(geog public.geography, maxchars integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geohash(geog public.geography, maxchars integer) TO geet;
GRANT ALL ON FUNCTION public.st_geohash(geog public.geography, maxchars integer) TO geet_web;


--
-- Name: FUNCTION st_geohash(geom public.geometry, maxchars integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geohash(geom public.geometry, maxchars integer) TO geet;
GRANT ALL ON FUNCTION public.st_geohash(geom public.geometry, maxchars integer) TO geet_web;


--
-- Name: FUNCTION st_geomcollfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomcollfromtext(text) TO geet;
GRANT ALL ON FUNCTION public.st_geomcollfromtext(text) TO geet_web;


--
-- Name: FUNCTION st_geomcollfromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomcollfromtext(text, integer) TO geet;
GRANT ALL ON FUNCTION public.st_geomcollfromtext(text, integer) TO geet_web;


--
-- Name: FUNCTION st_geomcollfromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomcollfromwkb(bytea) TO geet;
GRANT ALL ON FUNCTION public.st_geomcollfromwkb(bytea) TO geet_web;


--
-- Name: FUNCTION st_geomcollfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomcollfromwkb(bytea, integer) TO geet;
GRANT ALL ON FUNCTION public.st_geomcollfromwkb(bytea, integer) TO geet_web;


--
-- Name: FUNCTION st_geometricmedian(g public.geometry, tolerance double precision, max_iter integer, fail_if_not_converged boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geometricmedian(g public.geometry, tolerance double precision, max_iter integer, fail_if_not_converged boolean) TO geet;
GRANT ALL ON FUNCTION public.st_geometricmedian(g public.geometry, tolerance double precision, max_iter integer, fail_if_not_converged boolean) TO geet_web;


--
-- Name: FUNCTION st_geometryfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geometryfromtext(text) TO geet;
GRANT ALL ON FUNCTION public.st_geometryfromtext(text) TO geet_web;


--
-- Name: FUNCTION st_geometryfromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geometryfromtext(text, integer) TO geet;
GRANT ALL ON FUNCTION public.st_geometryfromtext(text, integer) TO geet_web;


--
-- Name: FUNCTION st_geometryn(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geometryn(public.geometry, integer) TO geet;
GRANT ALL ON FUNCTION public.st_geometryn(public.geometry, integer) TO geet_web;


--
-- Name: FUNCTION st_geometrytype(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geometrytype(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_geometrytype(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_geomfromewkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromewkb(bytea) TO geet;
GRANT ALL ON FUNCTION public.st_geomfromewkb(bytea) TO geet_web;


--
-- Name: FUNCTION st_geomfromewkt(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromewkt(text) TO geet;
GRANT ALL ON FUNCTION public.st_geomfromewkt(text) TO geet_web;


--
-- Name: FUNCTION st_geomfromgeohash(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromgeohash(text, integer) TO geet;
GRANT ALL ON FUNCTION public.st_geomfromgeohash(text, integer) TO geet_web;


--
-- Name: FUNCTION st_geomfromgeojson(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromgeojson(text) TO geet;
GRANT ALL ON FUNCTION public.st_geomfromgeojson(text) TO geet_web;


--
-- Name: FUNCTION st_geomfromgml(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromgml(text) TO geet;
GRANT ALL ON FUNCTION public.st_geomfromgml(text) TO geet_web;


--
-- Name: FUNCTION st_geomfromgml(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromgml(text, integer) TO geet;
GRANT ALL ON FUNCTION public.st_geomfromgml(text, integer) TO geet_web;


--
-- Name: FUNCTION st_geomfromkml(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromkml(text) TO geet;
GRANT ALL ON FUNCTION public.st_geomfromkml(text) TO geet_web;


--
-- Name: FUNCTION st_geomfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromtext(text) TO geet;
GRANT ALL ON FUNCTION public.st_geomfromtext(text) TO geet_web;


--
-- Name: FUNCTION st_geomfromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromtext(text, integer) TO geet;
GRANT ALL ON FUNCTION public.st_geomfromtext(text, integer) TO geet_web;


--
-- Name: FUNCTION st_geomfromtwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromtwkb(bytea) TO geet;
GRANT ALL ON FUNCTION public.st_geomfromtwkb(bytea) TO geet_web;


--
-- Name: FUNCTION st_geomfromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromwkb(bytea) TO geet;
GRANT ALL ON FUNCTION public.st_geomfromwkb(bytea) TO geet_web;


--
-- Name: FUNCTION st_geomfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geomfromwkb(bytea, integer) TO geet;
GRANT ALL ON FUNCTION public.st_geomfromwkb(bytea, integer) TO geet_web;


--
-- Name: FUNCTION st_georeference(rast public.raster, format text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_georeference(rast public.raster, format text) TO geet;
GRANT ALL ON FUNCTION public.st_georeference(rast public.raster, format text) TO geet_web;


--
-- Name: FUNCTION st_geotransform(public.raster, OUT imag double precision, OUT jmag double precision, OUT theta_i double precision, OUT theta_ij double precision, OUT xoffset double precision, OUT yoffset double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_geotransform(public.raster, OUT imag double precision, OUT jmag double precision, OUT theta_i double precision, OUT theta_ij double precision, OUT xoffset double precision, OUT yoffset double precision) TO geet;
GRANT ALL ON FUNCTION public.st_geotransform(public.raster, OUT imag double precision, OUT jmag double precision, OUT theta_i double precision, OUT theta_ij double precision, OUT xoffset double precision, OUT yoffset double precision) TO geet_web;


--
-- Name: FUNCTION st_gmltosql(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_gmltosql(text) TO geet;
GRANT ALL ON FUNCTION public.st_gmltosql(text) TO geet_web;


--
-- Name: FUNCTION st_gmltosql(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_gmltosql(text, integer) TO geet;
GRANT ALL ON FUNCTION public.st_gmltosql(text, integer) TO geet_web;


--
-- Name: FUNCTION st_hasarc(geometry public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_hasarc(geometry public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_hasarc(geometry public.geometry) TO geet_web;


--
-- Name: FUNCTION st_hasnoband(rast public.raster, nband integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_hasnoband(rast public.raster, nband integer) TO geet;
GRANT ALL ON FUNCTION public.st_hasnoband(rast public.raster, nband integer) TO geet_web;


--
-- Name: FUNCTION st_hausdorffdistance(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_hausdorffdistance(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_hausdorffdistance(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_hausdorffdistance(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_hausdorffdistance(geom1 public.geometry, geom2 public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_hausdorffdistance(geom1 public.geometry, geom2 public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION st_height(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_height(public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_height(public.raster) TO geet_web;


--
-- Name: FUNCTION st_hillshade(rast public.raster, nband integer, pixeltype text, azimuth double precision, altitude double precision, max_bright double precision, scale double precision, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_hillshade(rast public.raster, nband integer, pixeltype text, azimuth double precision, altitude double precision, max_bright double precision, scale double precision, interpolate_nodata boolean) TO geet;
GRANT ALL ON FUNCTION public.st_hillshade(rast public.raster, nband integer, pixeltype text, azimuth double precision, altitude double precision, max_bright double precision, scale double precision, interpolate_nodata boolean) TO geet_web;


--
-- Name: FUNCTION st_hillshade(rast public.raster, nband integer, customextent public.raster, pixeltype text, azimuth double precision, altitude double precision, max_bright double precision, scale double precision, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_hillshade(rast public.raster, nband integer, customextent public.raster, pixeltype text, azimuth double precision, altitude double precision, max_bright double precision, scale double precision, interpolate_nodata boolean) TO geet;
GRANT ALL ON FUNCTION public.st_hillshade(rast public.raster, nband integer, customextent public.raster, pixeltype text, azimuth double precision, altitude double precision, max_bright double precision, scale double precision, interpolate_nodata boolean) TO geet_web;


--
-- Name: FUNCTION st_histogram(rast public.raster, nband integer, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_histogram(rast public.raster, nband integer, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_histogram(rast public.raster, nband integer, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_histogram(rastertable text, rastercolumn text, nband integer, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_histogram(rastertable text, rastercolumn text, nband integer, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_histogram(rastertable text, rastercolumn text, nband integer, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_histogram(rast public.raster, nband integer, exclude_nodata_value boolean, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_histogram(rast public.raster, nband integer, exclude_nodata_value boolean, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_histogram(rast public.raster, nband integer, exclude_nodata_value boolean, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_histogram(rast public.raster, nband integer, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_histogram(rast public.raster, nband integer, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_histogram(rast public.raster, nband integer, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_histogram(rastertable text, rastercolumn text, nband integer, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_histogram(rastertable text, rastercolumn text, nband integer, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_histogram(rastertable text, rastercolumn text, nband integer, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_histogram(rast public.raster, nband integer, exclude_nodata_value boolean, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_histogram(rast public.raster, nband integer, exclude_nodata_value boolean, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_histogram(rast public.raster, nband integer, exclude_nodata_value boolean, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_interiorringn(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_interiorringn(public.geometry, integer) TO geet;
GRANT ALL ON FUNCTION public.st_interiorringn(public.geometry, integer) TO geet_web;


--
-- Name: FUNCTION st_interpolatepoint(line public.geometry, point public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_interpolatepoint(line public.geometry, point public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_interpolatepoint(line public.geometry, point public.geometry) TO geet_web;


--
-- Name: FUNCTION st_intersection(text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(text, text) TO geet;
GRANT ALL ON FUNCTION public.st_intersection(text, text) TO geet_web;


--
-- Name: FUNCTION st_intersection(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(public.geography, public.geography) TO geet;
GRANT ALL ON FUNCTION public.st_intersection(public.geography, public.geography) TO geet_web;


--
-- Name: FUNCTION st_intersection(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_intersection(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_intersection(rast public.raster, geomin public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(rast public.raster, geomin public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_intersection(rast public.raster, geomin public.geometry) TO geet_web;


--
-- Name: FUNCTION st_intersection(geomin public.geometry, rast public.raster, band integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(geomin public.geometry, rast public.raster, band integer) TO geet;
GRANT ALL ON FUNCTION public.st_intersection(geomin public.geometry, rast public.raster, band integer) TO geet_web;


--
-- Name: FUNCTION st_intersection(rast public.raster, band integer, geomin public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(rast public.raster, band integer, geomin public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_intersection(rast public.raster, band integer, geomin public.geometry) TO geet_web;


--
-- Name: FUNCTION st_intersection(rast1 public.raster, rast2 public.raster, nodataval double precision[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, rast2 public.raster, nodataval double precision[]) TO geet;
GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, rast2 public.raster, nodataval double precision[]) TO geet_web;


--
-- Name: FUNCTION st_intersection(rast1 public.raster, rast2 public.raster, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, rast2 public.raster, nodataval double precision) TO geet;
GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, rast2 public.raster, nodataval double precision) TO geet_web;


--
-- Name: FUNCTION st_intersection(rast1 public.raster, rast2 public.raster, returnband text, nodataval double precision[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, rast2 public.raster, returnband text, nodataval double precision[]) TO geet;
GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, rast2 public.raster, returnband text, nodataval double precision[]) TO geet_web;


--
-- Name: FUNCTION st_intersection(rast1 public.raster, rast2 public.raster, returnband text, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, rast2 public.raster, returnband text, nodataval double precision) TO geet;
GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, rast2 public.raster, returnband text, nodataval double precision) TO geet_web;


--
-- Name: FUNCTION st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, nodataval double precision[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, nodataval double precision[]) TO geet;
GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, nodataval double precision[]) TO geet_web;


--
-- Name: FUNCTION st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, nodataval double precision) TO geet;
GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, nodataval double precision) TO geet_web;


--
-- Name: FUNCTION st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, returnband text, nodataval double precision[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, returnband text, nodataval double precision[]) TO geet;
GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, returnband text, nodataval double precision[]) TO geet_web;


--
-- Name: FUNCTION st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, returnband text, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, returnband text, nodataval double precision) TO geet;
GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, returnband text, nodataval double precision) TO geet_web;


--
-- Name: FUNCTION st_intersects(text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersects(text, text) TO geet;
GRANT ALL ON FUNCTION public.st_intersects(text, text) TO geet_web;


--
-- Name: FUNCTION st_intersects(public.geography, public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersects(public.geography, public.geography) TO geet;
GRANT ALL ON FUNCTION public.st_intersects(public.geography, public.geography) TO geet_web;


--
-- Name: FUNCTION st_intersects(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersects(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_intersects(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_intersects(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersects(rast1 public.raster, rast2 public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_intersects(rast1 public.raster, rast2 public.raster) TO geet_web;


--
-- Name: FUNCTION st_intersects(geom public.geometry, rast public.raster, nband integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersects(geom public.geometry, rast public.raster, nband integer) TO geet;
GRANT ALL ON FUNCTION public.st_intersects(geom public.geometry, rast public.raster, nband integer) TO geet_web;


--
-- Name: FUNCTION st_intersects(rast public.raster, nband integer, geom public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersects(rast public.raster, nband integer, geom public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_intersects(rast public.raster, nband integer, geom public.geometry) TO geet_web;


--
-- Name: FUNCTION st_intersects(rast public.raster, geom public.geometry, nband integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersects(rast public.raster, geom public.geometry, nband integer) TO geet;
GRANT ALL ON FUNCTION public.st_intersects(rast public.raster, geom public.geometry, nband integer) TO geet_web;


--
-- Name: FUNCTION st_intersects(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_intersects(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet;
GRANT ALL ON FUNCTION public.st_intersects(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet_web;


--
-- Name: FUNCTION st_invdistweight4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_invdistweight4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet;
GRANT ALL ON FUNCTION public.st_invdistweight4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet_web;


--
-- Name: FUNCTION st_isclosed(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_isclosed(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_isclosed(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_iscollection(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_iscollection(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_iscollection(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_iscoveragetile(rast public.raster, coverage public.raster, tilewidth integer, tileheight integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_iscoveragetile(rast public.raster, coverage public.raster, tilewidth integer, tileheight integer) TO geet;
GRANT ALL ON FUNCTION public.st_iscoveragetile(rast public.raster, coverage public.raster, tilewidth integer, tileheight integer) TO geet_web;


--
-- Name: FUNCTION st_isempty(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_isempty(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_isempty(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_isempty(rast public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_isempty(rast public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_isempty(rast public.raster) TO geet_web;


--
-- Name: FUNCTION st_isring(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_isring(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_isring(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_issimple(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_issimple(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_issimple(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_isvalid(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_isvalid(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_isvalid(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_isvalid(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_isvalid(public.geometry, integer) TO geet;
GRANT ALL ON FUNCTION public.st_isvalid(public.geometry, integer) TO geet_web;


--
-- Name: FUNCTION st_isvaliddetail(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_isvaliddetail(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_isvaliddetail(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_isvaliddetail(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_isvaliddetail(public.geometry, integer) TO geet;
GRANT ALL ON FUNCTION public.st_isvaliddetail(public.geometry, integer) TO geet_web;


--
-- Name: FUNCTION st_isvalidreason(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_isvalidreason(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_isvalidreason(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_isvalidreason(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_isvalidreason(public.geometry, integer) TO geet;
GRANT ALL ON FUNCTION public.st_isvalidreason(public.geometry, integer) TO geet_web;


--
-- Name: FUNCTION st_isvalidtrajectory(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_isvalidtrajectory(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_isvalidtrajectory(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_length(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_length(text) TO geet;
GRANT ALL ON FUNCTION public.st_length(text) TO geet_web;


--
-- Name: FUNCTION st_length(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_length(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_length(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_length(geog public.geography, use_spheroid boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_length(geog public.geography, use_spheroid boolean) TO geet;
GRANT ALL ON FUNCTION public.st_length(geog public.geography, use_spheroid boolean) TO geet_web;


--
-- Name: FUNCTION st_length2d(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_length2d(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_length2d(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_length2d_spheroid(public.geometry, public.spheroid); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_length2d_spheroid(public.geometry, public.spheroid) TO geet;
GRANT ALL ON FUNCTION public.st_length2d_spheroid(public.geometry, public.spheroid) TO geet_web;


--
-- Name: FUNCTION st_length2dspheroid(public.geometry, public.spheroid); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_length2dspheroid(public.geometry, public.spheroid) TO geet;
GRANT ALL ON FUNCTION public.st_length2dspheroid(public.geometry, public.spheroid) TO geet_web;


--
-- Name: FUNCTION st_length_spheroid(public.geometry, public.spheroid); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_length_spheroid(public.geometry, public.spheroid) TO geet;
GRANT ALL ON FUNCTION public.st_length_spheroid(public.geometry, public.spheroid) TO geet_web;


--
-- Name: FUNCTION st_lengthspheroid(public.geometry, public.spheroid); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_lengthspheroid(public.geometry, public.spheroid) TO geet;
GRANT ALL ON FUNCTION public.st_lengthspheroid(public.geometry, public.spheroid) TO geet_web;


--
-- Name: FUNCTION st_line_interpolate_point(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_line_interpolate_point(public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_line_interpolate_point(public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION st_line_locate_point(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_line_locate_point(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_line_locate_point(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_line_substring(public.geometry, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_line_substring(public.geometry, double precision, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_line_substring(public.geometry, double precision, double precision) TO geet_web;


--
-- Name: FUNCTION st_linecrossingdirection(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linecrossingdirection(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_linecrossingdirection(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_linefromencodedpolyline(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linefromencodedpolyline(text, integer) TO geet;
GRANT ALL ON FUNCTION public.st_linefromencodedpolyline(text, integer) TO geet_web;


--
-- Name: FUNCTION st_linefrommultipoint(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linefrommultipoint(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_linefrommultipoint(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_linefromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linefromtext(text) TO geet;
GRANT ALL ON FUNCTION public.st_linefromtext(text) TO geet_web;


--
-- Name: FUNCTION st_linefromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linefromtext(text, integer) TO geet;
GRANT ALL ON FUNCTION public.st_linefromtext(text, integer) TO geet_web;


--
-- Name: FUNCTION st_linefromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linefromwkb(bytea) TO geet;
GRANT ALL ON FUNCTION public.st_linefromwkb(bytea) TO geet_web;


--
-- Name: FUNCTION st_linefromwkb(bytea, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linefromwkb(bytea, integer) TO geet;
GRANT ALL ON FUNCTION public.st_linefromwkb(bytea, integer) TO geet_web;


--
-- Name: FUNCTION st_lineinterpolatepoint(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_lineinterpolatepoint(public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_lineinterpolatepoint(public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION st_linelocatepoint(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linelocatepoint(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_linelocatepoint(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_linemerge(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linemerge(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_linemerge(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_linestringfromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linestringfromwkb(bytea) TO geet;
GRANT ALL ON FUNCTION public.st_linestringfromwkb(bytea) TO geet_web;


--
-- Name: FUNCTION st_linestringfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linestringfromwkb(bytea, integer) TO geet;
GRANT ALL ON FUNCTION public.st_linestringfromwkb(bytea, integer) TO geet_web;


--
-- Name: FUNCTION st_linesubstring(public.geometry, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linesubstring(public.geometry, double precision, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_linesubstring(public.geometry, double precision, double precision) TO geet_web;


--
-- Name: FUNCTION st_linetocurve(geometry public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_linetocurve(geometry public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_linetocurve(geometry public.geometry) TO geet_web;


--
-- Name: FUNCTION st_locate_along_measure(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_locate_along_measure(public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_locate_along_measure(public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION st_locate_between_measures(public.geometry, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_locate_between_measures(public.geometry, double precision, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_locate_between_measures(public.geometry, double precision, double precision) TO geet_web;


--
-- Name: FUNCTION st_locatealong(geometry public.geometry, measure double precision, leftrightoffset double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_locatealong(geometry public.geometry, measure double precision, leftrightoffset double precision) TO geet;
GRANT ALL ON FUNCTION public.st_locatealong(geometry public.geometry, measure double precision, leftrightoffset double precision) TO geet_web;


--
-- Name: FUNCTION st_locatebetween(geometry public.geometry, frommeasure double precision, tomeasure double precision, leftrightoffset double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_locatebetween(geometry public.geometry, frommeasure double precision, tomeasure double precision, leftrightoffset double precision) TO geet;
GRANT ALL ON FUNCTION public.st_locatebetween(geometry public.geometry, frommeasure double precision, tomeasure double precision, leftrightoffset double precision) TO geet_web;


--
-- Name: FUNCTION st_locatebetweenelevations(geometry public.geometry, fromelevation double precision, toelevation double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_locatebetweenelevations(geometry public.geometry, fromelevation double precision, toelevation double precision) TO geet;
GRANT ALL ON FUNCTION public.st_locatebetweenelevations(geometry public.geometry, fromelevation double precision, toelevation double precision) TO geet_web;


--
-- Name: FUNCTION st_longestline(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_longestline(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_longestline(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_m(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_m(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_m(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_makebox2d(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makebox2d(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_makebox2d(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_makeemptyraster(rast public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makeemptyraster(rast public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_makeemptyraster(rast public.raster) TO geet_web;


--
-- Name: FUNCTION st_makeemptyraster(width integer, height integer, upperleftx double precision, upperlefty double precision, pixelsize double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makeemptyraster(width integer, height integer, upperleftx double precision, upperlefty double precision, pixelsize double precision) TO geet;
GRANT ALL ON FUNCTION public.st_makeemptyraster(width integer, height integer, upperleftx double precision, upperlefty double precision, pixelsize double precision) TO geet_web;


--
-- Name: FUNCTION st_makeemptyraster(width integer, height integer, upperleftx double precision, upperlefty double precision, scalex double precision, scaley double precision, skewx double precision, skewy double precision, srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makeemptyraster(width integer, height integer, upperleftx double precision, upperlefty double precision, scalex double precision, scaley double precision, skewx double precision, skewy double precision, srid integer) TO geet;
GRANT ALL ON FUNCTION public.st_makeemptyraster(width integer, height integer, upperleftx double precision, upperlefty double precision, scalex double precision, scaley double precision, skewx double precision, skewy double precision, srid integer) TO geet_web;


--
-- Name: FUNCTION st_makeenvelope(double precision, double precision, double precision, double precision, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makeenvelope(double precision, double precision, double precision, double precision, integer) TO geet;
GRANT ALL ON FUNCTION public.st_makeenvelope(double precision, double precision, double precision, double precision, integer) TO geet_web;


--
-- Name: FUNCTION st_makeline(public.geometry[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makeline(public.geometry[]) TO geet;
GRANT ALL ON FUNCTION public.st_makeline(public.geometry[]) TO geet_web;


--
-- Name: FUNCTION st_makeline(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makeline(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_makeline(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_makepoint(double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makepoint(double precision, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_makepoint(double precision, double precision) TO geet_web;


--
-- Name: FUNCTION st_makepoint(double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makepoint(double precision, double precision, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_makepoint(double precision, double precision, double precision) TO geet_web;


--
-- Name: FUNCTION st_makepoint(double precision, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makepoint(double precision, double precision, double precision, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_makepoint(double precision, double precision, double precision, double precision) TO geet_web;


--
-- Name: FUNCTION st_makepointm(double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makepointm(double precision, double precision, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_makepointm(double precision, double precision, double precision) TO geet_web;


--
-- Name: FUNCTION st_makepolygon(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makepolygon(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_makepolygon(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_makepolygon(public.geometry, public.geometry[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makepolygon(public.geometry, public.geometry[]) TO geet;
GRANT ALL ON FUNCTION public.st_makepolygon(public.geometry, public.geometry[]) TO geet_web;


--
-- Name: FUNCTION st_makevalid(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makevalid(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_makevalid(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_mapalgebra(rast public.raster, pixeltype text, expression text, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rast public.raster, pixeltype text, expression text, nodataval double precision) TO geet;
GRANT ALL ON FUNCTION public.st_mapalgebra(rast public.raster, pixeltype text, expression text, nodataval double precision) TO geet_web;


--
-- Name: FUNCTION st_mapalgebra(rast public.raster, nband integer, pixeltype text, expression text, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rast public.raster, nband integer, pixeltype text, expression text, nodataval double precision) TO geet;
GRANT ALL ON FUNCTION public.st_mapalgebra(rast public.raster, nband integer, pixeltype text, expression text, nodataval double precision) TO geet_web;


--
-- Name: FUNCTION st_mapalgebra(rastbandargset public.rastbandarg[], callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rastbandargset public.rastbandarg[], callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]) TO geet;
GRANT ALL ON FUNCTION public.st_mapalgebra(rastbandargset public.rastbandarg[], callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]) TO geet_web;


--
-- Name: FUNCTION st_mapalgebra(rast1 public.raster, rast2 public.raster, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rast1 public.raster, rast2 public.raster, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO geet;
GRANT ALL ON FUNCTION public.st_mapalgebra(rast1 public.raster, rast2 public.raster, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO geet_web;


--
-- Name: FUNCTION st_mapalgebra(rast public.raster, nband integer[], callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rast public.raster, nband integer[], callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]) TO geet;
GRANT ALL ON FUNCTION public.st_mapalgebra(rast public.raster, nband integer[], callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]) TO geet_web;


--
-- Name: FUNCTION st_mapalgebra(rast public.raster, nband integer, callbackfunc regprocedure, mask double precision[], weighted boolean, pixeltype text, extenttype text, customextent public.raster, VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rast public.raster, nband integer, callbackfunc regprocedure, mask double precision[], weighted boolean, pixeltype text, extenttype text, customextent public.raster, VARIADIC userargs text[]) TO geet;
GRANT ALL ON FUNCTION public.st_mapalgebra(rast public.raster, nband integer, callbackfunc regprocedure, mask double precision[], weighted boolean, pixeltype text, extenttype text, customextent public.raster, VARIADIC userargs text[]) TO geet_web;


--
-- Name: FUNCTION st_mapalgebra(rast public.raster, nband integer, callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rast public.raster, nband integer, callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]) TO geet;
GRANT ALL ON FUNCTION public.st_mapalgebra(rast public.raster, nband integer, callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]) TO geet_web;


--
-- Name: FUNCTION st_mapalgebra(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO geet;
GRANT ALL ON FUNCTION public.st_mapalgebra(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO geet_web;


--
-- Name: FUNCTION st_mapalgebra(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]) TO geet;
GRANT ALL ON FUNCTION public.st_mapalgebra(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]) TO geet_web;


--
-- Name: FUNCTION st_mapalgebraexpr(rast public.raster, pixeltype text, expression text, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebraexpr(rast public.raster, pixeltype text, expression text, nodataval double precision) TO geet;
GRANT ALL ON FUNCTION public.st_mapalgebraexpr(rast public.raster, pixeltype text, expression text, nodataval double precision) TO geet_web;


--
-- Name: FUNCTION st_mapalgebraexpr(rast public.raster, band integer, pixeltype text, expression text, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebraexpr(rast public.raster, band integer, pixeltype text, expression text, nodataval double precision) TO geet;
GRANT ALL ON FUNCTION public.st_mapalgebraexpr(rast public.raster, band integer, pixeltype text, expression text, nodataval double precision) TO geet_web;


--
-- Name: FUNCTION st_mapalgebraexpr(rast1 public.raster, rast2 public.raster, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebraexpr(rast1 public.raster, rast2 public.raster, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO geet;
GRANT ALL ON FUNCTION public.st_mapalgebraexpr(rast1 public.raster, rast2 public.raster, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO geet_web;


--
-- Name: FUNCTION st_mapalgebraexpr(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebraexpr(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO geet;
GRANT ALL ON FUNCTION public.st_mapalgebraexpr(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO geet_web;


--
-- Name: FUNCTION st_mapalgebrafct(rast public.raster, onerastuserfunc regprocedure); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, onerastuserfunc regprocedure) TO geet;
GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, onerastuserfunc regprocedure) TO geet_web;


--
-- Name: FUNCTION st_mapalgebrafct(rast public.raster, band integer, onerastuserfunc regprocedure); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, band integer, onerastuserfunc regprocedure) TO geet;
GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, band integer, onerastuserfunc regprocedure) TO geet_web;


--
-- Name: FUNCTION st_mapalgebrafct(rast public.raster, onerastuserfunc regprocedure, VARIADIC args text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, onerastuserfunc regprocedure, VARIADIC args text[]) TO geet;
GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, onerastuserfunc regprocedure, VARIADIC args text[]) TO geet_web;


--
-- Name: FUNCTION st_mapalgebrafct(rast public.raster, pixeltype text, onerastuserfunc regprocedure); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, pixeltype text, onerastuserfunc regprocedure) TO geet;
GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, pixeltype text, onerastuserfunc regprocedure) TO geet_web;


--
-- Name: FUNCTION st_mapalgebrafct(rast public.raster, band integer, onerastuserfunc regprocedure, VARIADIC args text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, band integer, onerastuserfunc regprocedure, VARIADIC args text[]) TO geet;
GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, band integer, onerastuserfunc regprocedure, VARIADIC args text[]) TO geet_web;


--
-- Name: FUNCTION st_mapalgebrafct(rast public.raster, band integer, pixeltype text, onerastuserfunc regprocedure); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, band integer, pixeltype text, onerastuserfunc regprocedure) TO geet;
GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, band integer, pixeltype text, onerastuserfunc regprocedure) TO geet_web;


--
-- Name: FUNCTION st_mapalgebrafct(rast public.raster, pixeltype text, onerastuserfunc regprocedure, VARIADIC args text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, pixeltype text, onerastuserfunc regprocedure, VARIADIC args text[]) TO geet;
GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, pixeltype text, onerastuserfunc regprocedure, VARIADIC args text[]) TO geet_web;


--
-- Name: FUNCTION st_mapalgebrafct(rast public.raster, band integer, pixeltype text, onerastuserfunc regprocedure, VARIADIC args text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, band integer, pixeltype text, onerastuserfunc regprocedure, VARIADIC args text[]) TO geet;
GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, band integer, pixeltype text, onerastuserfunc regprocedure, VARIADIC args text[]) TO geet_web;


--
-- Name: FUNCTION st_mapalgebrafct(rast1 public.raster, rast2 public.raster, tworastuserfunc regprocedure, pixeltype text, extenttype text, VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast1 public.raster, rast2 public.raster, tworastuserfunc regprocedure, pixeltype text, extenttype text, VARIADIC userargs text[]) TO geet;
GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast1 public.raster, rast2 public.raster, tworastuserfunc regprocedure, pixeltype text, extenttype text, VARIADIC userargs text[]) TO geet_web;


--
-- Name: FUNCTION st_mapalgebrafct(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, tworastuserfunc regprocedure, pixeltype text, extenttype text, VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, tworastuserfunc regprocedure, pixeltype text, extenttype text, VARIADIC userargs text[]) TO geet;
GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, tworastuserfunc regprocedure, pixeltype text, extenttype text, VARIADIC userargs text[]) TO geet_web;


--
-- Name: FUNCTION st_mapalgebrafctngb(rast public.raster, band integer, pixeltype text, ngbwidth integer, ngbheight integer, onerastngbuserfunc regprocedure, nodatamode text, VARIADIC args text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mapalgebrafctngb(rast public.raster, band integer, pixeltype text, ngbwidth integer, ngbheight integer, onerastngbuserfunc regprocedure, nodatamode text, VARIADIC args text[]) TO geet;
GRANT ALL ON FUNCTION public.st_mapalgebrafctngb(rast public.raster, band integer, pixeltype text, ngbwidth integer, ngbheight integer, onerastngbuserfunc regprocedure, nodatamode text, VARIADIC args text[]) TO geet_web;


--
-- Name: FUNCTION st_max4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_max4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet;
GRANT ALL ON FUNCTION public.st_max4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet_web;


--
-- Name: FUNCTION st_max4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_max4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO geet;
GRANT ALL ON FUNCTION public.st_max4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO geet_web;


--
-- Name: FUNCTION st_maxdistance(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_maxdistance(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_maxdistance(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_mean4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mean4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet;
GRANT ALL ON FUNCTION public.st_mean4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet_web;


--
-- Name: FUNCTION st_mean4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mean4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO geet;
GRANT ALL ON FUNCTION public.st_mean4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO geet_web;


--
-- Name: FUNCTION st_mem_size(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mem_size(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_mem_size(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_memsize(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_memsize(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_memsize(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_memsize(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_memsize(public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_memsize(public.raster) TO geet_web;


--
-- Name: FUNCTION st_metadata(rast public.raster, OUT upperleftx double precision, OUT upperlefty double precision, OUT width integer, OUT height integer, OUT scalex double precision, OUT scaley double precision, OUT skewx double precision, OUT skewy double precision, OUT srid integer, OUT numbands integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_metadata(rast public.raster, OUT upperleftx double precision, OUT upperlefty double precision, OUT width integer, OUT height integer, OUT scalex double precision, OUT scaley double precision, OUT skewx double precision, OUT skewy double precision, OUT srid integer, OUT numbands integer) TO geet;
GRANT ALL ON FUNCTION public.st_metadata(rast public.raster, OUT upperleftx double precision, OUT upperlefty double precision, OUT width integer, OUT height integer, OUT scalex double precision, OUT scaley double precision, OUT skewx double precision, OUT skewy double precision, OUT srid integer, OUT numbands integer) TO geet_web;


--
-- Name: FUNCTION st_min4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_min4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet;
GRANT ALL ON FUNCTION public.st_min4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet_web;


--
-- Name: FUNCTION st_min4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_min4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO geet;
GRANT ALL ON FUNCTION public.st_min4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO geet_web;


--
-- Name: FUNCTION st_minconvexhull(rast public.raster, nband integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_minconvexhull(rast public.raster, nband integer) TO geet;
GRANT ALL ON FUNCTION public.st_minconvexhull(rast public.raster, nband integer) TO geet_web;


--
-- Name: FUNCTION st_mindist4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mindist4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet;
GRANT ALL ON FUNCTION public.st_mindist4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet_web;


--
-- Name: FUNCTION st_minimumboundingcircle(inputgeom public.geometry, segs_per_quarter integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_minimumboundingcircle(inputgeom public.geometry, segs_per_quarter integer) TO geet;
GRANT ALL ON FUNCTION public.st_minimumboundingcircle(inputgeom public.geometry, segs_per_quarter integer) TO geet_web;


--
-- Name: FUNCTION st_minimumboundingradius(public.geometry, OUT center public.geometry, OUT radius double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_minimumboundingradius(public.geometry, OUT center public.geometry, OUT radius double precision) TO geet;
GRANT ALL ON FUNCTION public.st_minimumboundingradius(public.geometry, OUT center public.geometry, OUT radius double precision) TO geet_web;


--
-- Name: FUNCTION st_minimumclearance(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_minimumclearance(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_minimumclearance(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_minimumclearanceline(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_minimumclearanceline(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_minimumclearanceline(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_minpossiblevalue(pixeltype text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_minpossiblevalue(pixeltype text) TO geet;
GRANT ALL ON FUNCTION public.st_minpossiblevalue(pixeltype text) TO geet_web;


--
-- Name: FUNCTION st_mlinefromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mlinefromtext(text) TO geet;
GRANT ALL ON FUNCTION public.st_mlinefromtext(text) TO geet_web;


--
-- Name: FUNCTION st_mlinefromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mlinefromtext(text, integer) TO geet;
GRANT ALL ON FUNCTION public.st_mlinefromtext(text, integer) TO geet_web;


--
-- Name: FUNCTION st_mlinefromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mlinefromwkb(bytea) TO geet;
GRANT ALL ON FUNCTION public.st_mlinefromwkb(bytea) TO geet_web;


--
-- Name: FUNCTION st_mlinefromwkb(bytea, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mlinefromwkb(bytea, integer) TO geet;
GRANT ALL ON FUNCTION public.st_mlinefromwkb(bytea, integer) TO geet_web;


--
-- Name: FUNCTION st_mpointfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mpointfromtext(text) TO geet;
GRANT ALL ON FUNCTION public.st_mpointfromtext(text) TO geet_web;


--
-- Name: FUNCTION st_mpointfromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mpointfromtext(text, integer) TO geet;
GRANT ALL ON FUNCTION public.st_mpointfromtext(text, integer) TO geet_web;


--
-- Name: FUNCTION st_mpointfromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mpointfromwkb(bytea) TO geet;
GRANT ALL ON FUNCTION public.st_mpointfromwkb(bytea) TO geet_web;


--
-- Name: FUNCTION st_mpointfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mpointfromwkb(bytea, integer) TO geet;
GRANT ALL ON FUNCTION public.st_mpointfromwkb(bytea, integer) TO geet_web;


--
-- Name: FUNCTION st_mpolyfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mpolyfromtext(text) TO geet;
GRANT ALL ON FUNCTION public.st_mpolyfromtext(text) TO geet_web;


--
-- Name: FUNCTION st_mpolyfromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mpolyfromtext(text, integer) TO geet;
GRANT ALL ON FUNCTION public.st_mpolyfromtext(text, integer) TO geet_web;


--
-- Name: FUNCTION st_mpolyfromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mpolyfromwkb(bytea) TO geet;
GRANT ALL ON FUNCTION public.st_mpolyfromwkb(bytea) TO geet_web;


--
-- Name: FUNCTION st_mpolyfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_mpolyfromwkb(bytea, integer) TO geet;
GRANT ALL ON FUNCTION public.st_mpolyfromwkb(bytea, integer) TO geet_web;


--
-- Name: FUNCTION st_multi(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_multi(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_multi(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_multilinefromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_multilinefromwkb(bytea) TO geet;
GRANT ALL ON FUNCTION public.st_multilinefromwkb(bytea) TO geet_web;


--
-- Name: FUNCTION st_multilinestringfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_multilinestringfromtext(text) TO geet;
GRANT ALL ON FUNCTION public.st_multilinestringfromtext(text) TO geet_web;


--
-- Name: FUNCTION st_multilinestringfromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_multilinestringfromtext(text, integer) TO geet;
GRANT ALL ON FUNCTION public.st_multilinestringfromtext(text, integer) TO geet_web;


--
-- Name: FUNCTION st_multipointfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_multipointfromtext(text) TO geet;
GRANT ALL ON FUNCTION public.st_multipointfromtext(text) TO geet_web;


--
-- Name: FUNCTION st_multipointfromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_multipointfromwkb(bytea) TO geet;
GRANT ALL ON FUNCTION public.st_multipointfromwkb(bytea) TO geet_web;


--
-- Name: FUNCTION st_multipointfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_multipointfromwkb(bytea, integer) TO geet;
GRANT ALL ON FUNCTION public.st_multipointfromwkb(bytea, integer) TO geet_web;


--
-- Name: FUNCTION st_multipolyfromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_multipolyfromwkb(bytea) TO geet;
GRANT ALL ON FUNCTION public.st_multipolyfromwkb(bytea) TO geet_web;


--
-- Name: FUNCTION st_multipolyfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_multipolyfromwkb(bytea, integer) TO geet;
GRANT ALL ON FUNCTION public.st_multipolyfromwkb(bytea, integer) TO geet_web;


--
-- Name: FUNCTION st_multipolygonfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_multipolygonfromtext(text) TO geet;
GRANT ALL ON FUNCTION public.st_multipolygonfromtext(text) TO geet_web;


--
-- Name: FUNCTION st_multipolygonfromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_multipolygonfromtext(text, integer) TO geet;
GRANT ALL ON FUNCTION public.st_multipolygonfromtext(text, integer) TO geet_web;


--
-- Name: FUNCTION st_ndims(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_ndims(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_ndims(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_nearestvalue(rast public.raster, pt public.geometry, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_nearestvalue(rast public.raster, pt public.geometry, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public.st_nearestvalue(rast public.raster, pt public.geometry, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION st_nearestvalue(rast public.raster, columnx integer, rowy integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_nearestvalue(rast public.raster, columnx integer, rowy integer, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public.st_nearestvalue(rast public.raster, columnx integer, rowy integer, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION st_nearestvalue(rast public.raster, band integer, pt public.geometry, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_nearestvalue(rast public.raster, band integer, pt public.geometry, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public.st_nearestvalue(rast public.raster, band integer, pt public.geometry, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION st_nearestvalue(rast public.raster, band integer, columnx integer, rowy integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_nearestvalue(rast public.raster, band integer, columnx integer, rowy integer, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public.st_nearestvalue(rast public.raster, band integer, columnx integer, rowy integer, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION st_neighborhood(rast public.raster, pt public.geometry, distancex integer, distancey integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_neighborhood(rast public.raster, pt public.geometry, distancex integer, distancey integer, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public.st_neighborhood(rast public.raster, pt public.geometry, distancex integer, distancey integer, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION st_neighborhood(rast public.raster, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_neighborhood(rast public.raster, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public.st_neighborhood(rast public.raster, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION st_neighborhood(rast public.raster, band integer, pt public.geometry, distancex integer, distancey integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_neighborhood(rast public.raster, band integer, pt public.geometry, distancex integer, distancey integer, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public.st_neighborhood(rast public.raster, band integer, pt public.geometry, distancex integer, distancey integer, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION st_neighborhood(rast public.raster, band integer, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_neighborhood(rast public.raster, band integer, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public.st_neighborhood(rast public.raster, band integer, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION st_node(g public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_node(g public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_node(g public.geometry) TO geet_web;


--
-- Name: FUNCTION st_normalize(geom public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_normalize(geom public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_normalize(geom public.geometry) TO geet_web;


--
-- Name: FUNCTION st_notsamealignmentreason(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_notsamealignmentreason(rast1 public.raster, rast2 public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_notsamealignmentreason(rast1 public.raster, rast2 public.raster) TO geet_web;


--
-- Name: FUNCTION st_npoints(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_npoints(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_npoints(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_nrings(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_nrings(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_nrings(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_numbands(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_numbands(public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_numbands(public.raster) TO geet_web;


--
-- Name: FUNCTION st_numgeometries(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_numgeometries(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_numgeometries(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_numinteriorring(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_numinteriorring(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_numinteriorring(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_numinteriorrings(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_numinteriorrings(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_numinteriorrings(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_numpatches(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_numpatches(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_numpatches(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_numpoints(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_numpoints(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_numpoints(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_offsetcurve(line public.geometry, distance double precision, params text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_offsetcurve(line public.geometry, distance double precision, params text) TO geet;
GRANT ALL ON FUNCTION public.st_offsetcurve(line public.geometry, distance double precision, params text) TO geet_web;


--
-- Name: FUNCTION st_orderingequals(geometrya public.geometry, geometryb public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_orderingequals(geometrya public.geometry, geometryb public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_orderingequals(geometrya public.geometry, geometryb public.geometry) TO geet_web;


--
-- Name: FUNCTION st_overlaps(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_overlaps(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_overlaps(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_overlaps(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_overlaps(rast1 public.raster, rast2 public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_overlaps(rast1 public.raster, rast2 public.raster) TO geet_web;


--
-- Name: FUNCTION st_overlaps(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_overlaps(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet;
GRANT ALL ON FUNCTION public.st_overlaps(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet_web;


--
-- Name: FUNCTION st_patchn(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_patchn(public.geometry, integer) TO geet;
GRANT ALL ON FUNCTION public.st_patchn(public.geometry, integer) TO geet_web;


--
-- Name: FUNCTION st_perimeter(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_perimeter(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_perimeter(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_perimeter(geog public.geography, use_spheroid boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_perimeter(geog public.geography, use_spheroid boolean) TO geet;
GRANT ALL ON FUNCTION public.st_perimeter(geog public.geography, use_spheroid boolean) TO geet_web;


--
-- Name: FUNCTION st_perimeter2d(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_perimeter2d(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_perimeter2d(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_pixelascentroid(rast public.raster, x integer, y integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pixelascentroid(rast public.raster, x integer, y integer) TO geet;
GRANT ALL ON FUNCTION public.st_pixelascentroid(rast public.raster, x integer, y integer) TO geet_web;


--
-- Name: FUNCTION st_pixelascentroids(rast public.raster, band integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pixelascentroids(rast public.raster, band integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer) TO geet;
GRANT ALL ON FUNCTION public.st_pixelascentroids(rast public.raster, band integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer) TO geet_web;


--
-- Name: FUNCTION st_pixelaspoint(rast public.raster, x integer, y integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pixelaspoint(rast public.raster, x integer, y integer) TO geet;
GRANT ALL ON FUNCTION public.st_pixelaspoint(rast public.raster, x integer, y integer) TO geet_web;


--
-- Name: FUNCTION st_pixelaspoints(rast public.raster, band integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pixelaspoints(rast public.raster, band integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer) TO geet;
GRANT ALL ON FUNCTION public.st_pixelaspoints(rast public.raster, band integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer) TO geet_web;


--
-- Name: FUNCTION st_pixelaspolygon(rast public.raster, x integer, y integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pixelaspolygon(rast public.raster, x integer, y integer) TO geet;
GRANT ALL ON FUNCTION public.st_pixelaspolygon(rast public.raster, x integer, y integer) TO geet_web;


--
-- Name: FUNCTION st_pixelaspolygons(rast public.raster, band integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pixelaspolygons(rast public.raster, band integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer) TO geet;
GRANT ALL ON FUNCTION public.st_pixelaspolygons(rast public.raster, band integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer) TO geet_web;


--
-- Name: FUNCTION st_pixelheight(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pixelheight(public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_pixelheight(public.raster) TO geet_web;


--
-- Name: FUNCTION st_pixelofvalue(rast public.raster, search double precision[], exclude_nodata_value boolean, OUT val double precision, OUT x integer, OUT y integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pixelofvalue(rast public.raster, search double precision[], exclude_nodata_value boolean, OUT val double precision, OUT x integer, OUT y integer) TO geet;
GRANT ALL ON FUNCTION public.st_pixelofvalue(rast public.raster, search double precision[], exclude_nodata_value boolean, OUT val double precision, OUT x integer, OUT y integer) TO geet_web;


--
-- Name: FUNCTION st_pixelofvalue(rast public.raster, search double precision, exclude_nodata_value boolean, OUT x integer, OUT y integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pixelofvalue(rast public.raster, search double precision, exclude_nodata_value boolean, OUT x integer, OUT y integer) TO geet;
GRANT ALL ON FUNCTION public.st_pixelofvalue(rast public.raster, search double precision, exclude_nodata_value boolean, OUT x integer, OUT y integer) TO geet_web;


--
-- Name: FUNCTION st_pixelofvalue(rast public.raster, nband integer, search double precision[], exclude_nodata_value boolean, OUT val double precision, OUT x integer, OUT y integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pixelofvalue(rast public.raster, nband integer, search double precision[], exclude_nodata_value boolean, OUT val double precision, OUT x integer, OUT y integer) TO geet;
GRANT ALL ON FUNCTION public.st_pixelofvalue(rast public.raster, nband integer, search double precision[], exclude_nodata_value boolean, OUT val double precision, OUT x integer, OUT y integer) TO geet_web;


--
-- Name: FUNCTION st_pixelofvalue(rast public.raster, nband integer, search double precision, exclude_nodata_value boolean, OUT x integer, OUT y integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pixelofvalue(rast public.raster, nband integer, search double precision, exclude_nodata_value boolean, OUT x integer, OUT y integer) TO geet;
GRANT ALL ON FUNCTION public.st_pixelofvalue(rast public.raster, nband integer, search double precision, exclude_nodata_value boolean, OUT x integer, OUT y integer) TO geet_web;


--
-- Name: FUNCTION st_pixelwidth(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pixelwidth(public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_pixelwidth(public.raster) TO geet_web;


--
-- Name: FUNCTION st_point(double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_point(double precision, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_point(double precision, double precision) TO geet_web;


--
-- Name: FUNCTION st_point_inside_circle(public.geometry, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_point_inside_circle(public.geometry, double precision, double precision, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_point_inside_circle(public.geometry, double precision, double precision, double precision) TO geet_web;


--
-- Name: FUNCTION st_pointfromgeohash(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pointfromgeohash(text, integer) TO geet;
GRANT ALL ON FUNCTION public.st_pointfromgeohash(text, integer) TO geet_web;


--
-- Name: FUNCTION st_pointfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pointfromtext(text) TO geet;
GRANT ALL ON FUNCTION public.st_pointfromtext(text) TO geet_web;


--
-- Name: FUNCTION st_pointfromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pointfromtext(text, integer) TO geet;
GRANT ALL ON FUNCTION public.st_pointfromtext(text, integer) TO geet_web;


--
-- Name: FUNCTION st_pointfromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pointfromwkb(bytea) TO geet;
GRANT ALL ON FUNCTION public.st_pointfromwkb(bytea) TO geet_web;


--
-- Name: FUNCTION st_pointfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pointfromwkb(bytea, integer) TO geet;
GRANT ALL ON FUNCTION public.st_pointfromwkb(bytea, integer) TO geet_web;


--
-- Name: FUNCTION st_pointinsidecircle(public.geometry, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pointinsidecircle(public.geometry, double precision, double precision, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_pointinsidecircle(public.geometry, double precision, double precision, double precision) TO geet_web;


--
-- Name: FUNCTION st_pointn(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pointn(public.geometry, integer) TO geet;
GRANT ALL ON FUNCTION public.st_pointn(public.geometry, integer) TO geet_web;


--
-- Name: FUNCTION st_pointonsurface(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_pointonsurface(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_pointonsurface(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_points(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_points(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_points(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_polyfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_polyfromtext(text) TO geet;
GRANT ALL ON FUNCTION public.st_polyfromtext(text) TO geet_web;


--
-- Name: FUNCTION st_polyfromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_polyfromtext(text, integer) TO geet;
GRANT ALL ON FUNCTION public.st_polyfromtext(text, integer) TO geet_web;


--
-- Name: FUNCTION st_polyfromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_polyfromwkb(bytea) TO geet;
GRANT ALL ON FUNCTION public.st_polyfromwkb(bytea) TO geet_web;


--
-- Name: FUNCTION st_polyfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_polyfromwkb(bytea, integer) TO geet;
GRANT ALL ON FUNCTION public.st_polyfromwkb(bytea, integer) TO geet_web;


--
-- Name: FUNCTION st_polygon(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_polygon(public.geometry, integer) TO geet;
GRANT ALL ON FUNCTION public.st_polygon(public.geometry, integer) TO geet_web;


--
-- Name: FUNCTION st_polygon(rast public.raster, band integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_polygon(rast public.raster, band integer) TO geet;
GRANT ALL ON FUNCTION public.st_polygon(rast public.raster, band integer) TO geet_web;


--
-- Name: FUNCTION st_polygonfromtext(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_polygonfromtext(text) TO geet;
GRANT ALL ON FUNCTION public.st_polygonfromtext(text) TO geet_web;


--
-- Name: FUNCTION st_polygonfromtext(text, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_polygonfromtext(text, integer) TO geet;
GRANT ALL ON FUNCTION public.st_polygonfromtext(text, integer) TO geet_web;


--
-- Name: FUNCTION st_polygonfromwkb(bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_polygonfromwkb(bytea) TO geet;
GRANT ALL ON FUNCTION public.st_polygonfromwkb(bytea) TO geet_web;


--
-- Name: FUNCTION st_polygonfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_polygonfromwkb(bytea, integer) TO geet;
GRANT ALL ON FUNCTION public.st_polygonfromwkb(bytea, integer) TO geet_web;


--
-- Name: FUNCTION st_polygonize(public.geometry[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_polygonize(public.geometry[]) TO geet;
GRANT ALL ON FUNCTION public.st_polygonize(public.geometry[]) TO geet_web;


--
-- Name: FUNCTION st_project(geog public.geography, distance double precision, azimuth double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_project(geog public.geography, distance double precision, azimuth double precision) TO geet;
GRANT ALL ON FUNCTION public.st_project(geog public.geography, distance double precision, azimuth double precision) TO geet_web;


--
-- Name: FUNCTION st_quantile(rast public.raster, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet;
GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet_web;


--
-- Name: FUNCTION st_quantile(rast public.raster, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, quantile double precision) TO geet;
GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, quantile double precision) TO geet_web;


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet;
GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet_web;


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, quantile double precision) TO geet;
GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, quantile double precision) TO geet_web;


--
-- Name: FUNCTION st_quantile(rast public.raster, exclude_nodata_value boolean, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, exclude_nodata_value boolean, quantile double precision) TO geet;
GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, exclude_nodata_value boolean, quantile double precision) TO geet_web;


--
-- Name: FUNCTION st_quantile(rast public.raster, nband integer, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, nband integer, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet;
GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, nband integer, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet_web;


--
-- Name: FUNCTION st_quantile(rast public.raster, nband integer, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, nband integer, quantile double precision) TO geet;
GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, nband integer, quantile double precision) TO geet_web;


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, exclude_nodata_value boolean, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, exclude_nodata_value boolean, quantile double precision) TO geet;
GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, exclude_nodata_value boolean, quantile double precision) TO geet_web;


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, nband integer, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, nband integer, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet;
GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, nband integer, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet_web;


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, nband integer, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, nband integer, quantile double precision) TO geet;
GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, nband integer, quantile double precision) TO geet_web;


--
-- Name: FUNCTION st_quantile(rast public.raster, nband integer, exclude_nodata_value boolean, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, nband integer, exclude_nodata_value boolean, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet;
GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, nband integer, exclude_nodata_value boolean, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet_web;


--
-- Name: FUNCTION st_quantile(rast public.raster, nband integer, exclude_nodata_value boolean, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, nband integer, exclude_nodata_value boolean, quantile double precision) TO geet;
GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, nband integer, exclude_nodata_value boolean, quantile double precision) TO geet_web;


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet;
GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO geet_web;


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, quantile double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, quantile double precision) TO geet;
GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, quantile double precision) TO geet_web;


--
-- Name: FUNCTION st_range4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_range4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet;
GRANT ALL ON FUNCTION public.st_range4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet_web;


--
-- Name: FUNCTION st_range4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_range4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO geet;
GRANT ALL ON FUNCTION public.st_range4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO geet_web;


--
-- Name: FUNCTION st_rastertoworldcoord(rast public.raster, columnx integer, rowy integer, OUT longitude double precision, OUT latitude double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rastertoworldcoord(rast public.raster, columnx integer, rowy integer, OUT longitude double precision, OUT latitude double precision) TO geet;
GRANT ALL ON FUNCTION public.st_rastertoworldcoord(rast public.raster, columnx integer, rowy integer, OUT longitude double precision, OUT latitude double precision) TO geet_web;


--
-- Name: FUNCTION st_rastertoworldcoordx(rast public.raster, xr integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rastertoworldcoordx(rast public.raster, xr integer) TO geet;
GRANT ALL ON FUNCTION public.st_rastertoworldcoordx(rast public.raster, xr integer) TO geet_web;


--
-- Name: FUNCTION st_rastertoworldcoordx(rast public.raster, xr integer, yr integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rastertoworldcoordx(rast public.raster, xr integer, yr integer) TO geet;
GRANT ALL ON FUNCTION public.st_rastertoworldcoordx(rast public.raster, xr integer, yr integer) TO geet_web;


--
-- Name: FUNCTION st_rastertoworldcoordy(rast public.raster, yr integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rastertoworldcoordy(rast public.raster, yr integer) TO geet;
GRANT ALL ON FUNCTION public.st_rastertoworldcoordy(rast public.raster, yr integer) TO geet_web;


--
-- Name: FUNCTION st_rastertoworldcoordy(rast public.raster, xr integer, yr integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rastertoworldcoordy(rast public.raster, xr integer, yr integer) TO geet;
GRANT ALL ON FUNCTION public.st_rastertoworldcoordy(rast public.raster, xr integer, yr integer) TO geet_web;


--
-- Name: FUNCTION st_reclass(rast public.raster, VARIADIC reclassargset public.reclassarg[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_reclass(rast public.raster, VARIADIC reclassargset public.reclassarg[]) TO geet;
GRANT ALL ON FUNCTION public.st_reclass(rast public.raster, VARIADIC reclassargset public.reclassarg[]) TO geet_web;


--
-- Name: FUNCTION st_reclass(rast public.raster, reclassexpr text, pixeltype text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_reclass(rast public.raster, reclassexpr text, pixeltype text) TO geet;
GRANT ALL ON FUNCTION public.st_reclass(rast public.raster, reclassexpr text, pixeltype text) TO geet_web;


--
-- Name: FUNCTION st_reclass(rast public.raster, nband integer, reclassexpr text, pixeltype text, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_reclass(rast public.raster, nband integer, reclassexpr text, pixeltype text, nodataval double precision) TO geet;
GRANT ALL ON FUNCTION public.st_reclass(rast public.raster, nband integer, reclassexpr text, pixeltype text, nodataval double precision) TO geet_web;


--
-- Name: FUNCTION st_relate(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_relate(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_relate(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_relate(geom1 public.geometry, geom2 public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_relate(geom1 public.geometry, geom2 public.geometry, integer) TO geet;
GRANT ALL ON FUNCTION public.st_relate(geom1 public.geometry, geom2 public.geometry, integer) TO geet_web;


--
-- Name: FUNCTION st_relate(geom1 public.geometry, geom2 public.geometry, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_relate(geom1 public.geometry, geom2 public.geometry, text) TO geet;
GRANT ALL ON FUNCTION public.st_relate(geom1 public.geometry, geom2 public.geometry, text) TO geet_web;


--
-- Name: FUNCTION st_relatematch(text, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_relatematch(text, text) TO geet;
GRANT ALL ON FUNCTION public.st_relatematch(text, text) TO geet_web;


--
-- Name: FUNCTION st_removepoint(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_removepoint(public.geometry, integer) TO geet;
GRANT ALL ON FUNCTION public.st_removepoint(public.geometry, integer) TO geet_web;


--
-- Name: FUNCTION st_removerepeatedpoints(geom public.geometry, tolerance double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_removerepeatedpoints(geom public.geometry, tolerance double precision) TO geet;
GRANT ALL ON FUNCTION public.st_removerepeatedpoints(geom public.geometry, tolerance double precision) TO geet_web;


--
-- Name: FUNCTION st_resample(rast public.raster, ref public.raster, usescale boolean, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_resample(rast public.raster, ref public.raster, usescale boolean, algorithm text, maxerr double precision) TO geet;
GRANT ALL ON FUNCTION public.st_resample(rast public.raster, ref public.raster, usescale boolean, algorithm text, maxerr double precision) TO geet_web;


--
-- Name: FUNCTION st_resample(rast public.raster, ref public.raster, algorithm text, maxerr double precision, usescale boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_resample(rast public.raster, ref public.raster, algorithm text, maxerr double precision, usescale boolean) TO geet;
GRANT ALL ON FUNCTION public.st_resample(rast public.raster, ref public.raster, algorithm text, maxerr double precision, usescale boolean) TO geet_web;


--
-- Name: FUNCTION st_resample(rast public.raster, scalex double precision, scaley double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_resample(rast public.raster, scalex double precision, scaley double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, algorithm text, maxerr double precision) TO geet;
GRANT ALL ON FUNCTION public.st_resample(rast public.raster, scalex double precision, scaley double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, algorithm text, maxerr double precision) TO geet_web;


--
-- Name: FUNCTION st_resample(rast public.raster, width integer, height integer, gridx double precision, gridy double precision, skewx double precision, skewy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_resample(rast public.raster, width integer, height integer, gridx double precision, gridy double precision, skewx double precision, skewy double precision, algorithm text, maxerr double precision) TO geet;
GRANT ALL ON FUNCTION public.st_resample(rast public.raster, width integer, height integer, gridx double precision, gridy double precision, skewx double precision, skewy double precision, algorithm text, maxerr double precision) TO geet_web;


--
-- Name: FUNCTION st_rescale(rast public.raster, scalexy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rescale(rast public.raster, scalexy double precision, algorithm text, maxerr double precision) TO geet;
GRANT ALL ON FUNCTION public.st_rescale(rast public.raster, scalexy double precision, algorithm text, maxerr double precision) TO geet_web;


--
-- Name: FUNCTION st_rescale(rast public.raster, scalex double precision, scaley double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rescale(rast public.raster, scalex double precision, scaley double precision, algorithm text, maxerr double precision) TO geet;
GRANT ALL ON FUNCTION public.st_rescale(rast public.raster, scalex double precision, scaley double precision, algorithm text, maxerr double precision) TO geet_web;


--
-- Name: FUNCTION st_resize(rast public.raster, percentwidth double precision, percentheight double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_resize(rast public.raster, percentwidth double precision, percentheight double precision, algorithm text, maxerr double precision) TO geet;
GRANT ALL ON FUNCTION public.st_resize(rast public.raster, percentwidth double precision, percentheight double precision, algorithm text, maxerr double precision) TO geet_web;


--
-- Name: FUNCTION st_resize(rast public.raster, width integer, height integer, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_resize(rast public.raster, width integer, height integer, algorithm text, maxerr double precision) TO geet;
GRANT ALL ON FUNCTION public.st_resize(rast public.raster, width integer, height integer, algorithm text, maxerr double precision) TO geet_web;


--
-- Name: FUNCTION st_resize(rast public.raster, width text, height text, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_resize(rast public.raster, width text, height text, algorithm text, maxerr double precision) TO geet;
GRANT ALL ON FUNCTION public.st_resize(rast public.raster, width text, height text, algorithm text, maxerr double precision) TO geet_web;


--
-- Name: FUNCTION st_reskew(rast public.raster, skewxy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_reskew(rast public.raster, skewxy double precision, algorithm text, maxerr double precision) TO geet;
GRANT ALL ON FUNCTION public.st_reskew(rast public.raster, skewxy double precision, algorithm text, maxerr double precision) TO geet_web;


--
-- Name: FUNCTION st_reskew(rast public.raster, skewx double precision, skewy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_reskew(rast public.raster, skewx double precision, skewy double precision, algorithm text, maxerr double precision) TO geet;
GRANT ALL ON FUNCTION public.st_reskew(rast public.raster, skewx double precision, skewy double precision, algorithm text, maxerr double precision) TO geet_web;


--
-- Name: FUNCTION st_retile(tab regclass, col name, ext public.geometry, sfx double precision, sfy double precision, tw integer, th integer, algo text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_retile(tab regclass, col name, ext public.geometry, sfx double precision, sfy double precision, tw integer, th integer, algo text) TO geet;
GRANT ALL ON FUNCTION public.st_retile(tab regclass, col name, ext public.geometry, sfx double precision, sfy double precision, tw integer, th integer, algo text) TO geet_web;


--
-- Name: FUNCTION st_reverse(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_reverse(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_reverse(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_rotate(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rotate(public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_rotate(public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION st_rotate(public.geometry, double precision, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rotate(public.geometry, double precision, public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_rotate(public.geometry, double precision, public.geometry) TO geet_web;


--
-- Name: FUNCTION st_rotate(public.geometry, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rotate(public.geometry, double precision, double precision, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_rotate(public.geometry, double precision, double precision, double precision) TO geet_web;


--
-- Name: FUNCTION st_rotatex(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rotatex(public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_rotatex(public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION st_rotatey(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rotatey(public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_rotatey(public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION st_rotatez(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rotatez(public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_rotatez(public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION st_rotation(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_rotation(public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_rotation(public.raster) TO geet_web;


--
-- Name: FUNCTION st_roughness(rast public.raster, nband integer, pixeltype text, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_roughness(rast public.raster, nband integer, pixeltype text, interpolate_nodata boolean) TO geet;
GRANT ALL ON FUNCTION public.st_roughness(rast public.raster, nband integer, pixeltype text, interpolate_nodata boolean) TO geet_web;


--
-- Name: FUNCTION st_roughness(rast public.raster, nband integer, customextent public.raster, pixeltype text, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_roughness(rast public.raster, nband integer, customextent public.raster, pixeltype text, interpolate_nodata boolean) TO geet;
GRANT ALL ON FUNCTION public.st_roughness(rast public.raster, nband integer, customextent public.raster, pixeltype text, interpolate_nodata boolean) TO geet_web;


--
-- Name: FUNCTION st_samealignment(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_samealignment(rast1 public.raster, rast2 public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_samealignment(rast1 public.raster, rast2 public.raster) TO geet_web;


--
-- Name: FUNCTION st_samealignment(ulx1 double precision, uly1 double precision, scalex1 double precision, scaley1 double precision, skewx1 double precision, skewy1 double precision, ulx2 double precision, uly2 double precision, scalex2 double precision, scaley2 double precision, skewx2 double precision, skewy2 double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_samealignment(ulx1 double precision, uly1 double precision, scalex1 double precision, scaley1 double precision, skewx1 double precision, skewy1 double precision, ulx2 double precision, uly2 double precision, scalex2 double precision, scaley2 double precision, skewx2 double precision, skewy2 double precision) TO geet;
GRANT ALL ON FUNCTION public.st_samealignment(ulx1 double precision, uly1 double precision, scalex1 double precision, scaley1 double precision, skewx1 double precision, skewy1 double precision, ulx2 double precision, uly2 double precision, scalex2 double precision, scaley2 double precision, skewx2 double precision, skewy2 double precision) TO geet_web;


--
-- Name: FUNCTION st_scale(public.geometry, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_scale(public.geometry, public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_scale(public.geometry, public.geometry) TO geet_web;


--
-- Name: FUNCTION st_scale(public.geometry, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_scale(public.geometry, double precision, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_scale(public.geometry, double precision, double precision) TO geet_web;


--
-- Name: FUNCTION st_scale(public.geometry, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_scale(public.geometry, double precision, double precision, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_scale(public.geometry, double precision, double precision, double precision) TO geet_web;


--
-- Name: FUNCTION st_scalex(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_scalex(public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_scalex(public.raster) TO geet_web;


--
-- Name: FUNCTION st_scaley(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_scaley(public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_scaley(public.raster) TO geet_web;


--
-- Name: FUNCTION st_segmentize(geog public.geography, max_segment_length double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_segmentize(geog public.geography, max_segment_length double precision) TO geet;
GRANT ALL ON FUNCTION public.st_segmentize(geog public.geography, max_segment_length double precision) TO geet_web;


--
-- Name: FUNCTION st_segmentize(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_segmentize(public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_segmentize(public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION st_setbandisnodata(rast public.raster, band integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setbandisnodata(rast public.raster, band integer) TO geet;
GRANT ALL ON FUNCTION public.st_setbandisnodata(rast public.raster, band integer) TO geet_web;


--
-- Name: FUNCTION st_setbandnodatavalue(rast public.raster, nodatavalue double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setbandnodatavalue(rast public.raster, nodatavalue double precision) TO geet;
GRANT ALL ON FUNCTION public.st_setbandnodatavalue(rast public.raster, nodatavalue double precision) TO geet_web;


--
-- Name: FUNCTION st_setbandnodatavalue(rast public.raster, band integer, nodatavalue double precision, forcechecking boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setbandnodatavalue(rast public.raster, band integer, nodatavalue double precision, forcechecking boolean) TO geet;
GRANT ALL ON FUNCTION public.st_setbandnodatavalue(rast public.raster, band integer, nodatavalue double precision, forcechecking boolean) TO geet_web;


--
-- Name: FUNCTION st_seteffectivearea(public.geometry, double precision, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_seteffectivearea(public.geometry, double precision, integer) TO geet;
GRANT ALL ON FUNCTION public.st_seteffectivearea(public.geometry, double precision, integer) TO geet_web;


--
-- Name: FUNCTION st_setgeoreference(rast public.raster, georef text, format text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setgeoreference(rast public.raster, georef text, format text) TO geet;
GRANT ALL ON FUNCTION public.st_setgeoreference(rast public.raster, georef text, format text) TO geet_web;


--
-- Name: FUNCTION st_setgeoreference(rast public.raster, upperleftx double precision, upperlefty double precision, scalex double precision, scaley double precision, skewx double precision, skewy double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setgeoreference(rast public.raster, upperleftx double precision, upperlefty double precision, scalex double precision, scaley double precision, skewx double precision, skewy double precision) TO geet;
GRANT ALL ON FUNCTION public.st_setgeoreference(rast public.raster, upperleftx double precision, upperlefty double precision, scalex double precision, scaley double precision, skewx double precision, skewy double precision) TO geet_web;


--
-- Name: FUNCTION st_setgeotransform(rast public.raster, imag double precision, jmag double precision, theta_i double precision, theta_ij double precision, xoffset double precision, yoffset double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setgeotransform(rast public.raster, imag double precision, jmag double precision, theta_i double precision, theta_ij double precision, xoffset double precision, yoffset double precision) TO geet;
GRANT ALL ON FUNCTION public.st_setgeotransform(rast public.raster, imag double precision, jmag double precision, theta_i double precision, theta_ij double precision, xoffset double precision, yoffset double precision) TO geet_web;


--
-- Name: FUNCTION st_setpoint(public.geometry, integer, public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setpoint(public.geometry, integer, public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_setpoint(public.geometry, integer, public.geometry) TO geet_web;


--
-- Name: FUNCTION st_setrotation(rast public.raster, rotation double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setrotation(rast public.raster, rotation double precision) TO geet;
GRANT ALL ON FUNCTION public.st_setrotation(rast public.raster, rotation double precision) TO geet_web;


--
-- Name: FUNCTION st_setscale(rast public.raster, scale double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setscale(rast public.raster, scale double precision) TO geet;
GRANT ALL ON FUNCTION public.st_setscale(rast public.raster, scale double precision) TO geet_web;


--
-- Name: FUNCTION st_setscale(rast public.raster, scalex double precision, scaley double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setscale(rast public.raster, scalex double precision, scaley double precision) TO geet;
GRANT ALL ON FUNCTION public.st_setscale(rast public.raster, scalex double precision, scaley double precision) TO geet_web;


--
-- Name: FUNCTION st_setskew(rast public.raster, skew double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setskew(rast public.raster, skew double precision) TO geet;
GRANT ALL ON FUNCTION public.st_setskew(rast public.raster, skew double precision) TO geet_web;


--
-- Name: FUNCTION st_setskew(rast public.raster, skewx double precision, skewy double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setskew(rast public.raster, skewx double precision, skewy double precision) TO geet;
GRANT ALL ON FUNCTION public.st_setskew(rast public.raster, skewx double precision, skewy double precision) TO geet_web;


--
-- Name: FUNCTION st_setsrid(geog public.geography, srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setsrid(geog public.geography, srid integer) TO geet;
GRANT ALL ON FUNCTION public.st_setsrid(geog public.geography, srid integer) TO geet_web;


--
-- Name: FUNCTION st_setsrid(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setsrid(public.geometry, integer) TO geet;
GRANT ALL ON FUNCTION public.st_setsrid(public.geometry, integer) TO geet_web;


--
-- Name: FUNCTION st_setsrid(rast public.raster, srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setsrid(rast public.raster, srid integer) TO geet;
GRANT ALL ON FUNCTION public.st_setsrid(rast public.raster, srid integer) TO geet_web;


--
-- Name: FUNCTION st_setupperleft(rast public.raster, upperleftx double precision, upperlefty double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setupperleft(rast public.raster, upperleftx double precision, upperlefty double precision) TO geet;
GRANT ALL ON FUNCTION public.st_setupperleft(rast public.raster, upperleftx double precision, upperlefty double precision) TO geet_web;


--
-- Name: FUNCTION st_setvalue(rast public.raster, geom public.geometry, newvalue double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setvalue(rast public.raster, geom public.geometry, newvalue double precision) TO geet;
GRANT ALL ON FUNCTION public.st_setvalue(rast public.raster, geom public.geometry, newvalue double precision) TO geet_web;


--
-- Name: FUNCTION st_setvalue(rast public.raster, x integer, y integer, newvalue double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setvalue(rast public.raster, x integer, y integer, newvalue double precision) TO geet;
GRANT ALL ON FUNCTION public.st_setvalue(rast public.raster, x integer, y integer, newvalue double precision) TO geet_web;


--
-- Name: FUNCTION st_setvalue(rast public.raster, nband integer, geom public.geometry, newvalue double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setvalue(rast public.raster, nband integer, geom public.geometry, newvalue double precision) TO geet;
GRANT ALL ON FUNCTION public.st_setvalue(rast public.raster, nband integer, geom public.geometry, newvalue double precision) TO geet_web;


--
-- Name: FUNCTION st_setvalue(rast public.raster, band integer, x integer, y integer, newvalue double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setvalue(rast public.raster, band integer, x integer, y integer, newvalue double precision) TO geet;
GRANT ALL ON FUNCTION public.st_setvalue(rast public.raster, band integer, x integer, y integer, newvalue double precision) TO geet_web;


--
-- Name: FUNCTION st_setvalues(rast public.raster, nband integer, geomvalset public.geomval[], keepnodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setvalues(rast public.raster, nband integer, geomvalset public.geomval[], keepnodata boolean) TO geet;
GRANT ALL ON FUNCTION public.st_setvalues(rast public.raster, nband integer, geomvalset public.geomval[], keepnodata boolean) TO geet_web;


--
-- Name: FUNCTION st_setvalues(rast public.raster, nband integer, x integer, y integer, newvalueset double precision[], noset boolean[], keepnodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setvalues(rast public.raster, nband integer, x integer, y integer, newvalueset double precision[], noset boolean[], keepnodata boolean) TO geet;
GRANT ALL ON FUNCTION public.st_setvalues(rast public.raster, nband integer, x integer, y integer, newvalueset double precision[], noset boolean[], keepnodata boolean) TO geet_web;


--
-- Name: FUNCTION st_setvalues(rast public.raster, nband integer, x integer, y integer, newvalueset double precision[], nosetvalue double precision, keepnodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setvalues(rast public.raster, nband integer, x integer, y integer, newvalueset double precision[], nosetvalue double precision, keepnodata boolean) TO geet;
GRANT ALL ON FUNCTION public.st_setvalues(rast public.raster, nband integer, x integer, y integer, newvalueset double precision[], nosetvalue double precision, keepnodata boolean) TO geet_web;


--
-- Name: FUNCTION st_setvalues(rast public.raster, x integer, y integer, width integer, height integer, newvalue double precision, keepnodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setvalues(rast public.raster, x integer, y integer, width integer, height integer, newvalue double precision, keepnodata boolean) TO geet;
GRANT ALL ON FUNCTION public.st_setvalues(rast public.raster, x integer, y integer, width integer, height integer, newvalue double precision, keepnodata boolean) TO geet_web;


--
-- Name: FUNCTION st_setvalues(rast public.raster, nband integer, x integer, y integer, width integer, height integer, newvalue double precision, keepnodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_setvalues(rast public.raster, nband integer, x integer, y integer, width integer, height integer, newvalue double precision, keepnodata boolean) TO geet;
GRANT ALL ON FUNCTION public.st_setvalues(rast public.raster, nband integer, x integer, y integer, width integer, height integer, newvalue double precision, keepnodata boolean) TO geet_web;


--
-- Name: FUNCTION st_sharedpaths(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_sharedpaths(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_sharedpaths(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_shift_longitude(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_shift_longitude(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_shift_longitude(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_shiftlongitude(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_shiftlongitude(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_shiftlongitude(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_shortestline(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_shortestline(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_shortestline(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_simplify(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_simplify(public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_simplify(public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION st_simplify(public.geometry, double precision, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_simplify(public.geometry, double precision, boolean) TO geet;
GRANT ALL ON FUNCTION public.st_simplify(public.geometry, double precision, boolean) TO geet_web;


--
-- Name: FUNCTION st_simplifypreservetopology(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_simplifypreservetopology(public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_simplifypreservetopology(public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION st_simplifyvw(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_simplifyvw(public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_simplifyvw(public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION st_skewx(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_skewx(public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_skewx(public.raster) TO geet_web;


--
-- Name: FUNCTION st_skewy(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_skewy(public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_skewy(public.raster) TO geet_web;


--
-- Name: FUNCTION st_slope(rast public.raster, nband integer, pixeltype text, units text, scale double precision, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_slope(rast public.raster, nband integer, pixeltype text, units text, scale double precision, interpolate_nodata boolean) TO geet;
GRANT ALL ON FUNCTION public.st_slope(rast public.raster, nband integer, pixeltype text, units text, scale double precision, interpolate_nodata boolean) TO geet_web;


--
-- Name: FUNCTION st_slope(rast public.raster, nband integer, customextent public.raster, pixeltype text, units text, scale double precision, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_slope(rast public.raster, nband integer, customextent public.raster, pixeltype text, units text, scale double precision, interpolate_nodata boolean) TO geet;
GRANT ALL ON FUNCTION public.st_slope(rast public.raster, nband integer, customextent public.raster, pixeltype text, units text, scale double precision, interpolate_nodata boolean) TO geet_web;


--
-- Name: FUNCTION st_snap(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_snap(geom1 public.geometry, geom2 public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_snap(geom1 public.geometry, geom2 public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION st_snaptogrid(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_snaptogrid(public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_snaptogrid(public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION st_snaptogrid(public.geometry, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_snaptogrid(public.geometry, double precision, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_snaptogrid(public.geometry, double precision, double precision) TO geet_web;


--
-- Name: FUNCTION st_snaptogrid(public.geometry, double precision, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_snaptogrid(public.geometry, double precision, double precision, double precision, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_snaptogrid(public.geometry, double precision, double precision, double precision, double precision) TO geet_web;


--
-- Name: FUNCTION st_snaptogrid(geom1 public.geometry, geom2 public.geometry, double precision, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_snaptogrid(geom1 public.geometry, geom2 public.geometry, double precision, double precision, double precision, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_snaptogrid(geom1 public.geometry, geom2 public.geometry, double precision, double precision, double precision, double precision) TO geet_web;


--
-- Name: FUNCTION st_snaptogrid(rast public.raster, gridx double precision, gridy double precision, scalexy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_snaptogrid(rast public.raster, gridx double precision, gridy double precision, scalexy double precision, algorithm text, maxerr double precision) TO geet;
GRANT ALL ON FUNCTION public.st_snaptogrid(rast public.raster, gridx double precision, gridy double precision, scalexy double precision, algorithm text, maxerr double precision) TO geet_web;


--
-- Name: FUNCTION st_snaptogrid(rast public.raster, gridx double precision, gridy double precision, scalex double precision, scaley double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_snaptogrid(rast public.raster, gridx double precision, gridy double precision, scalex double precision, scaley double precision, algorithm text, maxerr double precision) TO geet;
GRANT ALL ON FUNCTION public.st_snaptogrid(rast public.raster, gridx double precision, gridy double precision, scalex double precision, scaley double precision, algorithm text, maxerr double precision) TO geet_web;


--
-- Name: FUNCTION st_snaptogrid(rast public.raster, gridx double precision, gridy double precision, algorithm text, maxerr double precision, scalex double precision, scaley double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_snaptogrid(rast public.raster, gridx double precision, gridy double precision, algorithm text, maxerr double precision, scalex double precision, scaley double precision) TO geet;
GRANT ALL ON FUNCTION public.st_snaptogrid(rast public.raster, gridx double precision, gridy double precision, algorithm text, maxerr double precision, scalex double precision, scaley double precision) TO geet_web;


--
-- Name: FUNCTION st_split(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_split(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_split(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_srid(geog public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_srid(geog public.geography) TO geet;
GRANT ALL ON FUNCTION public.st_srid(geog public.geography) TO geet_web;


--
-- Name: FUNCTION st_srid(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_srid(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_srid(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_srid(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_srid(public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_srid(public.raster) TO geet_web;


--
-- Name: FUNCTION st_startpoint(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_startpoint(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_startpoint(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_stddev4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_stddev4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet;
GRANT ALL ON FUNCTION public.st_stddev4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet_web;


--
-- Name: FUNCTION st_stddev4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_stddev4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO geet;
GRANT ALL ON FUNCTION public.st_stddev4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO geet_web;


--
-- Name: FUNCTION st_subdivide(geom public.geometry, maxvertices integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_subdivide(geom public.geometry, maxvertices integer) TO geet;
GRANT ALL ON FUNCTION public.st_subdivide(geom public.geometry, maxvertices integer) TO geet_web;


--
-- Name: FUNCTION st_sum4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_sum4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet;
GRANT ALL ON FUNCTION public.st_sum4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO geet_web;


--
-- Name: FUNCTION st_sum4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_sum4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO geet;
GRANT ALL ON FUNCTION public.st_sum4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO geet_web;


--
-- Name: FUNCTION st_summary(public.geography); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_summary(public.geography) TO geet;
GRANT ALL ON FUNCTION public.st_summary(public.geography) TO geet_web;


--
-- Name: FUNCTION st_summary(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_summary(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_summary(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_summary(rast public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_summary(rast public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_summary(rast public.raster) TO geet_web;


--
-- Name: FUNCTION st_summarystats(rast public.raster, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_summarystats(rast public.raster, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public.st_summarystats(rast public.raster, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION st_summarystats(rastertable text, rastercolumn text, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_summarystats(rastertable text, rastercolumn text, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public.st_summarystats(rastertable text, rastercolumn text, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION st_summarystats(rast public.raster, nband integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_summarystats(rast public.raster, nband integer, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public.st_summarystats(rast public.raster, nband integer, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION st_summarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_summarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public.st_summarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION st_swapordinates(geom public.geometry, ords cstring); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_swapordinates(geom public.geometry, ords cstring) TO geet;
GRANT ALL ON FUNCTION public.st_swapordinates(geom public.geometry, ords cstring) TO geet_web;


--
-- Name: FUNCTION st_symdifference(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_symdifference(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_symdifference(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_symmetricdifference(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_symmetricdifference(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_symmetricdifference(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_tile(rast public.raster, width integer, height integer, padwithnodata boolean, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_tile(rast public.raster, width integer, height integer, padwithnodata boolean, nodataval double precision) TO geet;
GRANT ALL ON FUNCTION public.st_tile(rast public.raster, width integer, height integer, padwithnodata boolean, nodataval double precision) TO geet_web;


--
-- Name: FUNCTION st_tile(rast public.raster, nband integer[], width integer, height integer, padwithnodata boolean, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_tile(rast public.raster, nband integer[], width integer, height integer, padwithnodata boolean, nodataval double precision) TO geet;
GRANT ALL ON FUNCTION public.st_tile(rast public.raster, nband integer[], width integer, height integer, padwithnodata boolean, nodataval double precision) TO geet_web;


--
-- Name: FUNCTION st_tile(rast public.raster, nband integer, width integer, height integer, padwithnodata boolean, nodataval double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_tile(rast public.raster, nband integer, width integer, height integer, padwithnodata boolean, nodataval double precision) TO geet;
GRANT ALL ON FUNCTION public.st_tile(rast public.raster, nband integer, width integer, height integer, padwithnodata boolean, nodataval double precision) TO geet_web;


--
-- Name: FUNCTION st_touches(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_touches(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_touches(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_touches(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_touches(rast1 public.raster, rast2 public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_touches(rast1 public.raster, rast2 public.raster) TO geet_web;


--
-- Name: FUNCTION st_touches(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_touches(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet;
GRANT ALL ON FUNCTION public.st_touches(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet_web;


--
-- Name: FUNCTION st_tpi(rast public.raster, nband integer, pixeltype text, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_tpi(rast public.raster, nband integer, pixeltype text, interpolate_nodata boolean) TO geet;
GRANT ALL ON FUNCTION public.st_tpi(rast public.raster, nband integer, pixeltype text, interpolate_nodata boolean) TO geet_web;


--
-- Name: FUNCTION st_tpi(rast public.raster, nband integer, customextent public.raster, pixeltype text, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_tpi(rast public.raster, nband integer, customextent public.raster, pixeltype text, interpolate_nodata boolean) TO geet;
GRANT ALL ON FUNCTION public.st_tpi(rast public.raster, nband integer, customextent public.raster, pixeltype text, interpolate_nodata boolean) TO geet_web;


--
-- Name: FUNCTION st_transform(public.geometry, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_transform(public.geometry, integer) TO geet;
GRANT ALL ON FUNCTION public.st_transform(public.geometry, integer) TO geet_web;


--
-- Name: FUNCTION st_transform(geom public.geometry, to_proj text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_transform(geom public.geometry, to_proj text) TO geet;
GRANT ALL ON FUNCTION public.st_transform(geom public.geometry, to_proj text) TO geet_web;


--
-- Name: FUNCTION st_transform(geom public.geometry, from_proj text, to_srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_transform(geom public.geometry, from_proj text, to_srid integer) TO geet;
GRANT ALL ON FUNCTION public.st_transform(geom public.geometry, from_proj text, to_srid integer) TO geet_web;


--
-- Name: FUNCTION st_transform(geom public.geometry, from_proj text, to_proj text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_transform(geom public.geometry, from_proj text, to_proj text) TO geet;
GRANT ALL ON FUNCTION public.st_transform(geom public.geometry, from_proj text, to_proj text) TO geet_web;


--
-- Name: FUNCTION st_transform(rast public.raster, alignto public.raster, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_transform(rast public.raster, alignto public.raster, algorithm text, maxerr double precision) TO geet;
GRANT ALL ON FUNCTION public.st_transform(rast public.raster, alignto public.raster, algorithm text, maxerr double precision) TO geet_web;


--
-- Name: FUNCTION st_transform(rast public.raster, srid integer, scalexy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_transform(rast public.raster, srid integer, scalexy double precision, algorithm text, maxerr double precision) TO geet;
GRANT ALL ON FUNCTION public.st_transform(rast public.raster, srid integer, scalexy double precision, algorithm text, maxerr double precision) TO geet_web;


--
-- Name: FUNCTION st_transform(rast public.raster, srid integer, scalex double precision, scaley double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_transform(rast public.raster, srid integer, scalex double precision, scaley double precision, algorithm text, maxerr double precision) TO geet;
GRANT ALL ON FUNCTION public.st_transform(rast public.raster, srid integer, scalex double precision, scaley double precision, algorithm text, maxerr double precision) TO geet_web;


--
-- Name: FUNCTION st_transform(rast public.raster, srid integer, algorithm text, maxerr double precision, scalex double precision, scaley double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_transform(rast public.raster, srid integer, algorithm text, maxerr double precision, scalex double precision, scaley double precision) TO geet;
GRANT ALL ON FUNCTION public.st_transform(rast public.raster, srid integer, algorithm text, maxerr double precision, scalex double precision, scaley double precision) TO geet_web;


--
-- Name: FUNCTION st_translate(public.geometry, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_translate(public.geometry, double precision, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_translate(public.geometry, double precision, double precision) TO geet_web;


--
-- Name: FUNCTION st_translate(public.geometry, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_translate(public.geometry, double precision, double precision, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_translate(public.geometry, double precision, double precision, double precision) TO geet_web;


--
-- Name: FUNCTION st_transscale(public.geometry, double precision, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_transscale(public.geometry, double precision, double precision, double precision, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_transscale(public.geometry, double precision, double precision, double precision, double precision) TO geet_web;


--
-- Name: FUNCTION st_tri(rast public.raster, nband integer, pixeltype text, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_tri(rast public.raster, nband integer, pixeltype text, interpolate_nodata boolean) TO geet;
GRANT ALL ON FUNCTION public.st_tri(rast public.raster, nband integer, pixeltype text, interpolate_nodata boolean) TO geet_web;


--
-- Name: FUNCTION st_tri(rast public.raster, nband integer, customextent public.raster, pixeltype text, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_tri(rast public.raster, nband integer, customextent public.raster, pixeltype text, interpolate_nodata boolean) TO geet;
GRANT ALL ON FUNCTION public.st_tri(rast public.raster, nband integer, customextent public.raster, pixeltype text, interpolate_nodata boolean) TO geet_web;


--
-- Name: FUNCTION st_unaryunion(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_unaryunion(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_unaryunion(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_union(public.geometry[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_union(public.geometry[]) TO geet;
GRANT ALL ON FUNCTION public.st_union(public.geometry[]) TO geet_web;


--
-- Name: FUNCTION st_union(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_union(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_union(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_upperleftx(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_upperleftx(public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_upperleftx(public.raster) TO geet_web;


--
-- Name: FUNCTION st_upperlefty(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_upperlefty(public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_upperlefty(public.raster) TO geet_web;


--
-- Name: FUNCTION st_value(rast public.raster, pt public.geometry, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_value(rast public.raster, pt public.geometry, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public.st_value(rast public.raster, pt public.geometry, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION st_value(rast public.raster, x integer, y integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_value(rast public.raster, x integer, y integer, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public.st_value(rast public.raster, x integer, y integer, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION st_value(rast public.raster, band integer, pt public.geometry, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_value(rast public.raster, band integer, pt public.geometry, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public.st_value(rast public.raster, band integer, pt public.geometry, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION st_value(rast public.raster, band integer, x integer, y integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_value(rast public.raster, band integer, x integer, y integer, exclude_nodata_value boolean) TO geet;
GRANT ALL ON FUNCTION public.st_value(rast public.raster, band integer, x integer, y integer, exclude_nodata_value boolean) TO geet_web;


--
-- Name: FUNCTION st_valuecount(rast public.raster, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuecount(rast public.raster, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO geet;
GRANT ALL ON FUNCTION public.st_valuecount(rast public.raster, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO geet_web;


--
-- Name: FUNCTION st_valuecount(rast public.raster, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuecount(rast public.raster, searchvalue double precision, roundto double precision) TO geet;
GRANT ALL ON FUNCTION public.st_valuecount(rast public.raster, searchvalue double precision, roundto double precision) TO geet_web;


--
-- Name: FUNCTION st_valuecount(rastertable text, rastercolumn text, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuecount(rastertable text, rastercolumn text, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO geet;
GRANT ALL ON FUNCTION public.st_valuecount(rastertable text, rastercolumn text, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO geet_web;


--
-- Name: FUNCTION st_valuecount(rastertable text, rastercolumn text, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuecount(rastertable text, rastercolumn text, searchvalue double precision, roundto double precision) TO geet;
GRANT ALL ON FUNCTION public.st_valuecount(rastertable text, rastercolumn text, searchvalue double precision, roundto double precision) TO geet_web;


--
-- Name: FUNCTION st_valuecount(rast public.raster, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuecount(rast public.raster, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO geet;
GRANT ALL ON FUNCTION public.st_valuecount(rast public.raster, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO geet_web;


--
-- Name: FUNCTION st_valuecount(rast public.raster, nband integer, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuecount(rast public.raster, nband integer, searchvalue double precision, roundto double precision) TO geet;
GRANT ALL ON FUNCTION public.st_valuecount(rast public.raster, nband integer, searchvalue double precision, roundto double precision) TO geet_web;


--
-- Name: FUNCTION st_valuecount(rastertable text, rastercolumn text, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuecount(rastertable text, rastercolumn text, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO geet;
GRANT ALL ON FUNCTION public.st_valuecount(rastertable text, rastercolumn text, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO geet_web;


--
-- Name: FUNCTION st_valuecount(rastertable text, rastercolumn text, nband integer, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuecount(rastertable text, rastercolumn text, nband integer, searchvalue double precision, roundto double precision) TO geet;
GRANT ALL ON FUNCTION public.st_valuecount(rastertable text, rastercolumn text, nband integer, searchvalue double precision, roundto double precision) TO geet_web;


--
-- Name: FUNCTION st_valuecount(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuecount(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO geet;
GRANT ALL ON FUNCTION public.st_valuecount(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO geet_web;


--
-- Name: FUNCTION st_valuecount(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuecount(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision) TO geet;
GRANT ALL ON FUNCTION public.st_valuecount(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision) TO geet_web;


--
-- Name: FUNCTION st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO geet;
GRANT ALL ON FUNCTION public.st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO geet_web;


--
-- Name: FUNCTION st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision) TO geet;
GRANT ALL ON FUNCTION public.st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision) TO geet_web;


--
-- Name: FUNCTION st_valuepercent(rast public.raster, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuepercent(rast public.raster, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_valuepercent(rast public.raster, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_valuepercent(rast public.raster, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuepercent(rast public.raster, searchvalue double precision, roundto double precision) TO geet;
GRANT ALL ON FUNCTION public.st_valuepercent(rast public.raster, searchvalue double precision, roundto double precision) TO geet_web;


--
-- Name: FUNCTION st_valuepercent(rastertable text, rastercolumn text, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuepercent(rastertable text, rastercolumn text, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_valuepercent(rastertable text, rastercolumn text, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_valuepercent(rastertable text, rastercolumn text, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuepercent(rastertable text, rastercolumn text, searchvalue double precision, roundto double precision) TO geet;
GRANT ALL ON FUNCTION public.st_valuepercent(rastertable text, rastercolumn text, searchvalue double precision, roundto double precision) TO geet_web;


--
-- Name: FUNCTION st_valuepercent(rast public.raster, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuepercent(rast public.raster, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_valuepercent(rast public.raster, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_valuepercent(rast public.raster, nband integer, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuepercent(rast public.raster, nband integer, searchvalue double precision, roundto double precision) TO geet;
GRANT ALL ON FUNCTION public.st_valuepercent(rast public.raster, nband integer, searchvalue double precision, roundto double precision) TO geet_web;


--
-- Name: FUNCTION st_valuepercent(rastertable text, rastercolumn text, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuepercent(rastertable text, rastercolumn text, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_valuepercent(rastertable text, rastercolumn text, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_valuepercent(rastertable text, rastercolumn text, nband integer, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuepercent(rastertable text, rastercolumn text, nband integer, searchvalue double precision, roundto double precision) TO geet;
GRANT ALL ON FUNCTION public.st_valuepercent(rastertable text, rastercolumn text, nband integer, searchvalue double precision, roundto double precision) TO geet_web;


--
-- Name: FUNCTION st_valuepercent(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuepercent(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_valuepercent(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_valuepercent(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuepercent(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision) TO geet;
GRANT ALL ON FUNCTION public.st_valuepercent(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision) TO geet_web;


--
-- Name: FUNCTION st_valuepercent(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuepercent(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO geet;
GRANT ALL ON FUNCTION public.st_valuepercent(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO geet_web;


--
-- Name: FUNCTION st_valuepercent(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_valuepercent(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision) TO geet;
GRANT ALL ON FUNCTION public.st_valuepercent(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision) TO geet_web;


--
-- Name: FUNCTION st_voronoilines(g1 public.geometry, tolerance double precision, extend_to public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_voronoilines(g1 public.geometry, tolerance double precision, extend_to public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_voronoilines(g1 public.geometry, tolerance double precision, extend_to public.geometry) TO geet_web;


--
-- Name: FUNCTION st_voronoipolygons(g1 public.geometry, tolerance double precision, extend_to public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_voronoipolygons(g1 public.geometry, tolerance double precision, extend_to public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_voronoipolygons(g1 public.geometry, tolerance double precision, extend_to public.geometry) TO geet_web;


--
-- Name: FUNCTION st_width(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_width(public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_width(public.raster) TO geet_web;


--
-- Name: FUNCTION st_within(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_within(geom1 public.geometry, geom2 public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_within(geom1 public.geometry, geom2 public.geometry) TO geet_web;


--
-- Name: FUNCTION st_within(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_within(rast1 public.raster, rast2 public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_within(rast1 public.raster, rast2 public.raster) TO geet_web;


--
-- Name: FUNCTION st_within(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_within(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet;
GRANT ALL ON FUNCTION public.st_within(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO geet_web;


--
-- Name: FUNCTION st_wkbtosql(wkb bytea); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_wkbtosql(wkb bytea) TO geet;
GRANT ALL ON FUNCTION public.st_wkbtosql(wkb bytea) TO geet_web;


--
-- Name: FUNCTION st_wkttosql(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_wkttosql(text) TO geet;
GRANT ALL ON FUNCTION public.st_wkttosql(text) TO geet_web;


--
-- Name: FUNCTION st_worldtorastercoord(rast public.raster, pt public.geometry, OUT columnx integer, OUT rowy integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_worldtorastercoord(rast public.raster, pt public.geometry, OUT columnx integer, OUT rowy integer) TO geet;
GRANT ALL ON FUNCTION public.st_worldtorastercoord(rast public.raster, pt public.geometry, OUT columnx integer, OUT rowy integer) TO geet_web;


--
-- Name: FUNCTION st_worldtorastercoord(rast public.raster, longitude double precision, latitude double precision, OUT columnx integer, OUT rowy integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_worldtorastercoord(rast public.raster, longitude double precision, latitude double precision, OUT columnx integer, OUT rowy integer) TO geet;
GRANT ALL ON FUNCTION public.st_worldtorastercoord(rast public.raster, longitude double precision, latitude double precision, OUT columnx integer, OUT rowy integer) TO geet_web;


--
-- Name: FUNCTION st_worldtorastercoordx(rast public.raster, xw double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_worldtorastercoordx(rast public.raster, xw double precision) TO geet;
GRANT ALL ON FUNCTION public.st_worldtorastercoordx(rast public.raster, xw double precision) TO geet_web;


--
-- Name: FUNCTION st_worldtorastercoordx(rast public.raster, pt public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_worldtorastercoordx(rast public.raster, pt public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_worldtorastercoordx(rast public.raster, pt public.geometry) TO geet_web;


--
-- Name: FUNCTION st_worldtorastercoordx(rast public.raster, xw double precision, yw double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_worldtorastercoordx(rast public.raster, xw double precision, yw double precision) TO geet;
GRANT ALL ON FUNCTION public.st_worldtorastercoordx(rast public.raster, xw double precision, yw double precision) TO geet_web;


--
-- Name: FUNCTION st_worldtorastercoordy(rast public.raster, yw double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_worldtorastercoordy(rast public.raster, yw double precision) TO geet;
GRANT ALL ON FUNCTION public.st_worldtorastercoordy(rast public.raster, yw double precision) TO geet_web;


--
-- Name: FUNCTION st_worldtorastercoordy(rast public.raster, pt public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_worldtorastercoordy(rast public.raster, pt public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_worldtorastercoordy(rast public.raster, pt public.geometry) TO geet_web;


--
-- Name: FUNCTION st_worldtorastercoordy(rast public.raster, xw double precision, yw double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_worldtorastercoordy(rast public.raster, xw double precision, yw double precision) TO geet;
GRANT ALL ON FUNCTION public.st_worldtorastercoordy(rast public.raster, xw double precision, yw double precision) TO geet_web;


--
-- Name: FUNCTION st_wrapx(geom public.geometry, wrap double precision, move double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_wrapx(geom public.geometry, wrap double precision, move double precision) TO geet;
GRANT ALL ON FUNCTION public.st_wrapx(geom public.geometry, wrap double precision, move double precision) TO geet_web;


--
-- Name: FUNCTION st_x(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_x(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_x(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_xmax(public.box3d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_xmax(public.box3d) TO geet;
GRANT ALL ON FUNCTION public.st_xmax(public.box3d) TO geet_web;


--
-- Name: FUNCTION st_xmin(public.box3d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_xmin(public.box3d) TO geet;
GRANT ALL ON FUNCTION public.st_xmin(public.box3d) TO geet_web;


--
-- Name: FUNCTION st_y(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_y(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_y(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_ymax(public.box3d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_ymax(public.box3d) TO geet;
GRANT ALL ON FUNCTION public.st_ymax(public.box3d) TO geet_web;


--
-- Name: FUNCTION st_ymin(public.box3d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_ymin(public.box3d) TO geet;
GRANT ALL ON FUNCTION public.st_ymin(public.box3d) TO geet_web;


--
-- Name: FUNCTION st_z(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_z(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_z(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_zmax(public.box3d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_zmax(public.box3d) TO geet;
GRANT ALL ON FUNCTION public.st_zmax(public.box3d) TO geet_web;


--
-- Name: FUNCTION st_zmflag(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_zmflag(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_zmflag(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_zmin(public.box3d); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_zmin(public.box3d) TO geet;
GRANT ALL ON FUNCTION public.st_zmin(public.box3d) TO geet_web;


--
-- Name: FUNCTION text(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.text(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.text(public.geometry) TO geet_web;


--
-- Name: FUNCTION unlockrows(text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.unlockrows(text) TO geet;
GRANT ALL ON FUNCTION public.unlockrows(text) TO geet_web;


--
-- Name: FUNCTION updategeometrysrid(character varying, character varying, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.updategeometrysrid(character varying, character varying, integer) TO geet;
GRANT ALL ON FUNCTION public.updategeometrysrid(character varying, character varying, integer) TO geet_web;


--
-- Name: FUNCTION updategeometrysrid(character varying, character varying, character varying, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.updategeometrysrid(character varying, character varying, character varying, integer) TO geet;
GRANT ALL ON FUNCTION public.updategeometrysrid(character varying, character varying, character varying, integer) TO geet_web;


--
-- Name: FUNCTION updategeometrysrid(catalogn_name character varying, schema_name character varying, table_name character varying, column_name character varying, new_srid_in integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.updategeometrysrid(catalogn_name character varying, schema_name character varying, table_name character varying, column_name character varying, new_srid_in integer) TO geet;
GRANT ALL ON FUNCTION public.updategeometrysrid(catalogn_name character varying, schema_name character varying, table_name character varying, column_name character varying, new_srid_in integer) TO geet_web;


--
-- Name: FUNCTION updaterastersrid(table_name name, column_name name, new_srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.updaterastersrid(table_name name, column_name name, new_srid integer) TO geet;
GRANT ALL ON FUNCTION public.updaterastersrid(table_name name, column_name name, new_srid integer) TO geet_web;


--
-- Name: FUNCTION updaterastersrid(schema_name name, table_name name, column_name name, new_srid integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.updaterastersrid(schema_name name, table_name name, column_name name, new_srid integer) TO geet;
GRANT ALL ON FUNCTION public.updaterastersrid(schema_name name, table_name name, column_name name, new_srid integer) TO geet_web;


--
-- Name: FUNCTION st_3dextent(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_3dextent(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_3dextent(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_accum(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_accum(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_accum(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_clusterintersecting(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clusterintersecting(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_clusterintersecting(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_clusterwithin(public.geometry, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_clusterwithin(public.geometry, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_clusterwithin(public.geometry, double precision) TO geet_web;


--
-- Name: FUNCTION st_collect(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_collect(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_collect(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_countagg(public.raster, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_countagg(public.raster, boolean) TO geet;
GRANT ALL ON FUNCTION public.st_countagg(public.raster, boolean) TO geet_web;


--
-- Name: FUNCTION st_countagg(public.raster, integer, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_countagg(public.raster, integer, boolean) TO geet;
GRANT ALL ON FUNCTION public.st_countagg(public.raster, integer, boolean) TO geet_web;


--
-- Name: FUNCTION st_countagg(public.raster, integer, boolean, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_countagg(public.raster, integer, boolean, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_countagg(public.raster, integer, boolean, double precision) TO geet_web;


--
-- Name: FUNCTION st_extent(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_extent(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_extent(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_makeline(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_makeline(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_makeline(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_memcollect(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_memcollect(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_memcollect(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_memunion(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_memunion(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_memunion(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_polygonize(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_polygonize(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_polygonize(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_samealignment(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_samealignment(public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_samealignment(public.raster) TO geet_web;


--
-- Name: FUNCTION st_summarystatsagg(public.raster, boolean, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_summarystatsagg(public.raster, boolean, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_summarystatsagg(public.raster, boolean, double precision) TO geet_web;


--
-- Name: FUNCTION st_summarystatsagg(public.raster, integer, boolean); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_summarystatsagg(public.raster, integer, boolean) TO geet;
GRANT ALL ON FUNCTION public.st_summarystatsagg(public.raster, integer, boolean) TO geet_web;


--
-- Name: FUNCTION st_summarystatsagg(public.raster, integer, boolean, double precision); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_summarystatsagg(public.raster, integer, boolean, double precision) TO geet;
GRANT ALL ON FUNCTION public.st_summarystatsagg(public.raster, integer, boolean, double precision) TO geet_web;


--
-- Name: FUNCTION st_union(public.geometry); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_union(public.geometry) TO geet;
GRANT ALL ON FUNCTION public.st_union(public.geometry) TO geet_web;


--
-- Name: FUNCTION st_union(public.raster); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_union(public.raster) TO geet;
GRANT ALL ON FUNCTION public.st_union(public.raster) TO geet_web;


--
-- Name: FUNCTION st_union(public.raster, integer); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_union(public.raster, integer) TO geet;
GRANT ALL ON FUNCTION public.st_union(public.raster, integer) TO geet_web;


--
-- Name: FUNCTION st_union(public.raster, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_union(public.raster, text) TO geet;
GRANT ALL ON FUNCTION public.st_union(public.raster, text) TO geet_web;


--
-- Name: FUNCTION st_union(public.raster, public.unionarg[]); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_union(public.raster, public.unionarg[]) TO geet;
GRANT ALL ON FUNCTION public.st_union(public.raster, public.unionarg[]) TO geet_web;


--
-- Name: FUNCTION st_union(public.raster, integer, text); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.st_union(public.raster, integer, text) TO geet;
GRANT ALL ON FUNCTION public.st_union(public.raster, integer, text) TO geet_web;


--
-- Name: TABLE access_policy; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.access_policy TO geet WITH GRANT OPTION;
GRANT ALL ON TABLE public.access_policy TO geet_web;
GRANT ALL ON TABLE public.access_policy TO postgres;
GRANT ALL ON TABLE public.access_policy TO fra_user;
GRANT SELECT ON TABLE public.access_policy TO backup;


--
-- Name: SEQUENCE access_policy_id_seq; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.access_policy_id_seq TO geet_web;
GRANT SELECT ON SEQUENCE public.access_policy_id_seq TO geet;


--
-- Name: SEQUENCE app_version_id_seq; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.app_version_id_seq TO geet_web;


--
-- Name: TABLE app_version; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON TABLE public.app_version TO geet_web;
GRANT ALL ON TABLE public.app_version TO postgres;
GRANT ALL ON TABLE public.app_version TO fra_user;
GRANT SELECT ON TABLE public.app_version TO backup;


--
-- Name: TABLE attributes_master; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.attributes_master TO geet WITH GRANT OPTION;
GRANT ALL ON TABLE public.attributes_master TO geet_web;
GRANT ALL ON TABLE public.attributes_master TO postgres;
GRANT ALL ON TABLE public.attributes_master TO fra_user;
GRANT SELECT ON TABLE public.attributes_master TO backup;


--
-- Name: SEQUENCE att_master; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.att_master TO geet_web;
GRANT SELECT ON SEQUENCE public.att_master TO geet;


--
-- Name: TABLE attributes_master_lang_relation; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.attributes_master_lang_relation TO geet WITH GRANT OPTION;
GRANT ALL ON TABLE public.attributes_master_lang_relation TO geet_web;
GRANT ALL ON TABLE public.attributes_master_lang_relation TO postgres;
GRANT ALL ON TABLE public.attributes_master_lang_relation TO fra_user;
GRANT SELECT ON TABLE public.attributes_master_lang_relation TO backup;


--
-- Name: SEQUENCE attributes_master_lang_relation_srno_seq; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.attributes_master_lang_relation_srno_seq TO geet_web;
GRANT SELECT ON SEQUENCE public.attributes_master_lang_relation_srno_seq TO geet;


--
-- Name: TABLE beat_master; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.beat_master TO fra_user;
GRANT SELECT ON TABLE public.beat_master TO backup;


--
-- Name: SEQUENCE bh_id_seq; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.bh_id_seq TO geet_web;
GRANT SELECT ON SEQUENCE public.bh_id_seq TO geet;


--
-- Name: TABLE block_master; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.block_master TO fra_user;
GRANT SELECT ON TABLE public.block_master TO backup;


--
-- Name: SEQUENCE c_id; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.c_id TO geet_web;
GRANT SELECT ON SEQUENCE public.c_id TO geet;


--
-- Name: TABLE circle_master; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.circle_master TO fra_user;
GRANT SELECT ON TABLE public.circle_master TO backup;


--
-- Name: TABLE claim; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.claim TO fra_user;
GRANT SELECT ON TABLE public.claim TO backup;


--
-- Name: TABLE claimant; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.claimant TO fra_user;
GRANT SELECT ON TABLE public.claimant TO backup;


--
-- Name: TABLE compartment_master; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.compartment_master TO fra_user;
GRANT SELECT ON TABLE public.compartment_master TO backup;


--
-- Name: TABLE district_master; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.district_master TO geet WITH GRANT OPTION;
GRANT ALL ON TABLE public.district_master TO geet_web;
GRANT ALL ON TABLE public.district_master TO postgres;
GRANT ALL ON TABLE public.district_master TO fra_user;
GRANT SELECT ON TABLE public.district_master TO backup;


--
-- Name: TABLE division_master; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.division_master TO fra_user;
GRANT SELECT ON TABLE public.division_master TO backup;


--
-- Name: SEQUENCE eligibility_check_counter_id_seq; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.eligibility_check_counter_id_seq TO geet_web;


--
-- Name: TABLE error_info; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.error_info TO geet WITH GRANT OPTION;
GRANT ALL ON TABLE public.error_info TO geet_web;
GRANT ALL ON TABLE public.error_info TO postgres;
GRANT ALL ON TABLE public.error_info TO fra_user;
GRANT SELECT ON TABLE public.error_info TO backup;


--
-- Name: SEQUENCE err_master; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.err_master TO geet_web;
GRANT SELECT ON SEQUENCE public.err_master TO geet;


--
-- Name: TABLE form_data; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.form_data TO fra_user;
GRANT SELECT ON TABLE public.form_data TO backup;


--
-- Name: TABLE form_master; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.form_master TO fra_user;
GRANT SELECT ON TABLE public.form_master TO backup;


--
-- Name: TABLE geography_columns; Type: ACL; Schema: public; Owner: postgres
--

REVOKE SELECT ON TABLE public.geography_columns FROM PUBLIC;
GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.geography_columns TO geet;
GRANT ALL ON TABLE public.geography_columns TO geet_web;
GRANT ALL ON TABLE public.geography_columns TO fra_user;
GRANT SELECT ON TABLE public.geography_columns TO backup;


--
-- Name: TABLE geometry_columns; Type: ACL; Schema: public; Owner: postgres
--

REVOKE SELECT ON TABLE public.geometry_columns FROM PUBLIC;
GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.geometry_columns TO geet;
GRANT ALL ON TABLE public.geometry_columns TO geet_web;
GRANT ALL ON TABLE public.geometry_columns TO fra_user;
GRANT SELECT ON TABLE public.geometry_columns TO backup;


--
-- Name: TABLE gp_master; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.gp_master TO geet WITH GRANT OPTION;
GRANT ALL ON TABLE public.gp_master TO geet_web;
GRANT ALL ON TABLE public.gp_master TO postgres;
GRANT ALL ON TABLE public.gp_master TO fra_user;
GRANT SELECT ON TABLE public.gp_master TO backup;


--
-- Name: TABLE group_master; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.group_master TO geet WITH GRANT OPTION;
GRANT ALL ON TABLE public.group_master TO geet_web;
GRANT ALL ON TABLE public.group_master TO postgres;
GRANT ALL ON TABLE public.group_master TO fra_user;
GRANT SELECT ON TABLE public.group_master TO backup;


--
-- Name: SEQUENCE groups_master; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.groups_master TO geet_web;
GRANT SELECT ON SEQUENCE public.groups_master TO geet;


--
-- Name: SEQUENCE l_id; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.l_id TO geet_web;
GRANT SELECT ON SEQUENCE public.l_id TO geet;


--
-- Name: TABLE language_master; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.language_master TO geet WITH GRANT OPTION;
GRANT ALL ON TABLE public.language_master TO geet_web;
GRANT ALL ON TABLE public.language_master TO postgres;
GRANT ALL ON TABLE public.language_master TO fra_user;
GRANT SELECT ON TABLE public.language_master TO backup;


--
-- Name: TABLE layer_info; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.layer_info TO geet WITH GRANT OPTION;
GRANT ALL ON TABLE public.layer_info TO geet_web;
GRANT ALL ON TABLE public.layer_info TO postgres;
GRANT ALL ON TABLE public.layer_info TO fra_user;
GRANT SELECT ON TABLE public.layer_info TO backup;


--
-- Name: TABLE layer_primary_category; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.layer_primary_category TO geet WITH GRANT OPTION;
GRANT ALL ON TABLE public.layer_primary_category TO geet_web;
GRANT ALL ON TABLE public.layer_primary_category TO postgres;
GRANT ALL ON TABLE public.layer_primary_category TO fra_user;
GRANT SELECT ON TABLE public.layer_primary_category TO backup;


--
-- Name: SEQUENCE page_category_master_seq; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.page_category_master_seq TO geet_web;


--
-- Name: TABLE page_category_master; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.page_category_master TO geet WITH GRANT OPTION;
GRANT ALL ON TABLE public.page_category_master TO geet_web;
GRANT ALL ON TABLE public.page_category_master TO postgres;
GRANT ALL ON TABLE public.page_category_master TO fra_user;
GRANT SELECT ON TABLE public.page_category_master TO backup;


--
-- Name: SEQUENCE page_master_seq; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.page_master_seq TO geet_web;


--
-- Name: TABLE page_master; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.page_master TO geet WITH GRANT OPTION;
GRANT ALL ON TABLE public.page_master TO geet_web;
GRANT ALL ON TABLE public.page_master TO postgres;
GRANT ALL ON TABLE public.page_master TO fra_user;
GRANT SELECT ON TABLE public.page_master TO backup;


--
-- Name: TABLE pg_stat_statements; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.pg_stat_statements TO fra_user;
GRANT SELECT ON TABLE public.pg_stat_statements TO backup;


--
-- Name: TABLE range_master; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.range_master TO fra_user;
GRANT SELECT ON TABLE public.range_master TO backup;


--
-- Name: TABLE raster_columns; Type: ACL; Schema: public; Owner: postgres
--

REVOKE SELECT ON TABLE public.raster_columns FROM PUBLIC;
GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.raster_columns TO geet;
GRANT ALL ON TABLE public.raster_columns TO geet_web;
GRANT ALL ON TABLE public.raster_columns TO fra_user;
GRANT SELECT ON TABLE public.raster_columns TO backup;


--
-- Name: TABLE raster_overviews; Type: ACL; Schema: public; Owner: postgres
--

REVOKE SELECT ON TABLE public.raster_overviews FROM PUBLIC;
GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.raster_overviews TO geet;
GRANT ALL ON TABLE public.raster_overviews TO geet_web;
GRANT ALL ON TABLE public.raster_overviews TO fra_user;
GRANT SELECT ON TABLE public.raster_overviews TO backup;


--
-- Name: TABLE region_allocation; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.region_allocation TO geet WITH GRANT OPTION;
GRANT ALL ON TABLE public.region_allocation TO geet_web;
GRANT ALL ON TABLE public.region_allocation TO postgres;
GRANT ALL ON TABLE public.region_allocation TO fra_user;
GRANT SELECT ON TABLE public.region_allocation TO backup;


--
-- Name: SEQUENCE region_allocation_srno_seq; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.region_allocation_srno_seq TO geet_web;
GRANT SELECT ON SEQUENCE public.region_allocation_srno_seq TO geet;


--
-- Name: TABLE user_roles_relation; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.user_roles_relation TO geet WITH GRANT OPTION;
GRANT ALL ON TABLE public.user_roles_relation TO geet_web;
GRANT ALL ON TABLE public.user_roles_relation TO postgres;
GRANT ALL ON TABLE public.user_roles_relation TO fra_user;
GRANT SELECT ON TABLE public.user_roles_relation TO backup;


--
-- Name: SEQUENCE roles_seq; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.roles_seq TO geet_web;
GRANT SELECT ON SEQUENCE public.roles_seq TO geet;


--
-- Name: TABLE roles_master; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.roles_master TO geet WITH GRANT OPTION;
GRANT ALL ON TABLE public.roles_master TO geet_web;
GRANT ALL ON TABLE public.roles_master TO postgres;
GRANT ALL ON TABLE public.roles_master TO fra_user;
GRANT SELECT ON TABLE public.roles_master TO backup;


--
-- Name: SEQUENCE s_id; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.s_id TO geet_web;
GRANT SELECT ON SEQUENCE public.s_id TO geet;


--
-- Name: SEQUENCE sc_id; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.sc_id TO geet_web;
GRANT SELECT ON SEQUENCE public.sc_id TO geet;


--
-- Name: SEQUENCE schemes_master_sc_id_no_seq; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.schemes_master_sc_id_no_seq TO geet_web;
GRANT SELECT ON SEQUENCE public.schemes_master_sc_id_no_seq TO geet;


--
-- Name: SEQUENCE shg_master_id_seq; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.shg_master_id_seq TO geet_web;
GRANT SELECT ON SEQUENCE public.shg_master_id_seq TO geet;


--
-- Name: SEQUENCE shg_master_ind_relation_id_seq; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.shg_master_ind_relation_id_seq TO geet_web;
GRANT SELECT ON SEQUENCE public.shg_master_ind_relation_id_seq TO geet;


--
-- Name: TABLE sms_logs; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.sms_logs TO geet WITH GRANT OPTION;
GRANT ALL ON TABLE public.sms_logs TO geet_web;
GRANT ALL ON TABLE public.sms_logs TO postgres;
GRANT ALL ON TABLE public.sms_logs TO fra_user;
GRANT SELECT ON TABLE public.sms_logs TO backup;


--
-- Name: SEQUENCE sms_logs_sr_no_seq; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.sms_logs_sr_no_seq TO geet_web;
GRANT SELECT ON SEQUENCE public.sms_logs_sr_no_seq TO geet;


--
-- Name: TABLE sms_short_codes_master; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.sms_short_codes_master TO geet WITH GRANT OPTION;
GRANT ALL ON TABLE public.sms_short_codes_master TO geet_web;
GRANT ALL ON TABLE public.sms_short_codes_master TO postgres;
GRANT ALL ON TABLE public.sms_short_codes_master TO fra_user;
GRANT SELECT ON TABLE public.sms_short_codes_master TO backup;


--
-- Name: SEQUENCE sms_short_codes_master_sr_no_seq; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.sms_short_codes_master_sr_no_seq TO geet_web;
GRANT SELECT ON SEQUENCE public.sms_short_codes_master_sr_no_seq TO geet;


--
-- Name: TABLE spatial_ref_sys; Type: ACL; Schema: public; Owner: postgres
--

REVOKE SELECT ON TABLE public.spatial_ref_sys FROM PUBLIC;
GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.spatial_ref_sys TO geet;
GRANT ALL ON TABLE public.spatial_ref_sys TO geet_web;
GRANT ALL ON TABLE public.spatial_ref_sys TO fra_user;
GRANT SELECT ON TABLE public.spatial_ref_sys TO backup;


--
-- Name: SEQUENCE sr_no; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.sr_no TO geet_web;
GRANT SELECT ON SEQUENCE public.sr_no TO geet;


--
-- Name: TABLE states_master; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.states_master TO geet WITH GRANT OPTION;
GRANT ALL ON TABLE public.states_master TO geet_web;
GRANT ALL ON TABLE public.states_master TO postgres;
GRANT ALL ON TABLE public.states_master TO fra_user;
GRANT SELECT ON TABLE public.states_master TO backup;


--
-- Name: SEQUENCE states_master_sr_no_seq; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.states_master_sr_no_seq TO geet_web;
GRANT SELECT ON SEQUENCE public.states_master_sr_no_seq TO geet;


--
-- Name: TABLE tehsil_master; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.tehsil_master TO geet WITH GRANT OPTION;
GRANT ALL ON TABLE public.tehsil_master TO geet_web;
GRANT ALL ON TABLE public.tehsil_master TO postgres;
GRANT ALL ON TABLE public.tehsil_master TO fra_user;
GRANT SELECT ON TABLE public.tehsil_master TO backup;


--
-- Name: SEQUENCE tehsil_master_sr_no_seq; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.tehsil_master_sr_no_seq TO geet_web;
GRANT SELECT ON SEQUENCE public.tehsil_master_sr_no_seq TO geet;


--
-- Name: TABLE token_session; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.token_session TO geet WITH GRANT OPTION;
GRANT ALL ON TABLE public.token_session TO geet_web;
GRANT ALL ON TABLE public.token_session TO postgres;
GRANT ALL ON TABLE public.token_session TO fra_user;
GRANT SELECT ON TABLE public.token_session TO backup;


--
-- Name: SEQUENCE token_session_srno_seq; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.token_session_srno_seq TO geet_web;
GRANT SELECT ON SEQUENCE public.token_session_srno_seq TO geet;


--
-- Name: SEQUENCE u_id; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.u_id TO geet_web;
GRANT SELECT ON SEQUENCE public.u_id TO geet;


--
-- Name: SEQUENCE user_group_relation_sr_no_seq; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.user_group_relation_sr_no_seq TO geet_web;
GRANT SELECT ON SEQUENCE public.user_group_relation_sr_no_seq TO geet;


--
-- Name: TABLE user_group_relation; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.user_group_relation TO geet WITH GRANT OPTION;
GRANT ALL ON TABLE public.user_group_relation TO geet_web;
GRANT ALL ON TABLE public.user_group_relation TO postgres;
GRANT ALL ON TABLE public.user_group_relation TO fra_user;
GRANT SELECT ON TABLE public.user_group_relation TO backup;


--
-- Name: TABLE user_master; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.user_master TO geet WITH GRANT OPTION;
GRANT ALL ON TABLE public.user_master TO geet_web;
GRANT ALL ON TABLE public.user_master TO postgres;
GRANT ALL ON TABLE public.user_master TO fra_user;
GRANT SELECT ON TABLE public.user_master TO backup;


--
-- Name: SEQUENCE user_master_sr_no_seq; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.user_master_sr_no_seq TO geet_web;
GRANT SELECT ON SEQUENCE public.user_master_sr_no_seq TO geet;
GRANT ALL ON SEQUENCE public.user_master_sr_no_seq TO fra_user;


--
-- Name: SEQUENCE user_role_rel; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.user_role_rel TO geet_web;
GRANT SELECT ON SEQUENCE public.user_role_rel TO geet;


--
-- Name: TABLE village_master; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT SELECT,INSERT,REFERENCES,TRIGGER,UPDATE ON TABLE public.village_master TO geet WITH GRANT OPTION;
GRANT ALL ON TABLE public.village_master TO geet_web;
GRANT ALL ON TABLE public.village_master TO postgres;
GRANT ALL ON TABLE public.village_master TO fra_user;
GRANT SELECT ON TABLE public.village_master TO backup;


--
-- Name: SEQUENCE village_master_sr_no_seq; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.village_master_sr_no_seq TO geet_web;
GRANT SELECT ON SEQUENCE public.village_master_sr_no_seq TO geet;


--
-- Name: SEQUENCE w_id; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.w_id TO geet_web;
GRANT SELECT ON SEQUENCE public.w_id TO geet;


--
-- Name: SEQUENCE web_sessions_id_seq; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON SEQUENCE public.web_sessions_id_seq TO geet_web;


--
-- Name: TABLE web_sessions; Type: ACL; Schema: public; Owner: pgadmin
--

GRANT ALL ON TABLE public.web_sessions TO geet_web;
GRANT ALL ON TABLE public.web_sessions TO postgres;
GRANT ALL ON TABLE public.web_sessions TO fra_user;
GRANT SELECT ON TABLE public.web_sessions TO backup;


--
-- PostgreSQL database dump complete
--

