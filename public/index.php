<?php
require "../vendor/autoload.php";
use FRA\App\Core\App;
if(!isset($_SESSION))
{
	session_start();
}
/**
* Initaites the app call for IFMT
*/
$app = new App();

/**
* Call the route included
*/
$app->call();